<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Practise Tests at Brainwiz</title>
    <link rel="icon" type="image/png" sizes="32x32" href="img/favicon-32x32.png">
    <!-- style sheets -->
    <?php include 'headerstyles.php' ?>
</head>

<body>

    <!-- header -->
   <?php include 'header.php' ?>
    <!--/ header -->
    <!--sub page main -->
    <main class="subpage">
        <!-- sub page header -->
        <div class="page-header">
            <!-- container -->
            <div class="container">
               <!-- row -->
               <div class="row">
                   <div class="col-lg-6">
                       <h1>Practise <span class="fbold">Tests </span></h1>
                   </div>
               </div>
               <!--/ row --> 
            </div>
            <!--/ container -->
        </div>
        <!--/ sub page header -->

         <!-- brudcrumb -->
         <div class="container">
            <!-- row -->
            <div class="row">
                <!-- col -->
                <div class="col-lg-12">                   
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.php">Home</a></li>                        
                        <li class="breadcrumb-item active"><a>Practise Tests</a></li>
                    </ul>                    
                </div>
                <!--/col -->
            </div>
            <!--/ row -->
        </div>
        <!--/ brudcrumb -->
        <!-- sub page body -->
        <div class="subpage-body">
           <!-- container -->
           <div class="container">
              <!-- row -->
              <div class="row">
               <!-- col 3 -->
                <div class="col-lg-3 lefttab">
                    <ul class="resp-tabs-list ">
                        <li class="selectnav">Arithmetic</li>
                        <li>Logical Reasoning</li>
                        <li>Verbal Ability</li>
                    </ul>
                </div>
                <!--/ col 3-->

                <!-- col 9-->
                <div class="col-lg-9">

                  <!-- test row-->
                  <div class="row test-list-row">
                        <!-- test col -->
                        <div class="col-lg-3 col-sm-4 text-center">
                            <div class="test-list-col">
                                <img src="img/exam.png" alt="">
                                <h4 class="h6">Profit & Loss</h4>
                                <p>Total Test 3</p>
                                <a href="javascript:void(0)" class="whitebtn" data-toggle="modal" data-target="#test-pop">Take Test</a>
                            </div>                                        
                        </div>
                        <!--/ test col -->
                        <!-- test col -->
                        <div class="col-lg-3 col-sm-4 text-center">
                            <div class="test-list-col">
                                <img src="img/exam.png" alt="">
                                <h4 class="h6">Time & Work</h4>
                                <p>Total Test 3</p>
                                <a href="javascript:void(0)" class="whitebtn">Take Test</a>
                            </div>                                        
                        </div>
                        <!--/ test col -->
                        <!-- test col -->
                        <div class="col-lg-3 col-sm-4 text-center">
                            <div class="test-list-col">
                                <img src="img/exam.png" alt="">
                                <h4 class="h6">Problems on Ages</h4>
                                <p>Total Test 3</p>
                                <a href="javascript:void(0)" class="whitebtn">Take Test</a>
                            </div>                                        
                        </div>
                        <!--/ test col -->
                        <!-- test col -->
                        <div class="col-lg-3 col-sm-4 text-center">
                            <div class="test-list-col">
                                <img src="img/exam.png" alt="">
                                <h4 class="h6">Trains & Boats</h4>
                                <p>Total Test 3</p>
                                <a href="javascript:void(0)" class="whitebtn">Take Test</a>
                            </div>                                        
                        </div>
                        <!--/ test col -->
                        <!-- test col -->
                        <div class="col-lg-3 col-sm-4 text-center">
                            <div class="test-list-col">
                                <img src="img/exam.png" alt="">
                                <h4 class="h6">Permutation & Combination</h4>
                                <p>Total Test 3</p>
                                <a href="javascript:void(0)" class="whitebtn">Take Test</a>
                            </div>                                        
                        </div>
                        <!--/ test col -->
                        <!-- test col -->
                        <div class="col-lg-3 col-sm-4 text-center">
                            <div class="test-list-col">
                                <img src="img/exam.png" alt="">
                                <h4 class="h6">Probability</h4>
                                <p>Total Test 3</p>
                                <a href="javascript:void(0)" class="whitebtn">Take Test</a>
                            </div>                                        
                        </div>
                        <!--/ test col -->
                        <!-- test col -->
                        <div class="col-lg-3 col-sm-4 text-center">
                            <div class="test-list-col">
                                <img src="img/exam.png" alt="">
                                <h4 class="h6">Percentages</h4>
                                <p>Total Test 3</p>
                                <a href="javascript:void(0)" class="whitebtn">Take Test</a>
                            </div>                                        
                        </div>
                        <!--/ test col -->
                        <!-- test col -->
                        <div class="col-lg-3 col-sm-4 text-center">
                            <div class="test-list-col">
                                <img src="img/exam.png" alt="">
                                <h4 class="h6">Averages & Mixtures</h4>
                                <p>Total Test 3</p>
                                <a href="javascript:void(0)" class="whitebtn">Take Test</a>
                            </div>                                        
                        </div>
                        <!--/ test col -->
                        <!-- test col -->
                        <div class="col-lg-3 col-sm-4 text-center">
                            <div class="test-list-col">
                                <img src="img/exam.png" alt="">
                                <h4 class="h6">Ratio & Proportions</h4>
                                <p>Total Test 3</p>
                                <a href="javascript:void(0)" class="whitebtn">Take Test</a>
                            </div>                                        
                        </div>
                        <!--/ test col -->
                        <!-- test col -->
                        <div class="col-lg-3 col-sm-4 text-center">
                            <div class="test-list-col">
                                <img src="img/exam.png" alt="">
                                <h4 class="h6">Partnership</h4>
                                <p>Total Test 3</p>
                                <a href="javascript:void(0)" class="whitebtn">Take Test</a>
                            </div>                                        
                        </div>
                        <!--/ test col -->
                        <!-- test col -->
                        <div class="col-lg-3 col-sm-4 text-center">
                            <div class="test-list-col">
                                <img src="img/exam.png" alt="">
                                <h4 class="h6">Data Sufficiency</h4>
                                <p>Total Test 3</p>
                                <a href="javascript:void(0)" class="whitebtn">Take Test</a>
                            </div>                                        
                        </div>
                        <!--/ test col -->
                    </div>
                    <!--/ test row -->   

                    </div>
                    <!--/ col 9-->                  
                </div>         
               <!--/ col 9-->
              </div>
              <!--/ row -->
           </div>
           <!--/ container -->
        </div>
        <!--/ sub page body -->
    </main>

    <!--/ sub page main -->
    <!--footer -->
    <?php include 'footer.php' ?>
    <!--/ footer -->  
    <?php include 'footerscripts.php'?>

    <!-- popup table -->
    <div class="modal fade" id="test-pop" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content ">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Profit &amp; Loss</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
            <div class="modal-body custom-modalbody">
                  <!-- test row-->
                  <div class="row test-list-row justify-content-center">
                    <!-- test col -->
                    <div class="col-lg-3 col-sm-4 text-center">
                        <div class="test-list-col">
                            <img src="img/exam.png" alt="">
                            <h4 class="h6">Profit & Loss Test 1</h4>
                            <div class="py-2">
                                <span class="small d-block">No.of Qst: 10</span>
                                <span class="small d-block">Duration: 15:00 Min</span>
                            </div>
                            <a href="javascript:void(0)" class="whitebtn">Start Test</a>
                        </div>                                        
                    </div>
                    <!--/ test col -->
                    <!-- test col -->
                    <div class="col-lg-3 col-sm-4 text-center">
                        <div class="test-list-col">
                            <img src="img/exam.png" alt="">
                            <h4 class="h6">Profit & Loss Test 3</h4>
                            <div class="py-2">
                                <span class="small d-block">No.of Qst: 10</span>
                                <span class="small d-block">Duration: 15:00 Min</span>
                            </div>
                            <a href="javascript:void(0)" class="whitebtn">Start Test</a>
                        </div>                                        
                    </div>
                    <!--/ test col -->
                    <!-- test col -->
                    <div class="col-lg-3 col-sm-4 text-center">
                        <div class="test-list-col">
                            <img src="img/exam.png" alt="">
                            <h4 class="h6">Profit & Loss Test 3</h4>
                            <div class="py-2">
                                <span class="small d-block">No.of Qst: 10</span>
                                <span class="small d-block">Duration: 15:00 Min</span>
                            </div>
                            <a href="javascript:void(0)" class="whitebtn">Start Test</a>
                        </div>                                        
                    </div>
                    <!--/ test col -->
                </div>
                <!--/ test row --> 
            </div>        
        </div>
    </div>
    </div>
    <!--/ popup table -->
</body>

</html>