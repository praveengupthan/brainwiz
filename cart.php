<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Brainwiz Video Tutorials</title>
    <link rel="icon" type="image/png" sizes="32x32" href="img/favicon-32x32.png">
    <!-- style sheets -->
    <?php include 'headerstyles.php' ?>
</head>

<body>

    <!-- header -->
   <?php include 'header-postlogin.php' ?>
    <!--/ header -->
    <!--sub page main -->
    <main class="subpage">
        <!-- sub page header -->
        <div class="page-header">
            <!-- container -->
            <div class="container">
               <!-- row -->
               <div class="row">
                   <div class="col-lg-6">
                       <h1>Cart <span class="fbold">Items</span> </h1>
                   </div>
               </div>
               <!--/ row --> 
            </div>
            <!--/ container -->
        </div>
        <!--/ sub page header -->

         <!-- brudcrumb -->
         <div class="container">
            <!-- row -->
            <div class="row">
                <!-- col -->
                <div class="col-lg-12">                   
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                        <li class="breadcrumb-item active"><a>Cart Items</a></li>
                    </ul>                    
                </div>
                <!--/col -->
            </div>
            <!--/ row -->
        </div>
        <!--/ brudcrumb -->

        <!-- sub page body -->
        <div class="subpage-body">
           <!-- container -->
           <div class="container">
              <!-- row -->
              <div class="row justify-content-around">
                    <!-- left col -->
                    <div class="col-lg-8">

                        <!-- course row -->
                        <div class="course-row row">
                            <div class="col-lg-3">
                                <img src="img/crtcourseimg.svg" class="img-fluid">
                            </div>
                            <div class="col-lg-9">
                            <h6 class="h5 fblue text-right"><span class="oldprice">Rs: 2500</span> Rs: 1000</h6>
                                <h4 class="h5 fbold">CRT Special Training </h4>
                                <p class="fgray text-left">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Totam, necessitatibus!</p>

                                <p>
                                    <a href="javascript:void(0)"><span class="icon-trash icomoon"></span> Remove from Cart</a>

                                    <a class="pl-3" href="javascript:void(0)"><span class="icon-heart-o icomoon"></span> Move to Wishlist</a>
                                </p>
                            </div>
                        </div>
                        <!--/ course row -->

                         <!-- course row -->
                         <div class="course-row row">
                            <div class="col-lg-3">
                                <img src="img/cprogramming.svg" class="img-fluid">
                            </div>
                            <div class="col-lg-9">
                            <h6 class="h5 fblue text-right"><span class="oldprice">Rs: 2500</span> Rs: 1500</h6>
                                <h4 class="h5 fbold">C Programming Language </h4>
                                <p class="fgray text-left">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Totam, necessitatibus!</p>

                                <p>
                                    <a href="javascript:void(0)"><span class="icon-trash icomoon"></span> Remove from Cart</a>

                                    <a class="pl-3" href="javascript:void(0)"><span class="icon-heart-o icomoon"></span> Move to Wishlist</a>
                                </p>
                            </div>
                        </div>
                        <!--/ course row -->

                         <!-- course row -->
                         <div class="course-row row">
                            <div class="col-lg-3">
                                <img src="img/crtcourseimg.svg" class="img-fluid">
                            </div>
                            <div class="col-lg-9">
                            <h6 class="h5 fblue text-right"><span class="oldprice">Rs: 2500</span> Rs: 1000</h6>
                                <h4 class="h5 fbold">CRT Special Training </h4>
                                <p class="fgray text-left">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Totam, necessitatibus!</p>

                                <p>
                                    <a href="javascript:void(0)"><span class="icon-trash icomoon"></span> Remove from Cart</a>

                                    <a class="pl-3" href="javascript:void(0)"><span class="icon-heart-o icomoon"></span> Move to Wishlist</a>
                                </p>
                            </div>
                        </div>
                        <!--/ course row -->
                    </div>
                    <!--/ left col -->

                    <!-- right col for total -->
                    <div class="col-lg-3">
                        <h4 class="h5">Total</h4>
                        <h1 class="h2 fblue fbold">Rs:2800</h1>
                        <h6 class="h5 pb-3  border-bottom fblue"><span class="oldprice">Rs: 5800</span> 95% Off</h6>
                        <p class="pt-3">
                            <a class="bluebtn w-100 d-block" href="checkout.php">Checkout Now</a>
                        </p>
                    </div>
                    <!--/ right col for total  -->
              </div>
              <!--/ row -->
           </div>
           <!--/ container -->
        </div>
        <!--/ sub page body -->
    </main>

    <!--/ sub page main -->
    <!--footer -->
    <?php include 'footer.php' ?>
    <!--/ footer -->  
    <?php include 'footerscripts.php'?>

   
    


</body>

</html>