<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Brainwiz</title>
    <link rel="icon" type="image/png" sizes="32x32" href="img/favicon-32x32.png">
    <!-- style sheets -->
    <?php include 'headerstyles.php' ?>
</head>

<body>

    <!-- header -->
   <?php include 'header.php' ?>
    <!--/ header -->
    <!--sub page main -->
    <main class="subpage">
        <!-- sub page header -->
        <div class="page-header">
            <!-- container -->
            <div class="container">
               <!-- row -->
               <div class="row">
                   <div class="col-lg-6">
                       <h1>Privacy Policy </h1>
                   </div>
               </div>
               <!--/ row --> 
            </div>
            <!--/ container -->
        </div>
        <!--/ sub page header -->

         <!-- brudcrumb -->
         <div class="container">
            <!-- row -->
            <div class="row">
                <!-- col -->
                <div class="col-lg-12">                   
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.php">Home</a></li>                        
                        <li class="breadcrumb-item active"><a>Privacy Policy</a></li>
                    </ul>                    
                </div>
                <!--/col -->
            </div>
            <!--/ row -->
        </div>
        <!--/ brudcrumb -->
        <!-- sub page body -->
        <div class="subpage-body">
           <!-- container -->
           <div class="container">
              <!-- row -->
              <div class="row">
               <!-- col 9 -->
                <div class="col-lg-9">

                <p>At Udacity (“Udacity”, “we”, “us”, “our”) we respect your privacy rights. We have created this privacy policy (“Privacy Policy”) to explain how we treat the information collected and received from your use of our Services (as defined in our Terms of Use or “TOU”) provided through the website, http://www.udacity.com and any other websites or applications we own, operate, or control (the “Website(s)”). This Privacy Policy applies only to the Services, and we are not responsible for the practices of persons, companies, institutions or websites that Udacity does not control, manage, or employ. Capitalized terms not defined herein have the meanings set forth in the TOU.</p>

                <p>Please read this Privacy Policy as well as our TOU carefully, as they govern your use of Udacity. If you do not agree to this Privacy Policy, please do not use our Services.</p>

               <p>Udacity, Inc. is the data controller of your information. Please see the “Contact Information” section below for additional details. Please note that if you obtain access through Sponsored Services or our Employee and Enterprise Sponsored Access Services, the third-party sponsoring your participation in the Online Courses may provide us information subject to our agreement with them and therefore the third-party sponsor may be the controller with respect to such data.</p>

               <h2 class="h4">Important Information About Using Our Services</h2>
               <p>Please understand that by submitting any information to us, you acknowledge that we may collect, use, disclose and retain such information in accordance with this Privacy Policy and our TOU, and as permitted or required by law. If you do not agree with these terms, then please do not provide any information to us. Udacity may offer social sharing features as part of its Services which, among other features, allows you to make your careers Profile (as described below in Profiles and Career Services Section) public as well as sharing your details and information, including your Online Course progress with others outside of the Udacity Website(s) (“Social Sharing”). By electing to participate in any of our Social Sharing Services, you acknowledge that any information or materials you include in your Profile can be viewed by other persons accessing the Website(s), including potential or existing employers, as well as by others on applicable third-party websites. You also acknowledge that information you share through Social Services may remain accessible on third party websites and/or to those you have shared the information with in the event you make your Profile private after having made it public and/or ceasing to share your information and that Udacity has no control over the access of or use of information provided to third parties via Social Sharing outside its Website(s).</p>

               <h2 class="h4">What Information We Collect</h2>
               <p>We collect a variety of information that you provide directly to us. In general, we collect information from you through:</p>

               <ul class="list-content">
                   <li>Using any of our Services, including registering for a User Account through the Website(s)</li>
                   <li>Making a purchase from us, including registering and enrolling in any Online Course</li>
                   <li>Signing up for email updates</li>
                   <li>Uploading or posting to public forums included through the Services</li>
                   <li>Submitting requests or questions to us via forms or email</li>
                   <li>Requesting customer support and technical assistance</li>
                   <li>Name, address, telephone number and email address</li>
               </ul>
               <p>Participation in Online Courses. If you desire to participate in an Online Course, you will be asked to provide us with certain information necessary to conduct such an Online Course. This information may include, among other things, your name and email address. Udacity, or its third-party service providers, may also collect certain information from you in conjunction with assignments, exams and other assessments related to the Online Course. For example, as part of a proctored exam for an Online Course or to process your credential of completion for a Nanodegree program, Udacity (or its third party service providers, such as NetVerify) may collect certain information from you in order to (a) verify or authenticate your identity or submissions made by you, such as a signature for a test or assignment log, a photograph or recording of you (e.g., using a webcam) or information included on a government issued or other photo identification card or document, or (b) monitor your performance during an exam to confirm that you are abiding by the applicable test rules or requirements (e.g., confirming that you are not using prohibited resources). Udacity may also collect information from you or about your performance or accomplishments related to the course, such as quiz/exam scores, grades, project evaluations, teacher evaluations and other evaluations of your performance or accomplishments.</p>

               <h2 class="h4">Important Information About Using Our Services</h2>
               <p>Please understand that by submitting any information to us, you acknowledge that we may collect, use, disclose and retain such information in accordance with this Privacy Policy and our TOU, and as permitted or required by law. If you do not agree with these terms, then please do not provide any information to us. Udacity may offer social sharing features as part of its Services which, among other features, allows you to make your careers Profile (as described below in Profiles and Career Services Section) public as well as sharing your details and information, including your Online Course progress with others outside of the Udacity Website(s) (“Social Sharing”). By electing to participate in any of our Social Sharing Services, you acknowledge that any information or materials you include in your Profile can be viewed by other persons accessing the Website(s), including potential or existing employers, as well as by others on applicable third-party websites. You also acknowledge that information you share through Social Services may remain accessible on third party websites and/or to those you have shared the information with in the event you make your Profile private after having made it public and/or ceasing to share your information and that Udacity has no control over the access of or use of information provided to third parties via Social Sharing outside its Website(s).</p>
                  
                </div>         
               <!--/ col 9-->

               <!-- col 3-->
               <div class="col-lg-3">
                   <div class="career-block text-center">
                        <img src="img/premade-image-09.png">
                        <h2 class="h5 py-3">Advance Your Career</h2>
                        <p class="text-center">Enter your email below to download one of our free career guides</p>
                        <form>
                            <div class="form-group">
                                <input class="form-control" type="text" placeholder="Enter your Email"> 
                            </div>
                            <div class="form-group">
                                <input class="bluebtn w-100" type="submit" value="Submit"> 
                            </div>
                        </form>
                        <p class="small text-left">I consent and agree to receive email marketing communications from Udacity.  " that by clicking “Submit,” my data will be used in accordance with the  Udacity Terms of Use and Privacy Policy.</p>
                   </div>
               </div>
               <!--/ col 3-->
             
              

              



              </div>
              <!--/ row -->
           </div>
           <!--/ container -->
        </div>
        <!--/ sub page body -->
    </main>

    <!--/ sub page main -->
    <!--footer -->
    <?php include 'footer.php' ?>
    <!--/ footer -->  
    <?php include 'footerscripts.php'?>
</body>

</html>