<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Brainwiz</title>
    <link rel="icon" type="image/png" sizes="32x32" href="img/favicon-32x32.png">
    <!-- style sheets -->
    <?php include 'headerstyles.php' ?>
</head>

<body>

    <!-- header -->
   <?php include 'header.php' ?>
    <!--/ header -->
    <!--sub page main -->
    <main class="subpage">
        <!-- sub page header -->
        <div class="page-header">
            <!-- container -->
            <div class="container">
               <!-- row -->
               <div class="row">
                   <div class="col-lg-6">
                       <h1>Blogs </h1>
                   </div>
               </div>
               <!--/ row --> 
            </div>
            <!--/ container -->
        </div>
        <!--/ sub page header -->

         <!-- brudcrumb -->
         <div class="container">
            <!-- row -->
            <div class="row">
                <!-- col -->
                <div class="col-lg-12">                   
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                        <li class="breadcrumb-item"><a href="blog.php">Blogs</a></li>
                        <li class="breadcrumb-item active"><a>Blog Name will be here</a></li>
                    </ul>                    
                </div>
                <!--/col -->
            </div>
            <!--/ row -->
        </div>
        <!--/ brudcrumb -->
        <!-- sub page body -->
        <div class="subpage-body">
           <!-- container -->
           <div class="container">
              <!-- row -->
              <div class="row">
                  <!-- left description -->
                  <div class="col-lg-9">
                      <div class="blog-detail">
                          <h2 class="h4">How a Simple Landing page can Drastically Change the face of your Business</h2>
                          <ul class="list-seperator nav">
                                <li>
                                    <a>Posted by Admin</a>
                                </li>
                                <li>
                                    <a>On 25 November 2019</a>
                                </li>
                          </ul>
                          <figure class="py-3">
                              <img src="img/data/blogdetailimg.jpg" alt="" class="img-fluid">
                          </figure>
                          <article>
                              <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>

                              <h3 class="h5 fbold">Where can I get some?</h3>

                              <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum.</p>

                              <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters,  Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. </p>
                          </article>
                      </div>
                  </div>
                  <!--/ left description -->

                  <!-- recnet posts -->
                  <div class="col-lg-3 recent-posts">
                      <h3 class="h4">Recent Posts</h3>

                      <figure class="d-flex recentblog">
                            <a href="blog-detail.php"><img src="img/data/blogimg01.jpg"></a>
                            <a href="blog-detail.php" class="h6">Where does it come from? <span class="fgray">06 Apr 2019</span></a>
                      </figure>

                      <figure class="d-flex recentblog">
                            <a href="blog-detail.php"><img src="img/data/blogimg02.jpg"></a>
                            <a href="blog-detail.php" class="h6">Where does it come from? <span class="fgray">06 Apr 2019</span></a>
                      </figure>

                      <figure class="d-flex recentblog">
                            <a href="blog-detail.php"><img src="img/data/blogimg03.jpg"></a>
                            <a href="blog-detail.php" class="h6">Where does it come from? <span class="fgray">06 Apr 2019</span></a>
                      </figure>

                      <figure class="d-flex recentblog">
                            <a href="blog-detail.php"><img src="img/data/blogimg04.jpg"></a>
                            <a href="blog-detail.php" class="h6">Where does it come from? <span class="fgray">06 Apr 2019</span></a>
                      </figure>

                      <figure class="d-flex recentblog">
                            <a href="blog-detail.php"><img src="img/data/blogimg05.jpg"></a>
                            <a href="blog-detail.php" class="h6">Where does it come from? <span class="fgray">06 Apr 2019</span></a>
                      </figure>

                      <figure class="d-flex recentblog">
                            <a href="blog-detail.php"><img src="img/data/blogimg06.jpg"></a>
                            <a href="blog-detail.php" class="h6">Where does it come from? <span class="fgray">06 Apr 2019</span></a>
                      </figure>

                  </div>
                  <!--/ recent posts -->
             
              

              



              </div>
              <!--/ row -->
           </div>
           <!--/ container -->
        </div>
        <!--/ sub page body -->
    </main>

    <!--/ sub page main -->
    <!--footer -->
    <?php include 'footer.php' ?>
    <!--/ footer -->  
    <?php include 'footerscripts.php'?>
</body>

</html>