<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Clat Exam Pattern</title>
    <link rel="icon" type="image/png" sizes="32x32" href="img/favicon-32x32.png">
    <!-- style sheets -->
    <?php include 'headerstyles.php' ?>
</head>

<body>

    <!-- header -->
   <?php include 'header.php' ?>
    <!--/ header -->
    <!--sub page main -->
    <main class="subpage">
        <!-- sub page header -->
        <div class="page-header">
            <!-- container -->
            <div class="container">
               <!-- row -->
               <div class="row">
                   <div class="col-lg-6">
                       <h1>Clat Exam </h1>
                   </div>
               </div>
               <!--/ row --> 
            </div>
            <!--/ container -->
        </div>
        <!--/ sub page header -->

        <!-- brudcrumb -->
        <div class="container">
            <!-- row -->
            <div class="row">
                <!-- col -->
                <div class="col-lg-9">                   
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Examinations</a></li>
                        <li class="breadcrumb-item active"><a>Clat</a></li>
                    </ul>                    
                </div>
                <!--/col -->
            </div>
            <!--/ row -->
        </div>
        <!--/ brudcrumb -->

        <!-- sub page body -->
        <div class="subpage-body">
           <!-- container -->
           <div class="container">
               <!-- row -->
               <div class="row">
                    <!-- col 8 -->
                    <div class="col-lg-8">
                        <h2 class="h2 pb-2">CLAT 2020</h2>                        
                        <p>The Common Law Admission Test (CLAT) is a national level entrance exam for admissions to candidates in undergraduate (UG) and postgraduate (PG) law courses offered at NLUs and other colleges/ universities accepting exam scores.</p>
                       
                        <p>CLAT is conducted every year to select students for 22 National Law Universities in India, other than National Law University-Delhi, which has their own process to select the students. The exam is tentatively scheduled to be held in the second week of May. The exam is held for admissions in UG and PG programs offered by the National Law Universities.</p>

                        <p>Till 2018, the exam was conducted by NLUs on a rotational basis. However, from 2019 onwards it is being conducted by a permanent body formed by the universities i.e. The Consortium of NLUs. The consortium has it’s headquarter at NLSIU Bangalore and comprises:</p>

                        <ul class="page-list">                          
                            <li>An Executive Committee</li>
                            <li>The CLAT Convenor of the current year (NUSRL, Ranchi, for CLAT 2020)</li>
                            <li>The CLAT Convenor of the following year </li>
                            <li>Two co-opted Vice-Chancellors of NLUs</li>
                        </ul>

                        <h3 class="h4">CLAT Training in Hyderabad</h3>
                        <p>For CLAT Training, BRAINWIZ is the Best CLAT Training institute in Hyderabad. Here, training covers the complete Logical Reasoning and Verbal Ability sections of CLAT 2020 updated syllabus. Training will be with complete tricks and shortcuts which makes an easy way for the students to get a good score in the CLAT Entrance exams. Thousands of students from PAN India came to BRAINWIZ and got Trained on these modules and secured an high score in CLAT and got a chance for integrated courses of LLB as well as LLM courses. This makes BRAINWIZ to be the leading institute for CLAT Training in Ameerpet, Hyderabad.</p>
                        <p>CLAT EXAM 2020 Eligibility criteria, Exam Pattern and Syllabus are mentioning in detail below.</p>
                       
                        <h3 class="h4">CLAT Eligibility Criteria 2020</h3>
                        <p>To appear for CLAT exam, candidates need to fulfil the eligibility criteria as under:</p>

                        <h3 class="h4">CLAT Eligibility for five-year integrated courses:</h3>

                        <ul class="page-list">                          
                            <li>Candidates who have completed 10+2 or equivalent examination for national or state board of education.</li>
                            <li>They should have obtained minimum 45% marks if belonging to General/ Other Backward Class (OBC) / Specially-abled categories and, 40% marks if belonging to Scheduled Class (SC) / Scheduled Tribe (ST) categories. </li>
                            <li>Candidates can apply for CLAT 2020 irrespective of their age as the age limit criteria have been removed by BCI.  </li>                           
                        </ul>

                        <h3 class="h4">CLAT Eligibility for LLM Course:</h3>

                        <ul class="page-list">                          
                            <li>Candidates should have completed their graduation with at least 55% marks if belonging to General/ Other Backward Class (OBC)/ specially abled person categories and at least 50% marks if belonging to Scheduled Class (SC)/ Scheduled Tribe (ST) categories.</li>
                            <li>Candidates can apply for CLAT 2020 even if they are in the final year of their graduation degree. However, they should submit the proof of qualifying the final examination and completing graduation degree at the time of admission formalities.  </li>
                            <li>There is no upper age limit to apply for CLAT 2020 exam.  </li>                           
                        </ul>

                        <h3 class="h4">CLAT Process </h3>
                        <p>Candidates seeking admissions to NLUs or law schools accepting CLAT scores have to follow the process as under: </p>

                        <h3 class="h4">Stage I </h3>
                        <p>APPLY ONLINE:  Candidates have to apply online on the official website of CLAT consortium. </p>

                        
                        <h3 class="h4">Stage II </h3>
                        <p>DOWNLOAD ADMIT CARD: The Consortium will issue the admit cards to all applicants for appearing in the exam. </p>

                        <h3 class="h4">Stage III</h3>
                        <p>APPEAR FOR EXAM: Candidates have to appear at the allotted test centres to take the exam. </p>

                        <h3 class="h4">Stage IV</h3>
                        <p>DECLARATION OF RESULTS: CLAT Consortium will declare the results on the official website in the form of Merit lists.</p>

                        <h3 class="h4">Stage V</h3>
                        <p>ADMISSION PROCESS: The Shortlisted candidates have to participate in the counselling rounds of NLUs as per the schedule.</p>

                        <h2 class="h2 pb-2">CLAT 2020 Exam Highlights</h2> 

                        <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Exam Highlights</th>
                                    <th>Details</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Exam Name</td>
                                    <td>CLAT (Common Law Admission Test)</td>
                                </tr>
                                <tr>
                                    <td>
                                    CLAT Exam Conducting Body
                                    </td>
                                    <td>
                                    Consortium of National Law Universities
                                    </td>
                                </tr>
                                <tr>
                                    <td>CLAT Exam Level</td>
                                    <td>UG and PG</td>
                                </tr>
                                <tr>
                                    <td>CLAT Exam Frequency</td>
                                    <td>Once a year</td>
                                </tr>
                                <tr>
                                    <td>CLAT Exam Mode  </td>
                                    <td>Offline</td>
                                </tr>
                                <tr>
                                    <td>CLAT Exam Duration</td>
                                    <td>120 Minutes</td>
                                </tr>
                                <tr>
                                    <td>Number of Questions </td>
                                    <td>150</td>
                                </tr>
                                <tr>
                                    <td>Marking Scheme</td>
                                    <td>0.25 marks for each wrong answer</td>
                                </tr>
                                <tr>
                                    <td>CLAT Exam Language</td>
                                    <td>English</td>
                                </tr>
                                <tr>
                                    <td>CLAT Exam Purpose </td>
                                    <td>Admissions to BA LLB, BCom LLB, BSc LLB, BBA LLB, BSW LLB, LLM courses</td>
                                </tr>
                            </tbody>                           
                        </table>
                        </div>
                        <h3 class="h4">CLAT 2020 Sectional Weightage</h3>
                        <p>The consortium will release detailed exam pattern of CLAT 2020 in the last week of December 2019, however, as per the press release, it is expected that 'Legal Aptitude' section will be removed from the question paper. Also, the total number of questions will be 120-150 only. Refer the table below for the speculated sectional- weightage of CLAT 2020 paper:</p>

                        <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Subject Areas</th>
                                    <th>Questions per section</th>
                                    <th>Marks per section</th>                                    
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>English including Comprehension</td>
                                    <td>40 MCQ </td>
                                    <td>40 marks </td> 
                                </tr>
                                <tr>
                                    <td>Current Affairs including General Knowledge</td>
                                    <td>50 MCQ</td>
                                    <td>50 marks </td>
                                                                 
                                </tr>
                                <tr>
                                    <td>Quantitative Techniques (Elementry mathematics up to class 10th standard)</td>
                                    <td>20 MCQ</td>
                                    <td>20 marks </td> 
                                </tr>
                                <tr>
                                    <td>Legal Reasoning</td>
                                    <td>? MCQ (to be confirmed     by the consortium)</td>
                                    <td>? marks (to be confirmed by the consortium) </td>          
                                </tr>
                                <tr>
                                    <td>Logical Reasoning</td>
                                    <td>40 MCQ</td>
                                    <td>40 marks </td>          
                                </tr>
                                <tr>
                                    <td>Total Questions*</td>
                                    <td>150 MCQs</td>
                                    <td>150 marks </td>          
                                </tr>
                            </tbody>                           
                        </table>
                        </div>

                        <h3 class="h4">CLAT 2020 Marking Scheme </h3>
                        <p>Unlike the exam pattern. The marking scheme of CLAT will remain the same as earlier, i.e.</p>                       

                        <ul class="page-list pb-3">                          
                               <li>For every correct answer, candidates are awarded one mark.</li>
                               <li>However, for every wrong attempt, 0.25 marks are deducted from their total score.   </li>
                               <li>No marks will be awarded or deducted for questions not attempted.</li>
                        </ul>

                        <h3 class="h4">CLAT 2020 New Pattern </h3>
                        <p>In its 13th edition of CLAT, the consortium in a press conference held on 21st November, 2019 announced some major changes in the CLAT 2020 paper pattern. The changes are brought in with the view to improve the quality of students entering the National Law Universities.</p>

                        <p>CLAT 2020 will be held on 10th May, 2020. The consortium maintains that the 13th edition of Common Law Admission Test (CLAT) would be held in the usual manner, i.e. 2nd Sunday of May, 2020.</p>
                        
                        <p>CLAT 2020 will be offline. With respect to the mode of examination, the exam would be in the offline mode, same as 2019. Following the number of issues and problems with conducting the exam in online mode, CLAT 2019 was held in offline mode and the same would be continuing for CLAT 2020 as well.</p>

                        <p>Reduction in the number of questions from 200 to 120-150 – As stated by Prof. Faizan Mustafa (who has been heading the NLU Consortium as its President), “asking students to answer 200 questions in 120 minutes is not right as it puts students under lot of mental stress”. Therefore there will be a reduction in the number of questions from 200 to 120-150. The exact number of questions is however unknown at this time.</p>

                        <p>Comprehension based Questions – The consortium has informed that comprehension based questions will be asked from Quantitative Techniques, English, Current Affairs, Deductive Reasoning and Logical Reasoning.</p>

                        <p>Static GK – No questions from the static GK portion will be asked. Questions, only from the current affairs section will be asked and for this, the students only need to read newspapers.</p>

                        <p>No Legal GK – CLAT is no more a test of memory. It is a test of aptitude and for this very reason, there won’t be any questions from Legal Knowledge. Earlier also there have been papers with zero questions from the Legal Knowledge section. Example – 2018 CLAT paper. A similar pattern is expected in 2020 as well.</p>

                        <p>Data Interpretation – Hard-core mathematics questions like Time and Work will not be asked. Instead, students will be tested on Data Interpretation questions. Maths, already known as the big bad of CLAT could now be one of the easiest section of CLAT.</p>

                        <p>Deductive Reasoning – There has been a huge buzz amongst the students as to what is deductive reasoning? Will there be any legal section in CLAT or not? Just to clarify to all such students, the legal reasoning section i.e. the principle-fact based questions is basically your deductive reasoning. The only change is, no prior legal knowledge is required. There will be a legal section, but that too would be in the form of reasoning based questions. As stated by Prof. Faizan Mustafa, it is the job of NLUs to teach law. Students taking admissions in NLUs could not be expected to know the law.</p>

                        <p>No Vocabulary – As stated earlier, since CLAT is no more a test of memory, there won’t be any direct questions pertaining to vocabulary, synonyms etc.</p>

                        <p>All of the above mentioned changes have been brought in with a view to improve the quality of students entering the National Law Universities. As stated by Prof. Faizan Mustafa, “The idea is to get better students to National Law Universities who have competence in reading texts and demonstrate skills in inferential reasoning.”</p>
                      

                        <h3 class="h4">Section Wise Syllabus</h3>
                        <p>Verbal Ability</p>
                        <ul class="page-list pb-3">                           
                               <li>1. Vocabulary</li>
                               <li>2. Sentence Correction</li>      
                               <li>3. Reading Comprehension</li>  
                               <li>4. Fill in the blanks</li>
                               <li>5. Jumbled Sentences</li>
                               <li>6. Closed Paragraph</li>
                               <li>7. Summary Questions</li>
                               <li>8. Analogies & Reverse Analogies</li>                       
                        </ul>

                        <p>Logical Reasoning</p>
                        <ul class="page-list pb-3">                           
                               <li>1. Number series</li>
                               <li>2. Letter series</li>      
                               <li>3. Calendars</li>  
                               <li>4. Clocks</li>
                               <li>5. Cubes</li>
                               <li>6. Seating Arrangement</li>
                               <li>7. Deductions & Syllogisms</li>
                               <li>8. Blood Relations</li>    
                               <li>9. Logical Connectives</li>                   
                               <li>10. Logical Sequence</li>
                               <li>11. Logical Matching</li>
                        </ul>

                        <h3 class="h4">General Awareness</h3>                        
                        <p>In this section, candidates are evaluated on the basis of their general awareness including static general knowledge. Current affair questions test a candidate’s knowledge about the events happening nationally as well as internationally. To prepare for this section, candidates are advised to be up-to-date with news and other topics discussed in mainstream media from March 2019 to April 2020.</p>

                        <h3 class="h4">Mathematics </h3>
                        <p>In this section, candidate’s knowledge on elementary mathematics, that is, Maths taught up to Class X is tested.</p>

                        <h3 class="h4">Legal Aptitude </h3>
                        <p>In this section, a candidate’s interest in the field of law, research aptitude as well as problem-solving abilities is tested. </p>                        
                    </div>
                    <!--/ col 8--> 

                     <!-- course right faq -->
                   <div class="col-lg-4">   
                   <!-- sticky top -->
                   <div class="sticky-top">                     
                        <div class="custom-accord course-faq">
                            <h3 class="h5 pb-3">FAQs regarding CLAT Exam</h3>
                            <!-- accordion -->
                            <div class="accordion">
                                <!-- acc-->
                                <h3 class="panel-title">When will the CLAT 2020 applications begin? </h3>
                                <div class="panel-content">
                                    <p>The application form of CLAT exam will be available from January 1, 2020, onwards. 
                                    </p>
                                </div>
                                <!--/ acc-->
                                <!-- acc-->
                                <h3 class="panel-title">Where to apply for CLAT 2020? </h3>
                                <div class="panel-content">
                                    <p>The aspirants can apply for CLAT 2020 on the official website of the consortium of NLUs i.e. - <a href="https://clatconsortiumofnlu.ac.in/" target="_blank">https://clatconsortiumofnlu.ac.in/.</a> </p>
                                </div>
                                <!--/ acc-->
                                <!-- acc-->
                                <h3 class="panel-title">What is the CLAT eligibility criterion?</h3>
                                <div class="panel-content">
                                    <p>For undergraduate courses, candidates should have passed Class 12th examination or equivalent (regular mode of education). They should have secured at least 45% marks in the qualifying examination and 40% if belonging to SC/ ST category. </p>  
                                    <p>For the LLM course, candidates should hold an LLB degree or five-year integrated degree in law with 55% marks and 50% marks for SC/ ST category candidates. </p>                                 
                                </div>
                                <!--/ acc-->

                                 <!-- acc-->
                                 <h3 class="panel-title">Is there any age limit to appear for CLAT?</h3>
                                <div class="panel-content">
                                    <p>No, there is no upper age limit or a lower age limit to appear for the CLAT exam.</p>                                   
                                </div>
                                <!--/ acc-->

                                <!-- acc-->
                                <h3 class="panel-title">Why is the CLAT exam important?</h3>
                                <div class="panel-content">
                                    <p>CLAT is one of the most popular law entrance exams for admissions to top law schools in India i.e. 22 National Law Universities (NLUs). Moreover, 40 other private law colleges and universities across India offer admission to candidates in UG and PG law courses on the basis of CLAT scores. Therefore, with just one law entrance exam, candidates get admission opportunity at prominent law schools in India.</p>                                   
                                </div>
                                <!--/ acc-->

                                <!-- acc-->
                                <h3 class="panel-title">Who conducts the CLAT exam?</h3>
                                <div class="panel-content">
                                    <p>The law entrance exam is conducted by the CLAT convenor along with other members of the Consortium of NLUs. CLAT 2020 will be conducted by NUSRL, Ranchi. </p>                                   
                                </div>
                                <!--/ acc-->

                                <!-- acc-->
                                <h3 class="panel-title">When will the CLAT 2020 exam be held? </h3>
                                <div class="panel-content">
                                    <p>As per the official announcement by the consortium of NLUs, the upcoming exam of CLAT will be held on May 10, 2020. </p>                                   
                                </div>
                                <!--/ acc-->

                                <!-- acc-->
                                <h3 class="panel-title">Is CLAT an online exam?</h3>
                                <div class="panel-content">
                                    <p>No, CLAT is an offline exam conducted as a pen-and-paper based test. However, until 2018, CLAT was conducted in online mode only.</p>                                   
                                </div>
                                <!--/ acc-->

                                <!-- acc-->
                                <h3 class="panel-title">How many candidates take CLAT exam every year?</h3>
                                <div class="panel-content">
                                    <p>Around 50,000 candidates from different cities across India appear for the CLAT exam every year. </p>                                   
                                </div>
                                <!--/ acc-->

                                <!-- acc-->
                                <h3 class="panel-title">What changes have been made to CLAT 2020 exam pattern? </h3>
                                <div class="panel-content">
                                    <p>As per the press release by the consortium, there will be less number of questions in the upcoming CLAT exam i.e. only 120-150 questions. Moreover, each section will be carrying more questions based on comprehension.  </p>                                   
                                </div>
                                <!--/ acc-->

                                <!-- acc-->
                                <h3 class="panel-title">Is 'Legal Aptitude' section removed from the CLAT 2020 question paper? </h3>
                                <div class="panel-content">
                                    <p>The press notification regarding changes in CLAT exam pattern also stated that there be will only four sections in UG question paper i.e. Quantitative Techniques, English, Current Affairs, Deductive Reasoning, and Logical Reasoning, however, there is no information on the 'Legal Aptitude' section. The removal of the Legal Aptitude section will be confirmed after the detailed notification will be out in the last week of December 2019.</p>                                   
                                </div>
                                <!--/ acc-->

                                <!-- acc-->
                                <h3 class="panel-title">When will be the CLAT exam held?</h3>
                                <div class="panel-content">
                                    <p>The national-level law entrance exam will be held on May 10 next year. </p>                                   
                                </div>
                                <!--/ acc-->

                                <!-- acc-->
                                <h3 class="panel-title">Is CLAT an online exam or offline exam?</h3>
                                <div class="panel-content">
                                    <p>CLAT, as per the change in the exam mode last year remains an offline exam i.e. candidates will get question papers and OMR sheets to attempt the CLAT 2020 exam. </p>                                   
                                </div>
                                <!--/ acc-->

                                <!-- acc-->
                                <h3 class="panel-title">Is CLAT a tough exam? </h3>
                                <div class="panel-content">
                                    <p>The national-level law entrance exam is considered to be one of the toughest admission tests in India because of various reasons such as competition, exam pattern, number of seats and cut off marks. However, this year, the Consortium of NLUs has made some changes in Exam Pattern of CLAT 2020 to make it less difficult for candidates. </p>                                   
                                </div>
                                <!--/ acc-->

                                <!-- acc-->
                                <h3 class="panel-title">Is mathematics compulsory for CLAT? </h3>
                                <div class="panel-content">
                                    <p>No, it is not compulsory to have studied mathematics at Class 12th level as per the eligibility criteria of CLAT 2020, however, the question paper does have a section based on the subject. Though, the section of 'Elementary Mathematics' will be based on class 12th/ basic numerical ability only. </p>                                   
                                </div>
                                <!--/ acc-->

                                <!-- acc-->
                                <h3 class="panel-title">Where can I download question papers of CLAT 2020?</h3>
                                <div class="panel-content">
                                    <p>The candidates can request for previous years papers of CLAT exam while filling up the application form on payment of an additional fee. </p>                                   
                                </div>
                                <!--/ acc-->

                                <!-- acc-->
                                <h3 class="panel-title">Which is the most difficult section of CLAT exam? </h3>
                                <div class="panel-content">
                                    <p>The upcoming CLAT exam will have four sections including, Quantitative Techniques, English, Current Affairs, Deductive Reasoning and Logical Reasoning. It depends on the preparation level of the candidates, in which section they will find difficulty in attempting. However, this year as per the announcement made by the Vice-Chancellor of NALSAR, Hyderabad, there will be more comprehensive questions in all the sections.</p>                                   
                                </div>
                                <!--/ acc-->

                                <!-- acc-->
                                <h3 class="panel-title">Is the legal aptitude section of CLAT question removed?</h3>
                                <div class="panel-content">
                                    <p>It is not yet confirmed whether the question paper of CLAT 2020 will carry questions from 'Legal Aptitude' section. Till 2018, the question paper of CLAT carried 20 questions based on Legal Aptitude. </p>                                   
                                </div>
                                <!--/ acc-->

                                <!-- acc-->
                                <h3 class="panel-title">Is the exam pattern of CLAT PG changed? </h3>
                                <div class="panel-content">
                                    <p>As per the latest announcement by the consortium, there will be no changes in the exam pattern of CLAT PG question; it will be same as last year with descriptive questions. </p>                                   
                                </div>
                                <!--/ acc-->

                                <!-- acc-->
                                <h3 class="panel-title">What are the subjects for CLAT LLM? </h3>
                                <div class="panel-content">
                                    <p>The subjects asked in CLAT LLM/ PG question paper includes Constitutional Law, Contract, Torts, Criminal Law, International Law, IPR and Jurisprudence, Law topics and Contemporary Issues.  </p>                                   
                                </div>
                                <!--/ acc-->

                                <!-- acc-->
                                <h3 class="panel-title">How to prepare for CLAT exam? </h3>
                                <div class="panel-content">
                                    <p>To prepare for CLAT exam, candidates should take note of the changed exam pattern besides syllabus and subjects that will be asked in the question paper. Also, they should consistently prepare for all the sections for at least 6 months. A good preparation strategy should be made before starting the exam preparation. Taking a proper Training at BRAINWIZ, Hyderabad helps you to crack the CLAT Exam with a good score.</p>                                   
                                </div>
                                <!--/ acc-->
                            </div>
                            <!--/ accordion -->
                        </div>
                        </div>    
                        <!--/ sticky top -->    
                   </div>
                   <!--/ course right faq -->

               </div>
               <!--/ row -->
           </div>
            <!-- /container -->
        </div>
        <!--/ sub page body -->
    </main>
    <!--/ sub page main -->
    <!--footer -->
    <?php include 'footer.php' ?>
    <!--/ footer -->  
    <?php include 'footerscripts.php'?>

</body>

</html>