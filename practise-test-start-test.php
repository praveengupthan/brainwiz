<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Practise Tests at Brainwiz</title>
    <link rel="icon" type="image/png" sizes="32x32" href="img/favicon-32x32.png">
    <!-- style sheets -->
    <?php include 'headerstyles.php' ?>
</head>

<body>

    <!-- header -->
   <?php include 'header.php' ?>
    <!--/ header -->
    <!--sub page main -->
    <main class="subpage">
        <!-- sub page header -->
        <div class="page-header">
            <!-- container -->
            <div class="container">
               <!-- row -->
               <div class="row">
                   <div class="col-lg-6">
                       <h1>Time & Work Practice Test 6</h1>
                   </div>
               </div>
               <!--/ row --> 
            </div>
            <!--/ container -->
        </div>
        <!--/ sub page header -->

         <!-- brudcrumb -->
         <div class="container">
            <!-- row -->
            <div class="row">
                <!-- col -->
                <div class="col-lg-12">                   
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.php">Home</a></li>  
                        <li class="breadcrumb-item"><a href="practise-test.php">Practise Tests</a></li>                       
                        <li class="breadcrumb-item active"><a>Time & Work Practice Test 6</a></li>
                    </ul>                    
                </div>
                <!--/col -->
            </div>
            <!--/ row -->
        </div>
        <!--/ brudcrumb -->
        <!-- sub page body -->
        <div class="subpage-body">
           <!-- container -->
           <div class="container">
              <!-- row -->
              <div class="row">
               <!-- col 9 -->
                <div class="col-lg-12">
                <!-- primary details of test -->
                <div class="primary-details-practise-test">
                    <!-- row -->
                    <div class="row border-bottom pb-3 justify-content-end">
                       
                        <!-- col -->
                        <div class="col-lg-6 text-right">
                            <h2 class="time-exam"><time>00:00:00</time></h2>  
                        </div>
                        <!--/ col -->
                    </div>
                    <!--/ row -->

                    <!-- question and answer row -->
                    <div class="row py-3">
                        <!--question col -->
                        <div class="col-lg-9">
                            <!-- question and answer -->
                            <div>
                                <!-- question -->
                                <p>A train is 800 meter long and is running at the speed of 60 km per hour. Find the time it will take to pass a man standing at a crossing.</p>
                                <!--/ question -->
                                <!-- options -->
                                <ul class="list-group">
                                    <li class="list-group-item"><a href="javascript:void(0)"><span class="span-whitehighlate">A</span>48 seconds</a></li>
                                    <li class="list-group-item"><a href="javascript:void(0)"><span class="span-whitehighlate">B</span>52 seconds</a></li>
                                    <li class="list-group-item"><a href="javascript:void(0)"><span class="span-whitehighlate">C</span>40 seconds</a></li>
                                    <li class="list-group-item"><a href="javascript:void(0)"><span class="span-whitehighlate">D</span>60 seconds</a></li>
                                </ul>
                                <!--/ options -->
                            </div>
                            <!--/ questin and answer-->
                            
                             <!-- next previous row -->
                             <div class="row my-3 px-0">
                                <div class="col-lg-6">
                                    <input type="submit" class="bluebtn" value="Submit Text" data-toggle="modal" data-target="#yesrnomodal">
                                </div>
                                <div class="col-lg-6 text-right">
                                    <button class="whitebtn"><span class="icon-arrow-left icomoon"></span> Previous</button>
                                    <button class="whitebtn">Next <span class="icon-arrow-right icomoon"></span></button>
                                </div>
                            </div>
                            <!--/ next previous row -->
                        </div>
                        <!--/ question col -->
                        <!-- question number col -->
                        <div class="col-lg-3 text-right">
                            <h3 class="h6 text-right">Question <span class="fblue fbold">1</span> of 5</h3>
                        </div>
                        <!--/ questin number col -->
                    </div>
                    <!--/ question and answer row -->                   
                </div>
                <!--/ primary detailks of test -->
                  
                </div>         
               <!--/ col 12-->               
              </div>
              <!--/ row -->
           </div>
           <!--/ container -->
        </div>
        <!--/ sub page body -->
    </main>

    <!--/ sub page main -->
    <!--footer -->
    <?php include 'footer.php' ?>
    <!--/ footer -->  
    <?php include 'footerscripts.php'?>

<!-- The Modal for yes r no for submit test -->
  <div class="modal fade" id="yesrnomodal">
    <div class="modal-dialog">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Submit Text</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
         Do you Want to Really Submit Test
        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
           <a class="bluebtn" href="practise-test-scorecard.php">Yes</a>
          <button type="button" class="whitebtn" data-dismiss="modal">No</button>
        </div>
        
      </div>
    </div>
  </div>
  <!-- The Modal for yes r no for submit test-->

</body>

</html>