<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Brainwiz</title>
    <link rel="icon" type="image/png" sizes="32x32" href="img/favicon-32x32.png">
    <!-- style sheets -->
    <?php include 'headerstyles.php' ?>
</head>

<body>
    <!-- header -->
   <?php include 'header.php' ?>
    <!--/ header -->
    <!--sub page main -->
    <main class="subpage">
       

         <!-- brudcrumb -->
         <div class="container">
            <!-- row -->
            <div class="row">
                <!-- col -->
                <div class="col-lg-12">                   
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                        <li class="breadcrumb-item active"><a>Categories</a></li>
                    </ul>                    
                </div>
                <!--/col -->
            </div>
            <!--/ row -->
        </div>
        <!--/ brudcrumb -->

        <!-- sub page body -->
        <div class="subpage-body">
           <!-- container -->
           <div class="container">
                <!-- white box -->
                <div class="whitebox mb-4">
                    <div class="topicsBanner">
                        <!-- row -->
                        <div class="row">
                            <div class="col-md-3">
                                <img src="img/projectIcon.png" alt="">
                            </div>
                            <div class="col-md-9">
                                <h2 class="py-2">Learn and Master Tricks for Logical Reasoning for Placement with PrepInsta</h2>
                                <p>Below on this page you will find the easiest quickest ways to solve a question, formulas, shortcuts and tips and tricks to solve various Logical Reasoning Questions in Placements.</p>              
                            </div>
                        </div>
                        <!--/ row -->
                       
                                 
                    </div>
                </div>
                <!--/ white box -->

                <!-- description -->
                <div class="descriptionSection">
                    <article>
                        <p>Let us study some basic time and work concepts. In our daily lives, we come across so much work that needs to be completed within a specific time period. Usually, some persons are specifically designated to do these works. What will we do, if after some time we realize that the above said work would not be completed in the desired time? We assign more guys to help the earlier people and finish the work in time. This makes the very basic concept of time and work i.e. more people finish the work in lesser time, and lesser persons take more time to finish work.</p>

                        <p>Let us assume a well has to be dug which has to be done in 10 days. If a person can dig a well in 10 days, then in 1 day he will dig 1/10th of that well.  This basic approach can be applied to solve a majority of the time and work problems.</p>

                        <p>Another important concept that is used in time work problems is the combined efficiency of two or more persons. In questions on time and work, the rates at which certain persons or machines work alone are usually given, and it is necessary to compute the rate at which they work together (or vice versa).</p>

                        <p>Let us say, for example, it takes 3 & 6 hours for Bahubali and Kattappa, respectively, to break a dam working alone. So, in 1 hour Bahubali would have broken one-third or 1/3rd or 33% of the dam and Kattappa would have broken one-sixth or 1/6th or 16.67% of the dam. In 2 hours, Bahubali would have destroyed 1/3*2 or 33% *2 = 66.66% of the dam and kattappa would have destroyed 1/6th *2= 1/3= 33.33% of the dam. So if Both Bahubali and Kattappa work together, they would have destroyed 66.66+ 33.33 (2/3+1/3) or 100% of the dam in 2 hours. Therefore, if both worked together for 1 hour, they would have destroyed 1/3 + 1/6= ½ or half of the dam. Thus in 2 hours, the dam is destroyed.
Generalizing, we conclude that in 1 hour, Bahubali does 1/r of the job, Kattappa does 1/s of the job, and Bahubali & Kattappa together do 1/h of the job or that together they can finish the job in ‘h’ hours where the formula for work comes out as 1/r + 1/s = 1/h.</p>                      

                    </article>
                </div>
                <!--/ description -->
              <!-- row -->
              <div class="row">
                    <!-- left tab -->
                    <div class="col-md-12 categoriesPage d-flex"> 
                        <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                            <a class="nav-link active"  data-toggle="pill" href="#tab01">Companies</a>
                            <a class="nav-link"  data-toggle="pill" href="#tab02">Programming</a>
                            <a class="nav-link"  data-toggle="pill" href="#tab03">Aptitude</a>
                            <a class="nav-link"  data-toggle="pill" href="#tab04">Interview Questions</a>
                        </div>

                        <div class="tab-content" id="v-pills-tabContent">
                            <div class="tab-pane fade show active" id="tab01" role="tabpanel">
                                <!-- row -->
                                <div class="row">
                                        <!-- col -->
                                        <div class="col-md-4 colcatPage col-6">
                                            <a href="javascript:void(0)">
                                                <h6>Wipro NLTH</h6>
                                                <p>Lorem ipsum dolor sit amet consectetur.</p>
                                            </a>
                                        </div>
                                        <!--/ col -->
                                         <!-- col -->
                                         <div class="col-md-4 colcatPage col-6">
                                            <a href="javascript:void(0)">
                                                <h6>Wipro</h6>
                                                <p>Lorem ipsum dolor sit  consectetur.</p>
                                            </a>
                                        </div>
                                        <!--/ col -->
                                         <!-- col -->
                                         <div class="col-md-4 colcatPage col-6">
                                            <a href="javascript:void(0)">
                                                <h6>Capgemini</h6>
                                                <p>Lorem ipsum sit amet consectetur.</p>
                                            </a>
                                        </div>
                                        <!--/ col -->
                                          <!-- col -->
                                          <div class="col-md-4 colcatPage col-6">
                                            <a href="javascript:void(0)">
                                                <h6>CoCubes</h6>
                                                <p>Lorem ipsum sit amet consectetur.</p>
                                            </a>
                                        </div>
                                        <!--/ col -->
                                          <!-- col -->
                                          <div class="col-md-4 colcatPage col-6">
                                            <a href="javascript:void(0)">
                                                <h6>Tech Mahindra</h6>
                                                <p>Lorem ipsum sit amet consectetur.</p>
                                            </a>
                                        </div>
                                        <!--/ col -->
                                          <!-- col -->
                                          <div class="col-md-4 colcatPage col-6">
                                            <a href="javascript:void(0)">
                                                <h6>Deloitte</h6>
                                                <p>Lorem ipsum sit amet consectetur.</p>
                                            </a>
                                        </div>
                                        <!--/ col -->
                                          <!-- col -->
                                          <div class="col-md-4 colcatPage col-6">
                                            <a href="javascript:void(0)">
                                                <h6>IBM</h6>
                                                <p>Lorem ipsum sit amet consectetur.</p>
                                            </a>
                                        </div>
                                        <!--/ col -->
                                          <!-- col -->
                                          <div class="col-md-4 colcatPage col-6">
                                            <a href="javascript:void(0)">
                                                <h6>Infosys</h6>
                                                <p>Lorem ipsum sit amet consectetur.</p>
                                            </a>
                                        </div>
                                        <!--/ col -->
                                          <!-- col -->
                                          <div class="col-md-4 colcatPage col-6">
                                            <a href="javascript:void(0)">
                                                <h6>Mettl</h6>
                                                <p>Lorem ipsum sit amet consectetur.</p>
                                            </a>
                                        </div>
                                        <!--/ col -->
                                          <!-- col -->
                                          <div class="col-md-4 colcatPage col-6">
                                            <a href="javascript:void(0)">
                                                <h6>Mind Tree</h6>
                                                <p>Lorem ipsum sit amet consectetur.</p>
                                            </a>
                                        </div>
                                        <!--/ col -->
                                          <!-- col -->
                                          <div class="col-md-4 colcatPage col-6">
                                            <a href="javascript:void(0)">
                                                <h6>Persistent</h6>
                                                <p>Lorem ipsum sit amet consectetur.</p>
                                            </a>
                                        </div>
                                        <!--/ col -->
                                          <!-- col -->
                                          <div class="col-md-4 colcatPage col-6">
                                            <a href="javascript:void(0)">
                                                <h6>TCS Digital</h6>
                                                <p>Lorem ipsum sit amet consectetur.</p>
                                            </a>
                                        </div>
                                        <!--/ col -->
                                          <!-- col -->
                                          <div class="col-md-4 colcatPage col-6">
                                            <a href="javascript:void(0)">
                                                <h6>TCS NQT</h6>
                                                <p>Lorem ipsum sit amet consectetur.</p>
                                            </a>
                                        </div>
                                        <!--/ col -->
                                          <!-- col -->
                                          <div class="col-md-4 colcatPage col-6">
                                            <a href="javascript:void(0)">
                                                <h6>TCS iON CCQT</h6>
                                                <p>Lorem ipsum sit amet consectetur.</p>
                                            </a>
                                        </div>
                                        <!--/ col -->
                                          <!-- col -->
                                          <div class="col-md-4 colcatPage col-6">
                                            <a href="javascript:void(0)">
                                                <h6>Cognizant</h6>
                                                <p>Lorem ipsum sit amet consectetur.</p>
                                            </a>
                                        </div>
                                        <!--/ col -->
                                          <!-- col -->
                                          <div class="col-md-4 colcatPage col-6">
                                            <a href="javascript:void(0)">
                                                <h6>Accenture</h6>
                                                <p>Lorem ipsum sit amet consectetur.</p>
                                            </a>
                                        </div>
                                        <!--/ col -->
                                          <!-- col -->
                                          <div class="col-md-4 colcatPage col-6">
                                            <a href="javascript:void(0)">
                                                <h6>ZS Associates</h6>
                                                <p>Lorem ipsum sit amet consectetur.</p>
                                            </a>
                                        </div>
                                        <!--/ col -->
                                          <!-- col -->
                                          <div class="col-md-4 colcatPage col-6">
                                            <a href="javascript:void(0)">
                                                <h6>Hexaware</h6>
                                                <p>Lorem ipsum sit amet consectetur.</p>
                                            </a>
                                        </div>
                                        <!--/ col -->
                                          <!-- col -->
                                          <div class="col-md-4 colcatPage col-6">
                                            <a href="javascript:void(0)">
                                                <h6>TCS</h6>
                                                <p>Lorem ipsum sit amet consectetur.</p>
                                            </a>
                                        </div>
                                        <!--/ col -->
                                    </div>
                                    <!--/ row -->
                            </div>
                            <div class="tab-pane fade" id="tab02" role="tabpanel">
                                 <!-- row -->
                                 <div class="row">
                                       
                                       <!-- col -->
                                       <div class="col-md-4 colcatPage col-6">
                                         <a href="javascript:void(0)">
                                             <h6>TCS Digital</h6>
                                             <p>Lorem ipsum sit amet consectetur.</p>
                                         </a>
                                     </div>
                                     <!--/ col -->
                                       <!-- col -->
                                       <div class="col-md-4 colcatPage col-6">
                                         <a href="javascript:void(0)">
                                             <h6>TCS NQT</h6>
                                             <p>Lorem ipsum sit amet consectetur.</p>
                                         </a>
                                     </div>
                                     <!--/ col -->
                                       <!-- col -->
                                       <div class="col-md-4 colcatPage col-6">
                                         <a href="javascript:void(0)">
                                             <h6>TCS iON CCQT</h6>
                                             <p>Lorem ipsum sit amet consectetur.</p>
                                         </a>
                                     </div>
                                     <!--/ col -->
                                       <!-- col -->
                                       <div class="col-md-4 colcatPage col-6">
                                         <a href="javascript:void(0)">
                                             <h6>Cognizant</h6>
                                             <p>Lorem ipsum sit amet consectetur.</p>
                                         </a>
                                     </div>
                                     <!--/ col -->
                                       <!-- col -->
                                       <div class="col-md-4 colcatPage col-6">
                                         <a href="javascript:void(0)">
                                             <h6>Accenture</h6>
                                             <p>Lorem ipsum sit amet consectetur.</p>
                                         </a>
                                     </div>
                                     <!--/ col -->
                                       <!-- col -->
                                       <div class="col-md-4 colcatPage col-6">
                                         <a href="javascript:void(0)">
                                             <h6>ZS Associates</h6>
                                             <p>Lorem ipsum sit amet consectetur.</p>
                                         </a>
                                     </div>
                                     <!--/ col -->
                                       <!-- col -->
                                       <div class="col-md-4 colcatPage col-6">
                                         <a href="javascript:void(0)">
                                             <h6>Hexaware</h6>
                                             <p>Lorem ipsum sit amet consectetur.</p>
                                         </a>
                                     </div>
                                     <!--/ col -->
                                       <!-- col -->
                                       <div class="col-md-4 colcatPage col-6">
                                         <a href="javascript:void(0)">
                                             <h6>TCS</h6>
                                             <p>Lorem ipsum sit amet consectetur.</p>
                                         </a>
                                     </div>
                                     <!--/ col -->
                                 </div>
                                 <!--/ row -->
                            </div>
                            <div class="tab-pane fade" id="tab03" role="tabpanel">
                                <!-- row -->
                                <div class="row">
                                      
                                      <!-- col -->
                                      <div class="col-md-4 colcatPage col-6">
                                         <a href="javascript:void(0)">
                                             <h6>Capgemini</h6>
                                             <p>Lorem ipsum sit amet consectetur.</p>
                                         </a>
                                     </div>
                                     <!--/ col -->
                                       <!-- col -->
                                       <div class="col-md-4 colcatPage col-6">
                                         <a href="javascript:void(0)">
                                             <h6>CoCubes</h6>
                                             <p>Lorem ipsum sit amet consectetur.</p>
                                         </a>
                                     </div>
                                     <!--/ col -->
                                       <!-- col -->
                                       <div class="col-md-4 colcatPage col-6">
                                         <a href="javascript:void(0)">
                                             <h6>Tech Mahindra</h6>
                                             <p>Lorem ipsum sit amet consectetur.</p>
                                         </a>
                                     </div>
                                     <!--/ col -->
                                       <!-- col -->
                                       <div class="col-md-4 colcatPage col-6">
                                         <a href="javascript:void(0)">
                                             <h6>Deloitte</h6>
                                             <p>Lorem ipsum sit amet consectetur.</p>
                                         </a>
                                     </div>
                                     <!--/ col -->
                                       <!-- col -->
                                       <div class="col-md-4 colcatPage col-6">
                                         <a href="javascript:void(0)">
                                             <h6>IBM</h6>
                                             <p>Lorem ipsum sit amet consectetur.</p>
                                         </a>
                                     </div>
                                     <!--/ col -->
                                       <!-- col -->
                                       <div class="col-md-4 colcatPage col-6">
                                         <a href="javascript:void(0)">
                                             <h6>Infosys</h6>
                                             <p>Lorem ipsum sit amet consectetur.</p>
                                         </a>
                                     </div>
                                     <!--/ col -->
                                       <!-- col -->
                                       <div class="col-md-4 colcatPage col-6">
                                         <a href="javascript:void(0)">
                                             <h6>Mettl</h6>
                                             <p>Lorem ipsum sit amet consectetur.</p>
                                         </a>
                                     </div>
                                     <!--/ col -->                                         
                                       <!-- col -->
                                       <div class="col-md-4 colcatPage col-6">
                                         <a href="javascript:void(0)">
                                             <h6>ZS Associates</h6>
                                             <p>Lorem ipsum sit amet consectetur.</p>
                                         </a>
                                     </div>
                                     <!--/ col -->
                                       <!-- col -->
                                       <div class="col-md-4 colcatPage col-6">
                                         <a href="javascript:void(0)">
                                             <h6>Hexaware</h6>
                                             <p>Lorem ipsum sit amet consectetur.</p>
                                         </a>
                                     </div>
                                     <!--/ col -->
                                       <!-- col -->
                                       <div class="col-md-4 colcatPage col-6">
                                         <a href="javascript:void(0)">
                                             <h6>TCS</h6>
                                             <p>Lorem ipsum sit amet consectetur.</p>
                                         </a>
                                     </div>
                                     <!--/ col -->
                                 </div>
                                 <!--/ row -->
                            </div>
                            <div class="tab-pane fade" id="tab04" role="tabpanel">
                                 <!-- row -->
                                 <div class="row">
                                        <!-- col -->
                                        <div class="col-md-4 colcatPage col-6">
                                            <a href="javascript:void(0)">
                                                <h6>Wipro NLTH</h6>
                                                <p>Lorem ipsum dolor sit amet consectetur.</p>
                                            </a>
                                        </div>
                                        <!--/ col -->
                                         <!-- col -->
                                         <div class="col-md-4 colcatPage col-6">
                                            <a href="javascript:void(0)">
                                                <h6>Wipro</h6>
                                                <p>Lorem ipsum dolor sit  consectetur.</p>
                                            </a>
                                        </div>
                                        <!--/ col -->                                        
                                    </div>
                                    <!--/ row -->
                            </div>
                        </div>                  

                    </div>
                    <!--/ left tab -->
           </div>
           <!--/ container -->
        </div>
        <!--/ sub page body -->
    </main>

    <!--/ sub page main -->
    <!--footer -->
    <?php include 'footer.php' ?>
    <!--/ footer -->  
    <?php include 'footerscripts.php'?>
</body>

</html>