<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Elitmus Questions and Answers</title>
    <link rel="icon" type="image/png" sizes="32x32" href="img/favicon-32x32.png">
    <!-- style sheets -->
    <?php include 'headerstyles.php' ?>
</head>

<body>
    <!-- header -->
   <?php include 'header.php' ?>
    <!--/ header -->
    <!--sub page main -->
    <main class="subpage">
        <!-- sub page header -->
        <div class="page-header">
            <!-- container -->
            <div class="container">
               <!-- row -->
               <div class="row">
                   <div class="col-lg-6">
                       <h1>Elitmus <span class="fbold">Q &amp; A</span> </h1>
                   </div>
               </div>
               <!--/ row --> 
            </div>
            <!--/ container -->
        </div>
        <!--/ sub page header -->

         <!-- brudcrumb -->
         <div class="container">
            <!-- row -->
            <div class="row">
                <!-- col -->
                <div class="col-lg-12">                   
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                        <li class="breadcrumb-item"><a href="elitmus-detail.php">Elitmus</a></li>
                        <li class="breadcrumb-item active"><a>Geometry</a></li>
                    </ul>                    
                </div>
                <!--/col -->
            </div>
            <!--/ row -->
        </div>
        <!--/ brudcrumb -->

        <!-- sub page body -->
        <div class="subpage-body">
           <!-- container -->
           <div class="container">
              <!-- row -->
              <div class="row qanda-page">               
                <!-- col 4 -->
                <div class="col-lg-4"> 
                    <!-- navigation -->
                    <nav class="navbar navbar-expand-lg">                           
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#answersnav" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>

                        <div class="collapse navbar-collapse" id="answersnav">
                            <ul class="navbar">
                                <li class="nav-item ">
                                    <a class="nav-link active" href="#">GEOMETRY </span></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#">PERMUTATION & COMBINATION  </span></a>
                                </li>  
                                <li class="nav-item">
                                    <a class="nav-link" href="#">NUMBER SYSTEM </span></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#">TIME, SPEED & DISTANCE </span></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#">CRYPTARITHMETIC </span></a>
                                </li>    
                            </ul>                                
                        </div>
                    </nav>
                    <!--/ navigation -->  
                    </div>
                    <!--/ col 4-->

                    <!-- col 8-->
                    <div class="col-lg-8">
                    
                    <!-- questions and answrs -->
                    <!-- accordian -->
                    <div class="accordion">
                        <!-- qa row -->
                        <div class="col-qa">
                            <!-- problem -->
                            <div class="problem">
                                <div class="question">
                                    <p class="pb-0"><span class="fblue fbold">1. </span>A square was given. Inside the square there are white tiles and black tiles. Black tiles was among the diagonal of the square and dimensions of both white as well as black tiles is 1 cm x 1 cm. If there are 81 black tiles in the square. Then find the no of white tiles in it.</p>
                                </div>
                                <div class="options">
                                    <ul class="nav">
                                        <li class="nav-item"><span class="text-uppercase fbold">A.</span>1500</li>
                                        <li class="nav-item"><span class="text-uppercase fbold">B.</span>1600</li>
                                        <li class="nav-item"><span class="text-uppercase fbold">C.</span>1681</li>
                                        <li class="nav-item"><span class="text-uppercase fbold">D.</span>1900</li>
                                    </ul>
                                    <a class="fblue fbold text-uppercase view-answer panel-title" href="javascript:void(0)">View Answer</a>
                                    <!-- answer -->
                                    <div class="answer panel-content">
                                        <!-- row -->
                                        <div class="answer-heading d-flex justify-content-between">
                                            <h4 class="h5">Explanation</h4>                               
                                            <h4 class="h5">Answer: <span class="anser-value d-inline-block text-center">B</span></h4> 
                                        </div>
                                        <!--/ row -->
                                        <!-- row -->
                                        <div class="pt-3">
                                            <div class="describe">
                                                <p>In a square, number of squares on the diagonal is equal to the tiles on a single row. 
                                                    If there are even number of square on a side, then total squares on the diagonal is 2n - 1, otherwise 2n.</p>
                                                <p>As the total tiles on the diagonal are given as 81,</p>
                                                <p>then number of tiles on a side = 2n -1 = 81 so n = 41.</p>
                                                <p>So number of white tiles = 41<sup>2</sup> – 81 = 1681 – 81 = 1600</p>
                                            </div>
                                        </div>
                                        <!--/ row -->
                                    </div>
                                    <!--/ answer -->
                                </div>
                            </div>
                            <!--/ problem -->                      
                        </div>
                        <!--/ qa row -->                  


                         <!-- qa row -->
                         <div class="col-qa">
                            <!-- problem -->
                            <div class="problem">
                                <div class="question">
                                    <p class="pb-0"><span class="fblue fbold">2. </span>A largest cone is formed at the base of cube of side measuring 7 cm. What is the ratio of volume of cone to volume of cube? </p>
                                </div>
                                <!-- option -->
                                <div class="options">
                                    <ul class="nav">
                                        <li class="nav-item"><span class="text-uppercase fbold">A.</span>1936 m<sup>2</sup> </li>
                                        <li class="nav-item"><span class="text-uppercase fbold">B.</span>616m<sup>2</sup> </li>
                                        <li class="nav-item"><span class="text-uppercase fbold">C.</span>281 m<sup>2</sup> </li>
                                        <li class="nav-item"><span class="text-uppercase fbold">D.</span>Data not sufficient</li>
                                    </ul>
                                    <a class="fblue fbold text-uppercase view-answer panel-title" href="javascript:void(0)">View Answer</a>
                                    <!-- answer -->
                                    <div class="answer panel-content">
                                        <!-- row -->
                                        <div class="answer-heading d-flex justify-content-between">
                                            <h4 class="h5">Explanation</h4>                               
                                            <h4 class="h5">Answer: <span class="anser-value d-inline-block text-center">B</span></h4> 
                                        </div>
                                        <!--/ row -->
                                        <!-- row -->
                                        <div class="pt-3">
                                            <div class="describe">
                                                <p>In a square, number of squares on the diagonal is equal to the tiles on a single row. 
                                                    If there are even number of square on a side, then total squares on the diagonal is 2n - 1, otherwise 2n.</p>
                                                <p>As the total tiles on the diagonal are given as 81,</p>
                                                <p>then number of tiles on a side = 2n -1 = 81 so n = 41.</p>
                                                <p>So number of white tiles = 41<sup>2</sup> – 81 = 1681 – 81 = 1600</p>
                                            </div>
                                        </div>
                                        <!--/ row -->
                                    </div>
                                    <!--/ answer -->
                                </div>
                                <!--/ option-->
                            </div>
                            <!--/ problem -->                      
                        </div>
                        <!--/ qa row --> 

                         <!-- qa row -->
                         <div class="col-qa">
                            <!-- problem -->
                            <div class="problem">
                                <div class="question">
                                    <p class="pb-0"><span class="fblue fbold">3. </span>What is the maximum volume of a right circular cone, which can be cut from a solid cuboid having a dimensions 12 x 10 x 15? </p>
                                </div>
                                <!-- option -->
                                <div class="options">
                                    <ul class="nav">
                                        <li class="nav-item"><span class="text-uppercase fbold">A.</span>1.5 cm </li>
                                        <li class="nav-item"><span class="text-uppercase fbold">B.</span>2.6 cm</li>
                                        <li class="nav-item"><span class="text-uppercase fbold">C.</span>2.48 cm </li>
                                        <li class="nav-item"><span class="text-uppercase fbold">D.</span>2.7 cm</li>
                                    </ul>
                                    <a class="fblue fbold text-uppercase view-answer panel-title" href="javascript:void(0)">View Answer</a>
                                    <!-- answer -->
                                    <div class="answer panel-content">
                                        <!-- row -->
                                        <div class="answer-heading d-flex justify-content-between">
                                            <h4 class="h5">Explanation</h4>                               
                                            <h4 class="h5">Answer: <span class="anser-value d-inline-block text-center">B</span></h4> 
                                        </div>
                                        <!--/ row -->
                                        <!-- row -->
                                        <div class="pt-3">
                                            <div class="describe">
                                                <p>In a square, number of squares on the diagonal is equal to the tiles on a single row. 
                                                    If there are even number of square on a side, then total squares on the diagonal is 2n - 1, otherwise 2n.</p>
                                                <p>As the total tiles on the diagonal are given as 81,</p>
                                                <p>then number of tiles on a side = 2n -1 = 81 so n = 41.</p>
                                                <p>So number of white tiles = 41<sup>2</sup> – 81 = 1681 – 81 = 1600</p>
                                            </div>
                                        </div>
                                        <!--/ row -->
                                    </div>
                                    <!--/ answer -->
                                </div>
                                <!--/ option-->
                            </div>
                            <!--/ problem -->                      
                        </div>
                        <!--/ qa row -->  
                        
                        
                         <!-- qa row -->
                         <div class="col-qa">
                            <!-- problem -->
                            <div class="problem">
                                <div class="question">
                                    <p class="pb-0"><span class="fblue fbold">4. </span>Eden park is a cricket field while Jubilee Park is a football field in Mastnagar. In Mastnagar all cricket fields are circular and football fields are rectangular or square. Along the boundary of all the fields there are advertisement displays. In Mastnagar the length of the advertisement displays has to be the same across all playing fields. Area of the Jubilee park is 468 m2. The farthest distance between any two points in Jubilee park football field is 10√10 m. find the approximate area of Eden garden?                            </p>
                                </div>
                                <!-- option -->
                                <div class="options">
                                    <ul class="nav">
                                        <li class="nav-item"><span class="text-uppercase fbold">A.</span>1.5 cm </li>
                                        <li class="nav-item"><span class="text-uppercase fbold">B.</span>2.6 cm</li>
                                        <li class="nav-item"><span class="text-uppercase fbold">C.</span>2.48 cm </li>
                                        <li class="nav-item"><span class="text-uppercase fbold">D.</span>2.7 cm</li>
                                    </ul>
                                    <a class="fblue fbold text-uppercase view-answer panel-title" href="javascript:void(0)">View Answer</a>
                                    <!-- answer -->
                                    <div class="answer panel-content">
                                        <!-- row -->
                                        <div class="answer-heading d-flex justify-content-between">
                                            <h4 class="h5">Explanation</h4>                               
                                            <h4 class="h5">Answer: <span class="anser-value d-inline-block text-center">B</span></h4> 
                                        </div>
                                        <!--/ row -->
                                        <!-- row -->
                                        <div class="pt-3">
                                            <div class="describe">
                                                <p>In a square, number of squares on the diagonal is equal to the tiles on a single row. 
                                                    If there are even number of square on a side, then total squares on the diagonal is 2n - 1, otherwise 2n.</p>
                                                <p>As the total tiles on the diagonal are given as 81,</p>
                                                <p>then number of tiles on a side = 2n -1 = 81 so n = 41.</p>
                                                <p>So number of white tiles = 41<sup>2</sup> – 81 = 1681 – 81 = 1600</p>
                                            </div>
                                        </div>
                                        <!--/ row -->
                                    </div>
                                    <!--/ answer -->
                                </div>
                                <!--/ option-->
                            </div>
                            <!--/ problem -->                      
                        </div>
                        <!--/ qa row --> 
                        
                         <!-- qa row -->
                         <div class="col-qa">
                            <!-- problem -->
                            <div class="problem">
                                <div class="question">
                                    <p class="pb-0"><span class="fblue fbold">5. </span>From a circular sheet of radius of 30 cm, a sector of 10% area is removed. If the remaining part is used to make a conical surface, then the ratio of the radius and height of the cone is: </p>
                                </div>
                                <!-- option -->
                                <div class="options">
                                    <ul class="nav">
                                        <li class="nav-item"><span class="text-uppercase fbold">A.</span>1.5 cm </li>
                                        <li class="nav-item"><span class="text-uppercase fbold">B.</span>2.06 cm</li>
                                        <li class="nav-item"><span class="text-uppercase fbold">C.</span>2.48 cm </li>
                                        <li class="nav-item"><span class="text-uppercase fbold">D.</span>2.7 cm</li>
                                    </ul>
                                    <a class="fblue fbold text-uppercase view-answer panel-title" href="javascript:void(0)">View Answer</a>
                                    <!-- answer -->
                                    <div class="answer panel-content">
                                        <!-- row -->
                                        <div class="answer-heading d-flex justify-content-between">
                                            <h4 class="h5">Explanation</h4>                               
                                            <h4 class="h5">Answer: <span class="anser-value d-inline-block text-center">B</span></h4> 
                                        </div>
                                        <!--/ row -->
                                        <!-- row -->                                        
                                        <div class="pt-3">
                                            <div class="describe">
                                                <p>In a square, number of squares on the diagonal is equal to the tiles on a single row. 
                                                    If there are even number of square on a side, then total squares on the diagonal is 2n - 1, otherwise 2n.</p>
                                                <p>As the total tiles on the diagonal are given as 81,</p>
                                                <p>then number of tiles on a side = 2n -1 = 81 so n = 41.</p>
                                                <p>So number of white tiles = 41<sup>2</sup> – 81 = 1681 – 81 = 1600</p>
                                            </div>
                                        </div>
                                        <!--/ row -->
                                    </div>
                                    <!--/ answer -->
                                </div>
                                <!--/ option-->
                            </div>
                            <!--/ problem -->                      
                        </div>
                        <!--/ qa row -->  
                    </div>
                    <!--/ accordian -->
                    <!--/ questions and answers -->
                </div>
                <!--/ col 8 -->
              </div>
              <!--/ row -->
           </div>
           <!--/ container -->
        </div>
        <!--/ sub page body -->
    </main>
    <!--/ sub page main -->
    <!--footer -->
    <?php include 'footer.php' ?>
    <!--/ footer -->  
    <?php include 'footerscripts.php'?>
</body>

</html>