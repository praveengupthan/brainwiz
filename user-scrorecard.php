<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Brainwiz</title>
    <link rel="icon" type="image/png" sizes="32x32" href="img/favicon-32x32.png">
    <!-- style sheets -->
    <?php include 'headerstyles.php' ?>
</head>

<body>

    <!-- header -->
   <?php include 'header-postlogin.php' ?>
    <!--/ header -->
    <!--sub page main -->
    <main class="subpage">
        <!-- sub page header -->
        <div class="page-header">
            <!-- container -->
            <div class="container">
               <!-- row -->
               <div class="row">
                   <div class="col-lg-6">
                       <h1>Score <span class="fbold">Card</span></h1>
                   </div>
               </div>
               <!--/ row --> 
            </div>
            <!--/ container -->
        </div>
        <!--/ sub page header -->

        <!-- brudcrumb -->
        <div class="container">
            <!-- row -->
            <div class="row">
                <!-- col -->
                <div class="col-lg-12">                   
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                        <li class="breadcrumb-item"><a href="user-profile.php">Student Name Will be here</a></li>
                        <li class="breadcrumb-item active"><a>My Score Card</a></li>
                    </ul>                    
                </div>
                <!--/col -->
            </div>
            <!--/ row -->
        </div>
        <!--/ brudcrumb -->

        <!-- sub page body -->
        <div class="subpage-body">
           <!-- container -->
           <div class="container">
                <!-- row -->
                <div class="row">
                    <!-- left col 4-->
                    <div class="col-lg-4">
                       <?php include 'userleft-nav.php'?>
                    </div>
                    <!--/ left col 4-->

                    <!-- right container -->
                    <div class="col-lg-8">
                        <div class="right-usersection">
                            <!-- title -->
                            <div class="user-right-title">
                                <h3 class="h4 mb-0">My Score</h3>
                                <p><small>Your Completed Exams Reports</small></p>
                            </div>
                            <!--/ title -->

                            <!-- right user body -->
                            <div class="user-rightsection">
                                <!-- basic score -->
                                <div class="row border-bottom">
                                    <!-- col graph -->
                                    <div class="col-lg-8 text-center">
                                    <svg viewBox="0 0 64 64" class="pie">
                                        <circle r="25%" cx="50%" cy="50%" style="stroke-dasharray: 90.0 100">
                                        </circle> 
                                    </svg>

                                    <!-- color status-->
                                    <div class="color-percentages d-flex py-3">
                                        <div class="pie-block text-center">
                                            <span class="color-block balance-courses"></span>
                                            <span>Pending Courses - 90%</span>
                                        </div>
                                        <div class="pie-block text-center">
                                            <span class="color-block completed-courses"></span>
                                            <span>Completed Courses - 10%</span>
                                        </div>
                                    </div>
                                    <!--/ color status -->
                                    </div>  
                                    <!--/ col graph -->
                                    <!-- col -->
                                    <div class="col-lg-4 align-self-center">
                                        <!-- score scale -->
                                        <div class="scrore-scale">
                                            <p class="d-flex justify-content-between">
                                                <span>Verbal</span>
                                                <span>10%</span>
                                            </p>
                                            <div class="scale">
                                                <div style="width:10%" class="scale-in"></div>
                                            </div>
                                        </div>
                                        <!--/ score scale -->
                                         <!-- score scale -->
                                         <div class="scrore-scale">
                                            <p class="d-flex justify-content-between">
                                                <span>Verbal</span>
                                                <span>12%</span>
                                            </p>
                                            <div class="scale">
                                                <div style="width:12%" class="scale-in"></div>
                                            </div>
                                        </div>
                                        <!--/ score scale -->
                                         <!-- score scale -->
                                         <div class="scrore-scale">
                                            <p class="d-flex justify-content-between">
                                                <span>Arthimetic</span>
                                                <span>25%</span>
                                            </p>
                                            <div class="scale">
                                                <div style="width:25%" class="scale-in"></div>
                                            </div>
                                        </div>
                                        <!--/ score scale -->
                                    </div>
                                    <!--/ col -->
                                </div>
                                <!--/ basic score -->

                                <!-- row -->
                                <div class="row pt-3">
                                    <!-- col 12-->
                                    <div class="col-lg-12 ">
                                        <div class="form-group float-right">
                                            <select class="form-control">
                                                <option>Select Test</option>
                                                <option>Tech Mahindra Test 1</option>
                                                <option>Tech Mahindra Test 2</option>
                                                <option>Tech Mahindra Test 3</option>
                                                <option>Tech Mahindra Test 4</option>
                                            </select>
                                        </div>
                                    </div>
                                    <!--/ col 12 -->
                                    <!-- col 12 -->
                                    <div class="col-lg-12">
                                        <!-- table -->
                                        <table class="table table-responsive">
                                            <thead>
                                                <tr>
                                                    <th scope="col">Category</th>
                                                    <th scope="col">Total Questions </th>
                                                    <th scope="col">Attempted </th>                               
                                                    <th scope="col">Correct</th>
                                                    <th scope="col">Score</th>
                                                    <th scope="col">Percentage </th>    
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td scope="row">Verbal</td>
                                                    <td>25</td>
                                                    <td>1</td>  
                                                    <td>0</td>
                                                    <td>0</td>
                                                    <td>0%</td>                                      
                                                </tr>
                                                <tr>
                                                    <td scope="row">Arithmatic</td>
                                                    <td>25</td>
                                                    <td>1</td>  
                                                    <td>0</td>
                                                    <td>0</td>
                                                    <td>0%</td>                                      
                                                </tr>
                                                <tr>
                                                    <td scope="row">reasoning</td>
                                                    <td>25</td>
                                                    <td>1</td>  
                                                    <td>0</td>
                                                    <td>0</td>
                                                    <td>0%</td>                                      
                                                </tr>
                                                <tfoot>
                                                    <tr>
                                                        <td scope="row">Overall Score</td>
                                                        <td>75</td>
                                                        <td>7</td>  
                                                        <td>1</td>
                                                        <td>1</td>                                   
                                                        <td>1.33%</td>    
                                                    </tr>
                                                </tfoot>
                                            </tbody>
                                        </table>
                                        <!--/ table -->
                                    </div>
                                    <!--/ col 12 -->
                                </div>
                                <!--/ row -->
                            </div>
                            <!--/ right user body -->
                        </div>
                    </div>
                    <!--/ right container -->
                </div>
                <!--/ row -->
           </div>
           <!--/ container -->
        </div>
        <!--/ sub page body -->
    </main>
    <!--/ sub page main -->
    <!--footer -->
    <?php include 'footer.php' ?>
    <!--/ footer -->  
    <?php include 'footerscripts.php'?>

</body>

</html>