<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Brainwiz</title>
    <link rel="icon" type="image/png" sizes="32x32" href="img/favicon-32x32.png">
    <!-- style sheets -->
    <?php include 'headerstyles.php' ?>
</head>

<body>

    <!-- header -->
   <?php include 'header.php' ?>
    <!--/ header -->
    <!--sub page main -->
    <main class="subpage">
        <!-- sub page header -->
        <div class="page-header">
            <!-- container -->
            <div class="container">
               <!-- row -->
               <div class="row">
                   <div class="col-lg-6">
                       <h1>Contact <span class="fbold">Us</span></h1>
                   </div>
               </div>
               <!--/ row --> 
            </div>
            <!--/ container -->
        </div>
        <!--/ sub page header -->
         <!-- brudcrumb -->
         <div class="container">
            <!-- row -->
            <div class="row">
                <!-- col -->
                <div class="col-lg-12">                   
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                        <li class="breadcrumb-item active"><a>Contact</a></li>
                    </ul>                    
                </div>
                <!--/col -->
            </div>
            <!--/ row -->
        </div>
        <!--/ brudcrumb -->

        <!-- sub page body -->
        <div class="subpage-body">
           <!-- container -->
           <div class="container contactpage">
              <!-- row -->
              <div class="row">
                    <!-- col -->
                    <div class="col-lg-8">                      

                        <h2 class="h5 py-2">Drop us Message</h2>

                        <form class="contact-form">
                            <!-- row -->
                            <div class="row">
                                <!-- col -->
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Write Name</label>
                                        <input type="text" class="form-control">
                                    </div>
                                </div>
                                <!--/ col -->   

                                  <!-- col -->
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Phone Number</label>
                                        <input type="text" class="form-control">
                                    </div>
                                </div>
                                <!--/ col -->   
                            </div>
                            <!--/ row -->                            
                             <!-- row -->
                             <div class="row">                               
                                  <!-- col -->
                                  <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Select  Course</label>
                                        <select class="form-control">
                                            <option>Select Course</option>
                                            <option>Campus Recruitment</option>
                                            <option>C Programming</option>
                                            <option>Web Development</option>
                                        </select>
                                    </div>
                                </div>
                                <!--/ col -->
                            </div>
                            <!--/ row -->
                            <!-- row -->
                             <div class="row">
                                <!-- col -->
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label>Write Message</label>
                                        <textarea class="form-control" style="height:100px;"></textarea>
                                    </div>
                                </div>
                                <!--/ col -->                                 
                            </div>
                            <!--/ row -->
                            <!--row -->
                            <div class="row">
                                <div class="col-lg-12">
                                    <input type="submit" class="bluebtn" value="Submit Your Query">
                                </div>
                            </div>
                            <!--/ row -->
                        </form>
                    </div>
                    <!--/ col -->

                    <!-- contact address column-->
                    <div class="col-lg-4">
                        <h2 class="h5 py-2">Hyderabad</h2>
                        <img src="img/charminar.jpg" class="img-fluid" alt="">

                        <table class="table table-borderless mt-3 contacttable">
                            <tr>
                                <td><span class="icon-sent-mail icomoon"></span></td>
                                <td>
                                    <h6 class="h6">Email</h6>
                                    <p>info@gobrainwiz.com</p>
                                </td>
                            </tr>
                            <tr>
                                <td><span class="icon-telephone icomoon"></span></td>
                                <td>
                                    <h6 class="h6">Phone Number</h6>
                                    <p>+91 81421 23938</p>
                                </td>
                            </tr>
                            <tr>
                                <td><span class="icon-pin icomoon"></span></td>
                                <td>
                                    <h6 class="h6">Contact Address</h6>
                                    <p class="text-left"># 3rd Floor, Beside Reliance Fresh, Near Satyam, Theatre , Srinivasa Nagar, Ameerpet,  Hyderabad.</p>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <!--/ contact address column -->
              </div>
              <!--/ row -->
              <!-- map row -->
              <div class="row maprow">
                <div class="col-lg-12">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d30451.17976932182!2d78.448345!3d17.44068!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xc940fb93864b35d!2sBRAINWIZ_CRT%20Training%20Institute!5e0!3m2!1sen!2sin!4v1573179284981!5m2!1sen!2sin" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
                </div>
              </div>
              <!--/ map row -->
           </div>
           <!--/ container -->
        </div>
        <!--/ sub page body -->
    </main>

    <!--/ sub page main -->
    <!--footer -->
    <?php include 'footer.php' ?>
    <!--/ footer -->  

    <?php include 'footerscripts.php'?>

</body>

</html>