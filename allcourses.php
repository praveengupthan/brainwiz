<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Brainwiz</title>
    <link rel="icon" type="image/png" sizes="32x32" href="img/favicon-32x32.png">
    <!-- style sheets -->
    <?php include 'headerstyles.php' ?>
</head>

<body>

    <!-- header -->
   <?php include 'header.php' ?>
    <!--/ header -->
    <!--sub page main -->
    <main class="subpage">
        <!-- sub page header -->
        <div class="page-header">
            <!-- container -->
            <div class="container">
               <!-- row -->
               <div class="row">
                   <div class="col-lg-6">
                       <h1>Courses <span class="fbold">We Offered</span></h1>
                   </div>
               </div>
               <!--/ row --> 
            </div>
            <!--/ container -->
        </div>
        <!--/ sub page header -->

        <!-- brudcrumb -->
        <div class="container">
            <!-- row -->
            <div class="row">
                <!-- col -->
                <div class="col-lg-12">                   
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                        <li class="breadcrumb-item active"><a>All Courses</a></li>
                    </ul>                    
                </div>
                <!--/col -->
            </div>
            <!--/ row -->
        </div>
        <!--/ brudcrumb -->

        <!-- sub page body -->
        <div class="subpage-body">
           <!-- container -->
           <div class="container">
                <!-- row -->
                <div class="row justify-content-center">
                   <div class="col-lg-6 text-center">
                       <h2 class="h2 fblue fbold">All Courses </h2>
                       <p class="text-center">Widely Researched, Cherry Picked and Industry Approved Courses</p>                       
                   </div>
               </div>
               <!--/ row --> 
               <!-- row -->
               <div class="row py-3">
                <!-- col -->
                <div class="col-lg-4">
                    <div class="course-col">                       
                        <a href="crt-detail.php"><img src="img/crtcourseimg.svg" alt="" class="img-fluid "></a>
                        <article class="p-3 text-center">
                            <h3><a href="crt-detail.php">Campus Recruitment Training </a></h3>
                            <p  class="pb-3 text-center">Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptate   </p>
                            <div class="d-flex justify-content-between">
                                <h5 class="h6 fblue">Faculty Name</h5>
                                <h6 class="h6 fblue"> <a href="#"><span class="icon-download icomoon"></span> Curriculum</a></h6>
                            </div>
                        </article>
                    </div>
                </div>
                <!--/ col -->

                  <!-- col -->
                  <div class="col-lg-4">
                    <div class="course-col">                       
                        <a href="amcat-detail.php"><img src="img/amcat-courseimg.svg" alt="" class="img-fluid "></a>
                        <article class="p-3 text-center">
                            <h3><a href="amcat-detail.php">Amcat </a></h3>
                            <p  class="pb-3 text-center">Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptate   </p>
                            <div class="d-flex justify-content-between">
                            <h5 class="h6 fblue">Faculty Name</h5>
                            <h6 class="h6 fblue"> <a href="#"><span class="icon-download icomoon"></span> Curriculum</a></h6>                      
                            </div>
                        </article>
                    </div>
                </div>
                <!--/ col -->

                 <!-- col -->
                 <div class="col-lg-4">
                    <div class="course-col">                       
                        <a href="javascript:void(0)"><img src="img/elitmus-courseimg.svg" alt="" class="img-fluid "></a>
                        <article class="p-3 text-center">
                            <h3><a href="javascript:void(0)">E-Litmus </a></h3>
                            <p  class="pb-3 text-center">Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptate   </p>
                            <div class="d-flex justify-content-between">
                                <h5 class="h6 fblue">Faculty Name</h5>
                                <h6 class="h6 fblue"> <a href="#"><span class="icon-download icomoon"></span> Curriculum</a></h6>
                            </div>
                        </article>
                    </div>
                </div>
                <!--/ col -->

                 <!-- col -->
                 <div class="col-lg-4">
                    <div class="course-col">                       
                        <a href="cocubes-detail.php"><img src="img/cocubescourseimg.svg" alt="" class="img-fluid "></a>
                        <article class="p-3 text-center">
                            <h3><a href="cocubes-detail.php">CoCubes </a></h3>
                            <p  class="pb-3 text-center">Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptate   </p>
                            <div class="d-flex justify-content-between">
                                <h5 class="h6 fblue">Faculty Name</h5>
                                <h6 class="h6 fblue"> <a href="#"><span class="icon-download icomoon"></span> Curriculum</a></h6>
                            </div>
                        </article>
                    </div>
                </div>
                <!--/ col -->

                <!-- col -->
                <div class="col-lg-4">
                    <div class="course-col">                       
                        <a href="javascript:void(0)"><img src="img/cprogramming.svg" alt="" class="img-fluid "></a>
                        <article class="p-3 text-center">
                            <h3><a href="javascript:void(0)">C Language Programming</a></h3>
                            <p  class="pb-3 text-center">Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptate   </p>
                            <div class="d-flex justify-content-between">
                                <h5 class="h6 fblue">Faculty Name</h5>
                                <h6 class="h6 fblue"> <a href="#"><span class="icon-download icomoon"></span> Curriculum</a></h6>
                            </div>
                        </article>
                    </div>
                </div>
                <!--/ col -->

               </div>
               <!--/ row -->
           </div>
            <!--/ container -->
        </div>
        <!--/ sub page body -->
    </main>
    <!--/ sub page main -->
    <!--footer -->
    <?php include 'footer.php' ?>
    <!--/ footer -->  
    <?php include 'footerscripts.php'?>

</body>

</html>