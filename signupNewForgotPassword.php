<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Brainwiz</title>
    <link rel="icon" type="image/png" sizes="32x32" href="img/favicon-32x32.png">
    <!-- style sheets -->
    <?php include 'headerstyles.php' ?>
</head>

<body class="signupBody">
    <!-- sign in -->
    <div class="signUpNew">
        <!-- container fluid -->
        <div class="containerSign">
            <!-- row -->
           <div class="row">
               <!-- col -->
               <div class="col-md-6 px-0">
                   <img src="img/signupimg.jpg" alt="" class="img-fluid signleftimg">
               </div>
               <!--/ row -->
               <!-- col -->
               <div class="col-md-6 px-0 align-self-center">
                   <div class="NewSignupRight">
                   <!-- registration form -->
                   <div class="registrationForm">
                       <h2 class="text-uppercase">FORGOT PASSWORD?</h2>
                      <!-- form -->
                      <form>
                        <!-- row -->
                       <div class="row">
                           <!-- col -->
                           <div class="col-md-12">
                               <div class="form-group">
                                   <label>Enter Email ID</label>
                                   <div class="input-group">
                                       <input type="text" class="form-control">
                                   </div>
                               </div>
                           </div>
                           <!--/ col -->
                                            

                       </div>
                       <!--/ row -->
                       <!-- row -->
                       <div class="row pt-3">
                           <!-- col -->
                           <div class="col-md-6">
                               <input type="submit" value="Cancel" class="bluebtn">
                               <input type="submit" value="Send Rest Link" class="bluebtn">
                           </div>
                           <!--/ col -->

                            <!-- col -->
                            <div class="col-md-12 pt-4">
                                    <p>Already Registered User? <a class="fblue" href="signupNewLogin.php">Click here</a></p>
                              </div>
                              <!--/ col -->   
                       </div>
                       <!--/ row -->
                       
                      </form>
                      <!--/ form -->
                   </div>
                   <!--/ registrationform -->
                   </div>
               </div>
               <!--/ row -->
           </div>
           <!--/ row -->
        </div>
        <!--/ container fluid -->

    </div>
    <!--/ sign in --> 
    <?php include 'footerscripts.php'?>

</body>

</html>