<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Brainwiz Practise Test Solutions</title>
    <link rel="icon" type="image/png" sizes="32x32" href="img/favicon-32x32.png">
    <!-- style sheets -->
    <?php include 'headerstyles.php' ?>
</head>

<body>

    <!-- header -->
   <?php include 'header.php' ?>
    <!--/ header -->
    <!--sub page main -->
    <main class="subpage">
        <!-- sub page header -->
        <div class="page-header">
            <!-- container -->
            <div class="container">
               <!-- row -->
               <div class="row">
                   <div class="col-lg-6">
                       <h1>Tech Mahindra Test 1 </h1>
                       <p><a href="javascript:void(0)"><span class="icon-arrow-left icomoon"></span> Back to Practise Tests</a></p>
                   </div>
               </div>
               <!--/ row --> 
            </div>
            <!--/ container -->
        </div>
        <!--/ sub page header -->

         <!-- brudcrumb -->
         <div class="container">
            <!-- row -->
            <div class="row">
                <!-- col -->
                <div class="col-lg-12">                   
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.php">Home</a></li>   
                        <li class="breadcrumb-item"><a href="practise-test.php">Practise Tests</a></li>                     
                        <li class="breadcrumb-item active"><a>Tech Mahindra Test 1</a></li>
                    </ul>                    
                </div>
                <!--/col -->
            </div>
            <!--/ row -->
        </div>
        <!--/ brudcrumb -->
        <!-- sub page body -->
        <div class="subpage-body">
           <!-- container -->
           <div class="container">
              <!-- row -->
              <div class="row">
               <!-- col 12 -->
                <div class="col-lg-12">
                    <!-- tab -->
                    <div class="parentHorizontalTab simpletabs">
                        <ul class="resp-tabs-list hor_1">
                            <li>Verbal</li>
                            <li>Arithmetic</li>
                            <li>Reasoning</li>
                        </ul>
                        <!-- responsvie container -->
                        <div class="resp-tabs-container hor_1">

                             <!-- Verbal -->
                             <div class="show-solution">                              
                                <!-- question and answer -->
                                <div class="solution">
                                    <div class="quest">
                                        <p><span>1.</span> At this stage of civilisation, when many nations are brought in to close and vital contact for good and evil, it is essential, as never before, that their gross ignorance of one another should be diminished, that they should begin to understand a little of one another's historical experience and resulting mentality. It is the fault of the English to expect the people of other countries to react as they do, to political and international situations. Our genuine goodwill and good intentions are often brought to nothing, because we expect other people to be like us. This would be corrected if we knew the history, not necessarily in detail but in broad outlines, of the social and political conditions which have given to each nation its present character.</p>
                                        <p>According to the author of 'Mentality' of a nation is mainly product of its</p>
                                    </div>

                                    <ul class="nav">
                                        <li class="nav-item">
                                            <span>A</span> History <small>Correct Answer</small>
                                        </li>
                                        <li class="nav-item">
                                            <span>B</span> International position 
                                        </li>
                                        <li class="nav-item">
                                            <span>C</span> Politics
                                        </li>
                                        <li class="nav-item">
                                            <span>D</span> Personal character
                                        </li>
                                    </ul>
                                </div>
                                <!--/ question and answer -->       
                                
                                 <!-- question and answer -->
                                 <div class="solution">
                                    <div class="quest">
                                        <p><span>2.</span> At this stage of civilisation, when many nations are brought in to close and vital contact for good and evil, it is essential, as never before, that their gross ignorance of one another should be diminished, that they should begin to understand a little of one another's historical experience and resulting mentality. It is the fault of the English to expect the people of other countries to react as they do, to political and international situations. Our genuine goodwill and good intentions are often brought to nothing, because we expect other people to be like us. This would be corrected if we knew the history, not necessarily in detail but in broad outlines, of the social and political conditions which have given to each nation its present character.</p>
                                        <p>The need for a greater understanding between nations</p>
                                    </div>

                                    <ul class="nav">
                                        <li class="nav-item">
                                            <span>A</span> was always there 
                                        </li>
                                        <li class="nav-item">
                                            <span>B</span> is no longer there
                                        </li>
                                        <li class="nav-item">
                                            <span>C</span> is more today than ever before
                                        </li>
                                        <li class="nav-item">
                                            <span>D</span> will always be there <small>Correct Answer</small>
                                        </li>
                                        <li class="nav-item">
                                            <span>D</span> None of these
                                        </li>
                                    </ul>
                                </div>
                                <!--/ question and answer -->    
                                
                                 <!-- question and answer -->
                                 <div class="solution">
                                    <div class="quest">
                                        <p><span>3.</span> At this stage of civilisation, when many nations are brought in to close and vital contact for good and evil, it is essential, as never before, that their gross ignorance of one another should be diminished, that they should begin to understand a little of one another's historical experience and resulting mentality. It is the fault of the English to expect the people of other countries to react as they do, to political and international situations. Our genuine goodwill and good intentions are often brought to nothing, because we expect other people to be like us. This would be corrected if we knew the history, not necessarily in detail but in broad outlines, of the social and political conditions which have given to each nation its present character.</p>
                                        <p>Until the end of first year, Cyril retained his interest in</p>
                                    </div>

                                    <ul class="nav">
                                        <li class="nav-item">
                                            <span>A</span> bird's eggs
                                        </li>
                                        <li class="nav-item">
                                            <span>B</span> butterflies and fossils <small>Correct Answer</small>
                                        </li>
                                        <li class="nav-item">
                                            <span>C</span> dissecting bird's
                                        </li>
                                        <li class="nav-item">
                                            <span>D</span> drawing pictures of athletes 
                                        </li>
                                        <li class="nav-item">
                                            <span>D</span> None of these
                                        </li>
                                    </ul>
                                </div>
                                <!--/ question and answer --> 

                                 <!-- question and answer -->
                                 <div class="solution">
                                    <div class="quest">
                                        <p><span>4.</span> At this stage of civilisation, when many nations are brought in to close and vital contact for good and evil, it is essential, as never before, that their gross ignorance of one another should be diminished, that they should begin to understand a little of one another's historical experience and resulting mentality. It is the fault of the English to expect the people of other countries to react as they do, to political and international situations. Our genuine goodwill and good intentions are often brought to nothing, because we expect other people to be like us. This would be corrected if we knew the history, not necessarily in detail but in broad outlines, of the social and political conditions which have given to each nation its present character.</p>
                                        <p>Cyril did not want to climb trees because he</p>
                                    </div>

                                    <ul class="nav">
                                        <li class="nav-item">
                                            <span>A</span> loved to play on the ground
                                        </li>
                                        <li class="nav-item">
                                            <span>B</span> was scared of falling down <small>Correct Answer</small>
                                        </li>
                                        <li class="nav-item">
                                            <span>C</span> disliked trees
                                        </li>
                                        <li class="nav-item">
                                            <span>D</span> thought it was childish
                                        </li>
                                        <li class="nav-item">
                                            <span>D</span> None of these
                                        </li>
                                    </ul>
                                </div>
                                <!--/ question and answer --> 

                                 <!-- question and answer -->
                                 <div class="solution">
                                    <div class="quest">
                                        <p><span>5.</span> At this stage of civilisation, when many nations are brought in to close and vital contact for good and evil, it is essential, as never before, that their gross ignorance of one another should be diminished, that they should begin to understand a little of one another's historical experience and resulting mentality. It is the fault of the English to expect the people of other countries to react as they do, to political and international situations. Our genuine goodwill and good intentions are often brought to nothing, because we expect other people to be like us. This would be corrected if we knew the history, not necessarily in detail but in broad outlines, of the social and political conditions which have given to each nation its present character.</p>
                                        <p>Cyril's early schooling was in some ways like home life because</p>
                                    </div>

                                    <ul class="nav">
                                        <li class="nav-item">
                                            <span>A</span> he had all his old friends with him
                                        </li>
                                        <li class="nav-item">
                                            <span>B</span> the food and the climate were same as at home 
                                        </li>
                                        <li class="nav-item">
                                            <span>C</span> he kept an doing what gave joy and recreation at home <small>Correct Answer</small>
                                        </li>
                                        <li class="nav-item">
                                            <span>D</span> his family visited him often
                                        </li>
                                        <li class="nav-item">
                                            <span>D</span> None of these
                                        </li>
                                    </ul>
                                </div>
                                <!--/ question and answer --> 

                            </div>
                            <!--/ Verbal -->

                             <!-- Arithmetic -->
                             <div class="show-solution">                              
                                 <!-- question and answer -->
                                 <div class="solution">
                                    <div class="quest">
                                        <p><span>1.</span> In a row of trees, one tree is 12th from the either end of the row. How many tress are there in the row?</p>
                                    </div>

                                    <ul class="nav">
                                        <li class="nav-item">
                                            <span>A</span> 24
                                        </li>
                                        <li class="nav-item">
                                            <span>B</span> 25
                                        </li>
                                        <li class="nav-item">
                                            <span>C</span> 23<small>Correct Answer</small>
                                        </li>
                                        <li class="nav-item">
                                            <span>D</span> 20
                                        </li>                                        
                                    </ul>
                                </div>
                                <!--/ question and answer --> 
                                
                                <!-- question and answer -->
                                <div class="solution">
                                    <div class="quest">
                                        <p><span>2.</span>  An article is sold at a profit of 25%, if it was sold at twice of its selling price. What would be the profit percentage?</p>
                                    </div>

                                    <ul class="nav">
                                        <li class="nav-item">
                                            <span>A</span> 100%
                                        </li>
                                        <li class="nav-item">
                                            <span>B</span> 200% <small class="wrong-ans">Your Answer</small>
                                        </li>
                                        <li class="nav-item">
                                            <span >C</span> 150%
                                        </li>
                                        <li class="nav-item">
                                            <span>D</span> 250% <small class="correct.ans">Correct Answer</small>
                                        </li>                                        
                                    </ul>
                                </div>
                                <!--/ question and answer -->   
                                
                                 <!-- question and answer -->
                                 <div class="solution">
                                    <div class="quest">
                                        <p><span>3.</span>  Three hundred passengers are travelling in white, silver and black cars; each of these cars is carrying 6, 5 and 3 passengers, respectively. If the number of white and silver cars is equal and there is only one black car, what is the total number of cars?</p>
                                    </div>

                                    <ul class="nav">
                                        <li class="nav-item">
                                            <span>A</span> 52 <small class="correct-ans">Correct Answer</small>
                                        </li>
                                        <li class="nav-item">
                                            <span>B</span> 55 
                                        </li>
                                        <li class="nav-item">
                                            <span >C</span> 54
                                        </li>
                                        <li class="nav-item">
                                            <span>D</span> 53 <small class="wrong-ans">Your Answer</small>
                                        </li>                                        
                                    </ul>
                                </div>
                                <!--/ question and answer --> 

                                 <!-- question and answer -->
                                 <div class="solution">
                                    <div class="quest">
                                        <p><span>4.</span>  The  cost  price  of  two  types  of  tea  are  Rs.  180  per kg  and  Rs.  200 per  kg  respectively. On mixing  them  in  the  ratio  5:3, the  mixture is  sold  at  Rs. 210  per  kg .  In the whole transaction, the gain percent is?</p>
                                    </div>

                                    <ul class="nav">
                                        <li class="nav-item">
                                            <span>A</span> 10%  <small class="wrong-ans">Your Answer</small>
                                        </li>
                                        <li class="nav-item">
                                            <span>B</span> 11%
                                        </li>
                                        <li class="nav-item">
                                            <span>C</span> 12% <small class="correct-ans">Correct Answer</small>
                                        </li>
                                        <li class="nav-item">
                                            <span>D</span> 13% 
                                        </li>                                        
                                    </ul>
                                </div>
                                <!--/ question and answer --> 


                            </div>
                            <!--/ Arithmetic -->

                             <!-- Arithmetic -->
                             <div class="show-solution">                              
                                 <!-- question and answer -->
                                 <div class="solution">
                                    <div class="quest">
                                        <p><span>1.</span> In a certain code language if the word ‘MUSEUM’ is coded as ‘LSPAPG’, then how will the word ‘PALACE’ be coded in that language?</p>
                                    </div>

                                    <ul class="nav">
                                        <li class="nav-item">
                                            <span>A</span> OYIWXY
                                        </li>
                                        <li class="nav-item">
                                            <span>B</span> OYIXYW <small class="correct-ans">Correct Answer</small>
                                        </li>
                                        <li class="nav-item">
                                            <span>C</span> IYXYWO
                                        </li>
                                        <li class="nav-item">
                                            <span>D</span> YXWYOI      
                                        </li>                                        
                                    </ul>
                                </div>
                                <!--/ question and answer -->    
                                
                                 <!-- question and answer -->
                                 <div class="solution">
                                    <div class="quest">
                                        <p><span>2.</span>In a certain code language, if the number 1 is assigned to all the letters in odd numbered places in the alphabet and the remaining letters are assigned the number 2, than what is the code for the word ‘DANCE’?</p>
                                    </div>

                                    <ul class="nav">
                                        <li class="nav-item">
                                            <span>A</span> 21211 <small class="correct-ans">Correct Answer</small>
                                        </li>
                                        <li class="nav-item">
                                            <span>B</span> 12121 
                                        </li>
                                        <li class="nav-item">
                                            <span>C</span> 22111
                                        </li>
                                        <li class="nav-item">
                                            <span>D</span> 21121      
                                        </li>    
                                        <li class="nav-item">
                                            <span>D</span> None of these      
                                        </li>                                    
                                    </ul>
                                </div>
                                <!--/ question and answer --> 

                                 <!-- question and answer -->
                                 <div class="solution">
                                    <div class="quest">
                                        <p><span>3.</span>PZQW : NXOU :: FISK :?</p>
                                    </div>

                                    <ul class="nav">
                                        <li class="nav-item">
                                            <span>A</span> EFPJ 
                                        </li>
                                        <li class="nav-item">
                                            <span>B</span> FERI 
                                        </li>
                                        <li class="nav-item">
                                            <span>C</span> DGQI <small class="correct-ans">Correct Answer</small>
                                        </li>
                                        <li class="nav-item">
                                            <span>D</span> HKUM      
                                        </li>    
                                        <li class="nav-item">
                                            <span>D</span> None of these      
                                        </li>                                    
                                    </ul>
                                </div>
                                <!--/ question and answer --> 

                                <!-- question and answer -->
                                <div class="solution">
                                    <div class="quest">
                                        <p><span>4.</span>SUW : RST :: DFH :?</p>
                                    </div>

                                    <ul class="nav">
                                        <li class="nav-item">
                                            <span>A</span> DEF  <small class="correct-ans">Correct Answer</small>
                                        </li>
                                        <li class="nav-item">
                                            <span>B</span> FGH 
                                        </li>
                                        <li class="nav-item">
                                            <span>C</span> CDE 
                                        </li>
                                        <li class="nav-item">
                                            <span>D</span> GHI      
                                        </li>    
                                        <li class="nav-item">
                                            <span>D</span> None of these      
                                        </li>                                    
                                    </ul>
                                </div>
                                <!--/ question and answer --> 

                                <!-- question and answer -->
                                <div class="solution">
                                    <div class="quest">
                                        <p><span>5.</span>A is the husband of B. E is the daughter of C. A is the father of C. How is B related to E?</p>
                                    </div>

                                    <ul class="nav">
                                        <li class="nav-item">
                                            <span>A</span> mother  
                                        </li>
                                        <li class="nav-item">
                                            <span>B</span> grand mother  <small class="correct-ans">Correct Answer</small>
                                        </li>
                                        <li class="nav-item">
                                            <span>C</span> aunt 
                                        </li>
                                        <li class="nav-item">
                                            <span>D</span> cousin      
                                        </li>    
                                        <li class="nav-item">
                                            <span>D</span> None of these      
                                        </li>                                    
                                    </ul>
                                </div>
                                <!--/ question and answer --> 

                            </div>
                            <!--/ Arithmetic -->
                        </div>
                        <!-- responsive container -->
                    </div>
                    <!--/ taB -->
                </div>         
               <!--/ col 12-->
              </div>
              <!--/ row -->
           </div>
           <!--/ container -->
        </div>
        <!--/ sub page body -->
    </main>

    <!--/ sub page main -->
    <!--footer -->
    <?php include 'footer.php' ?>
    <!--/ footer -->  
    <?php include 'footerscripts.php'?>
</body>

</html>