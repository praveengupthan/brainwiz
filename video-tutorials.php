<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Brainwiz Video Tutorials</title>
    <link rel="icon" type="image/png" sizes="32x32" href="img/favicon-32x32.png">
    <!-- style sheets -->
    <?php include 'headerstyles.php' ?>
</head>

<body>

    <!-- header -->
   <?php include 'header.php' ?>
    <!--/ header -->
    <!--sub page main -->
    <main class="subpage">
        <!-- sub page header -->
        <div class="page-header d-none d-sm-block">
            <!-- container -->
            <div class="container">
               <!-- row -->
               <div class="row">
                   <div class="col-lg-6">
                       <h1>Video <span class="fbold">Tutorials</span> </h1>
                   </div>
               </div>
               <!--/ row --> 
            </div>
            <!--/ container -->
        </div>
        <!--/ sub page header -->

         <!-- brudcrumb -->
         <div class="container">
            <!-- row -->
            <div class="row">
                <!-- col -->
                <div class="col-lg-12">                   
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                        <li class="breadcrumb-item active"><a>Video Tutorials</a></li>
                    </ul>                    
                </div>
                <!--/col -->
            </div>
            <!--/ row -->
        </div>
        <!--/ brudcrumb -->

        <!-- sub page body -->
        <div class="subpage-body">
           <!-- container -->
           <div class="container">
              <!-- row -->
              <div class="row">
                <!-- col left -->
                <div class="col-lg-4 leftnav-tutorial">
                    <h2 class="h6 d-flex justify-content-between">
                        <span>Detailed Topics</span>
                        <a class="fwhite" id="videos-show-icon" href="javascript:void(0)"><span class="icon-list-ul icomoon"></span></a>
                    </h2>

                    <ul class="nav flex-column flex-nowrap leftnav" id="video-listitems">                       
                        <li class="nav-item"><a class="nav-link" href="#">Time Speed and Distance</a></li> 
                        <li class="nav-item"><a class="nav-link" href="#">Tiem & Work</a></li> 
                        <li class="nav-item"><a class="nav-link" href="#">Number System</a></li>
                        <li class="nav-item"><a class="nav-link" href="#">Cryptharithmetic</a></li>
                        <li class="nav-item"><a class="nav-link" href="#">Mensuration</a></li>
                        <li class="nav-item"><a class="nav-link" href="#">Cubes</a></li>
                        <li class="nav-item"><a class="nav-link" href="#">Percentages</a></li>
                        <li class="nav-item"><a class="nav-link" href="#">Data Suffiency</a></li>
                        <li class="nav-item"><a class="nav-link" href="#">Permutation and Combination</a></li>
                        <li class="nav-item"><a class="nav-link" href="#">Blood Relations & Direction Sense</a></li>
                        <li class="nav-item"><a class="nav-link" href="#">Rations, Partnership and Problem on Ages</a></li>
                    </ul>

                </div>
                <!--/ col left -->

                <!-- col right -->
                <div class="col-lg-8 right-videos">
                    <!-- title -->
                    <div class="right-title d-flex justify-content-between">
                        <h2 class="h6">Time Speed and Distance</h2>
                        <span>25 Videos</span>
                    </div>
                    <!--/ title -->
                    <!-- row -->
                    <div class="row">
                        <!-- col video-->
                        <div class="col-lg-6">
                            <div class="video-col whitebox" data-toggle="modal" data-target="#videopop">
                                <h6>
                                    <a class="d-block pb-2" href="javascript:void(0)">A chain rule based question</a>
                                </h6>
                                <a href="javascript:void(0)">
                                    <img src="img/data/vid01.jpg" alt="" class="img-fluid">
                                </a>
                                <p class="d-flex justify-content-between pt-2 pb-0 mb-0">
                                    <span class="fblue">Duration:00:50</span>
                                    <a class="fblue" href="javascript:void(0)">VIEW NOW</a>
                                </p>   
                                <span class="icon-youtube-play icomoon"></span>                         
                            </div>
                        </div>
                        <!--/ colvideo -->

                         <!-- col video-->
                         <div class="col-lg-6">
                            <div class="video-col whitebox">
                                <h6>
                                    <a class="d-block pb-2" href="javascript:void(0)">A chain rule based question</a>
                                </h6>
                                <a href="javascript:void(0)">
                                    <img src="img/data/vid02.png" alt="" class="img-fluid">
                                </a>
                                <p class="d-flex justify-content-between pt-2 pb-0 mb-0">
                                    <span class="fblue">Duration:00:50</span>
                                    <a class="fblue" href="javascript:void(0)">VIEW NOW</a>
                                </p>   
                                <span class="icon-youtube-play icomoon"></span>                                
                            </div>
                        </div>
                        <!--/ colvideo -->

                        <!-- col video-->
                        <div class="col-lg-6">
                            <div class="video-col whitebox">
                                <h6>
                                    <a class="d-block pb-2" href="javascript:void(0)">A chain rule based question</a>
                                </h6>
                                <a href="javascript:void(0)">
                                    <img src="img/data/vid03.png" alt="" class="img-fluid">
                                </a>
                                <p class="d-flex justify-content-between pt-2 pb-0 mb-0">
                                    <span class="fblue">Duration:00:50</span>
                                    <a class="fblue" href="javascript:void(0)">VIEW NOW</a>
                                </p>
                                <span class="icon-youtube-play icomoon"></span>                       
                            </div>
                        </div>
                        <!--/ colvideo -->

                        <!-- col video-->
                        <div class="col-lg-6">
                            <div class="video-col whitebox">
                                <h6>
                                    <a class="d-block pb-2" href="javascript:void(0)">A chain rule based question</a>
                                </h6>
                                <a href="javascript:void(0)">
                                    <img src="img/data/vid04.png" alt="" class="img-fluid">
                                </a>
                                <p class="d-flex justify-content-between pt-2 pb-0 mb-0">
                                    <span class="fblue">Duration:00:50</span>
                                    <a class="fblue" href="javascript:void(0)">VIEW NOW</a>
                                </p>
                                <span class="icon-youtube-play icomoon"></span>                                   
                            </div>
                        </div>
                        <!--/ colvideo -->

                         <!-- col video-->
                         <div class="col-lg-6">
                            <div class="video-col whitebox">
                                <h6>
                                    <a class="d-block pb-2" href="javascript:void(0)">A chain rule based question</a>
                                </h6>
                                <a href="javascript:void(0)">
                                    <img src="img/data/vid05.png" alt="" class="img-fluid">
                                </a>
                                <p class="d-flex justify-content-between pt-2 pb-0 mb-0">
                                    <span class="fblue">Duration:00:50</span>
                                    <a class="fblue" href="javascript:void(0)">VIEW NOW</a>
                                </p>
                                <span class="icon-youtube-play icomoon"></span>                                   
                            </div>
                        </div>
                        <!--/ colvideo -->

                         <!-- col video-->
                         <div class="col-lg-6">
                            <div class="video-col whitebox">
                                <h6>
                                    <a class="d-block pb-2" href="javascript:void(0)">A chain rule based question</a>
                                </h6>
                                <a href="javascript:void(0)">
                                    <img src="img/data/vid06.png" alt="" class="img-fluid">
                                </a>
                                <p class="d-flex justify-content-between pt-2 pb-0 mb-0">
                                    <span class="fblue">Duration:00:50</span>
                                    <a class="fblue" href="javascript:void(0)">VIEW NOW</a>
                                </p>
                                <span class="icon-youtube-play icomoon"></span>                                   
                            </div>
                        </div>
                        <!--/ colvideo -->

                         <!-- col video-->
                         <div class="col-lg-6">
                            <div class="video-col whitebox">
                                <h6>
                                    <a class="d-block pb-2" href="javascript:void(0)">A chain rule based question</a>
                                </h6>
                                <a href="javascript:void(0)">
                                    <img src="img/data/vid07.png" alt="" class="img-fluid">
                                </a>
                                <p class="d-flex justify-content-between pt-2 pb-0 mb-0">
                                    <span class="fblue">Duration:00:50</span>
                                    <a class="fblue" href="javascript:void(0)">VIEW NOW</a>
                                </p>
                                <span class="icon-youtube-play icomoon"></span>                            
                            </div>
                        </div>
                        <!--/ colvideo -->

                         <!-- col video-->
                         <div class="col-lg-6">
                            <div class="video-col whitebox">
                                <h6>
                                    <a class="d-block pb-2" href="javascript:void(0)">A chain rule based question</a>
                                </h6>
                                <a href="javascript:void(0)">
                                    <img src="img/data/vid08.png" alt="" class="img-fluid">
                                </a>
                                <p class="d-flex justify-content-between pt-2 pb-0 mb-0">
                                    <span class="fblue">Duration:00:50</span>
                                    <a class="fblue" href="javascript:void(0)">VIEW NOW</a>
                                </p>
                                <span class="icon-youtube-play icomoon"></span>                                   
                            </div>
                        </div>
                        <!--/ colvideo -->
                    </div>
                    <!--/ row -->
                </div>
                <!--/ col right -->
              </div>
              <!--/ row -->
           </div>
           <!--/ container -->
        </div>
        <!--/ sub page body -->
    </main>

    <!--/ sub page main -->
    <!--footer -->
    <?php include 'footer.php' ?>
    <!--/ footer -->  
    <?php include 'footerscripts.php'?>

    <!-- Modal video -->
    <div class="modal fade video-modal" id="videopop" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1" aria-hidden="true" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog  modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">                   
                    <button id="close" type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <iframe class="modalvideo" width="100%" src="https://www.youtube.com/embed/st0IJgPwtD8" allow="autoplay" frameborder="0" allow="accelerometer; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>                   
                </div>
            </div>
        </div>
    </div>
    <!--/ modal video -->


<script>
    $(document).ready(function(){
        $("#videos-show-icon").click(function(){
            $("#video-listitems").slideToggle();
        });
    });
</script> 
</body>

</html>