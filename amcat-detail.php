<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Amcat at Brainwiz</title>
    <link rel="icon" type="image/png" sizes="32x32" href="img/favicon-32x32.png">
    <!-- style sheets -->
    <?php include 'headerstyles.php' ?>
</head>

<body>

    <!-- header -->
   <?php include 'header.php' ?>
    <!--/ header -->
    <!--sub page main -->
    <main class="subpage">
        <!-- sub page header -->
        <div class="page-header">
            <!-- container -->
            <div class="container">
               <!-- row -->
               <div class="row">
                   <div class="col-lg-6">
                       <h1>AMCAT </h1>
                   </div>
               </div>
               <!--/ row --> 
            </div>
            <!--/ container -->
        </div>
        <!--/ sub page header -->

        <!-- brudcrumb -->
        <div class="container">
            <!-- row -->
            <div class="row">
                <!-- col -->
                <div class="col-lg-12">                   
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                        <li class="breadcrumb-item"><a href="allcourses.php">Courses</a></li>
                        <li class="breadcrumb-item active"><a>AMCAT</a></li>
                    </ul>                    
                </div>
                <!--/col -->
            </div>
            <!--/ row -->
        </div>
        <!--/ brudcrumb -->

        <!-- sub page body -->
        <div class="subpage-body">
           <!-- container -->
           <div class="container">
               <!-- row -->
               <div class="row">
                   <!-- left col -->
                   <div class="col-lg-8">
                    <!-- row-->
                    <div class="row justify-content-center">
                        <!-- col 12 -->
                       <div class="col-lg-12 ">
                           <div class="bggray p-4 text-center">
                            <img style="height:300px;" src="img/amcat-courseimg.svg" alt="" class="img-fluid">
                                <h2 class="h2 py-2 fbold">What is AMCAT? </h2>
                                <p class="text-center">The AMCAT is an computer adaptive test which measures the job applicants on critical areas like logical reasoning, communication skills, quantitative skills and job specific domain skills thus helping recruiters identify the suitability of the candidate. And also AMCAT’s comprehensive feedback report helps you identify your strong and weak areas so that you can work to improve in highlighted areas or apply for specific industries or jobs where your strengths are better suited.</p>

                                <p class="text-center">While most aptitude tests only measure a test taker’s verbal comprehension and reasoning abilities, the AMCAT additionally evaluates personality traits and domain skills, thus becoming an ideal test to match jobs to candidates. Post-test, AMCAT Score helps much candidates with suitable jobs based on their performance on the test.</p>

                                <p class="text-center">AMCAT is fast emerging as an industry benchmark with 700+ companies using it as a compulsory testing mechanism for entry level roles. The Client list covers multiple industries like IT Service, Banking & Financial services, Automobiles, Telecom etc. There are thousands of small companies which are looking for good talent and provide great opportunities for growth. </p>
                           </div>

                           <h3 class="h2 py-2 fbold">Benefits of AMCAT Exam </h3>
                           <p>More than 700 companies are tied up with AMCAT to hire fresher’s and experienced candidates. And based on the AMCAT score, job recommendations will come from top MNC’s.  The only Criteria of AMCAT is your Test score. And based on that it provides the feedback on assessments of strengths and weakness of students which helps them to improve their performance. The AMCAT Job profiles available in various sectors like IT, Pharma, ITES, Retail, Manufacturing, Finance etc.  </p>

                           <h3 class="h2 py-2 fbold">AMCAT Training in Hyderabad </h3>
                           <p>Among all the Institutes, BRAINWIZ is one of the Best AMCAT Training Institute in Hyderabad. Here Students will get specific AMCAT Training based on all modules considering all the candidates from basic to high level, where covering all Quantitative Aptitude, Reasoning, Verbal and various MNC Puzzles.  In BRAINWIZ, AMCAT Coaching evaluates the right candidates who match with their ideal job requirements. Thousands of BRAINWIZ students successfully taken AMCAT Test and secured a good score and placed in top MNC’s through this AMCAT Exam. Keeping all the students strengths and weakness in mind and giving the training accordingly in each module is the main motive of the BRAINWIZ Institute.  </p>

                           <p>AMCAT Training in Ameerpet, BRAINWIZ is one of the leading AMCAT Training Institute in Hyderabad. Here students will be trained on company based specific questions and puzzles. BRAINWIZ will mainly focus on various MNC Questions like TCS Questions, Infosys Questions, Mind tree, IBM, Wipro programs, Accenture Questions etc. Training will be given on different modules covering all the AMCAT Syllabus and providing AMCAT Test series includingdifferent MNC Pattern papers. </p>

                           <h3 class="h2 py-2 fbold">AMCAT Syllabus </h3>
                           <p>AMCAT Test helps so many students from various domains in different fields with the help of a detailed analysis in each domain.  </p>

                           <p>AMCAT consists of various modules which you can choose according to your education background and profile. And there are certain modules which are mandatory for all the students like Aptitude and Language modules, whereas others are optional according to your profile. An option will be given to the students to select any two domain specific modules during the test. Your choice of modules increases your visibility for particular kind of jobs, for instance, if you take the programming Module, you will be visible to more IT companies whereas taking a HR module will help you to find HR profiles. It is best for you to go through the module list now and decide which optional module you would like to take. So it is recommended to that you go through the module description and benefitting job profiles before you go to take the AMCAT Test. </p>

                           <p>AMCAT exam consists of 4 compulsory modules and 2 optional modules. The duration of the exam depends on the type of modules. There is no negative marking in the exam but it's always better to answer every question properly as it will affect the overall marks. AMCAT consists of 3 types of modules: </p>

                           <ol class="orderlist">
                               <li>Language and Aptitude (Compulsory) - English Ability, Quantitative ability, Logical ability. </li>
                               <li>Personality (Compulsory) - AMCAT Personality Inventory. </li>
                               <li> Skill specific Modules (Optional) - Domain or interest related modules like Computer programming for CS/IT students. Electronics and Semiconductor for ECE students, Finance and Marketing for MBA students</li>
                           </ol>

                           <!-- course curriculum -->
                           <div class="bggray p-4 custom-accord">
                                <h3 class="h2 py-1 fbold text-center">AMCAT Syllabus for Compulsory Modules: </h3> 

                                <!-- accordian -->
                                <div class="accordion course-accordion">
                                    <!-- acc-->
                                    <h3 class="panel-title">English</h3>
                                    <div class="panel-content">
                                        <ul class="list-seperator nav">
                                            <li class="nav-item"><a class="nav-link">Number of Questions: <span class="fbold fblue">18</span></a></li>
                                            <li class="nav-item"><a class="nav-link">Module Duration: <span class="fbold fblue">18 minutes</span></a></li>
                                        </ul>

                                        <h4 class="h6 py-2">Detailed Syllabus: </h4>
                                        <h5 class="h5">Vocabulary</h5>

                                        <ul class="page-list">
                                            <li>Synonyms </li>
                                            <li>Antonyms</li>
                                            <li>Spotting Errors</li>
                                            <li>Sentence Correction</li>
                                            <li>Sentence Improvement </li>
                                            <li>Sentence Completion</li>
                                            <li>Reading Comprehension</li>
                                            <li>Jumbled Sentences</li>
                                        </ul>
                                    </div>
                                    <!--/ acc-->
                                    <!-- acc-->
                                    <h3 class="panel-title">Quantitative Ability </h3>
                                    <div class="panel-content">
                                        <ul class="list-seperator nav">
                                            <li class="nav-item"><a class="nav-link">Number of Questions: <span class="fbold fblue">16</span></a></li>
                                            <li class="nav-item"><a class="nav-link">Module Duration: <span class="fbold fblue">20 minutes</span></a></li>
                                        </ul>
                                        <h4 class="h6 py-2">Detailed Syllabus: </h4>
                                        <ul class="page-list">
                                            <li>Number System</li>
                                            <li>Divisibility</li>
                                            <li>HCF and LCM</li>
                                            <li>Decimal fractions</li>
                                            <li>Power</li>
                                            <li>Profit and Loss</li>
                                            <li>Percentages</li>
                                            <li>Simple and Compound Interest</li>
                                            <li>Time, Speed and Distance</li>
                                            <li>Inverse</li>
                                            <li>Logarithms</li>
                                            <li>Permutation and Combinations</li>
                                            <li>Probability</li>
                                            <li>Time & Work</li>
                                            <li>Pipes & Cistern</li>
                                            <li>Averages</li>
                                            <li>Allegations & Mixtures</li>
                                            <li>Ratio & proportions</li>
                                            <li>Problems on Ages</li>
                                            <li>Simple interest & Compound interest</li>
                                            <li>Surds & indices</li>                       
                                        </ul>
                                    </div>
                                    <!--/ acc-->                                   
                                     <!-- acc-->
                                     <h3 class="panel-title">Logical Ability</h3>
                                    <div class="panel-content">
                                        <ul class="list-seperator nav">
                                            <li class="nav-item"><a class="nav-link">Number of Questions: <span class="fbold fblue">12</span></a></li>
                                            <li class="nav-item"><a class="nav-link">Module Duration: <span class="fbold fblue">15 minutes</span></a></li>
                                        </ul>
                                        <h4 class="h6 py-2">Detailed Syllabus: </h4>
                                        <ul class="page-list">
                                            <li>Coding & Decoding</li>
                                            <li>Data Sufficiency</li>
                                            <li>Direction Sense</li>
                                            <li>Logical Word Sequence</li>
                                            <li>Objective Reasoning</li>
                                            <li>Puzzles</li>
                                            <li>Number Series</li>
                                            <li>Analogy & Classification</li>
                                            <li>Blood Relations</li>
                                            <li>Letter Series</li>
                                            <li>ODD MAN OUT</li>
                                            <li>Seating Arrangements</li>
                                            <li>Data Sufficiency</li>
                                            <li>Clocks & Calendars</li>                     
                                        </ul>
                                    </div>
                                    <!--/ acc-->
                                     <!-- acc-->
                                     <h3 class="panel-title">Information Gathering & Synthesis</h3>
                                    <div class="panel-content">
                                        <h4 class="h6 py-2">Detailed Syllabus: </h4>
                                        <ul class="list-seperator nav">
                                            <li class="nav-item"><a class="nav-link">Number of Questions: <span class="fbold fblue">12</span></a></li>
                                            <li class="nav-item"><a class="nav-link">Module Duration: <span class="fbold fblue">15 minutes</span></a></li>
                                        </ul>
                                        <h4 class="h6 py-2">Detailed Syllabus: </h4>
                                        <ul class="page-list">
                                            <li>Data Interpretation</li>
                                            <li>Information Ordering</li>
                                            <li>Information Processing</li>               
                                        </ul>
                                    </div>
                                    <!--/ acc-->
                                     <!-- acc-->
                                     <h3 class="panel-title">Aspiring Minds Personality Inventory(AMPI)</h3>
                                    <div class="panel-content">
                                        <ul class="list-seperator nav">
                                            <li class="nav-item"><a class="nav-link">Number of Questions: <span class="fbold fblue">90</span></a></li>
                                            <li class="nav-item"><a class="nav-link">Module Duration: <span class="fbold fblue">20 minutes</span></a></li>
                                        </ul>
                                        
                                        <ul class="page-list">
                                            <li>Extraversion</li>
                                            <li>Conscientiousness</li>
                                            <li>Neuroticism</li>
                                            <li>Openness to Experience</li>
                                            <li>Agreeableness</li>         
                                        </ul>
                                    </div>
                                    <!--/ acc-->
                                    <h3 class="h2 py-1 fbold">AMCAT Syllabus for Optional Modules- General </h3> 

                                     <!-- acc-->
                                     <h3 class="panel-title">Automata Fix  </h3>
                                    <div class="panel-content">
                                        <ul class="list-seperator nav">
                                            <li class="nav-item"><a class="nav-link">Number of Questions: <span class="fbold fblue">7</span></a></li>
                                            <li class="nav-item"><a class="nav-link">Module Duration: <span class="fbold fblue">20 minutes</span></a></li>
                                        </ul>
                                        <h4 class="h6 py-2">Detailed Syllabus: </h4>
                                        <ul class="page-list">
                                            <li>Basic programming</li>
                                            <li>Control Structures</li>
                                            <li>Linear data structures</li>
                                            <li>Sorting and searching algorithms</li>
                                            <li>Conditional statements</li>
                                            <li>Advanced data structures</li>                                
                                        </ul>                                       
                                    </div>
                                    <!--/ acc-->

                                    <!-- acc-->
                                    <h3 class="panel-title">Fundamentals of Physics  </h3>
                                    <div class="panel-content">
                                        <h4 class="h6 py-2">Detailed Syllabus: </h4>
                                        <ul class="list-seperator nav">
                                            <li class="nav-item"><a class="nav-link">Number of Questions: <span class="fbold fblue">15</span></a></li>
                                            <li class="nav-item"><a class="nav-link">Module Duration: <span class="fbold fblue">15 minutes</span></a></li>
                                        </ul>
                                       
                                        <ul class="page-list">
                                            <li>Rotational and Fluid Mechanics</li>
                                            <li>Oscillations and Waves</li>
                                            <li>Magneto Statics and Electro Dynamics</li>
                                            <li>Optics</li>
                                            <li>Nuclear and Particle Physics</li>
                                            <li>Kinematics and Dynamics</li>
                                            <li>Electrostatics</li>
                                            <li>Thermal Physics</li>                       
                                        </ul>                                       
                                    </div>
                                    <!--/ acc-->

                                     <!-- acc-->
                                     <h3 class="panel-title">Fundamentals of Chemistry  </h3>
                                    <div class="panel-content">
                                        <ul class="list-seperator nav">
                                            <li class="nav-item"><a class="nav-link">Number of Questions: <span class="fbold fblue">18</span></a></li>
                                            <li class="nav-item"><a class="nav-link">Module Duration: <span class="fbold fblue">15 minutes</span></a></li>
                                        </ul>
                                        <h4 class="h6 py-2">Detailed Syllabus: </h4>
                                        <ul class="page-list">
                                            <li>Basic Concepts</li>
                                            <li>Chemical Bonding</li>
                                            <li>Chemical Thermodynamics</li>
                                            <li>Electrochemistry</li>
                                            <li>Gaseous State</li>
                                            <li>Solutions and Colligative Properties</li>
                                            <li>Chemical and Ionic Equilibrium</li>
                                            <li>Types of Organic Reactions</li>
                                            <li>Chemical Kinetics</li>
                                            <li>Periodic Table and Periodic Properties</li>
                                            <li>Coordination Compounds</li>
                                            <li>Atomic Structure</li>
                                            <li>Purification and Characterization of Organic Compounds</li>        
                                        </ul>                                       
                                    </div>
                                    <!--/ acc-->

                                     <!-- acc-->
                                     <h3 class="panel-title">Basic Biology  </h3>
                                    <div class="panel-content">
                                        <ul class="list-seperator nav">
                                            <li class="nav-item"><a class="nav-link">Number of Questions: <span class="fbold fblue">22</span></a></li>
                                            <li class="nav-item"><a class="nav-link">Module Duration: <span class="fbold fblue">15 minutes</span></a></li>
                                        </ul>
                                        <h4 class="h6 py-2">Detailed Syllabus: </h4>
                                        <ul class="page-list">
                                            <li>Brain and Skeleton</li>
                                            <li>Respiratory and Cardiac System</li>
                                            <li>Brain and Pituitary, Thyroid, Parathyroid Glands</li>
                                            <li>Neuro-transmission</li>
                                            <li>Respiration</li>
                                            <li>Muscular System and Alimentary Canal</li>
                                            <li>Lymphatic System and Complete Blood System</li>
                                            <li>Urinary System</li>
                                            <li>Reproductive System</li>
                                            <li>Adrenals, Pancreas, Testes and Ovary</li>
                                            <li>Endocrine Glands and Gastrointestinal Tract</li>    
                                        </ul>                                       
                                    </div>
                                    <!--/ acc-->

                                     <!-- acc-->
                                     <h3 class="panel-title">Marketing  </h3>
                                    <div class="panel-content">
                                        <ul class="list-seperator nav">
                                            <li class="nav-item"><a class="nav-link">Number of Questions: <span class="fbold fblue">16</span></a></li>
                                            <li class="nav-item"><a class="nav-link">Module Duration: <span class="fbold fblue">15 minutes</span></a></li>
                                        </ul>
                                        <h4 class="h6 py-2">Detailed Syllabus: </h4>
                                        <ul class="page-list">
                                            <li>Basic Concepts of Marketing</li>
                                            <li>Markets and Marketing management</li>
                                            <li>Product Strategy and Product Line Decisions</li>
                                            <li>Analysing Consumer Behaviour</li>
                                            <li>STP</li>
                                            <li>Marketing Mix</li>
                                            <li>Pricing Decisions</li>
                                            <li>Marketing Strategies and Plans</li>
                                            <li>Product Life Cycle</li>
                                            <li>Fields of Marketing (Retail Marketing, Service Marketing, International Marketing and Rural Marketing)</li>
                                            <li>Promotion Tools</li>
                                            <li>Distribution</li>
                                            <li>Advertising</li>
                                            <li>Media Planning</li>
                                            <li>Brand Concept and Positioning</li>
                                        </ul>                                       
                                    </div>
                                    <!--/ acc-->

                                     <!-- acc-->
                                     <h3 class="panel-title">Financial Accounting  </h3>
                                    <div class="panel-content">
                                        <ul class="list-seperator nav">
                                            <li class="nav-item"><a class="nav-link">Number of Questions: <span class="fbold fblue">15</span></a></li>
                                            <li class="nav-item"><a class="nav-link">Module Duration: <span class="fbold fblue">15 minutes</span></a></li>
                                        </ul>
                                        <h4 class="h6 py-2">Detailed Syllabus: </h4>
                                        <ul class="page-list">
                                            <li>Journalizing and Posting</li>
                                            <li>Subsidiary Books & Accounts</li>
                                            <li>Accounts basics</li>
                                            <li>Assets and Liabilities</li>
                                            <li>Inventories</li>
                                            <li>Financial Statements Analysis</li>
                                            <li>Financial Ratios</li>
                                            <li>Balance Sheet</li>
                                            <li>Cost Concepts</li>
                                            <li>Income Statement and Cash Flow</li>
                                        </ul>                                       
                                    </div>
                                    <!--/ acc-->

                                    <!-- acc-->
                                    <h3 class="panel-title">Financial & Banking Services  </h3>
                                    <div class="panel-content">
                                        <ul class="list-seperator nav">
                                            <li class="nav-item"><a class="nav-link">Number of Questions: <span class="fbold fblue">22</span></a></li>
                                            <li class="nav-item"><a class="nav-link">Module Duration: <span class="fbold fblue">20 minutes</span></a></li>
                                        </ul>
                                        <h4 class="h6 py-2">Detailed Syllabus: </h4>
                                        <ul class="page-list">
                                            <li>Stocks</li>
                                            <li>Bonds</li>
                                            <li>Credit Cards</li>
                                            <li>Loans</li>
                                            <li>Mutual Funds</li>
                                            <li>Balance Sheet</li>
                                            <li>Fixed Deposits/Savings Account</li>
                                            <li>Options</li>
                                            <li>Derivatives</li>
                                        </ul>                                       
                                    </div>
                                    <!--/ acc-->

                                    <!-- acc-->
                                    <h3 class="panel-title">Human Resources  </h3>
                                    <div class="panel-content">
                                        <ul class="list-seperator nav">
                                            <li class="nav-item"><a class="nav-link">Number of Questions: <span class="fbold fblue">15</span></a></li>
                                            <li class="nav-item"><a class="nav-link">Module Duration: <span class="fbold fblue">15 minutes</span></a></li>
                                        </ul>
                                        <h4 class="h6 py-2">Detailed Syllabus: </h4>
                                        <ul class="page-list">
                                            <li>Human Resource Management</li>
                                            <li>HR Planning</li>
                                            <li>Training and Development</li>
                                            <li>Performance Appraisal</li>
                                            <li>Organizational behaviour</li>
                                            <li>Compensation Management</li>
                                            <li>Organizational Structure and Design</li>
                                            <li>Industrial Relations and Labour Welfare Acts</li>
                                            <li>Individual and Group Behaviour</li>
                                            <li>Other Aspects of Labour Welfare</li>
                                        </ul>                                       
                                    </div>
                                    <!--/ acc-->

                                     <!-- acc-->
                                     <h3 class="panel-title">Operations Management  </h3>
                                    <div class="panel-content">
                                        <ul class="list-seperator nav">
                                            <li class="nav-item"><a class="nav-link">Number of Questions: <span class="fbold fblue">10</span></a></li>
                                            <li class="nav-item"><a class="nav-link">Module Duration: <span class="fbold fblue">15 minutes</span></a></li>
                                        </ul>
                                        <h4 class="h6 py-2">Detailed Syllabus: </h4>
                                        <ul class="page-list">
                                            <li>Planning and Development</li>
                                            <li>Resources Planning</li>
                                            <li>Capacity Planning</li>
                                            <li>Project Planning</li>
                                            <li>Supply Chain Management</li>
                                            <li>Statistical Quality Control</li>
                                            <li>Control Chart</li>
                                            <li>Layout Planning</li>
                                            <li>Process Selection</li>
                                            <li>Product Design and Development</li>
                                            <li>Sourcing and Purchasing</li>
                                            <li>Logistics and Warehousing</li>
                                            <li>Sampling Operating Characteristic Curve</li>
                                            <li>Quality Management</li>
                                            <li>Basics of Quality Management</li>
                                            <li>Inventory Management</li>
                                        </ul>                                       
                                    </div>
                                    <!--/ acc-->

                                     <!-- acc-->
                                     <h3 class="panel-title">Pharmaceutical Sciences  </h3>
                                    <div class="panel-content">
                                        <ul class="list-seperator nav">
                                            <li class="nav-item"><a class="nav-link">Number of Questions: <span class="fbold fblue">15</span></a></li>
                                            <li class="nav-item"><a class="nav-link">Module Duration: <span class="fbold fblue">10 minutes</span></a></li>
                                        </ul>
                                        <h4 class="h6 py-2">Detailed Syllabus: </h4>
                                        <ul class="page-list">
                                            <li>Drug manufacture</li>
                                            <li>Drug delivery, release and action</li>
                                            <li>Pharmacy know how</li>
                                            <li>Basic pharmacology and therapeutics</li>
                                            <li>Drug action on nervous system and endocrine glands</li>
                                            <li>Drug action on circulatory system and GI tract</li>
                                            <li>Chromatography and electrochemistry</li>
                                            <li>Titration</li>
                                            <li>Other analytical techniques</li>
                                        </ul>                                       
                                    </div>
                                    <!--/ acc-->

                                     <!-- acc-->
                                     <h3 class="panel-title">Front Office Management  </h3>
                                    <div class="panel-content">
                                        <ul class="list-seperator nav">
                                            <li class="nav-item"><a class="nav-link">Number of Questions: <span class="fbold fblue">18</span></a></li>
                                            <li class="nav-item"><a class="nav-link">Module Duration: <span class="fbold fblue">15 minutes</span></a></li>
                                        </ul>
                                        <h4 class="h6 py-2">Detailed Syllabus: </h4>
                                        <ul class="page-list">
                                            <li>Basics of Front office organization</li>
                                            <li>Hotel organization</li>
                                            <li>Tariff Structure</li>
                                            <li>Introduction to guest cycle/guest handling</li>
                                            <li>Reservations</li>
                                            <li>Room selling techniques</li>
                                            <li>Guest safety and security, special requests</li>
                                            <li>Planning and Evaluating FO Operations</li>
                                            <li>Accounting fundamentals</li>
                                        </ul>                                       
                                    </div>
                                    <!--/ acc-->

                                     <!-- acc-->
                                     <h3 class="panel-title">Housekeeping  </h3>
                                    <div class="panel-content">
                                        <ul class="list-seperator nav">
                                            <li class="nav-item"><a class="nav-link">Number of Questions: <span class="fbold fblue">18</span></a></li>
                                            <li class="nav-item"><a class="nav-link">Module Duration: <span class="fbold fblue">15 minutes</span></a></li>
                                        </ul>
                                        <h4 class="h6 py-2">Detailed Syllabus: </h4>
                                        <ul class="page-list">
                                            <li>Organization of the House Keeping Department</li>
                                            <li>Organization Chart of the Housekeeping Department</li>
                                            <li>Routine systems and records</li>
                                            <li>Guest room cleaning procedures</li>
                                            <li>Cleaning knowledge and practice</li>
                                            <li>Other Housekeeping Activities</li>
                                            <li>Planning and organizing</li>
                                            <li>Cleaning and Procedures</li>
                                            <li>Linen and Laundry</li>
                                            <li>Horticulture</li>
                                            <li>Safety and security</li>
                                        </ul>                                       
                                    </div>
                                    <!--/ acc-->

                                      <!-- acc-->
                                      <h3 class="panel-title">Culinary Skills  </h3>
                                    <div class="panel-content">
                                        <ul class="list-seperator nav">
                                            <li class="nav-item"><a class="nav-link">Number of Questions: <span class="fbold fblue">18</span></a></li>
                                            <li class="nav-item"><a class="nav-link">Module Duration: <span class="fbold fblue">15 minutes</span></a></li>
                                        </ul>
                                        <h4 class="h6 py-2">Detailed Syllabus: </h4>
                                        <ul class="page-list">
                                            <li>Cooking Principles and Equipment</li>
                                            <li>Principles and methods of cooking</li>
                                            <li>Equipment’s and new developments</li>
                                            <li>Meals Preparation and Presentation</li>
                                            <li>Different courses of meal</li>
                                            <li>Presentation of dish</li>
                                            <li>Culinary Math</li>
                                            <li>Safety and Sanitation</li>
                                            <li>Safety and Sanitation</li>
                                            <li>Kitchen Organization</li>
                                        </ul>                                       
                                    </div>
                                    <!--/ acc-->

                                     <!-- acc-->
                                     <h3 class="panel-title">Food and Beverage Service </h3>
                                    <div class="panel-content">
                                        <ul class="list-seperator nav">
                                            <li class="nav-item"><a class="nav-link">Number of Questions: <span class="fbold fblue">18</span></a></li>
                                            <li class="nav-item"><a class="nav-link">Module Duration: <span class="fbold fblue">15 minutes</span></a></li>
                                        </ul>
                                        <h4 class="h6 py-2">Detailed Syllabus: </h4>
                                        <ul class="page-list">
                                            <li>Service Planning and Preparation</li>
                                            <li>Departmental Organization and Staffing</li>
                                            <li>Meals and menu planning</li>
                                            <li>Food Service Areas and Preparation for service</li>
                                            <li>Room service</li>
                                            <li>Operations and Safety Measures</li>
                                            <li>Order Taking and Receive Payments</li>
                                            <li>Food and Beverage Service Equipment and Table Service</li>
                                            <li>Food Safety and Environmental Concerns</li>
                                            <li>Beverages and Tobacco</li>
                                            <li>Tobacco</li>
                                            <li>Dispense Bars, Alcoholic Beverages and Cellar</li>
                                            <li>Non Alcoholic Beverages</li>
                                            <li>Cocktails and Mixed Drinks</li>
                                        </ul>                                       
                                    </div>
                                    <!--/ acc-->                                   

                                     <!-- acc-->
                                     <h3 class="panel-title">Basic Statistics </h3>
                                    <div class="panel-content">
                                        <ul class="list-seperator nav">
                                            <li class="nav-item"><a class="nav-link">Number of Questions: <span class="fbold fblue">12</span></a></li>
                                            <li class="nav-item"><a class="nav-link">Module Duration: <span class="fbold fblue">15 minutes</span></a></li>
                                        </ul>
                                        <h4 class="h6 py-2">Detailed Syllabus: </h4>
                                        <ul class="page-list">
                                            <li>Exploratory Analysis</li>
                                            <li>Design of experiments, Sampling, Sampling Error, Sampling Bias</li>
                                            <li>Measures of Central Tendency and Dispersion</li>
                                            <li>Statistical survey and Presentation of data</li>
                                            <li>Correlation</li>
                                            <li>Formulating Null & Alternate Hypothesis, Type I and Type II errors</li>
                                            <li>Regression</li>
                                            <li>Statistical Inference</li>
                                            <li>Confidence intervals</li>
                                            <li>Probability</li>
                                            <li>Basics of Probability</li>
                                            <li>z-test/t-test, p-value</li>
                                            <li>Probability density function (PDF) and Cumulative distribution function(CDF)</li>
                                            <li>Standard distributions</li>
                                        </ul>                                       
                                    </div>
                                    <!--/ acc-->

                                    
                                     <!-- acc-->
                                     <h3 class="panel-title">Computer Science </h3>
                                    <div class="panel-content">
                                        <ul class="list-seperator nav">
                                            <li class="nav-item"><a class="nav-link">Number of Questions: <span class="fbold fblue">20</span></a></li>
                                            <li class="nav-item"><a class="nav-link">Module Duration: <span class="fbold fblue">20 minutes</span></a></li>
                                        </ul>
                                        <h4 class="h6 py-2">Detailed Syllabus: </h4>
                                        <h5 class="h5">Operating System & Computer Architecture</h5>
                                        <ul class="page-list">
                                            <li>Basics of Operating system</li>
                                            <li>Computer Architecture</li>
                                            <li>Process Communication and Synchronization</li>
                                            <li>Process Management</li>
                                            <li>I/O and File Management</li>
                                            <li>Memory Management</li>
                                        </ul> 
                                        <h5 class="h5">DBMS</h5>
                                        <ul class="page-list">
                                            <li>Basic concept, Data model, Views, Operation, TRC, DRC, Architecture</li>
                                            <li>Normalization, Generalization, ERD, Key, Database, SQL, Joins, Indexing</li>
                                        </ul>  
                                        <h5 class="h5">Computer Networks</h5>
                                        <ul class="page-list">
                                            <li>Basics of Computer Networks & Communication</li>
                                            <li>Routing</li>
                                            <li>Reference Network Model and Protocol</li>
                                        </ul>                                             
                                    </div>
                                    <!--/ acc-->

                                    <h3 class="h2 py-1 fbold">AMCAT Syllabus for Optional Modules -Engineering: </h3> 

                                    <!-- acc-->
                                    <h3 class="panel-title">Electronics & Semiconductors </h3>
                                    <div class="panel-content">
                                        <ul class="list-seperator nav">
                                            <li class="nav-item"><a class="nav-link">Number of Questions: <span class="fbold fblue">12</span></a></li>
                                            <li class="nav-item"><a class="nav-link">Module Duration: <span class="fbold fblue">15 minutes</span></a></li>
                                        </ul>
                                        <h4 class="h6 py-2">Detailed Syllabus: </h4>
                                        <ul class="page-list">
                                            <li>Basics of semiconductor </li>
                                            <li>Two terminal devices</li>
                                            <li>Three terminal devices</li>
                                            <li>Basic for circuit analysis</li>
                                            <li>Small Signal and Large Signal Circuit Analysis</li>
                                            <li>Feedback, stability and oscillators</li>
                                            <li>Op-amps</li>
                                            <li>Filters</li>
                                            <li>Sequential Circuits</li>
                                            <li>VLSI Basics</li>
                                            <li>Boolean Algebra and minimization of Boolean functions</li>
                                            <li>Logic families</li>
                                            <li>Combinational Circuits</li>
                                        </ul>                                       
                                    </div>
                                    <!--/ acc-->

                                    <!-- acc-->
                                    <h3 class="panel-title">Telecommunication Engineering </h3>
                                    <div class="panel-content">
                                        <ul class="list-seperator nav">
                                            <li class="nav-item"><a class="nav-link">Number of Questions: <span class="fbold fblue">12</span></a></li>
                                            <li class="nav-item"><a class="nav-link">Module Duration: <span class="fbold fblue">15 minutes</span></a></li>
                                        </ul>
                                        <h4 class="h6 py-2">Detailed Syllabus: </h4>
                                        <ul class="page-list">
                                            <li>Communication</li>
                                            <li>Analog Communication</li>
                                            <li>Digital Communication</li>
                                            <li>Optics</li>
                                            <li>Transmission lines and waveguides</li>
                                            <li>Microwave Engineering</li>
                                            <li>Radar</li>
                                            <li>Electromagnetism</li>
                                            <li>Antennas and wave propagation</li>
                                            <li>Radar</li>
                                            <li>Electromagnetism</li>
                                            <li>Electrostatics</li>
                                            <li>Electromagnetic theory</li>
                                            <li>Magnetostatics</li>
                                        </ul>                                       
                                    </div>
                                    <!--/ acc-->

                                    
                                    <!-- acc-->
                                    <h3 class="panel-title">Electrical  Engineering </h3>
                                    <div class="panel-content">
                                        <ul class="list-seperator nav">
                                            <li class="nav-item"><a class="nav-link">Number of Questions: <span class="fbold fblue">12</span></a></li>
                                            <li class="nav-item"><a class="nav-link">Module Duration: <span class="fbold fblue">15 minutes</span></a></li>
                                        </ul>
                                        <h4 class="h6 py-2">Detailed Syllabus: </h4>
                                        <ul class="page-list">                                     
                                            <li>Fundamentals of Electrical Engineering</li>
                                            <li>Basic electrical engineering</li>
                                            <li>Electrical machines</li>
                                            <li>Instrumentation and control</li>
                                            <li>Instruments and measurements</li>
                                            <li>Power machines</li>
                                            <li>Control system</li>
                                            <li>Electronics</li>
                                            <li>Analog and digital electronics</li>
                                            <li>Power electronics</li>
                                        </ul>                                       
                                    </div>
                                    <!--/ acc-->

                                      <!-- acc-->
                                      <h3 class="panel-title">Mechanical  Engineering </h3>
                                    <div class="panel-content">
                                        <ul class="list-seperator nav">
                                            <li class="nav-item"><a class="nav-link">Number of Questions: <span class="fbold fblue">12</span></a></li>
                                            <li class="nav-item"><a class="nav-link">Module Duration: <span class="fbold fblue">15 minutes</span></a></li>
                                        </ul>
                                        <h4 class="h6 py-2">Detailed Syllabus: </h4>
                                        <ul class="page-list">                                     
                                            <li>Manufacturing Science</li>
                                            <li>Engineering materials</li>
                                            <li>CAD/CAM</li>
                                            <li>Industrial engineering</li>
                                            <li>Production engineering</li>
                                            <li>Thermodynamics and IC Engines</li>
                                            <li>Thermodynamic cycles and steam generators</li>
                                            <li>Heat transfer, refrigeration and air conditioning</li>
                                            <li>IC engines</li>
                                            <li>Fluid and Machine Mechanics</li>
                                            <li>Fluid mechanics and fluid machinery</li>
                                            <li>Strength of materials</li>
                                            <li>Theory of machines</li>
                                            <li>Machine Design</li>
                                        </ul>                                       
                                    </div>
                                    <!--/ acc-->

                                       <!-- acc-->
                                       <h3 class="panel-title">Civil Engineering</h3>
                                    <div class="panel-content">
                                        <ul class="list-seperator nav">
                                            <li class="nav-item"><a class="nav-link">Number of Questions: <span class="fbold fblue">40</span></a></li>
                                            <li class="nav-item"><a class="nav-link">Module Duration: <span class="fbold fblue">25 minutes</span></a></li>
                                        </ul>
                                        <h4 class="h6 py-2">Detailed Syllabus: </h4>
                                        <ul class="page-list">
                                            <li>Structural Engineering</li>
                                            <li>Applied mechanics</li>
                                            <li>Strength of materials</li>
                                            <li>Building materials and construction</li>
                                            <li>Theory of structures</li>
                                            <li>Steel structures</li>
                                            <li>Concrete technology</li>
                                            <li>R.C.C. Design</li>
                                            <li>Geotechnical and Water Resources Engineering</li>
                                            <li>Soil mechanics</li>
                                            <li>Hydraulic engineering</li>
                                            <li>Water supply engineering</li>
                                            <li>Transportation Engineering and Surveying</li>
                                            <li>Highways engineering</li>
                                            <li>Railway engineering</li>
                                            <li>Estimation and costing</li>
                                            <li>Surveying</li>
                                        </ul>                                       
                                    </div>
                                    <!--/ acc-->

                                    
                                       <!-- acc-->
                                       <h3 class="panel-title">Instrumentation Engineering</h3>
                                        <div class="panel-content">
                                            <ul class="list-seperator nav">
                                                <li class="nav-item"><a class="nav-link">Number of Questions: <span class="fbold fblue">25</span></a></li>
                                                <li class="nav-item"><a class="nav-link">Module Duration: <span class="fbold fblue">20 minutes</span></a></li>
                                            </ul>
                                            <h4 class="h6 py-2">Detailed Syllabus: </h4>
                                            <ul class="page-list">
                                                <li>Instrumentation and Control</li>
                                                <li>Transducers and industrial instrumentation</li>
                                                <li>Electronic instrumentation and measurements</li>
                                                <li>Control systems and process control</li>
                                                <li>Analytical and optical instrumentation</li>
                                                <li>Electronics</li>
                                                <li>Analog electronics</li>
                                                <li>Digital electronics</li>
                                                <li>Communications</li>
                                                <li>Fundamentals of network analysis and synthesis</li>
                                                <li>Microprocessor and microcontroller</li>
                                                <li>Signals and Communication System</li>
                                                <li>Signal and systems</li>
                                            </ul>                                       
                                        </div>
                                        <!--/ acc-->

                                          <!-- acc-->
                                       <h3 class="panel-title">Industrial Engineering</h3>
                                        <div class="panel-content">
                                            <ul class="list-seperator nav">
                                                <li class="nav-item"><a class="nav-link">Number of Questions: <span class="fbold fblue">25</span></a></li>
                                                <li class="nav-item"><a class="nav-link">Module Duration: <span class="fbold fblue">20 minutes</span></a></li>
                                            </ul>
                                            <h4 class="h6 py-2">Detailed Syllabus: </h4>
                                            <ul class="page-list">
                                                <li>Design of Manufacturing Systems</li>
                                                <li>Product design and development</li>
                                                <li>Work system design</li>
                                                <li>Production planning and inventory control</li>
                                                <li>Management information system</li>
                                                <li>Facility design</li>
                                                <li>Quality Control and Management</li>
                                                <li>Quality management</li>
                                                <li>Operation research</li>
                                                <li>Engineering economy and costing</li>
                                                <li>Reliability and Costing</li>
                                                <li>Reliability and maintenance</li>
                                            </ul>                                       
                                        </div>
                                        <!--/ acc-->

                                         <!-- acc-->
                                       <h3 class="panel-title">Production Engineering</h3>
                                        <div class="panel-content">
                                            <ul class="list-seperator nav">
                                                <li class="nav-item"><a class="nav-link">Number of Questions: <span class="fbold fblue">12</span></a></li>
                                                <li class="nav-item"><a class="nav-link">Module Duration: <span class="fbold fblue">15 minutes</span></a></li>
                                            </ul>
                                            <h4 class="h6 py-2">Detailed Syllabus: </h4>
                                            <ul class="page-list">
                                                <li>Production Technology and Analysis</li>
                                                <li>Metal casting</li>
                                                <li>Metal forming</li>
                                                <li>Metal joining</li>
                                                <li>Manufacturing Analysis</li>
                                                <li>Metal Cutting and Tool Design</li>
                                                <li>Machining and machine tool operators</li>
                                                <li>Tool engineering</li>
                                                <li>Metrology and inspection</li>
                                                <li>Material Science and CIM</li>
                                                <li>Polymers and composites</li>
                                                <li>Computer integrated manufacturing</li>
                                            </ul>                                       
                                        </div>
                                        <!--/ acc-->

                                         <!-- acc-->
                                       <h3 class="panel-title">Metallurgical Engineering</h3>
                                        <div class="panel-content">
                                            <ul class="list-seperator nav">
                                                <li class="nav-item"><a class="nav-link">Number of Questions: <span class="fbold fblue">12</span></a></li>
                                                <li class="nav-item"><a class="nav-link">Module Duration: <span class="fbold fblue">15 minutes</span></a></li>
                                            </ul>
                                            <h4 class="h6 py-2">Detailed Syllabus: </h4>
                                            <ul class="page-list">
                                                <li>Fuels and furnaces mineral beneficiation</li>
                                                <li>Non Ferrous Technology - Iron and steel</li>
                                                <li>Metallurgical thermodynamics</li>
                                                <li>Metal casting, joining and forming</li>
                                                <li>Corrosion science</li>
                                                <li>Non ferrous materials</li>
                                                <li>Phase transformation and heat treatment</li>
                                                <li>Material testing and characterization</li>
                                            </ul>                                       
                                        </div>
                                        <!--/ acc-->

                                         <!-- acc-->
                                       <h3 class="panel-title">Chemical Engineering</h3>
                                        <div class="panel-content">
                                            <ul class="list-seperator nav">
                                                <li class="nav-item"><a class="nav-link">Number of Questions: <span class="fbold fblue">12</span></a></li>
                                                <li class="nav-item"><a class="nav-link">Module Duration: <span class="fbold fblue">15 minutes</span></a></li>
                                            </ul>
                                            <h4 class="h6 py-2">Detailed Syllabus: </h4>
                                            <ul class="page-list">
                                                <li>Transport Phenomena</li>
                                                <li>Fluid mechanics</li>
                                                <li>Heat transfer</li>
                                                <li>Mass transfer</li>
                                                <li>Chemical Process Engineering and Technology</li>
                                                <li>Process engineering and technology</li>
                                                <li>Chemical technology</li>
                                                <li>Chemical Process Principles and Design</li>
                                                <li>Chemical reaction engineering</li>
                                                <li>Chemical engineering thermodynamics</li>
                                                <li>Stoichiometry and process calculations</li>
                                            </ul>                                       
                                        </div>
                                        <!--/ acc-->

                                        <!-- acc-->
                                       <h3 class="panel-title">Automotive Engineering</h3>
                                        <div class="panel-content">
                                            <ul class="list-seperator nav">
                                                <li class="nav-item"><a class="nav-link">Number of Questions: <span class="fbold fblue">15</span></a></li>
                                                <li class="nav-item"><a class="nav-link">Module Duration: <span class="fbold fblue">15 minutes</span></a></li>
                                            </ul>
                                            <h4 class="h6 py-2">Detailed Syllabus: </h4>
                                            <ul class="page-list">
                                                <li>Engine Components and Transmission</li>
                                                <li>Automotive Petrol and Diesel Engines</li>
                                                <li>Automotive Chassis</li>
                                                <li>Power Units and Transmission</li>
                                                <li>Automotive Electrical Systems and Electronics</li>
                                                <li>Auto vehicle technology and Electrical</li>
                                                <li>Vehicle Body Engineering</li>
                                                <li>Vehicle Dynamics</li>
                                                <li>Heat Transfer and Combustion</li>
                                                <li>Auto-Maintenance and Turn-Up</li>
                                                <li>General Mechanical Engineering</li>
                                                <li>Strength of Materials</li>
                                                <li>Fluid Mechanics and Fluid Machinery</li>
                                                <li>Thermodynamics Cycles and Steam Generators</li>
                                                <li>Manufacturing Methods</li>
                                                <li>Machine Design</li>
                                                <li>Theory of Machines</li>
                                                <li>Industrial Engineering</li>
                                                <li>Engineering Materials</li>
                                            </ul>                                       
                                        </div>
                                        <!--/ acc-->

                                        
                                        <!-- acc-->
                                       <h3 class="panel-title">Petrochemical Engineering</h3>
                                        <div class="panel-content">
                                            <ul class="list-seperator nav">
                                                <li class="nav-item"><a class="nav-link">Number of Questions: <span class="fbold fblue">13</span></a></li>
                                                <li class="nav-item"><a class="nav-link">Module Duration: <span class="fbold fblue">15 minutes</span></a></li>
                                            </ul>
                                            <h4 class="h6 py-2">Detailed Syllabus: </h4>
                                            <ul class="page-list">
                                                <li>Petrochemicals</li>
                                                <li>Chemical Processes & Engineering</li>
                                                <li>Chemical Reaction Engineering</li>
                                                <li>Industrial Chemical Technology</li>
                                                <li>Polymer Technology</li>
                                                <li>Organic chemistry and calculations</li>
                                                <li>Fluid and Thermal Principles of Petrochemical Engineering</li>
                                                <li>Principles of thermodynamics and Multicomponent distillation</li>
                                                <li>Fluid Mechanics</li>
                                                <li>Heat and Mass transfer</li>
                                                <li>Petroleum Composition and Processing</li>
                                                <li>Petroleum exploration and processing</li>
                                                <li>Process design and refining</li>
                                                <li>Process dynamics and instrumental analysis</li>
                                                <li>Novel separation process</li>
                                                <li>Energy and risk management</li>
                                            </ul>                                       
                                        </div>
                                        <!--/ acc-->

                                          <!-- acc-->
                                       <h3 class="panel-title">Aeronautical Engineering</h3>
                                        <div class="panel-content">
                                            <ul class="list-seperator nav">
                                                <li class="nav-item"><a class="nav-link">Number of Questions: <span class="fbold fblue">15</span></a></li>
                                                <li class="nav-item"><a class="nav-link">Module Duration: <span class="fbold fblue">15 minutes</span></a></li>
                                            </ul>
                                            <h4 class="h6 py-2">Detailed Syllabus: </h4>
                                            <ul class="page-list">
                                                <li>Flight mechanics</li>
                                                <li>Airplane performance</li>
                                                <li>Atmosphere</li>
                                                <li>Dynamic stability</li>
                                                <li>Static stability</li>
                                                <li>Space dynamics</li>
                                                <li>Aircraft Structures</li>
                                                <li>Stress and strain</li>
                                                <li>Flight vehicle structures</li>
                                                <li>Structural dynamics</li>
                                                <li>Propulsion:</li>
                                                <li>Aerothermodynamics of non-rotating propulsion components</li>
                                                <li>Turbomachinery</li>
                                                <li>Aerodynamics</li>
                                                <li>Airfoils and wings</li>
                                                <li>Basic fluid mechanics</li>
                                                <li>Viscous flows</li>
                                                <li>Compressible flows</li>
                                                <li>Wind tunnel testing</li>
                                            </ul>                                       
                                        </div>
                                        <!--/ acc-->

                                          <!-- acc-->
                                       <h3 class="panel-title">Paint Technology</h3>
                                        <div class="panel-content">
                                            <ul class="list-seperator nav">
                                                <li class="nav-item"><a class="nav-link">Number of Questions: <span class="fbold fblue">20</span></a></li>
                                                <li class="nav-item"><a class="nav-link">Module Duration: <span class="fbold fblue">15 minutes</span></a></li>
                                            </ul>
                                            <h4 class="h6 py-2">Detailed Syllabus: </h4>
                                            <ul class="page-list">
                                                <li>Coating - Manufacturing, Evaluation, Types</li>
                                                <li>Formulation principles and manufacturing of coatings</li>
                                                <li>Coating properties and evaluation</li>
                                                <li>Industrial and specialty coatings, decorative and eco-friendly coatings</li>
                                                <li>Raw Materials and Precursors</li>
                                                <li>Introduction to components of surface coatings</li>
                                                <li>Organic, inorganic pigments, extenders, dyestuff, natural resins and polymers</li>
                                                <li>Synthetic resins and polymers</li>
                                                <li>Paint Application and Troubleshooting</li>
                                                <li>Surface treatment and coating applications</li>
                                                <li>Coating defects</li>
                                            </ul>                                       
                                        </div>
                                        <!--/ acc-->

                                           <!-- acc-->
                                       <h3 class="panel-title">Polymer Engineering</h3>
                                        <div class="panel-content">
                                            <ul class="list-seperator nav">
                                                <li class="nav-item"><a class="nav-link">Number of Questions: <span class="fbold fblue">20</span></a></li>
                                                <li class="nav-item"><a class="nav-link">Module Duration: <span class="fbold fblue">15 minutes</span></a></li>
                                            </ul>
                                            <h4 class="h6 py-2">Detailed Syllabus: </h4>
                                            <ul class="page-list">
                                                <li>Polymer Synthesis and Properties</li>
                                                <li>Synthesis and properties</li>
                                                <li>Polymer processing</li>
                                                <li>Polymer theology</li>
                                                <li>Polymer Chemistry and Characterization</li>
                                                <li>Chemistry of polymers</li>
                                                <li>Polymer characterization</li>
                                                <li>Application of Polymers</li>
                                                <li>Polymer testing</li>
                                                <li>Polymer technology</li>
                                                <li>Polymer blends </li>
                                            </ul>                                       
                                        </div>
                                        <!--/ acc-->

                                         <!-- acc-->
                                       <h3 class="panel-title">Fundamentals of Chemistry</h3>
                                        <div class="panel-content">
                                            <ul class="list-seperator nav">
                                                <li class="nav-item"><a class="nav-link">Number of Questions: <span class="fbold fblue">18</span></a></li>
                                                <li class="nav-item"><a class="nav-link">Module Duration: <span class="fbold fblue">15 minutes</span></a></li>
                                            </ul>
                                            <h4 class="h6 py-2">Detailed Syllabus: </h4>
                                            <ul class="page-list">
                                                <li>Physical Chemistry</li>
                                                <li>Chemical bonding</li>
                                                <li>Gaseous state</li>
                                                <li>Chemical thermodynamics</li>
                                                <li>Chemical and ionic equilibrium</li>
                                                <li>Solutions and colligative properties</li>
                                                <li>Electrochemistry</li>
                                                <li>Chemical kinetics</li>
                                                <li>Organic Chemistry</li>
                                                <li>Basic Concepts</li>
                                                <li>Purification and characterization of organic compounds</li>
                                                <li>Types of organic reactions</li>
                                                <li>Inorganic Chemistry</li>
                                                <li>Periodic table and periodic properties</li>
                                                <li>Atomic Structure</li>
                                                <li>Coordination compounds</li>
                                            </ul>                                       
                                        </div>
                                        <!--/ acc-->
                                        <h3 class="h2 py-2 fbold">AMCAT Syllabus for Optional Modules – Management</h3>

                                           <!-- acc-->
                                       <h3 class="panel-title">Financial & Banking Services</h3>
                                        <div class="panel-content">
                                            <ul class="list-seperator nav">
                                                <li class="nav-item"><a class="nav-link">Number of Questions: <span class="fbold fblue">22</span></a></li>
                                                <li class="nav-item"><a class="nav-link">Module Duration: <span class="fbold fblue">20 minutes</span></a></li>
                                            </ul>
                                            <h4 class="h6 py-2">Detailed Syllabus: </h4>
                                            <ul class="page-list">
                                                <li>Investment Products</li>
                                                <li>Stocks</li>
                                                <li>Bonds</li>
                                                <li>Mutual Funds</li>
                                                <li>Derivatives</li>
                                                <li>Banking Products</li>
                                                <li>Banking</li>
                                                <li>Debt</li>
                                                <li>Basic Economics and Taxation</li>
                                                <li>Macro Economics</li>
                                                <li>Taxation</li>
                                            </ul>                                       
                                        </div>
                                        <!--/ acc-->

                                         <!-- acc-->
                                       <h3 class="panel-title">Marketing</h3>
                                        <div class="panel-content">
                                            <ul class="list-seperator nav">
                                                <li class="nav-item"><a class="nav-link">Number of Questions: <span class="fbold fblue">16</span></a></li>
                                                <li class="nav-item"><a class="nav-link">Module Duration: <span class="fbold fblue">15 minutes</span></a></li>
                                            </ul>
                                            <h4 class="h6 py-2">Detailed Syllabus: </h4>
                                            <ul class="page-list">
                                                <li>Basic Concepts of Marketing</li>
                                                <li>Markets and Marketing management</li>
                                                <li>Analysing Consumer Behaviour</li>
                                                <li>STP</li>
                                                <li>Marketing Research</li>
                                                <li>Marketing Strategies and Plans</li>
                                                <li>Fields of Marketing (Retail Marketing, Service Marketing, International Marketing and Rural Marketing)</li>
                                                <li>Consumer behaviour</li>
                                                <li>Marketing Mix</li>
                                                <li>Product Strategy and Product Line Decisions</li>
                                                <li>Product Life Cycle</li>
                                                <li>Pricing Decisions</li>
                                                <li>Promotion Tools</li>
                                                <li>Distribution</li>
                                                <li>Advertising and Brand Management</li>
                                                <li>Advertising</li>
                                                <li>Media Planning</li>
                                                <li>Brand Concept and Positioning</li>
                                            </ul>                                       
                                        </div>
                                        <!--/ acc-->

                                          <!-- acc-->
                                       <h3 class="panel-title">Marketing</h3>
                                        <div class="panel-content">
                                            <ul class="list-seperator nav">
                                                <li class="nav-item"><a class="nav-link">Number of Questions: <span class="fbold fblue">15</span></a></li>
                                                <li class="nav-item"><a class="nav-link">Module Duration: <span class="fbold fblue">15 minutes</span></a></li>
                                            </ul>
                                            <h4 class="h6 py-2">Detailed Syllabus: </h4>
                                            <ul class="page-list">
                                                <li>Human Resource Management</li>
                                                <li>HR Planning</li>
                                                <li>Training and Development</li>
                                                <li>Compensation Management</li>
                                                <li>Performance Appraisal</li>
                                                <li>Organizational behaviour</li>
                                                <li>Industrial behaviour</li>
                                                <li>Group behaviour</li>
                                                <li>Organizational structure and design</li>
                                                <li>Industrial Relations and Labour Welfare</li>
                                                <li>Acts</li>
                                                <li>Other Aspects of Labour Welfare</li>
                                            </ul>                                       
                                        </div>
                                        <!--/ acc-->

                                         <!-- acc-->
                                       <h3 class="panel-title">Operations Management</h3>
                                        <div class="panel-content">
                                            <ul class="list-seperator nav">
                                                <li class="nav-item"><a class="nav-link">Number of Questions: <span class="fbold fblue">15</span></a></li>
                                                <li class="nav-item"><a class="nav-link">Module Duration: <span class="fbold fblue">15 minutes</span></a></li>
                                            </ul>
                                            <h4 class="h6 py-2">Detailed Syllabus: </h4>
                                            <ul class="page-list">
                                                <li>Planning and Development</li>
                                                <li>Capacity Planning</li>
                                                <li>Resources Planning</li>
                                                <li>Layout planning</li>
                                                <li>Process selection</li>
                                                <li>Project planning</li>
                                                <li>Quality Management</li>
                                                <li>Product Design and Development</li>
                                                <li>Sourcing and Purchasing</li>
                                                <li>Logistics and Warehousing</li>
                                                <li>Inventory Management</li>
                                                <li>Supply Chain Management</li>
                                                <li>Statistical Quality Control</li>
                                                <li>Control Chart</li>
                                                <li>Sampling Operating Characteristic Curve</li>
                                                <li>Basics of Quality Management</li>
                                            </ul>                                       
                                        </div>
                                        <!--/ acc-->
                                </div>
                                <!--/ accordian -->
                           </div>
                           <!--/ course curriculum -->
                           
                           <h3 class="h2 py-1 fbold text-center">AMCAT Exam Pattern  </h3> 
                                <p>The updated AMCAT Exam Pattern consists of 6 compulsory modules and 2 optional modules. These optional modules can be chosen based on your field of interest.</p>

                                  <!-- table -->
                                  <table class="table">
                                    <thead>
                                        <tr>
                                            <th scope="col">Section</th>
                                            <th scope="col">Number of questions </th> 
                                            <th scope="col">Time duration </th> 
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td scope="row">Quantitative Ability Tech </td>
                                            <td>16</td>
                                            <td>20 Mins</td>     
                                        </tr> 
                                        <tr>
                                            <td scope="row">Quantitative Ability Tech </td>
                                            <td>14</td>
                                            <td>20 Mins</td>     
                                        </tr> 
                                        <tr>
                                            <td scope="row">English Ability </td>
                                            <td>18</td>
                                            <td>18 Mins</td>     
                                        </tr> 
                                        <tr>
                                            <td scope="row">Logical Ability </td>
                                            <td>12</td>
                                            <td>15 Mins</td>     
                                        </tr> 
                                        <tr>
                                            <td scope="row">Information Gathering & Synthesis </td>
                                            <td>12</td>
                                            <td>15 Mins</td>     
                                        </tr> 
                                        <tr>
                                            <td scope="row">AMCAT Personality Inventory </td>
                                            <td>90</td>
                                            <td>20 Mins</td>     
                                        </tr> 
                                        <tr>
                                            <td scope="row">Domain-specific test (Can select any 2 modules of your choice) </td>
                                            <td>(based on modules you select)</td>
                                            <td>(based on modules you select)</td>     
                                        </tr> 
                                    </tbody>
                                </table>
                                <!--/ table -->  

                                <h3 class="h2 py-1 fbold text-center">Which AMCAT Module should choose? </h3> 
                                <p>BRAINWIZ has listed down the recommended modules based on companies eligibility Criteria. This is the most difficult choice you have to make so you should be careful in selecting the domain specific modules which related to your stream and accordingly you will get call letters.</p>

                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th scope="col">Stream</th>
                                            <th scope="col">Recommended Modules for you  </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td scope="row">Computer Science</td>
                                            <td>Automata Fix, Computer Science</td> 
                                        </tr> 
                                        <tr>
                                            <td scope="row">Electronics</td>
                                            <td>Electronics and Semiconductor Engineering, Automata, Computer Programming, Telecommunications Engineering, Computer Science, Electrical Engineering</td> 
                                        </tr> 
                                        <tr>
                                            <td scope="row">Mechanical Engineering</td>
                                            <td>Mechanical Engineering, Automotive Engineering, Production Engineering, Computer Programming</td> 
                                        </tr> 
                                        <tr>
                                            <td scope="row">Electrical Engineering</td>
                                            <td>Electrical Engineering, Automata, Electronics and Semiconductor Engineering, Computer Programming, Telecommunications Engineering, Computer Science</td> 
                                        </tr> 
                                        <tr>
                                            <td scope="row">Electronics & Instrumentation</td>
                                            <td>Electronics and Semiconductor Engineering, Automata, Computer Programming, Instrumentation Engineering, Electrical Engineering, Computer Science</td> 
                                        </tr> 
                                        <tr>
                                            <td scope="row">Electronics & Telecommunications</td>
                                            <td>Electronics and Semiconductor Engineering, Automata, Computer Programming, Telecommunications Engineering, Electrical Engineering, Computer Science</td> 
                                        </tr> 
                                        <tr>
                                            <td scope="row">Civil</td>
                                            <td>Civil Engineering, Computer Programming</td> 
                                        </tr> 
                                        <tr>
                                            <td scope="row">Electronics and Electrical</td>
                                            <td>Electronics and Semiconductor Engineering, Automata, Computer Programming, Electrical Engineering, Telecommunications Engineering, Computer Science.</td> 
                                        </tr> 
                                    </tbody>
                                </table>
                       </div>
                       <!-- col 12 -->
                    </div>
                    <!--/ row-->
                   </div>
                   <!--/left col-->

                   <!-- course right faq -->
                   <div class="col-lg-4">
                        
                        <div class="custom-accord sticky-top course-faq">
                            <h3 class="h5 pb-3">Faq's about AMCAT</h3>
                            <!-- accordion -->
                            <div class="accordion">
                                <!-- acc-->
                                <h3 class="panel-title">How do I register for AMCAT?</h3>
                                <div class="panel-content">
                                    <p>Visit their homepage www.myamcat.com. And Click on the Register button which is on the top of the homepage. You can set your password by entering your Email id. Then enter all your basic profile details like Name, contact number, Degree and the graduation year and then click on submit button. Verify your email address with the OTP received to the mail inbox.  Enter the OTP in the Input box and click on Login. You will then be redirected to the user dashboard where you can book slot for AMCAT Test and other products and services. You can also directly login through social accounts like Facebook, Google and Twitter by clicking on the respective social icon.
                                    </p>
                                </div>
                                <!--/ acc-->
                                <!-- acc-->
                                <h3 class="panel-title">Is there any eligibility criteria to take AMCAT?</h3>
                                <div class="panel-content">
                                    <p>No, there is no eligibility criteria to take the AMCAT Test, anyone can take the exam like the students who have completed or in the final year of (B.E/B.Tech), (BCA, B.Com), MBA etc.</p>
                                </div>
                                <!--/ acc-->
                                <!-- acc-->
                                <h3 class="panel-title">I would like to take the AMCAT exam. How can I schedule AMCAT?</h3>
                                <div class="panel-content">
                                    <p>AMCAT will held in various centers in different states around the country. You can choose your desired slot on a particular date in your city. </p>
                                    <p>First Visit the link Book AMCAT in their website. Enter your personal details and click on continue, then in the next step select a city and choose a desired slot like convenient center, Date and Time. And then you need to make a payment through any of the provided payment methods. You will receive the AMCAT Admit Card with test center details after your payment was success. Then you will be successfully scheduled your AMCAT Exam Slot.</p>
                                </div>
                                <!--/ acc-->
                                <!-- acc-->
                                <h3 class="panel-title">I have scheduled my AMCAT exam. What should I do next?</h3>
                                <div class="panel-content">
                                    <p>Once you done with the payment, check your email inbox where you will receive the AMCAT Admit Card. Take a printout of it and paste your latest photograph on it. Please carry that Admit Card along with the Id proof on the scheduled date and time to the AMCAT test center where the address was clearly mentioned on the Admit Card. And prepare well before giving the exam with all the AMCAT syllabus and Sample papers which was given in our website www.gobrainwiz.in.</p>
                                </div>
                                <!--/ acc-->
                                <!-- acc-->
                                <h3 class="panel-title">What do I do if I want to reschedule my AMCAT exam?</h3>
                                <div class="panel-content">
                                    <p>Yes, you can reschedule your AMCAT exam and give the test at another available slot as long as your existing test slot is more than 24 hours away. </p>
                                    <p>So, first login to your account, visit the profile page and click on the button Reschedule. On Schedule AMCAT page, choose the desired slot (date and time).  And then confirm the slot change on the payment page and you have successfully rescheduled your AMCAT Exam. </p>
                                    <p><span class="fbold">Note:</span> Rescheduling for the first time is free up to 7 days before the test, whereas subsequent reschedules can be changed as follows.</p>

                                    <ul class="page-list">
                                        <li>Less than 24 hours for the test (24 hours): Test cannot be rescheduled.</li>
                                        <li>1-5 days before the Test: Rs.350+ Taxes</li>
                                        <li>6-7 days before the Test: Rs.250+Taxes</li>
                                    </ul>
                                </div>
                                <!--/ acc-->
                                <!-- acc-->
                                <h3 class="panel-title">How can I change the center for my AMCAT exam?</h3>
                                <div class="panel-content">
                                    <p>You cannot change the AMCAT center but you can reschedule your exam. While rescheduling you can change the Date, Time and center for your AMCAT. </p>
                                </div>
                                <!--/ acc-->
                                <!-- acc-->
                                <h3 class="panel-title">Can I take the AMCAT test again?</h3>
                                <div class="panel-content">
                                    <p>Yes, you can give the AMCAT Test again after 45 days of the last attempt. </p>
                                    <p>Login to your account in myamcat.com and visit the link Schedule AMCAT and choose your desired slot. You should choose City, Center, Date and Time for AMCAT test and complete the next payment process. So now you can take your AMCAT test again on the selected slot. </p>
                                    <p>Note: </p>
                                    <p>New AMCAT Score will be valid if you take the AMCAT exam after 45 days from the last attempt.</p>
                                    <p>Your AMCAT subscription date will expire as per your latest AMCAT Exam.</p>
                                </div>
                                <!--/ acc-->
                                <!-- acc-->
                                <h3 class="panel-title">How much do I need to pay to book AMCAT exam?</h3>
                                <div class="panel-content">
                                    <p>You need to pay Rs. 1,100 to book your AMCAT exam. Then you will receive all premium job opportunities and interview calls after giving the AMCAT Exam with a good score.</p>
                                </div>
                                <!--/ acc-->
                                <!-- acc-->
                                <h3 class="panel-title">How do I prepare for the AMCAT?</h3>
                                <div class="panel-content">
                                    <p>AMCAT exam consists of 4 compulsory and 2 optional modules. The modules consists of Aptitude, personality and core knowledge in the field of interest i.e. field of study or bachelors/Masters.</p>
                                    <ol class="orderlist">
                                        <li>You can prepare for Aptitude and Language modules from any aptitude book available in the market. However, you should know the topics in those modules. Check the syllabus from the below mentioned link.</li>
                                        <li>For the domain knowledge section, you should check the syllabus of the modules from the given link and prepare accordingly with your choice of books.</li>
                                        <li>Personality module requires no preparation. This module contains basic situational questions which require honest response of users. </li>
                                        <li>There is no particular source from which you can prepare for AMCAT. You should focus on getting all of your concepts right. </li>
                                    </ol>
                                    <p>Please prepare with our AMCAT Video tutorials, Classroom Training, Sample papers and previously asked questions and take some AMCAT Test series in www.gobrainwiz.in/AMCAT</p>
                                </div>
                                <!--/ acc-->
                                <!-- acc-->
                                <h3 class="panel-title">What is the exam pattern of AMCAT? What is the marking scheme?</h3>
                                <div class="panel-content">
                                    <p>AMCAT exam consists of 4 compulsory modules and 2 optional modules. The duration of the exam depends on the type of modules. There is no negative marking in the exam but it's always better to answer every question properly as it will affect the overall marks. AMCAT consists of 3 types of modules: </p>

                                    <ol class="orderlist">
                                        <li>Language and Aptitude (Compulsory) - English Ability, Quantitative ability, Logical ability.</li>
                                        <li>Personality (Compulsory) - AMCAT Personality Inventory.</li>
                                        <li>Skill specific Modules (Optional) - Domain or interest related modules like Computer programming for CS/IT students. Electronics and Semiconductor for ECE students, Finance and Marketing for MBA students</li> 
                                    </ol>
                                    <table class="table">
                                        <thead>
                                           <tr>
                                                <td>Section</td>
                                                <td>Number of questions</td>
                                                <td>Time duration</td>
                                           </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>Quantitative Ability</td>
                                                <td>16</td>
                                                <td>18 mins</td>
                                            </tr>
                                            <tr>
                                                <td>English Ability</td>
                                                <td>18</td>
                                                <td>16 mins</td>
                                            </tr>
                                            <tr>
                                                <td>Logical Reasoning</td>
                                                <td>14</td>
                                                <td>16 mins</td>
                                            </tr>
                                            <tr>
                                                <td>AMCAT Personality Inventory</td>
                                                <td>90</td>
                                                <td>20 mins</td>
                                            </tr>
                                            <tr>
                                                <td>Domain-specific test (Select any 1 or 2 modules)</td>
                                                <td>(based on the modules you select)</td>
                                                <td>(based on the modules you select)</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <!--/ acc-->

                                 <!-- acc-->
                                 <h3 class="panel-title">What is the validity for my AMCAT score?</h3>
                                <div class="panel-content">
                                    <p>Your AMCAT scores are valid for a year (until your AMCAT subscription expires).
                                    If you don't want to take AMCAT again, you can simply extend your AMCAT subscription to extend the validity of your scores and continue applying for jobs.</p> 
                                </div>
                                <!--/ acc-->

                                 <!-- acc-->
                                 <h3 class="panel-title">How to check my AMCAT ID after the exam?</h3>
                                <div class="panel-content">
                                    <p>You need to login to your myamcat account and visit the scores page to check AMCAT ID. In Score page, you will see your AMCAT ID on the top left side of scores block.</p> 
                                </div>
                                <!--/ acc-->

                                 <!-- acc-->
                                 <h3 class="panel-title">My AMCAT scores are low. Will I get job opportunities? What should I do?</h3>
                                <div class="panel-content">
                                    <p>If your scores are low in just one module, there are chances that you will get job opportunities depending upon the criteria set by different companies.</p> 
                                    <p>If your scores are low in all modules of the AMCAT exam, you should read and understand all the points mentioned in the feedback report.</p>
                                </div>
                                <!--/ acc-->

                                 <!-- acc-->
                                 <h3 class="panel-title">Can I calculate my overall score in the AMCAT?</h3>
                                <div class="panel-content">
                                    <p>There is no overall score in AMCAT. Each module has its own individual score in order to help you identify your strengths and weaknesses. Companies also shortlist using different cut-off for different modules.</p>
                                </div>
                                <!--/ acc-->

                                <!-- acc-->
                                <h3 class="panel-title">When will I get my AMCAT result?</h3>
                                <div class="panel-content">
                                    <p>You will get your AMCAT result typically after 6-7 working days of your AMCAT test attempt. Your scores and report will be shared with you on registered email address.</p>
                                </div>
                                <!--/ acc-->

                                <!-- acc-->
                                <h3 class="panel-title">What is a good score in AMCAT?</h3>
                                <div class="panel-content">
                                    <p>Different industries/companies have different criteria in different sections of the test. While some companies look for high scores in some areas, others might choose a different score. Hence there is no ONE good score. </p>

                                    <p>The section Job Match in the feedback report informs a candidate about his/her chances for getting selected in companies of different sectors and the subjects to improve on, in order to get into the sector of his or her choice.</p>

                                    <p>Apart from the right scores, job shortlisting depends on a variety of factors such as:</p>

                                    <ul class="page-list">
                                        <li>the name of the company that is hiring at that time</li>
                                        <li>the area/region in which the company is hiring</li>
                                        <li>Specific qualification/eligibility of the company currently hiring</li>
                                    </ul>

                                    <p>Aspiring Minds will actively reach out to you over email and SMS as soon as there is a job opportunity for you</p>
                                </div>
                                <!--/ acc-->

                                 <!-- acc-->
                                 <h3 class="panel-title">How do I read the AMCAT feedback report?</h3>
                                <div class="panel-content">
                                    <p>AMCAT feedback report consists of the following sections: </p>  

                                    <ol class="orderlist">
                                        <li>AMCAT Scores</li>
                                        <li>Module Feedback</li>
                                        <li>Your Personality</li>
                                        <li>Your Industry and Job fit</li>
                                        <li>How to improve your employability</li>
                                        <li>Next steps to be ready for your desired job role</li>
                                    </ol>

                                    <p>Aspiring Minds will actively reach out to you over email and SMS as soon as there is a job opportunity for you</p>
                                </div>
                                <!--/ acc-->

                                <!-- acc-->
                                <h3 class="panel-title">What are the type of jobs provided by AMCAT?</h3>
                                <div class="panel-content">
                                    <p>Following are the type of jobs provided on myamcat.com </p>  

                                    <ol class="orderlist">
                                        <li>Premium Jobs: These are the latest jobs on myamcat.com for which only AMCAT takers can apply.</li>
                                        <li>Preferred Jobs: These are the jobs matching to your preferences like Location, type of companies, salary expectations. You would see preferred tag for jobs when you are logged in.</li>
                                        <li>Skill matching Jobs: The jobs are matched in accordance with your AMCAT scores, graduation year, degree etc.</li>                                       
                                    </ol>

                                    <p>You can visit the Jobs page to filter on the basis of job type from the left panel and apply accordingly.</p>
                                </div>
                                <!--/ acc-->

                                <!-- acc-->
                                <h3 class="panel-title">How will I get information about various drives?</h3>
                                <div class="panel-content">
                                    <p>If you have taken the AMCAT exam and have a valid AMCAT subscription, you will receive information about various drives on your registered email address. </p>  

                                    <ol class="orderlist">
                                        <li>If you face any issue in receiving the Job invites and email communication, you should update your preferences and also you can visit the Jobs page to check out the latest job opportunities</li> 
                                    </ol>
                                </div>
                                <!--/ acc-->

                                <!-- acc-->
                                <h3 class="panel-title">How can I check my job application status?</h3>
                                <div class="panel-content">
                                    <p>You need to log into your myamcat account with your credentials and visit the myjobs page. It may take some time to receive an update on the application as different companies follow different hiring processes as per their timelines.</p> 
                                </div>
                                <!--/ acc-->

                                 <!-- acc-->
                                 <h3 class="panel-title">How can I apply for AMCAT jobs? Is there any criteria to apply for jobs?</h3>
                                <div class="panel-content">
                                    <p>You should have taken the AMCAT exam. And your AMCAT scores should have been generated and visible on the Dashboard or Scores page. Then you should have a valid AMCAT subscription. You can check your AMCAT subscription validity on your dashboard. If you have taken the AMCAT exam and have a valid subscription, you can visit the jobs page on myamcat.com and click on apply button to apply for the jobs.</p> 
                                </div>
                                <!--/ acc-->

                                 <!-- acc-->
                                 <h3 class="panel-title">What is AMCAT premium? How is it useful to me?</h3>
                                <div class="panel-content">
                                    <p>AMCAT Premium is a three-in-one package which makes it easier for you to get a perfect job. It is a combo package to help build your professional resume and prepare you for interviews with the AI powered Mock Interview assessment tool. AMCAT premium consists of </p> 

                                    <ul class="page-list">
                                        <li>Create professional resume</li>
                                        <li>MockAI</li>
                                        <li>PrepAMCAT</li>
                                    </ul>

                                </div>
                                <!--/ acc-->

                                 <!-- acc-->
                                 <h3 class="panel-title">Will AMCAT premium combo guarantee a job?</h3>
                                <div class="panel-content">
                                    <p>No, AMCAT premium cannot guarantee a job. It helps you in preparing professional resume for the job opportunities and Mock Interviews. And as seen in the past, people with premium have 60 to 70% better chances of getting their dream jobs through these tools.</p>
                                </div>
                                <!--/ acc-->

                            </div>
                            <!--/ accordion -->
                        </div>
                   </div>
                   <!--/ course right faq -->
               </div>
               <!--/ row -->
           </div>
            <!-- /container -->
        </div>
        <!--/ sub page body -->
    </main>
    <!--/ sub page main -->
    <!--footer -->
    <?php include 'footer.php' ?>
    <!--/ footer -->  
    <?php include 'footerscripts.php'?>

</body>

</html>