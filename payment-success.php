<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Brainwiz Video Tutorials</title>
    <link rel="icon" type="image/png" sizes="32x32" href="img/favicon-32x32.png">
    <!-- style sheets -->
    <?php include 'headerstyles.php' ?>
</head>

<body>

    <!-- header -->
   <?php include 'header-postlogin.php' ?>
    <!--/ header -->
    <!--sub page main -->
    <main class="subpage">
        <!-- sub page header -->
        <div class="page-header">
            <!-- container -->
            <div class="container">
               <!-- row -->
               <div class="row">
                   <div class="col-lg-6">
                       <h1>Payment <span class="fbold">Success</span> </h1>
                   </div>
               </div>
               <!--/ row --> 
            </div>
            <!--/ container -->
        </div>
        <!--/ sub page header -->        

        <!-- sub page body -->
        <div class="subpage-body">
           <!-- container -->
           <div class="container">
              <!-- row -->
              <div class="row">
                        <!--left col -->
                        <!-- successful article -->
                        <div class="col-lg-12 text-center successpayment py-4 border-bottom">
                            <p class="text-center"><span class="icon-thumbs-up icomoon"></span></p>
                            <h5 class="h3 fbold pt-1 mb-0 pb-3 text-center"> Payment Success</h5>
                            <p class="text-center">Thank you! Your Payment of Rs: 3,000 has been received</p>
                            <p class="text-center"><span>Order ID: IC-12334567</span><span class="px-4">Transaction ID: 123456</span></p>
                            <p class="fbold pb-2 text-center">Click on the following Button, You Can Track the  Status, Payment Details of Course.</p>
                           <p class="text-center py-3">
                                <a href="user-purchase-history.php"class="whitebtn  ">Course History</a>
                                <a href="javascript:void(0)"class="bluebtn">View Invoice</a>   
                            </p>                                                   
                        </div>
                        <!--/ successful article -->
                        <!--/left col -->                       
                    </div>
                    <!--/ row -->
           </div>
           <!--/ container -->
        </div>
        <!--/ sub page body -->
    </main>

    <!--/ sub page main -->
    <!--footer -->
    <?php include 'footer.php' ?>
    <!--/ footer -->  
    <?php include 'footerscripts.php'?>

</body>

</html>