<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Brainwiz</title>
    <link rel="icon" type="image/png" sizes="32x32" href="img/favicon-32x32.png">
    <!-- style sheets -->
    <?php include 'headerstyles.php' ?>
</head>

<body>

    <!-- header -->
   <?php include 'header.php' ?>
    <!--/ header -->
    <!--sub page main -->
    <main class="subpage">
        <!-- sub page header -->
        <div class="page-header">
            <!-- container -->
            <div class="container">
               <!-- row -->
               <div class="row">
                   <div class="col-lg-6">
                       <h1>Terms of Use </h1>
                   </div>
               </div>
               <!--/ row --> 
            </div>
            <!--/ container -->
        </div>
        <!--/ sub page header -->

         <!-- brudcrumb -->
         <div class="container">
            <!-- row -->
            <div class="row">
                <!-- col -->
                <div class="col-lg-12">                   
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.php">Home</a></li>                        
                        <li class="breadcrumb-item active"><a>Terms of Use</a></li>
                    </ul>                    
                </div>
                <!--/col -->
            </div>
            <!--/ row -->
        </div>
        <!--/ brudcrumb -->
        <!-- sub page body -->
        <div class="subpage-body">
           <!-- container -->
           <div class="container">
              <!-- row -->
              <div class="row">
               <!-- col 9 -->
                <div class="col-lg-9">

                <h2 class="h4">Introduction and Acceptance of Terms </h2>

                <p>Udacity, Inc. (collectively, "we," "us" or "Udacity") provides you access to the Udacity Services (defined below) subject to the terms and conditions described in this document (this "Terms of Use") and any other guidelines, rules or licenses posted in connection with any Online Courses (defined below). This Terms of Use also includes our Privacy Policy, incorporated by this reference.</p>

                <p>By using this Service, clicking “I Agree”, registering a User Account (defined below), purchasing an Online Course, or by indicating your agreement to this Terms of Use through a similar mechanism, you are agreeing to be bound by this Terms of Use. If you do not agree to this Terms of Use, do not use the Services. If you under the age of 18, but at least 13 years of age, you represent and agree that you are an emancipated minor or possess legal parental or guardian consent to use the Services and are fully able and competent to enter into all of the conditions, obligations, affirmations, representations and warranties set forth in this Terms of Use. The Services are not intended for, nor directed at, individuals under the age of 13 (or under the applicable age of consent in your jurisdiction); if you are under 13 years of age (or the applicable age of consent in your jurisdiction), do not use the Services.</p>

               <p>NOTICE REGARDING DISPUTE RESOLUTION: These Terms of Use contain provisions that govern how claims you and we may have against each other are resolved (see the Section entitled ‘Arbitration’ below), including an agreement and obligation to arbitrate disputes, which will, subject to limited exceptions, require you to submit claims you have against us to binding arbitration, unless you opt-out in accordance with the Arbitration Section. Unless you opt-out of arbitration: (a) you will only be permitted to pursue claims against us on an individual basis, not as part of any class or representative action or proceeding and (b) you will only be permitted to seek relief (including monetary, injunctive, and declaratory relief) on an individual basis.</p>

             
               <p>SPECIAL NOTICE FOR CALIFORNIA RESIDENTS: Under California Education Code (Section 94874 (f)), Udacity is limited to accepting under $2,500 in fees for any individual Nanodegree program pursuant to the exemption set forth in the statute. Therefore, in order to comply with the California licensure requirements, Udacity shall limit paid subscriptions for California residents to a period of six (6) months (at current rate of $399) to any individual Nanodegree program. For more information please see https://www.udacity.com/regulatory-information</p>

               <h2 class="h4">Services</h2>
               <p>Udacity provides many services (each a “Service” and together the “Services”) through its website(s) located at www.udacity.com and www.knowlabs.com, as well as any other websites and applications owned, operated, or controlled by us (each a “Website” and collectively the “Websites”), including, without limitation, all information, content, services, and materials made available through any Website, social media channels, or other online or onsite channels that enable you to participate in any Udacity online educational programs (collectively, the “Online Courses”) and related services, such as careers services, or any part thereof For clarity, Udacity offers certain Online Courses that are curated, specially designed, and produced which are referred to as “Nanodegree” programs. A verified certificate of accomplishment upon conclusion of the applicable Online Course ("Verified Certificate"), shall be awarded, in the sole discretion of Udacity and the instructors for the Online Courses, to Students (defined below) who have completed the course to the satisfaction of Udacity including without limitation being in compliance with all Udacity policies.</p>

              
               <p>Udacity may also offer Online Courses in partnership with accredited universities and other educational institutions, corporate sponsors and non-profit foundations and institutions (each an “Educational Partner”) whereby academic credit or certification may be offered solely by that Educational Partner. Please see the section of this Terms of Use pertaining to Online Courses with Educational Partners below for specifics.</p>

              
               <p>Udacity may also partner with companies who will sponsor or pay for Online Courses for Users or those companies’ employees/associated persons. Please see the Sponsored Services and Employee and Enterprise Sponsored Access sections of this Terms of User below for further information.</p>
                  
                </div>         
               <!--/ col 9-->

               <!-- col 3-->
               <div class="col-lg-3">
                   <div class="career-block text-center">
                        <img src="img/premade-image-09.png">
                        <h2 class="h5 py-3">Advance Your Career</h2>
                        <p class="text-center">Enter your email below to download one of our free career guides</p>
                        <form>
                            <div class="form-group">
                                <input class="form-control" type="text" placeholder="Enter your Email"> 
                            </div>
                            <div class="form-group">
                                <input class="bluebtn w-100" type="submit" value="Submit"> 
                            </div>
                        </form>
                        <p class="small text-left">I consent and agree to receive email marketing communications from Udacity.  " that by clicking “Submit,” my data will be used in accordance with the  Udacity Terms of Use and Privacy Policy.</p>
                   </div>
               </div>
               <!--/ col 3-->
             
              

              



              </div>
              <!--/ row -->
           </div>
           <!--/ container -->
        </div>
        <!--/ sub page body -->
    </main>

    <!--/ sub page main -->
    <!--footer -->
    <?php include 'footer.php' ?>
    <!--/ footer -->  
    <?php include 'footerscripts.php'?>
</body>

</html>