<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Brainwiz</title>
    <link rel="icon" type="image/png" sizes="32x32" href="img/favicon-32x32.png">
    <!-- style sheets -->
    <?php include 'headerstyles.php' ?>
</head>

<body>

    <!-- header -->
   <?php include 'header.php' ?>
    <!--/ header -->
    <!--sub page main -->
    <main class="subpage">
        <!-- sub page header -->
        <div class="page-header">
            <!-- container -->
            <div class="container">
               <!-- row -->
               <div class="row">
                   <div class="col-lg-6">
                       <h1>Blogs </h1>
                   </div>
               </div>
               <!--/ row --> 
            </div>
            <!--/ container -->
        </div>
        <!--/ sub page header -->

         <!-- brudcrumb -->
         <div class="container">
            <!-- row -->
            <div class="row">
                <!-- col -->
                <div class="col-lg-12">                   
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                        <li class="breadcrumb-item active"><a>Blogs</a></li>
                    </ul>                    
                </div>
                <!--/col -->
            </div>
            <!--/ row -->
        </div>
        <!--/ brudcrumb -->

        <!-- sub page body -->
        <div class="subpage-body">
           <!-- container -->
           <div class="container">
              <!-- row -->
              <div class="row">
              <!-- col -->
              <div class="col-lg-4">
                    <div class="blog-col text-center">
                        <a href="blogdetail.php"><img src="img/data/blogimg01.jpg" alt="" class="img-fluid"></a>
                        <article>
                            <h2 class="h6 pt-2">
                                <a href="blogdetail.php">Blog News Name will be here</a>
                            </h2>
                            <ul class="list-seperator nav justify-content-center">
                                <li><a>Admin</a> </li>
                                <li><a>Posted on 16 Nov 2019</a> </li>
                            </ul>
                            <p class="text-center"> The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested.</p>
                        </article>
                    </div>
              </div>
              <!--/ col -->

               <!-- col -->
               <div class="col-lg-4">
                    <div class="blog-col text-center">
                        <a href="blogdetail.php"><img src="img/data/blogimg02.jpg" alt="" class="img-fluid"></a>
                        <article>
                            <h2 class="h6 pt-2">
                                <a href="blogdetail.php">Blog News Name will be here</a>
                            </h2>
                            <ul class="list-seperator nav justify-content-center">
                                <li><a>Admin</a> </li>
                                <li><a>Posted on 16 Nov 2019</a> </li>
                            </ul>
                            <p class="text-center"> The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested.</p>
                        </article>
                    </div>
              </div>
              <!--/ col -->

               <!-- col -->
               <div class="col-lg-4">
                    <div class="blog-col text-center">
                        <a href="blogdetail.php"><img src="img/data/blogimg03.jpg" alt="" class="img-fluid"></a>
                        <article>
                            <h2 class="h6 pt-2">
                                <a href="blogdetail.php">Blog News Name will be here</a>
                            </h2>
                            <ul class="list-seperator nav justify-content-center">
                                <li><a>Admin</a> </li>
                                <li><a>Posted on 16 Nov 2019</a> </li>
                            </ul>
                            <p class="text-center"> The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested.</p>
                        </article>
                    </div>
              </div>
              <!--/ col -->

               <!-- col -->
               <div class="col-lg-4">
                    <div class="blog-col text-center">
                        <a href="blogdetail.php"><img src="img/data/blogimg04.jpg" alt="" class="img-fluid"></a>
                        <article>
                            <h2 class="h6 pt-2">
                                <a href="blogdetail.php">Blog News Name will be here</a>
                            </h2>
                            <ul class="list-seperator nav justify-content-center">
                                <li><a>Admin</a> </li>
                                <li><a>Posted on 16 Nov 2019</a> </li>
                            </ul>
                            <p class="text-center"> The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested.</p>
                        </article>
                    </div>
              </div>
              <!--/ col -->

               <!-- col -->
               <div class="col-lg-4">
                    <div class="blog-col text-center">
                        <a href="blogdetail.php"><img src="img/data/blogimg05.jpg" alt="" class="img-fluid"></a>
                        <article>
                            <h2 class="h6 pt-2">
                                <a href="blogdetail.php">Blog News Name will be here</a>
                            </h2>
                            <ul class="list-seperator nav justify-content-center">
                                <li><a>Admin</a> </li>
                                <li><a>Posted on 16 Nov 2019</a> </li>
                            </ul>
                            <p class="text-center"> The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested.</p>
                        </article>
                    </div>
              </div>
              <!--/ col -->

               <!-- col -->
               <div class="col-lg-4">
                    <div class="blog-col text-center">
                        <a href="blogdetail.php"><img src="img/data/blogimg06.jpg" alt="" class="img-fluid"></a>
                        <article>
                            <h2 class="h6 pt-2">
                                <a href="blogdetail.php">Blog News Name will be here</a>
                            </h2>
                            <ul class="list-seperator nav justify-content-center">
                                <li><a>Admin</a> </li>
                                <li><a>Posted on 16 Nov 2019</a> </li>
                            </ul>
                            <p class="text-center"> The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested.</p>
                        </article>
                    </div>
              </div>
              <!--/ col -->
              </div>
              <!--/ row -->
           </div>
           <!--/ container -->
        </div>
        <!--/ sub page body -->
    </main>

    <!--/ sub page main -->
    <!--footer -->
    <?php include 'footer.php' ?>
    <!--/ footer -->  
    <?php include 'footerscripts.php'?>
</body>

</html>