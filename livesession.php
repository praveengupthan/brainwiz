<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Brainwiz</title>
    <link rel="icon" type="image/png" sizes="32x32" href="img/favicon-32x32.png">
    <!-- style sheets -->
    <?php include 'headerstyles.php' ?>
</head>

<body>
   
    <!-- container  -->
    <div class="container-fluid">
        <!-- row -->
        <div class="row">
            <!-- left col -->
            <div class="col-md-8">
                <h3 class="py-3"> <span class="icon-chevron-circle-left h4 pr-3"></span> Today Live Session </h3>
                <!-- video section -->
                <div class="liveSession">
                     <h4 class="h3 pb-4">Number System Day 2</h4>
                     <h6>Lession will go live in </h6>
                     <section class="wrapper">
                        <section id="countdown-container" class="countdown-container">
                            <article id="js-countdown" class="countdown">
                                <section id="js-days" class="number"></section>
                                <section id="js-separator" class="separator">:</section>
                                <section id="js-hours" class="number"></section>
                                <section id="js-separator" class="separator">:</section>
                                <section id="js-minutes" class="number"></section>
                                <section id="js-separator" class="separator">:</section>
                                <section id="js-seconds" class="number"></section>
                            </article>
                        </section>
                     </section>   
                   
                     
                          <a href="javascript:void(0)" class="newSubmitButton w-50 ">Action Button</a>
                      
                            
                </div>
                <!--/ video section -->
               
            </div>
            <!--/ left col -->

             <!-- right col -->
             <div class="col-md-4">
                <h4 class="h3 py-4">Syllabus</h4>

                <!-- table -->
                <div class="syllabus_rt">
                    <table class="table table-striped">
                        <tr>
                          <td width="2%"><span class="icon-play-circle icomoon"></span></td>
                          <td>
                              <p class="pb-1 mb-0"><a href="javascript:void(0)">Course Introduction</a></p>
                              <p class="small text-right"><i>6 Oct 2020, at 06:30 PM</i></p>
                          </td>
                        </tr>

                        <tr>
                          <td width="2%"><span class="icon-play-circle icomoon"></span></td>
                          <td>
                              <p class="pb-1 mb-0"><a href="javascript:void(0)">What is Angular?</a></p>
                              <p class="small text-right"><i>6 Oct 2020, at 06:30 PM</i></p>
                          </td>
                        </tr>

                        <tr>
                          <td width="2%"><span class="icon-play-circle icomoon"></span></td>
                          <td>
                              <p class="pb-1 mb-0"><a href="javascript:void(0)">Join our Online Learning Community</a></p>
                              <p class="small text-right"><i>6 Oct 2020, at 06:30 PM</i></p>
                          </td>
                        </tr>

                        <tr>
                          <td width="2%"><span class="icon-play-circle icomoon"></span></td>
                          <td>
                              <p class="pb-1 mb-0"><a href="javascript:void(0)">Angular vs Angular 2 vs Angular 9</a></p>
                              <p class="small text-right"><i>6 Oct 2020, at 06:30 PM</i></p>
                          </td>
                        </tr>

                        <tr>
                          <td width="2%"><span class="icon-play-circle icomoon"></span></td>
                          <td>
                              <p class="pb-1 mb-0"><a href="javascript:void(0)">CLI Deep Dive & Troubleshooting</a></p>
                              <p class="small text-right"><i>6 Oct 2020, at 06:30 PM</i></p>
                          </td>
                        </tr>

                        <tr>
                          <td width="2%"><span class="icon-play-circle icomoon"></span></td>
                          <td>
                              <p class="pb-1 mb-0"><a href="javascript:void(0)">Join our Online Learning Community Lorem ipsum dolor, sit amet consectetur adipisicing elit. Quam, dolorum.</a></p>
                              <p class="small text-right"><i>6 Oct 2020, at 06:30 PM</i></p>
                          </td>
                        </tr>

                    </table>
                </div>
                <!--/ table -->

             </div>
            <!--/ right col -->


        </div>
        <!--/ row -->
    </div>
    <!--/ container -->



    <?php include 'footerscripts.php'?>

    <script>
       $(function() {
  
  var targetDate  = new Date(Date.UTC(2017, 3, 01));
  var now   = new Date();

  window.days = daysBetween(now, targetDate);
  var secondsLeft = secondsDifference(now, targetDate);
  window.hours = Math.floor(secondsLeft / 60 / 60);
  secondsLeft = secondsLeft - (window.hours * 60 * 60);
  window.minutes = Math.floor(secondsLeft / 60 );
  secondsLeft = secondsLeft - (window.minutes * 60);
  console.log(secondsLeft);
  window.seconds = Math.floor(secondsLeft);

  startCountdown();
});
var interval;

function daysBetween( date1, date2 ) {
  //Get 1 day in milliseconds
  var one_day=1000*60*60*24;

  // Convert both dates to milliseconds
  var date1_ms = date1.getTime();
  var date2_ms = date2.getTime();

  // Calculate the difference in milliseconds
  var difference_ms = date2_ms - date1_ms;
    
  // Convert back to days and return
  return Math.round(difference_ms/one_day); 
}

function secondsDifference( date1, date2 ) {
  //Get 1 day in milliseconds
  var one_day=1000*60*60*24;

  // Convert both dates to milliseconds
  var date1_ms = date1.getTime();
  var date2_ms = date2.getTime();
  var difference_ms = date2_ms - date1_ms;
  var difference = difference_ms / one_day; 
  var offset = difference - Math.floor(difference);
  return offset * (60*60*24);
}



function startCountdown() {
  $('#input-container').hide();
  $('#countdown-container').show();
  
  displayValue('#js-days', window.days);
  displayValue('#js-hours', window.hours);
  displayValue('#js-minutes', window.minutes);
  displayValue('#js-seconds', window.seconds);

  interval = setInterval(function() {
    if (window.seconds > 0) {
      window.seconds--;
      displayValue('#js-seconds', window.seconds);
    } else {
      // Seconds is zero - check the minutes
      if (window.minutes > 0) {
        window.minutes--;
        window.seconds = 59;
        updateValues('minutes');
      } else {
        // Minutes is zero, check the hours
        if (window.hours > 0)  {
          window.hours--;
          window.minutes = 59;
          window.seconds = 59;
          updateValues('hours');
        } else {
          // Hours is zero
          window.days--;
          window.hours = 23;
          window.minutes = 59;
          window.seconds = 59;
          updateValues('days');
        }
        // $('#js-countdown').addClass('remove');
        // $('#js-next-container').addClass('bigger');
      }
    }
  }, 1000);
}


function updateValues(context) {
  if (context === 'days') {
    displayValue('#js-days', window.days);
    displayValue('#js-hours', window.hours);
    displayValue('#js-minutes', window.minutes);
    displayValue('#js-seconds', window.seconds);
  } else if (context === 'hours') {
    displayValue('#js-hours', window.hours);
    displayValue('#js-minutes', window.minutes);
    displayValue('#js-seconds', window.seconds);
  } else if (context === 'minutes') {
    displayValue('#js-minutes', window.minutes);
    displayValue('#js-seconds', window.seconds);
  }
}

function displayValue(target, value) {
  var newDigit = $('<span></span>');
  $(newDigit).text(pad(value))
    .addClass('new');
  $(target).prepend(newDigit);
  $(target).find('.current').addClass('old').removeClass('current');
  setTimeout(function() {
    $(target).find('.old').remove();
    $(target).find('.new').addClass('current').removeClass('new');
  }, 900);
}

function pad(number) {
  return ("0" + number).slice(-2);
}
    </script>

</body>

</html>