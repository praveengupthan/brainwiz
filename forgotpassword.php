<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Brainwiz</title>
    <link rel="icon" type="image/png" sizes="32x32" href="img/favicon-32x32.png">
    <!-- style sheets -->
    <?php include 'headerstyles.php' ?>
</head>

<body>
    <!-- sign in -->
    <div class="sign">
        <!-- container fluid -->
        <div class="container-fluid">
            <!-- row -->
            <div class="row align-items-center">
                <!-- left col -->
               <?php include 'sign-leftcol.php'?>
                <!--/left col -->

                <!-- right col -->
                <div class="col-lg-6">                   
                    <div class="signcol">
                        <h2 class="h5">Reset Password</h2>

                        <form>                           
                            <div class="form-group">
                                <input type="text" placeholder="Email / Phone Number" class="form-control" name="" required>
                            </div>
                            <div class="form-group">
                                <input type="submit" value="Reset Password" class="bluebtn w-100 text-uppercase">
                            </div>
                           
                            <div class="form-group text-center">
                                <p>Already have Password? <a class="fblue fbold" href="signin.php">Sign in</a></p>
                            </div>
                            <div class="form-group text-center">
                               <p>On click Reset Password you will get Reset link to your Registered Email.</p>
                            </div>
                        </form>
                    </div>
                </div>
                <!--/ right col -->
            </div>
            <!--/ row -->
        </div>
        <!--/ container fluid -->

    </div>
    <!--/ sign in --> 
    <?php include 'footerscripts.php'?>

</body>

</html>