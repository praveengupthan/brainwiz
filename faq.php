<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Brainwiz</title>
    <link rel="icon" type="image/png" sizes="32x32" href="img/favicon-32x32.png">
    <!-- style sheets -->
    <?php include 'headerstyles.php' ?>
</head>

<body>

    <!-- header -->
   <?php include 'header.php' ?>
    <!--/ header -->
    <!--sub page main -->
    <main class="subpage">
        <!-- sub page header -->
        <div class="page-header">
            <!-- container -->
            <div class="container">
               <!-- row -->
               <div class="row">
                   <div class="col-lg-6">
                       <h1>Faq's </h1>
                   </div>
               </div>
               <!--/ row --> 
            </div>
            <!--/ container -->
        </div>
        <!--/ sub page header -->

         <!-- brudcrumb -->
         <div class="container">
            <!-- row -->
            <div class="row">
                <!-- col -->
                <div class="col-lg-12">                   
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.php">Home</a></li>                        
                        <li class="breadcrumb-item active"><a>Faq's</a></li>
                    </ul>                    
                </div>
                <!--/col -->
            </div>
            <!--/ row -->
        </div>
        <!--/ brudcrumb -->
        <!-- sub page body -->
        <div class="subpage-body">
           <!-- container -->
           <div class="container">
              <!-- row -->
              <div class="row">
               <!-- col 9 -->
                <div class="col-lg-9">
                    <!-- tab -->
                    <div class="parentHorizontalTab simpletabs">
                        <ul class="resp-tabs-list hor_1">
                            <li>General Enquiries</li>
                            <li>Pricing & Plans</li>
                            <li>Our Services</li>
                        </ul>
                        <!-- responsvie container -->
                        <div class="resp-tabs-container hor_1">

                             <!-- general enquireis -->
                             <div class="custom-accord">                              
                                <h3 class="h5 pb-3">General Enquries</h3>
                                <div class="accordion">
                                    <h3 class="panel-title">How can we get the services we need?</h3>
                                    <div class="panel-content">
                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                                    </div>
                                    <h3 class="panel-title">Is there any way to receive it?</h3>
                                    <div class="panel-content">
                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                                    </div>
                                    <h3 class="panel-title">What do I need to do to graduate from a Nanodegree program?</h3>
                                    <div class="panel-content">
                                        <p>You will need to review all the content and successfully complete all projects with a grade of “Meets Specifications” for your specific Nanodegree program as set forth in the curriculum and syllabus. Since you pay month-by-month, it's up to you when you want to plan to graduate.</p>
                                    </div>
                                    <h3 class="panel-title">What is a Nanodegree Program?</h3>
                                    <div class="panel-content">
                                        <p>A Udacity Nanodegree® Program is a unique online educational offering designed to bridge the gap between learning and career goals. We partner with industry leaders and experts who understand what skills are in demand in the applicable job market. Students enroll in a specific course offering (e.g. Intro to Programming) and will be prompted to view the online course as well as complete a series of projects and support courses designed to help them develop job-relevant skills and build a portfolio to show prospective employers. Services related to the Nanodegree program may include classroom mentorship, moderated forums, and project reviews to ensure a personalized experience. Additional or different services may be offered in certain Nanodegree Programs which will be noted in their respective Program Details page.</p>
                                    </div>
                                    <h3 class="panel-title">How can we get the services we need?</h3>
                                    <div class="panel-content">
                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                                    </div>
                                    <h3 class="panel-title">Is there any way to receive it?</h3>
                                    <div class="panel-content">
                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                                    </div>
                                    <h3 class="panel-title">What do I need to do to graduate from a Nanodegree program?</h3>
                                    <div class="panel-content">
                                        <p>You will need to review all the content and successfully complete all projects with a grade of “Meets Specifications” for your specific Nanodegree program as set forth in the curriculum and syllabus. Since you pay month-by-month, it's up to you when you want to plan to graduate.</p>
                                    </div>
                                </div>                             
                            </div>
                            <!--/ general enquiries -->

                             <!-- pricing & enquiryes -->
                             <div class="custom-accord">
                                <h3 class="h5 pb-3">Pricing & Plans</h3>
                                <div class="accordion">
                                    <h3 class="panel-title">How can we get the services we need?</h3>
                                    <div class="panel-content">
                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                                    </div>
                                    <h3 class="panel-title">Is there any way to receive it?</h3>
                                    <div class="panel-content">
                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                                    </div>
                                    <h3 class="panel-title">What do I need to do to graduate from a Nanodegree program?</h3>
                                    <div class="panel-content">
                                        <p>You will need to review all the content and successfully complete all projects with a grade of “Meets Specifications” for your specific Nanodegree program as set forth in the curriculum and syllabus. Since you pay month-by-month, it's up to you when you want to plan to graduate.</p>
                                    </div>
                                    <h3 class="panel-title">What is a Nanodegree Program?</h3>
                                    <div class="panel-content">
                                        <p>A Udacity Nanodegree® Program is a unique online educational offering designed to bridge the gap between learning and career goals. We partner with industry leaders and experts who understand what skills are in demand in the applicable job market. Students enroll in a specific course offering (e.g. Intro to Programming) and will be prompted to view the online course as well as complete a series of projects and support courses designed to help them develop job-relevant skills and build a portfolio to show prospective employers. Services related to the Nanodegree program may include classroom mentorship, moderated forums, and project reviews to ensure a personalized experience. Additional or different services may be offered in certain Nanodegree Programs which will be noted in their respective Program Details page.</p>
                                    </div>
                                    <h3 class="panel-title">How can we get the services we need?</h3>
                                    <div class="panel-content">
                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                                    </div>
                                    <h3 class="panel-title">Is there any way to receive it?</h3>
                                    <div class="panel-content">
                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                                    </div>
                                    <h3 class="panel-title">What do I need to do to graduate from a Nanodegree program?</h3>
                                    <div class="panel-content">
                                        <p>You will need to review all the content and successfully complete all projects with a grade of “Meets Specifications” for your specific Nanodegree program as set forth in the curriculum and syllabus. Since you pay month-by-month, it's up to you when you want to plan to graduate.</p>
                                    </div>
                                </div>                               
                            </div>
                            <!--/ pricing & enquiries -->

                             <!-- Services -->
                             <div class="custom-accord">
                                <h3 class="h5 pb-3">Services</h3>
                                <div class="accordion">
                                    <h3 class="panel-title">How can we get the services we need?</h3>
                                    <div class="panel-content">
                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                                    </div>
                                    <h3 class="panel-title">Is there any way to receive it?</h3>
                                    <div class="panel-content">
                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                                    </div>
                                    <h3 class="panel-title">What do I need to do to graduate from a Nanodegree program?</h3>
                                    <div class="panel-content">
                                        <p>You will need to review all the content and successfully complete all projects with a grade of “Meets Specifications” for your specific Nanodegree program as set forth in the curriculum and syllabus. Since you pay month-by-month, it's up to you when you want to plan to graduate.</p>
                                    </div>
                                    <h3 class="panel-title">What is a Nanodegree Program?</h3>
                                    <div class="panel-content">
                                        <p>A Udacity Nanodegree® Program is a unique online educational offering designed to bridge the gap between learning and career goals. We partner with industry leaders and experts who understand what skills are in demand in the applicable job market. Students enroll in a specific course offering (e.g. Intro to Programming) and will be prompted to view the online course as well as complete a series of projects and support courses designed to help them develop job-relevant skills and build a portfolio to show prospective employers. Services related to the Nanodegree program may include classroom mentorship, moderated forums, and project reviews to ensure a personalized experience. Additional or different services may be offered in certain Nanodegree Programs which will be noted in their respective Program Details page.</p>
                                    </div>
                                    <h3 class="panel-title">How can we get the services we need?</h3>
                                    <div class="panel-content">
                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                                    </div>
                                    <h3 class="panel-title">Is there any way to receive it?</h3>
                                    <div class="panel-content">
                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                                    </div>
                                    <h3 class="panel-title">What do I need to do to graduate from a Nanodegree program?</h3>
                                    <div class="panel-content">
                                        <p>You will need to review all the content and successfully complete all projects with a grade of “Meets Specifications” for your specific Nanodegree program as set forth in the curriculum and syllabus. Since you pay month-by-month, it's up to you when you want to plan to graduate.</p>
                                    </div>
                                </div>  
                            </div>
                                <!--/ Services --> 
                        </div>
                        <!-- responsive container -->
                    </div>
                    <!--/ taB -->               
                  
                </div>         
               <!--/ col 9-->

               <!-- col 3-->
               <div class="col-lg-3">
                   <div class="career-block text-center">
                        <img src="img/premade-image-09.png">
                        <h2 class="h5 py-3">Advance Your Career</h2>
                        <p class="text-center">Enter your email below to download one of our free career guides</p>
                        <form>
                            <div class="form-group">
                                <input class="form-control" type="text" placeholder="Enter your Email"> 
                            </div>
                            <div class="form-group">
                                <input class="bluebtn w-100" type="submit" value="Submit"> 
                            </div>
                        </form>
                        <p class="small text-left">I consent and agree to receive email marketing communications from Udacity.  " that by clicking “Submit,” my data will be used in accordance with the  Udacity Terms of Use and Privacy Policy.</p>
                   </div>
               </div>
               <!--/ col 3-->
             
              

              



              </div>
              <!--/ row -->
           </div>
           <!--/ container -->
        </div>
        <!--/ sub page body -->
    </main>

    <!--/ sub page main -->
    <!--footer -->
    <?php include 'footer.php' ?>
    <!--/ footer -->  
    <?php include 'footerscripts.php'?>
</body>

</html>