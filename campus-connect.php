<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Campus Connect</title>
    <link rel="icon" type="image/png" sizes="32x32" href="img/favicon-32x32.png">
    <!-- style sheets -->
    <?php include 'headerstyles.php'?>
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
</head>

<body>
    <!-- header -->
   <?php include 'header.php'?>
    <!--/ header -->
    <!--sub page main -->
    <main class="subpage">
        <section class="campus-connect">
            <div class="top-banner">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 align-self-center">
                            <div>
                                <h1 class="fbold pb-2">Get your students hiered in the top tech companies</h1>
                                <h6>We stand firm in our belief that every aspiring engineer deserves the necessary technical skills for a successful career.</h6>
                            </div>
                            <div>
                                <a class="bluebtn d-inline-block" href="javascript:void(0)" data-toggle="modal" data-target="#enquiryNow">Get Started</a>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <img src="img/getconnectimg.webp" alt="" class="img-fluid">
                        </div>
                    </div>
                </div>
            </div>

            <!-- trusted institutes-->
            <section class="trusted-institutes">
                <div class="container">
                    <div class="row justify-content-center pb-5">
                        <div class="col-md-8 text-center">
                            <h2 class="h2 fbold">Trusted by top institutes</h2>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2">
                            <div class="trusted-logo">
                                <img src="img/trusted-logos/presidency.svg" alt="" class="img-fluid">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="trusted-logo">
                                <img src="img/trusted-logos/gear.svg" alt="" class="img-fluid">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="trusted-logo">
                                <img src="img/trusted-logos/narayana.svg" alt="" class="img-fluid">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="trusted-logo">
                                <img src="img/trusted-logos/dps.svg" alt="" class="img-fluid">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="trusted-logo">
                                <img src="img/trusted-logos/orchids.svg" alt="" class="img-fluid">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="trusted-logo">
                                <img src="img/trusted-logos/pw.svg" alt="" class="img-fluid">
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="trusted-logo">
                                <img src="img/trusted-logos/allen.svg" alt="" class="img-fluid">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="trusted-logo">
                                <img src="img/trusted-logos/lpu.svg" alt="" class="img-fluid">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="trusted-logo">
                                <img src="img/trusted-logos/akash.svg" alt="" class="img-fluid">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="trusted-logo">
                                <img src="img/trusted-logos/masters-union.svg" alt="" class="img-fluid">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="trusted-logo">
                                <img src="img/trusted-logos/scindia.svg" alt="" class="img-fluid">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="trusted-logo">
                                <img src="img/trusted-logos/nxt wave.svg" alt="" class="img-fluid">
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="trusted-logo">
                                <img src="img/trusted-logos/whiteHatJr.svg" alt="" class="img-fluid">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="trusted-logo">
                                <img src="img/trusted-logos/resonance.svg" alt="" class="img-fluid">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="trusted-logo">
                                <img src="img/trusted-logos/scaler.svg" alt="" class="img-fluid">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="trusted-logo">
                                <img src="img/trusted-logos/t-john.svg" alt="" class="img-fluid">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="trusted-logo">
                                <img src="img/trusted-logos/manavrachna.svg" alt="" class="img-fluid">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="trusted-logo">
                                <img src="img/trusted-logos/dps.svg" alt="" class="img-fluid">
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!--/ trusted institutes -->

             <!-- partner with us -->
             <section class="partnerwithus">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-md-8 text-center">
                            <h2 class="h2 fbold">Why Partner with us</h2>
                            <p>Partnering with Smart Interviews gives your students an opportunity to get placed in top tech companies. Our course is designed to provide the best educational expertise that makes students stand out from the competition</p>
                        </div>
                    </div>

                    <div class="row pt-5">
                        <div class="col-md-3">
                            <div class="highlet-col">
                                <h2>120+</h2>
                                <h5>Happy Academies</h5>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="highlet-col">
                                <h2>200+</h2>
                                <h5>Associated Faculties</h5>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="highlet-col">
                                <h2>1M+</h2>
                                <h5>Active Learners</h5>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="highlet-col">
                                <h2>4K</h2>
                                <h5>Courses Launched</h5>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!--/ partner with us -->

            <!-- why choose brainwiz courses -->
            <section class="upskillingcourses">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4">
                            <img src="img/whybrainwizimg.png" alt="" class="img-fluid rounded">
                        </div>
                        <div class="col-md-8 align-self-center">
                            <h6>Expertly structured courses for you specific upskilling needs</h6>

                            <h2 class="h2 fbold">Why choose Brainwiz Courses?</h2>

                            <ul class="pt-4">
                                <li class="d-flex mb-4">
                                    <div class="iconwhy"><span class="icon-flag icomoon"></span></div>
                                    <h6 class="ml-3 align-self-center">Build consistency with structured courses</h6>
                                </li>
                                <li class="d-flex mb-4">
                                    <div class="iconwhy"><span class="icon-flag icomoon"></span></div>
                                    <h6 class="ml-3 align-self-center">Self-paced learning with live 1:1 doubt support</h6>
                                </li>
                                <li class="d-flex mb-4">
                                    <div class="iconwhy"><span class="icon-flag icomoon"></span></div>
                                    <h6 class="ml-3 align-self-center">Designed by MAANG Techies turned Educators</h6>
                                </li>
                                <li class="d-flex mb-4">
                                    <div class="iconwhy"><span class="icon-flag icomoon"></span></div>
                                    <h6 class="ml-3 align-self-center">Top Courses with 7 years of Proven Success</h6>
                                </li>
                            </ul>
                            <div>
                            <a class="bluebtn d-inline-block" href="javascript:void(0)">Explore Upskilling Courses</a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!--/ why chose upskilliung courses -->

             <!-- programs images -->
             <section class="programs-gallery">
                <div class="container">
                    <div class="row justify-content-center pb-5">
                        <div class="col-md-8 text-center">
                            <h2 class="h2 fbold">The Brainwiz Campus: Live and breathe Bengaluru's startup ecosystem</h2>
                            <p>New age space with 50,000 square feet of common spaces, in house gym and football turf!</p>
                        </div>
                    </div>
                </div>
                <div class="marquee">
                    <div class="marquee--inner">
                        <div class="d-flex marimg">
                            <img src="img/galimg/1.png" alt="" class="img-fluid">
                            <img src="img/galimg/2.png" alt="" class="img-fluid">
                            <img src="img/galimg/3.png" alt="" class="img-fluid">
                            <img src="img/galimg/4.png" alt="" class="img-fluid">
                            <img src="img/galimg/5.png" alt="" class="img-fluid">
                            <img src="img/galimg/6.png" alt="" class="img-fluid">
                            <img src="img/galimg/7.png" alt="" class="img-fluid">
                            <img src="img/galimg/8.png" alt="" class="img-fluid">
                            <img src="img/galimg/9.png" alt="" class="img-fluid">
                            <img src="img/galimg/10.png" alt="" class="img-fluid">
                        </div>
                    </div>
                </div>
            </section>
            <!--/ programs images -->

            <!-- our success stories -->
            <div class="success-stories">
                <!-- container -->
                <div class="container">
                    <!-- title row -->
                    <div class="row justify-content-center titlerow">
                        <div class="col-lg-6 text-center">
                            <h3>Our Ninjas at <span>top tech companies</span></h3>
                            <p>Students Trained by Pavan Jaiswal 2019 Batch <a href="students.php" class="fgreen d-inline-block pl-2"> View All</a></p>
                        </div>
                    </div>
                    <!--/ title row -->
                </div>
                <!--/ container -->
                <!-- container fluid -->
                <div class="container">
                    <!-- slick -->
                    <!-- slick slider row -->
                    <div class="successStories custom-slick students-slick">
                        <!-- slide -->
                        <div class="slide">
                            <a href="javascript:void(0)" data-toggle="modal" data-target="#student-testimonial">
                                <img src="img/data/student01.jpg" alt="" class="img-fluid">
                                <h4>Praveen guptha</h4>
                                <div class="companylogo"><img src="img/logos/company01.png" alt=""></div>
                            </a>
                        </div>
                        <!--/ slide -->
                        <!-- slide -->
                        <div class="slide">
                            <a href="javascript:void(0)">
                                <img src="img/data/student02.jpg" alt="" class="img-fluid">
                                <h4>Pavan Jaiswal</h4>
                                <div class="companylogo"><img src="img/logos/company02.png" alt=""></div>
                            </a>
                        </div>
                        <!--/ slide -->
                        <!-- slide -->
                        <div class="slide">
                            <a href="javascript:void(0)">
                                <img src="img/data/student03.jpg" alt="" class="img-fluid">
                                <h4>Sarwani</h4>
                                <div class="companylogo"><img src="img/logos/company03.png" alt=""></div>
                            </a>
                        </div>
                        <!--/ slide -->
                        <!-- slide -->
                        <div class="slide">
                            <a href="javascript:void(0)">
                                <img src="img/data/student04.jpg" alt="" class="img-fluid">
                                <h4>Kiran Kumar</h4>
                                <div class="companylogo"><img src="img/logos/company04.png" alt=""></div>
                            </a>
                        </div>
                        <!--/ slide -->

                        <!-- slide -->
                        <div class="slide">
                            <a href="javascript:void(0)">
                                <img src="img/data/student05.jpg" alt="" class="img-fluid">
                                <h4>Suresh Varma</h4>
                                <div class="companylogo"><img src="img/logos/company05.png" alt=""></div>
                            </a>
                        </div>
                        <!--/ slide -->

                        <!-- slide -->
                        <div class="slide">
                            <a href="javascript:void(0)">
                                <img src="img/data/student06.jpg" alt="" class="img-fluid">
                                <h4>Ragini</h4>
                                <div class="companylogo"><img src="img/logos/company06.png" alt=""></div>
                            </a>
                        </div>
                        <!--/ slide -->

                        <!-- slide -->
                        <div class="slide">
                            <a href="javascript:void(0)">
                                <img src="img/data/student01.jpg" alt="" class="img-fluid">
                                <h4>Ramesh Kumar</h4>
                                <div class="companylogo"><img src="img/logos/company07.png" alt=""></div>
                            </a>
                        </div>
                        <!--/ slide -->

                        <!-- slide -->
                        <div class="slide">
                            <a href="javascript:void(0)">
                                <img src="img/data/student02.jpg" alt="" class="img-fluid">
                                <h4>Acaharya Upadhoya</h4>
                                <div class="companylogo"><img src="img/logos/company08.png" alt=""></div>
                            </a>
                        </div>
                        <!--/ slide -->
                    </div>
                    <!--/ slick slider row -->
                    <!--/ slick -->
                </div>
                <!--/ container fluid -->
            </div>
            <!--/ our success stories -->

            <!-- fouder -->
            <section class="founderVideo">
                <div class="facultysec">
                    <div class="container">
                        <h2 class="h2 fbold text-center pb-4">Faculty Spotlight, Founder & Instructor</h2>
                    </div>
                    <div class="container">
                        <div class="row py-3 justify-content-between">
                            <div class="col-md-5 align-self-center">
                                <h3 class="fbold h5">Learn Operations Management from Prof. Achal Bassamboo</h3>
                                <ul class="list-content pt-3 px-0">
                                    <li>Charles E. Morrison Professor of Decision Science</li>
                                    <li>Professor of Operations & Co-Director of MMM Program at Kellogg School of Management</li>
                                    <li>PhD Operations Management from Stanform University</li>
                                </ul>
                            </div>
                            <div class="col-md-6">
                                <div class="facvidoe">
                                    <iframe class="rounded" width="100%" height="300" src="https://www.youtube.com/embed/_NlS7bnLnn0" title="Students Gave Only Positive Feedback About the Course | Placement Officer about Smart Coder Course" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!--/ founder -->

            <!-- platform -->
            <section class="platform services-home">
                <div class="container">
                    <div class="text-center py-3">
                        <h2 class="h2 fbold text-center">Make Coding your ultimate superpower</h2>
                        <p>A coding boot camp that will open your gateway to endless opportunities in the world of coding</p>
                    </div>
                    <div class="row justify-content-center d-none">
                        <div class="col-md-6">
                            <img src="img/dbgif.png" alt="" class="img-fluid">
                        </div>
                    </div>
                     <!-- row -->
                    <div class="row pt-5">
                        <!-- col -->
                        <div class="col-lg-3 text-center">
                            <div class="service-col">
                                <a data-toggle="modal" data-target="#weeklyschedule" href="javascript:void(0)"><span class="icon-timetable icomoon"></span></a>
                                <h4>2 way Interactive Live Sessions</h4>
                                <p>We design SEO friendly eCommerce website to attract your audience with customized modules </p>
                            </div>
                        </div>
                        <!--/ col -->
                        <!-- col -->
                        <div class="col-lg-3 text-center">
                            <div class="service-col">
                                <a href="video-tutorials.php"><span class="icon-youtube icomoon"></span></a>
                                <h4>Regular Mock Tests to Speed improve</h4>
                                <p>Personalized mobile and desktop app for test series, payment collection and secure video streaming</p>
                            </div>
                        </div>
                        <!--/ col -->
                        <!-- col -->
                        <div class="col-lg-3 text-center">
                            <div class="service-col">
                                <a href="testseries.php"><span class="icon-computer icomoon"></span></a>
                                <h4>Hands on Learning Experience</h4>
                                <p>Stream your video lectures securely and without any lag with your personalized mobile and desktop app</p>
                            </div>
                        </div>
                        <!--/ col -->
                        <!-- col -->
                        <div class="col-lg-3 text-center">
                            <div class="service-col">
                                <a href="practise-test.php"><span class="icon-wedding-planning icomoon"></span></a>
                                <h4>Certificate of Completion</h4>
                                <p>Improve your speed & accuracy by Topic wise Tests. </p>
                            </div>
                        </div>
                        <!--/ col -->
                    </div>
                    <!--/ row -->
                </div>
            </section>
            <!--/ platform -->

            <!-- tab -->
            <div class="giftab">
                <div class="container">
                        <h2 class="h2 fbold text-center py-5">A 3-stage learning model to turn you into a Coding Brainwiz</h2>

                        <!-- tab-->
                        <div class="tab-custom pt-3">
                        <div class="row">
                            <div class="col-3">
                                <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                                    <a class="nav-link active" id="v-pills-home-tab" data-toggle="pill" data-target="#v-pills-home" type="button" role="tab" aria-controls="v-pills-home" aria-selected="true">
                                        <h5>Learn</h5>
                                        <p>Experience seamless learning with problem solving modules, leaderboard and awards.</p>
                                    </a>
                                    <a class="nav-link" id="v-pills-profile-tab" data-toggle="pill" data-target="#v-pills-profile" type="button" role="tab" aria-controls="v-pills-profile" aria-selected="false">
                                        <h5>Excel</h5>
                                        <p>Track your skill level and make meaningful progress for you to grow</p>
                                    </a>
                                    <a class="nav-link" id="v-pills-messages-tab" data-toggle="pill" data-target="#v-pills-messages" type="button" role="tab" aria-controls="v-pills-messages" aria-selected="false">
                                        <h5>Standout</h5>
                                        <p>Standout to recruiters, showcase ratings, get feedback and interview insights.</p>
                                    </a>
                                </div>
                            </div>
                            <div class="col-9">
                                <div class="tab-content" id="v-pills-tabContent">
                                    <div class="tab-pane fade show active" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">
                                        <img src="img/web_standout-33057.webp" alt="" class="img-fluid">
                                    </div>
                                    <div class="tab-pane fade" id="v-pills-profile" role="tabpanel" aria-labelledby="v-pills-profile-tab">
                                        <img src="img/web_excel-33055.webp" alt="" class="img-fluid">
                                    </div>
                                    <div class="tab-pane fade" id="v-pills-messages" role="tabpanel" aria-labelledby="v-pills-messages-tab">
                                        <img src="img/web_learn-33056.webp" alt="" class="img-fluid">
                                    </div>

                                </div>
                            </div>
                            </div>

                        </div>
                        <!--/ tab -->
                </div>
            </div>
            <!--/ tab -->

            <!-- advantages -->
            <section class="advantages">
                <div class="container">
                    <h2 class="h2 fbold text-center py-5">The Brainwiz advantage</h2>

                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td align="center">Brainwiz</td>
                                    <td align="center">Free resources</td>
                                    <td align="center">Other courses</td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Structured + problem solving based</td>
                                    <td align="center">
                                        <img src="img/check.svg" alt="" style="width:20px;">
                                    </span></td>
                                    <td align="center"><span class="icon-close"></span></td>
                                    <td align="center"><span class="icon-check"></span></td>
                                </tr>
                                <tr>
                                    <td>Fastest 1:1 doubt support</td>
                                    <td align="center">
                                        <img src="img/check.svg" alt="" style="width:20px;">
                                    </span></td>
                                    <td align="center"><span class="icon-close"></span></td>
                                    <td align="center"><span class="icon-close"></span></td>
                                </tr>
                                <tr>
                                    <td>Integrated prep platform</td>
                                    <td align="center">
                                        <img src="img/check.svg" alt="" style="width:20px;">
                                    </span></td>
                                    <td align="center"><span class="icon-close"></span></td>
                                    <td align="center"><span class="icon-close"></span></td>
                                </tr>
                                <tr>
                                    <td>Profiles highlighted on Naukri</td>
                                    <td align="center">
                                        <img src="img/check.svg" alt="" style="width:20px;">
                                    </span></td>
                                    <td align="center"><span class="icon-close"></span></td>
                                    <td align="center"><span class="icon-close"></span></td>
                                </tr>
                            </tbody>
                        </table>

                    </div>

                    <!-- scale -->
                    <div class="pb-3">
                        <p class="pb-0 mb-0"><small>Your dream role, faster and with confidence!</small></p>
                        <div class="progress-container">
                            <div class="progress-bar">Brainwiz 100%</div>
                        </div>
                    </div>

                    <div>
                        <p class="pb-0 mb-0"><small>Average role, under-confident</small></p>
                        <div class="othersprogress">
                            <div class="progress-bar2">Others</div>
                        </div>
                    </div>
                    <!--/ scale -->
                </div>
            </section>
            <!--/ advantages -->

             <!-- partner with us-->
             <section class="partnerwithus-videos">
                <div class="container">
                    <div class="row justify-content-center pb-2">
                        <div class="col-md-8 text-center">
                            <h2 class="h2 fbold">Our Partnerships</h2>
                            <p>Our partners offer their viewpoints on teaming up with us.</p>
                        </div>
                    </div>

                    <div class="videos-gallery">
                          <!-- slick slider row -->
                            <div class="partnersvideos custom-slick students-slick">
                                <!-- slide -->
                                <div class="slide">
                                    <iframe width="100%" height="280" src="https://www.youtube.com/embed/_NlS7bnLnn0" title="Students Gave Only Positive Feedback About the Course | Placement Officer about Smart Coder Course" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
                                </div>
                                <!--/ slide -->
                                <!-- slide -->
                                <div class="slide">
                                    <iframe width="100%" height="280" src="https://www.youtube.com/embed/_NlS7bnLnn0" title="Students Gave Only Positive Feedback About the Course | Placement Officer about Smart Coder Course" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
                                </div>
                                <!--/ slide -->

                                <!-- slide -->
                                <div class="slide">
                                    <iframe width="100%" height="280" src="https://www.youtube.com/embed/_NlS7bnLnn0" title="Students Gave Only Positive Feedback About the Course | Placement Officer about Smart Coder Course" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
                                </div>
                                <!--/ slide -->

                                  <!-- slide -->
                                <div class="slide">
                                    <iframe width="100%" height="280" src="https://www.youtube.com/embed/_NlS7bnLnn0" title="Students Gave Only Positive Feedback About the Course | Placement Officer about Smart Coder Course" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
                                </div>
                                <!--/ slide -->
                            </div>
                    </div>

                    <div class="text-center pt-4">
                        <a href="javascript:void(0)" class="bluebtn h5 d-inline-block m-0" data-toggle="modal" data-target="#partnerwithus">Partner with us</a>
                    </div>
                </div>
            </section>
            <!--/ partner with us-->
        </section>




    </main>
    <!--/ sub page main -->
    <!--footer -->
    <?php include 'footer-campus-connect.php'?>
    <!--/ footer -->
    <?php include 'footerscripts.php'?>

<div class="modal fade" id="enquiryNow" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog custom-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Enquiry</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body p-4">
        <p>Please fill these details and we will get in touch with you</p>

        <form>
            <div class="form-group">
                <label for="name">Your Full name</label>
                <input type="text" class="form-control" id="name" placeholder="Your Full Name *">
            </div>

            <div class="form-group">
                <label for="collegename">College Name</label>
                <input type="text" class="form-control" id="collegename" placeholder="College Name *">
            </div>

            <div class="form-group">
                <label for="phonenumber">Phone Number</label>
                <input type="number" class="form-control" id="phonenumber" placeholder="Phone Number *">
            </div>

            <div class="form-group">
                <label for="email">Official Email</label>
                <input type="number" class="form-control" id="email" placeholder="Email *">
            </div>


        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Submit</button>
      </div>
    </div>
  </div>
</div>



<div class="modal fade" id="partnerwithus" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog custom-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Partner with us</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body p-4">
        <p>Please fill these details and we will get in touch with you</p>

        <form>
            <div class="form-group">
                <label for="name">Your Full name</label>
                <input type="text" class="form-control" id="name" placeholder="Your Full Name *">
            </div>

            <div class="form-group">
                <label for="collegename">College Name</label>
                <input type="text" class="form-control" id="collegename" placeholder="College Name *">
            </div>

            <div class="form-group">
                <label for="phonenumber">Phone Number</label>
                <input type="number" class="form-control" id="phonenumber" placeholder="Phone Number *">
            </div>

            <div class="form-group">
                <label for="email">Official Email</label>
                <input type="number" class="form-control" id="email" placeholder="Email *">
            </div>


        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Submit</button>
      </div>
    </div>
  </div>
</div>

</body>

</html>
