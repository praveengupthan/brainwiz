<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Brainwiz</title>
    <link rel="icon" type="image/png" sizes="32x32" href="img/favicon-32x32.png">
    <!-- style sheets -->
    <?php include 'headerstyles.php' ?>
</head>

<body>

    <!-- header -->
   <?php include 'header-postlogin.php' ?>
    <!--/ header -->
    <!--sub page main -->
    <main class="subpage">
        <!-- sub page header -->
        <div class="page-header">
            <!-- container -->
            <div class="container">
               <!-- row -->
               <div class="row">
                   <div class="col-lg-6">
                       <h1>Profile</h1>
                   </div>
               </div>
               <!--/ row --> 
            </div>
            <!--/ container -->
        </div>
        <!--/ sub page header -->

        <!-- brudcrumb -->
        <div class="container">
            <!-- row -->
            <div class="row">
                <!-- col -->
                <div class="col-lg-12">                   
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                        <li class="breadcrumb-item"><a href="user-profile.php">Student Name Will be here</a></li>
                        <li class="breadcrumb-item active"><a>My Courses</a></li>
                    </ul>                    
                </div>
                <!--/col -->
            </div>
            <!--/ row -->
        </div>
        <!--/ brudcrumb -->

        <!-- sub page body -->
        <div class="subpage-body">
           <!-- container -->
           <div class="container">
                <!-- row -->
                <div class="row">
                    <!-- left col 4-->
                    <div class="col-lg-4">
                       <?php include 'userleft-nav.php'?>
                    </div>
                    <!--/ left col 4-->

                    <!-- right container -->
                    <div class="col-lg-8">
                        <div class="right-usersection">
                            <!-- title -->
                            <div class="user-right-title">
                                <h3 class="h4 mb-0">My Courses</h3>
                                <p><small>Your Selected Courses</small></p>
                            </div>
                            <!--/ title -->

                            <!-- right user body -->
                            <div class="user-rightsection">
                                <!--  row -->
                                <div class="row">
                                    <!-- col -->
                                    <div class="col-lg-6">
                                        <div class="course-col position-relative"> 

                                            <a href="premium-course-detail.php">
                                                <img src="img/cocubescourseimg.svg" alt="" class="img-fluid" data-toggle="popover" data-trigger="hover" title="CoCubes" data-content="Campus Recruitment Training is a comprehensive program which is exclusively designed for the graduate students preparing them for recruitment of various IT and NON IT companies, and ensuring that the students are well trained and equipped to get through the companies easily.">
                                            </a>
                                            <article class="p-3 text-center">
                                                <h3><a href="premium-course-detail.php">CoCubes </a></h3>
                                                <p  class="pb-3 text-center">Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptate   </p>
                                                <div class="d-flex justify-content-between">
                                                    <h5 class="h6 fblue">
                                                        <a href="premium-course-detail.php">Start Course</a>
                                                    </h5>
                                                    <h5 class="h6 fblue">
                                                        <a href="premium-course-detail.php">View Syllabus</a>
                                                    </h5>                                                    
                                                </div>
                                            </article>
                                        </div>
                                    </div>
                                    <!--/ col -->

                                     <!-- col -->
                                    <div class="col-lg-6">
                                        <div class="course-col position-relative"> 
                                            <a href="premium-course-detail.php">
                                                <img src="img/cprogramming.svg" alt="" class="img-fluid" data-toggle="popover" data-trigger="hover" title="C Language Programming" data-content="Campus Recruitment Training is a comprehensive program which is exclusively designed for the graduate students preparing them for recruitment of various IT and NON IT companies, and ensuring that the students are well trained and equipped to get through the companies easily.">
                                            </a>
                                            <article class="p-3 text-center">
                                                <h3><a href="premium-course-detail.php">C Language Programming</a></h3>
                                                <p  class="pb-3 text-center">Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptate</p>
                                                <div class="d-flex justify-content-between">
                                                    <h5 class="h6 fblue">
                                                        <a href="premium-course-detail.php">Start Course</a>
                                                    </h5>
                                                    <h5 class="h6 fblue">
                                                        <a href="premium-course-detail.php">View Syllabus</a>
                                                    </h5>                                                    
                                                </div>
                                            </article>
                                        </div>
                                    </div>
                                    <!--/ col -->
                                </div>
                                <!--/ row -->
                            </div>
                            <!--/ right user body -->
                        </div>
                    </div>
                    <!--/ right container -->
                </div>
                <!--/ row -->
           </div>
           <!--/ container -->
        </div>
        <!--/ sub page body -->
    </main>
    <!--/ sub page main -->
    <!--footer -->
    <?php include 'footer.php' ?>
    <!--/ footer -->  
    <?php include 'footerscripts.php'?>

</body>

</html>