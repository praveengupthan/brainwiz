<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>CCat Exam Pattern</title>
    <link rel="icon" type="image/png" sizes="32x32" href="img/favicon-32x32.png">
    <!-- style sheets -->
    <?php include 'headerstyles.php' ?>
</head>

<body>

    <!-- header -->
   <?php include 'header.php' ?>
    <!--/ header -->
    <!--sub page main -->
    <main class="subpage">
        <!-- sub page header -->
        <div class="page-header">
            <!-- container -->
            <div class="container">
               <!-- row -->
               <div class="row">
                   <div class="col-lg-6">
                       <h1>C-Cat Exam </h1>
                   </div>
               </div>
               <!--/ row --> 
            </div>
            <!--/ container -->
        </div>
        <!--/ sub page header -->

        <!-- brudcrumb -->
        <div class="container">
            <!-- row -->
            <div class="row">
                <!-- col -->
                <div class="col-lg-9">                   
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Examinations</a></li>
                        <li class="breadcrumb-item active"><a>C-Cat</a></li>
                    </ul>                    
                </div>
                <!--/col -->
            </div>
            <!--/ row -->
        </div>
        <!--/ brudcrumb -->

        <!-- sub page body -->
        <div class="subpage-body">
           <!-- container -->
           <div class="container">
               <!-- row -->
               <div class="row">
                    <!-- col 8 -->
                    <div class="col-lg-8">
                        <h2 class="h2 pb-2">About C-CAT</h2>                        
                        <p>C-DAC started building Indian Language Computing Solutions with setting up of GIST group (Graphics and Intelligence based Script Technology); National Centre for Software Technology (NCST) set up in 1985 had also initiated work in Indian Language Computing around the same period.</p>
                       
                        <p>Electronic Research and Development Centre of India (ER&DCI) with various constituents starting as adjunct entities of various State Electronic Corporations, had been brought under the hold of Department of Electronics and Telecommunications (now MeitY) in around 1988. They were focusing on various aspects of applied electronics, technology and applications.</p>

                        <p>C-DAC started its education & training activities in 1994 as a spin-off with the passage of time, it grew to large efforts to meet the growing needs of Indian Industry for finishing schools. C-DAC has today emerged as a premier R&D organization in IT&E (Information Technologies and Electronics) in the country working on strengthening national technological capabilities in the context of global developments in the field and responding to change in the market need in selected foundation areas. In that process, C-DAC represents a unique facet working in close junction with Meaty to realize nation’s policy and pragmatic interventions and initiatives in Information Technology. As an institution for high-end Research and Development (R&D), C-DAC has been at the forefront of the Information Technology (IT) revolution, constantly building capacities in emerging/enabling technologies and innovating and leveraging its expertise, calibre, skill sets to develop and deploy IT products and solutions for different sectors of the economy, as per the mandate of its parent, the Ministry of Electronics and Information Technology, Ministry of Communications and Information Technology, Government of India and other stakeholders including funding agencies, collaborators, users and the market-place.</p>

                        <h3 class="h4">C-CAT Training in Hyderabad</h3>
                        <p>For C-CAT exclusive training, BRAINWIZ is the Best C-CAT Training institute in Hyderabad. It gives complete training covering the entire C-CAT updated syllabus and trained thousands of students from PAN India to secure a good score and get entered into C-DAC. After entering into CDAC, they even placed in various MNC’s through this BRAINWIZ Training at Ameerpet, as they get trained on various modules like Quantitative Aptitude, Logical Reasoning, Verbal Ability and Soft skills, which helps the students to clear all the rounds of Interviews. </p>
                       
                        <h3 class="h4">CDAC C-CAT Exam Pattern 2020-2021</h3>

                       <p>In order to understand various aspects of the exam like the type of questions, marking scheme, time duration, etc., candidates can check the CDAC CCAT 2020-2021 exam pattern below. </p>                      

                       <div class="table-responsive">
                        <table class="table">                           
                            <tbody>
                                <tr>
                                    <td>Mode of Exam</td>
                                    <td>Online</td>
                                </tr>
                                <tr>
                                    <td>
                                    Number of Questions
                                    </td>
                                    <td>
                                    50 per paper
                                    </td>
                                </tr>
                                <tr>
                                    <td>Type of Questions</td>
                                    <td>Multiple Choice questions</td>
                                </tr>
                                <tr>
                                    <td>Medium of Exam</td>
                                    <td>English</td>
                                </tr>
                                <tr>
                                    <td>Duration of Exam  </td>
                                    <td>1 hour per paper</td>
                                </tr>
                                <tr>
                                    <td>Total marks</td>
                                    <td>150 per paper</td>
                                </tr>
                                <tr>
                                    <td>Marking Scheme </td>
                                    <td>+3 for every correct answer, -1 for every wrong answer</td>
                                </tr>                                
                            </tbody>                           
                        </table>
                        </div>
                        <h3 class="h4">CDAC C-CAT Syllabus 2020-2021</h3>
                        <p>The Centre for Development of Advanced Computing (CDAC) determines the Common Admission Test (C-CAT) syllabus. All the subjects and topics which are covered in the exam are mentioned in the CDAC CCAT syllabus. The syllabus of C-CAT differs for various PG diploma courses. It is essential for candidates to be well versed with the CDAC CCAT latest syllabus so that their preparation can be completed on time. Knowing the CDAC C-CAT syllabus will allow candidates to prepare efficiently and prevent wasting their time on insignificant topics.</p>

                        <p>The course-wise CDAC CCAT 2019 syllabus is given below so candidates can prepare accordingly. The various PG diploma courses offered have been divided into three sections, namely- section A, B and C. Depending on the category of courses opted by the candidate, he/ she will have to appear for either Section A or A+B or A+B+C. </p>

                        <p>C-CAT has three sections (Section A, Section B, Section C) of one-hour duration each. As shown in Table 1, depending on the category of courses selected by the candidate, he/she will have to either appear for just one test paper (Section A) or two test papers (Section A and Section B) or all the three test papers (Section A, Section B and Section C). The medium of C-CAT is English.</p>

                        <p>CDAC C-CAT Syllabus 2020-2021 Topic</p>

                        <div class="table-responsive">
                            <table class="table">                           
                                <tbody>
                                    <tr>
                                        <td>Section A | Time (1 hour)</td>
                                        <td>English, Quantitative Aptitude & Reasoning</td>
                                        
                                    </tr>
                                    <tr>
                                        <td>Section B | Time (1 hour)</td>
                                        <td>Computer Fundamentals, Data Structures, C Programming, Data Communication & Networking, Object Oriented Programming Concepts & Operating Systems</td> 
                                                                    
                                    </tr>
                                    <tr>
                                        <td>Section C | Time (1 hour)</td>
                                        <td>Computer Architecture, Digital Electronics & Microprocessors</td>  
                                    </tr>                                
                                </tbody>                           
                            </table>
                        </div>

                        <h3 class="h4">Categories of PG Diploma courses and the corresponding test paper(s). </h3>
                        <h3 class="h4">Section A - PG Diploma in Geoinformatics (PG-DGi)</h3>
                        <h3 class="h4">Section A + Section B</h3>                                           

                        <ul class="page-list pb-3">                          
                               <li>PG Diploma in Advanced Computing (PG-DAC)</li>
                               <li>PG Diploma in Mobile Computing (PG-DMC) </li>
                               <li>PG Diploma in System Software Development (PG-DSSD)</li>
                               <li>PG Diploma in IT Infrastructure, Systems & Security (PG-DITISS)</li>
                               <li>PG Diploma in Big Data Analytics (PG-DBDA)</li>
                               <li>PG Diploma in Internet of Things (PG-DIoT)</li>
                               <li>PG Diploma in HPC System Administration (PG-HPCSA)</li>
                        </ul>

                        <h3 class="h4">Section A + Section B + Section C</h3>                                           

                        <ul class="page-list pb-3">                          
                               <li>PG Diploma in Embedded Systems Design (PG-DESD)</li>
                               <li>PG Diploma in VLSI Design (PG-DVLSI) </li>
                               <li>PG Diploma in Biomedical Instrumentation & Health Informatics (PG-DBIHI)</li> 
                        </ul>


                        <h3 class="h4">C-CAT Examination Fee Details</h3>
                        <p>For candidates who opt (in the online application) to use their own laptops for C-CAT, the examination fee for Category I (Section A) is Rs. 1000/-, for Category II (Sections A & B), is Rs. 1200/-, and for Category III (Sections A, B & C) is Rs. 1400/-. A computer usage charge of Rs 200 will be added to the fee if the candidate opts in the online application to use the computer of the C-CAT centre. Candidates may note that the C-CAT examination fee once paid is not refundable.</p>

                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th colspan="2">Fee for Own Laptop Users</th>
                                        <th colspan="2">Fee For Centre's Computer Users</th>  
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>A:</td>
                                        <td>Rs. 1000/- </td>
                                        <td>A:</td> 
                                        <td>Rs. 1200/- </td> 
                                    </tr>
                                    <tr>
                                        <td>A + B:</td>
                                        <td>Rs. 1200/- </td>
                                        <td>A +B:</td> 
                                        <td>Rs. 1400/- </td> 
                                    </tr>
                                    <tr>
                                        <td>A + B + C:</td>
                                        <td>Rs. 1400/-</td>
                                        <td>A + B + C:</td> 
                                        <td>Rs. 1600/- </td> 
                                    </tr>                               
                                </tbody>                           
                            </table>
                        </div>

                        
                      
                        <h2 class="h2">C-CAT Paper Syllabus</h2>
                        <h3 class="h4">Quantitative Aptitude Syllabus</h3>                        
                        <ul class="page-list pb-3">                           
                               <li>1. Probability</li>
                               <li>2. Permutations & Combinations</li>      
                               <li>3. Algebra</li>  
                               <li>4. Averages</li>
                               <li>5. Time Speed & Distance</li>
                               <li>6. Time & Work</li>
                               <li>7. Profit & Loss</li>
                               <li>8.Ratio & Proportion</li>  
                               <li>9. Simple & Compound Interest</li>
                               <li>10. Percentage</li>
                               <li>11. Number Series</li>
                               <li>12. Mixtures & Allegations</li>
                               <li>13. Simplification</li>
                               <li>14. Number System</li>
                               <li>15. Heights and Distances</li>
                               <li>16. Geometry & Menstruation</li>
                               <li>17. Data Sufficiency,</li>
                               <li>18. Logarithms</li>
                               <li>19. Progressions</li>
                               <li>20. LCM and HCL</li>
                               <li>21. Pipes and Cisterns</li>
                               <li>22. Partnership</li>
                               <li>23.Boats and Streams</li>                     
                        </ul>

                        <h3 class="h4">Reasoning Syllabus</h3>                        
                        <ul class="page-list pb-3">                           
                               <li>1. Number Series</li>
                               <li>2. Letter Series</li>      
                               <li>3. Analogies</li>  
                               <li>4. Puzzles</li>
                               <li>5. Syllogisms</li>
                               <li>6. Binary Logic</li>
                               <li>7. Clocks & Calendars</li>
                               <li>8.Cubes & Dice</li>  
                               <li>9. Classification</li>
                               <li>10. Blood Relations</li>
                               <li>11. Coding-Decoding</li>
                               <li>12. Data Sufficiency</li>
                               <li>13. Seating Arrangement</li>
                               <li>14. Venn Diagrams</li>
                               <li>15. Problem Solving</li>
                               <li>16. Coded Inequalities</li>
                               <li>17. Double Line-up</li>
                               <li>18. Logical Deductions</li>
                               <li>19. Routes & Networks</li>
                               <li>20. Grouping & Selections</li>
                               <li>21. Evaluating Course of Action</li>
                               <li>22. Statements and Conclusions</li>
                               <li>23.Mathematical and Computer Operations</li> 
                               <li>23. Critical Reasoning</li>
                               <li>24. Inferences</li>
                               <li>25. Situation Reaction Test</li>
                               <li>26. Decision Making</li>
                               <li>27. Symbols and Notations</li>
                               <li>28. Direction Sense Test</li>
                               <li>29. Logical Sequence Of Words</li>
                               <li>30 .Assertion and Reason</li>
                               <li>31. Verification of Truth of the Statement</li>
                               <li>32. Statements and Assumptions</li>
                               <li>33.Data Interpretation</li>                                                
                        </ul>

                        <h3 class="h4">Verbal Ability Syllabus</h3>                        
                        <ul class="page-list pb-3">                           
                            <li>1. Synonyms</li>
                            <li>2. Antonyms</li>
                            <li>3. Sentence Completion</li>
                            <li>4. Spelling Test</li>
                            <li>5. Passage Completion</li>
                            <li>6. Sentence Arrangement</li>
                            <li>7. Idioms and Phrases</li>
                            <li>8. Para Completion</li>
                            <li>9. Error Correction (Underlined Part)</li>
                            <li>10. Fill in the blanks</li>
                            <li>11. Prepositions</li>
                            <li>12. Active and Passive Voice</li>
                            <li>13. Spotting Errors</li>
                            <li>14. Substitution</li>
                            <li>15. Transformation</li>
                            <li>16. Sentence Improvement</li>
                            <li>17. Joining Sentences</li>
                            <li>18. Error Correction (Phrase in Bold)</li>
                            <li>19. Articles</li>
                            <li>20. Gerunds</li>
                            <li>21. Identify the Errors</li>
                            <li>22. Plural Forms</li>
                            <li>23. Odd Words</li>
                            <li>24. Prepositions</li>
                            <li>25. Suffix</li>
                            <li>26. Tense</li>
                            <li>27. Adjectives</li>
                            <li>28. Homophones</li>
                            <li>29. Identify the Sentences</li>
                            <li>30. Nouns</li>
                            <li>31.Sentence pattern</li>                                           
                        </ul>        
                    </div>
                    <!--/ col 8--> 

                     <!-- course right faq -->
                   <div class="col-lg-4">   
                   <!-- sticky top -->
                   <div class="sticky-top">                     
                        <div class="custom-accord course-faq">
                            <h3 class="h5 pb-3">FAQ’s of CDAC C-CAT 2019</h3>
                            <!-- accordion -->
                            <div class="accordion">
                                <!-- acc-->
                                <h3 class="panel-title">What is C-CAT? </h3>
                                <div class="panel-content">
                                    <p>C-CAT is CDAC’s Common Admission Test (C-CAT). It is an entry level examination for various PG Diploma courses offered by Centre for Development of Advanced computing (C-DAC) conducted at all India level.
                                    </p>
                                </div>
                                <!--/ acc-->
                                <!-- acc-->
                                <h3 class="panel-title">What courses does CDAC offer? </h3>
                                <div class="panel-content">
                                    <p>The Centre for Development of Advanced Computing conducts various Postgraduate Diploma Courses.</p>
                                </div>
                                <!--/ acc-->
                                <!-- acc-->
                                <h3 class="panel-title">How many institutes across India offer admissions through C-CAT?</h3>
                                <div class="panel-content">
                                    <p>There are 30 CDAC Institutes that offer 6 month PG diploma courses across India. </p>                                                                
                                </div>
                                <!--/ acc-->

                                 <!-- acc-->
                                 <h3 class="panel-title">Is CDAC a Government Institute or a private institute?</h3>
                                <div class="panel-content">
                                    <p>CDAC is a Research and Development institute under the Ministry of Electronics & Information Technology (Government of INDIA).</p>                                   
                                </div>
                                <!--/ acc-->

                                <!-- acc-->
                                <h3 class="panel-title">When is the C-CAT Conducted?</h3>
                                <div class="panel-content">
                                    <p>C-CAT is conducted twice a year in the months of June and December.</p>                                   
                                </div>
                                <!--/ acc-->

                                <!-- acc-->
                                <h3 class="panel-title">What are the minimum eligibility criteria for appearing for C-CAT?</h3>
                                <div class="panel-content">
                                    <p>Candidates who have successfully completed their Engineering Degree or MCA or a Post Graduate in Computer Science/IT are eligible for appearing for C-CAT. </p>                                   
                                </div>
                                <!--/ acc-->                               
                            </div>
                            <!--/ accordion -->
                        </div>
                        </div>    
                        <!--/ sticky top -->    
                   </div>
                   <!--/ course right faq -->

               </div>
               <!--/ row -->
           </div>
            <!-- /container -->
        </div>
        <!--/ sub page body -->
    </main>
    <!--/ sub page main -->
    <!--footer -->
    <?php include 'footer.php' ?>
    <!--/ footer -->  
    <?php include 'footerscripts.php'?>

</body>

</html>