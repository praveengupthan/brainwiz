<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>SSC-CGL EXAM 2020</title>
    <link rel="icon" type="image/png" sizes="32x32" href="img/favicon-32x32.png">
    <!-- style sheets -->
    <?php include 'headerstyles.php' ?>
</head>

<body>

    <!-- header -->
   <?php include 'header.php' ?>
    <!--/ header -->
    <!--sub page main -->
    <main class="subpage">
        <!-- sub page header -->
        <div class="page-header">
            <!-- container -->
            <div class="container">
               <!-- row -->
               <div class="row">
                   <div class="col-lg-6">
                       <h1>SSC-CGL EXAM </h1>
                   </div>
               </div>
               <!--/ row --> 
            </div>
            <!--/ container -->
        </div>
        <!--/ sub page header -->

        <!-- brudcrumb -->
        <div class="container">
            <!-- row -->
            <div class="row">
                <!-- col -->
                <div class="col-lg-9">                   
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Examinations</a></li>
                        <li class="breadcrumb-item active"><a>SSC-CGL EXAM</a></li>
                    </ul>                    
                </div>
                <!--/col -->
            </div>
            <!--/ row -->
        </div>
        <!--/ brudcrumb -->

        <!-- sub page body -->
        <div class="subpage-body">
           <!-- container -->
           <div class="container">
               <!-- row -->
               <div class="row">
                    <!-- col 12 -->
                    <div class="col-lg-12">
                        <h2 class="h2 pb-2">SSC-CGL EXAM 2020</h2>                        
                        <p>The Combined Graduate Level (CGL) Exam conducted annually by Staff Selection Commission (SSC) consists of four tiers, i.e., Tier – I, Tier – II, Tier – III and Tier – IV. SSC Combined Graduate Level (CGL) Examination 2019-20 is approaching soon next year. Now students can restart their preparation as SSC will conduct CGL Tier - I Exam from 2nd March to 11th March 2020 and CGL Tier-II & III Exam from 22nd June 2020 to 25th June 2020 in online mode. For cracking all the phases of SSC CGL 2019-20 Exam students must start the preparation with a concrete study plan. The important ingredients for creating a good study plan are: the latest exam pattern and detailed syllabus of the respective phase of the exam. So, we have compiled the detailed syllabus of SSC CGL 2019-20 Exam including Tier – I, Tier – II, Tier – III and Tier – IV with latest exam pattern which will help you in preparing for the exam in a systematic manner.</p>

                        <h2 class="h2 pb-2">SSC CGL Tier – I, Tier – II, Tier – III and Tier – IV Exam Pattern 2019-20</h2>  
                        <p>Indian Air Force (IAF) has released the detailed notification for AFCAT (1) 2020 exam on December 1. According to the notification, the online test of AFCAT is scheduled to be conducted on February 22 and 23, 2020. Interested candidates can fill AFCAT application form 2020 online from December 1 to 30, 2019. Total 249 vacancies will be filled through AFCAT entry. Earlier, IAF had released short notification for AFCAT 2020 exam. </p>

                        <div class="table-responsive">
                        <table class="table">   
                            <thead>
                                <tr>
                                    <th>Tier</th>
                                    <th>Type of Exam</th>
                                    <th>Mode of Exam</th>
                                </tr>
                            </thead>                        
                            <tbody>
                                <tr>
                                    <td>Tier – I </td>
                                    <td>Objective Multiple Choice</td>
                                    <td>Computer Based (Online)</td>
                                </tr>
                                <tr>
                                    <td>Tier – II</td>
                                    <td>Objective Multiple Choice</td>
                                    <td>Computer Based (Online)</td>
                                </tr>
                                <tr>
                                    <td>Tier – III</td>
                                    <td>Descriptive Paper in English or Hindi</td>
                                    <td>Pen and Paper mode (Offline)</td>
                                </tr>
                                <tr>
                                    <td>Tier – IV</td>
                                    <td>Skill Test: Data Entry Speed Test (DEST)/ Computer Proficiency Test (CPT)</td>
                                    <td>Wherever Applicable (Not necessary for all the post)</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>Document Verification</td>
                                    <td>Applicable for all</td>
                                </tr>                                                           
                            </tbody>                           
                        </table>
                        </div>

                        <p>SSC has made some changes in the exam pattern in the past few years. Students must make note of changes in exam pattern and syllabus before beginning the SSC CGL exam preparation. Below are some important points to take note of:</p>

                        <ul class="page-list">
                            <li>Tier –I objective exam will be of 60 minutes duration in online mode.</li>
                            <li>Tier –II objective exam will consist of 4 papers, each paper will be of 60 minutes duration in online mode.</li>
                            <li>Tier – III descriptive exam will be of 60 minutes duration in pen and paper mode.</li>
                            <li>There will be no sectional cut-off.</li>                            
                        </ul>                      
                        
                        <h3 class="h3">SSC CGL 2019-20 Tier – I, Tier – II, Tier – III and Tier – IV Syllabus</h3>    
                        <p>After thoroughly going through the exam pattern of SSC CGL, next step is to understand the detailed syllabus of various phases of the exam. Since the SSC CGL is comprised of 4 different tiers, it is imperative to understand their syllabus independently.</p>                                       

                        <h3 class="h3">SSC CGL 2019-20 Tier - I Complete Syllabus</h3>    
                        <p>SSC CGL 2019-20 Tier-I exam is an objective exam which will be conducted online. The exam comprises of four sections having 100 questions (25 questions in each section) which will account for a total of 200 marks (maximum 50 marks in each section). The time duration of Tier-1 exam will be 60 minutes. The section wise details of the exam are shown in the table given below:</p>

                        <div class="table-responsive">
                        <table class="table">   
                            <thead>
                                <tr>
                                    <th>Section</th>
                                    <th>Questions </th>
                                    <th>Marks</th>
                                    <th>Time</th>
                                </tr>
                            </thead>                        
                            <tbody>
                                <tr>
                                    <td>General Intelligence & Reasoning  </td>
                                    <td>25</td>
                                    <td>50</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>General Awareness  </td>
                                    <td>25</td>
                                    <td>50</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>Quantitative Aptitude  </td>
                                    <td>25</td>
                                    <td>50</td>
                                    <td>60 Minutes (in total)</td>
                                </tr>
                                <tr>
                                    <td>English Language & Comprehension </td>
                                    <td>25</td>
                                    <td>50</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>Total</td>
                                    <td>100</td>
                                    <td>200</td>
                                    <td>&nbsp;</td>
                                </tr>                                                           
                            </tbody>                           
                        </table>
                        </div>
                       
                        <p>There is negative marking in SSC CGL 2019-20 Tier 1 Exam. 0.5 marks will be deducted from every question that is attempted wrong by a candidate in all the sections.</p>
                        <p>The exam duration is 80 minutes for candidates who are visually handicapped and suffering from Cerebral Palsy.</p>

                        <h2 class="h5">Subject-wise Detailed Syllabus of SSC CGL Tier – I 2019-20 Exam</h2>
                        <p>The above-mentioned syllabus was just a brief of SSC CGL Tier-I Exam. Students must go through the detailed syllabus of all the sections and frame a study plan which will be helpful to identify and focus on their weakest topics.</p>

                        <h2 class="h2">Detailed Syllabus of SSC-CGL Tier-I sections:</h2>
                        <h2 class="h2">General Intelligence & Reasoning</h2>
                        <p>General Intelligence and Reasoning includes questions of both verbal and non-verbal type. This component may include questions on analogies, similarities and differences, space visualization, spatial orientation, problem solving, analysis, judgment, decision making, visual memory, discrimination, observation, relationship concepts, arithmetical reasoning and figural classification, arithmetic number series, non-verbal series, coding and decoding, statement conclusion, syllogistic reasoning etc.</p>

                        <p>The topics are, Semantic Analogy, Symbolic/ Number Analogy, Figural Analogy, Semantic Classification, Symbolic/ Number Classification, Figural Classification, Semantic Series, Number Series, Figural Series, Problem Solving, Word Building, Coding & de-coding, Numerical Operations, symbolic Operations, Trends, Space Orientation, Space Visualization, Venn Diagrams, Drawing inferences, Punched hole/ pattern- folding & un-folding, Figural Pattern- folding and completion, Indexing, Address matching, Date & city matching, Classification of centre codes/roll numbers, Small & Capital letters/ numbers coding, decoding and classification, Embedded Figures, Critical thinking, Emotional Intelligence, Social Intelligence, Other sub-topics, if any</p>

                        <h2 class="h2">Quantitative Aptitude Syllabus</h2>
                        <p>Quantitative Aptitude questions will be designed to test the ability of appropriate use of numbers and number sense of the candidate. The scope of the test will be computation of whole numbers, decimals, fractions and relationships between numbers, Profit and Loss, Discount, Partnership Business, Mixture and Alligation, Time and distance, Time & Work, Percentage. Ratio & Proportion, Square roots, Averages, Interest, Basic algebraic identities of School Algebra & Elementary surds, Graphs of Linear Equations, Triangle and its various kinds of centres, Congruence and similarity of triangles, Circle and its chords, tangents, angles subtended by chords of a circle, common tangents to two or more circles, Triangle, Quadrilaterals, Regular Polygons, Circle, Right Prism, Right Circular Cone, Right Circular Cylinder, Sphere, Heights and Distances, Histogram, Frequency polygon, Bar diagram & Pie chart, Hemispheres, Rectangular Parallelepiped, Regular Right Pyramid with triangular or square base, Trigonometric ratio, Degree and Radian Measures, Standard Identities, Complementary angles.</p>

                        <h2 class="h2">English Syllabus</h2>
                        <p>This section measures Candidates' ability to understand correct English, his/ her basic comprehension and writing ability, etc. would be tested. This section may include questions on Phrases and Idioms, One word Substitution, Sentence Correction, Error Spotting, Fill in the Blanks, Spellings Correction, Reading Comprehension, Synonyms-Antonyms, Active Passive, Sentence Rearrangement, Sentence Improvement, Cloze test etc.</p>

                        <h2 class="h2">General Awareness Syllabus</h2>
                        <p>Questions in this section will be aimed at testing the candidates' general awareness (GK+GS) of the environment around him and its application to society. The test will also include questions relating to India and its neighbouring countries especially pertaining History, Culture, Geography, Economic Scene, General Policy & Scientific Research. The questions will also be asked from Science, Current Affairs, Books and Authors, Sports, Important Schemes, Important Days, Portfolios, People in News etc.</p>

                        <p>After the preliminary exam has been conducted, it will be necessary to gear up for Mains Exam of SSC CGL 2019-20 Exam.</p>

                        <h2 class="h2">SSC-CGL Syllabus Tier II</h2>
                        <p>SSC CGL 2019-20 Tier-II exam is an objective exam which will be conducted online. The exam comprises of four Papers - Quantitative Abilities, English Language and Comprehension, Statistics and General Studies (Finance & Economics). The time duration for each Paper is 2 Hours. The section-wise details of the exam are shown in the table given below:</p>

                       
                        <div class="table-responsive">
                        <table class="table">   
                            <thead>
                                <tr>
                                    <th>Paper</th>
                                    <th>Subject </th>
                                    <th>No. of Questions</th>
                                    <th>Max Marks</th>
                                    <th>Exam Duration</th>
                                </tr>
                            </thead>                        
                            <tbody>
                                <tr>
                                    <td>I  </td>
                                    <td>Quantitative Abilities</td>
                                    <td>100</td>
                                    <td>200</td>
                                    <td>2 Hours</td>
                                </tr>
                                <tr>
                                    <td>II  </td>
                                    <td>English Language and Comprehension</td>
                                    <td>200</td>
                                    <td>200</td>
                                    <td>2 Hours</td>
                                </tr>
                                <tr>
                                    <td>III  </td>
                                    <td>Statistics</td>
                                    <td>100</td>
                                    <td>200</td>
                                    <td>2 Hours</td>
                                </tr>
                                <tr>
                                    <td>IV </td>
                                    <td>General Studies (Finance & Economics)</td>
                                    <td>100</td>
                                    <td>200</td>
                                    <td>2 Hours</td>
                                </tr>                                                                                           
                            </tbody>                           
                        </table>
                        </div>

                        <h2 class="h2">Points to remember:</h2>
                        <ul class="page-list">
                            <li>Paper-I and Paper-II are compulsory for all posts.</li>
                            <li>Paper-III will be for only those candidates who apply for the post of Junior Statistical Officer (JSO) and who are shortlisted in Tier-I for this Post/Paper.</li>
                            <li>Paper-IV will be for only those candidates who are shortlisted in Tier-I for Paper-IV, i.e., for the posts of Assistant Audit Officer/ Assistant Accounts Officer.</li>
                            <li>There will be negative marking of 0.25 marks for each wrong answer in Paper-II (English Language and Comprehension) and of 0.50 marks for each wrong answer in Paper-I, Paper-III and Paper-IV.</li>
                            <li>Questions in Paper-I will be of Matriculation Level, Paper-II of 10+2 Level and in Paper-III and Paper-IV of Graduation Level.</li>
                            <li>The exam duration is 2 hours and 40 minutes for candidates who are visually handicapped. Subject-wise Detailed Syllabus of SSC CGL Tier – II 2019-20 Exam</li>
                        </ul>

                        <p>The above-mentioned syllabus was just a brief of SSC CGL Tier-II Exam. Students must go through the detailed syllabus of all the papers and build a proper strategy to score well in the respective subjects.</p>

                        <div class="table-responsive">

                        <table class="table"> 
                            <tbody>
                                <tr>
                                    <td>Quantitative Aptitude </td>
                                    <td>English Language & Comprehension</td>
                                    <td>Statistics</td>
                                    <td>General Studies: Finance and Economics</td>                                    
                                </tr>
                                <tr>
                                    <td>Simplification  </td>
                                    <td>Reading Comprehension</td>
                                    <td>Collection and Representation of Data</td>
                                    <td>Finance and Accounting</td>                                   
                                </tr>
                                <tr>
                                    <td>Interest </td>
                                    <td>Spelling</td>
                                    <td>Measure of Dispersion</td>
                                    <td>Fundamental Principles</td>                                   
                                </tr>
                                <tr>
                                    <td>Averages </td>
                                    <td>Fill in the Blanks</td>
                                    <td>Measure of Central Tendency</td>
                                    <td>Financial Accounting</td>                                    
                                </tr>                                                                                           
                                <tr>
                                    <td>Percentage </td>
                                    <td>Phrases and Idioms</td>
                                    <td>Moments, Skewness and Kurtosis</td>
                                    <td>Basic Concepts of Accounting</td>                                    
                                </tr>  
                                <tr>
                                    <td>Ratio and Proportion </td>
                                    <td>One Word Substitution</td>
                                    <td>Correlation and Regression</td>
                                    <td>Self-Balancing Ledger</td>                                    
                                </tr>
                                <tr>
                                    <td>Speed, Distance and Time </td>
                                    <td>Sentence Correction</td>
                                    <td>Random Variables</td>
                                    <td>Forms of Market and price determination in different markets</td>                                    
                                </tr>
                                <tr>
                                    <td>Number System </td>
                                    <td>Error Spotting</td>
                                    <td>Random Variables</td>
                                    <td>Theory of Production and cost</td>                                    
                                </tr>
                                <tr>
                                    <td>Mensuration </td>
                                    <td>Cloze Test</td>
                                    <td>Sampling Theory</td>
                                    <td>Economics and Governance</td>                                    
                                </tr>
                                <tr>
                                    <td>Data Interpretation </td>
                                    <td>Para Jumbles</td>
                                    <td>Analysis and Variance</td>
                                    <td>Comptroller and Auditor General of India</td>                                    
                                </tr>
                                <tr>
                                    <td>Time and Work</td>
                                    <td>Synonyms-Antonyms</td>
                                    <td>Time Series Analysis</td>
                                    <td>Finance Commission</td>                                    
                                </tr>
                                <tr>
                                    <td>Algebra</td>
                                    <td>Active-Passive Voice</td>
                                    <td>Index Number</td>
                                    <td>Theory of Demand and Supply</td>                                    
                                </tr>
                                <tr>
                                    <td>Trigonometry</td>
                                    <td>Direct-Indirect Speech</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>                                    
                                </tr>
                                <tr>
                                    <td>Geometry</td>
                                    <td>Direct-Indirect Speech</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>                                    
                                </tr>
                                <tr>
                                    <td>Data Sufficiency</td>
                                    <td>Direct-Indirect Speech</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>                                    
                                </tr>
                            </tbody>                           
                        </table>  
                        </div>                   
                        
                        

                        <h2 class="h2">Syllabus in Detail:</h2>
                        <h2 class="h2">Paper-I (Quantitative Abilities) Syllabus:</h2>
                        <p>The questions will be designed to test the ability of appropriate use of numbers and number sense of the candidate. The scope of the test will be the computation of whole numbers, decimals, fractions and relationships between numbers, Percentage, Ratio & Proportion, Square roots, Averages, Interest, Profit and Loss, Discount, Partnership Business, Mixture and Alligation, Time and distance, Time & Work, Basic algebraic identities of School Algebra & Elementary surds, Graphs of Linear Equations, Triangle and its various kinds of centres, Congruence and similarity of triangles, Circle and its chords, tangents, angles subtended by chords of a circle, common tangents to two or more circles, Triangle, Quadrilaterals, Regular Polygons, Circle, Right Prism, Right Circular Cone, Right Circular Cylinder, Sphere, Hemispheres, Rectangular Parallelepiped, Regular Right Pyramid with triangular or square base, Trigonometric ratio, Degree and Radian Measures, Standard Identities, Complementary angles, Heights and Distances, Histogram, Frequency polygon, Bar diagram & Pie chart.</p>

                        <h2 class="h2">Paper-II (English Language and Comprehension) Syllabus:</h2>
                        <p>Questions in this section will be designed to test the candidate's understanding and knowledge of English Language and will be based on spot the error, fill in 21 the blanks, synonyms, antonyms, spelling/ detecting misspelled words, idioms & phrases, one word substitution, improvement of sentences, active/ passive voice of verbs, conversion into direct/ indirect narration, shuffling of sentence parts, shuffling of sentences in a passage, cloze passage & comprehension passage.</p>
                       

                        <h2 class="h2">Paper-III (Statistics) Syllabus:</h2>
                        <p>1. Collection, Classification and Presentation of Statistical Data – Primary and Secondary data, Methods of data collection; Tabulation of data; Graphs and charts; Frequency distributions; Diagrammatic presentation of frequency distributions.</p>

                        <p>2. Measures of Central Tendency - Common measures of central tendency – mean median and mode; Partition values- quartiles, deciles, percentiles.</p>

                        <p>3. Measures of Dispersion- Common measures dispersion – range, quartile deviations, mean deviation and standard deviation; Measures of relative dispersion.</p>

                        <p>4. Moments, Skewness and Kurtosis – Different types of moments and their relationship; meaning of skewness and kurtosis; different measures of skewness and kurtosis.</p>

                        <p>5. Correlation and Regression – Scatter diagram; simple correlation coefficient; simple regression lines; Spearman‟s rank correlation; Measures of association of attributes; Multiple regression; Multiple and partial correlation (For three variables only).</p>

                        <p>6. Probability Theory – Meaning of probability; Different definitions of probability; Conditional probability; Compound probability; Independent events; Bayes‟ theorem.</p>

                        <p>7. Random Variable and Probability Distributions – Random variable; Probability functions; Expectation and Variance of a random variable; Higher moments of a random variable; Binomial, Poisson, Normal and Exponential distributions; Joint distribution of two random variable (discrete).</p>

                        <p>8. Sampling Theory – Concept of population and sample; Parameter and statistic, Sampling and non-sampling errors; Probability and nonprobability sampling techniques(simple random sampling, stratified sampling, multistage sampling, multiphase sampling, cluster sampling, systematic sampling, purposive sampling, convenience sampling and quota sampling); Sampling distribution(statement only); Sample size decisions.</p>

                        <p>9. Statistical Inference - Point estimation and interval estimation, Properties of a good estimator, Methods of estimation (Moments method, Maximum likelihood method, Least squares method), Testing of hypothesis, Basic concept of testing, Small sample and large sample tests, Tests based on Z, t, Chi-square and F statistic, Confidence intervals.</p>

                        <p>10. Analysis of Variance - Analysis of one-way classified data and twoway classified data.</p>

                        <p>11. Time Series Analysis - Components of time series, Determinations of trend component by different methods, Measurement of seasonal variation by different methods.</p>

                        <p>12. Index Numbers - Meaning of Index Numbers, Problems in the construction of index numbers, Types of index number, Different formulae, Base shifting and splicing of index numbers, Cost of living Index Numbers, Uses of Index Numbers.</p>

                        <h2 class="h2">Paper-IV (General Studies-Finance and Economics):</h2>
                        <h3 class="h3">Part A: Finance and Accounts-(80 marks):</h3>
                        <h3 class="h3">Fundamental principles and basic concept of Accounting:</h3>

                        <div class="table-responsive">
                        <table class="table">
                            <tr>
                                <td>1.1</td>
                                <td>Financial Accounting: Nature and scope, Limitations of Financial Accounting, Basic concepts and Conventions, Generally Accepted Accounting Principles.</td>
                            </tr>
                            <tr>
                                <td>1.2 </td>
                                <td>Basic concepts of accounting: Single and double entry, Books of original Entry, Bank Reconciliation, Journal, ledgers, Trial Balance, Rectification of Errors, Manufacturing, Trading, Profit & loss Appropriation Accounts, Balance Sheet Distinction between Capital and Revenue Expenditure, Depreciation Accounting, Valuation of Inventories, Non-profit organisations Accounts, Receipts and Payments and Income & Expenditure Accounts, Bills of Exchange, Self Balancing Ledgers.</td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>Part B: Economics and Governance-(120 marks):</td>
                            </tr>
                            <tr>
                                <td>2.1 </td>
                                <td>Comptroller & Auditor General of India- Constitutional provisions, Role and responsibility.</td>
                            </tr>
                            <tr>
                                <td>2.2 </td>
                                <td>Finance Commission-Role and functions.</td>
                            </tr>
                            <tr>
                                <td>2.3 </td>
                                <td>Basic Concept of Economics and introduction to Micro Economics: Definition, scope and nature of Economics, Methods of economic study and Central problems of an economy and Production possibilities curve.</td>
                            </tr>
                            <tr>
                                <td>2.4 </td>
                                <td>Theory of Demand and Supply: Meaning and determinants of demand, Law of demand and Elasticity of demand, Price, income and cross elasticity; Theory of consumer‟s behaviour Marshallian approach and Indifference curve approach, Meaning and determinants of supply, Law of supply and Elasticity of Supply.</td>
                            </tr>
                            <tr>
                                <td>2.5 </td>
                                <td>Theory of Production and cost: Meaning and Factors of production, Laws of production- Law of variable proportions and Laws of returns to scale.</td>
                            </tr>
                            <tr>
                                <td>2.6 </td>
                                <td>Forms of Market and price determination in different markets: Various forms of markets-Perfect Competition, Monopoly, Monopolistic Competition and Oligopoly ad Price determination in these markets.</td>
                            </tr>
                            <tr>
                                <td>2.7 </td>
                                <td>Indian Economy:</td>
                            </tr>
                            <tr>
                                <td>2.7.1 </td>
                                <td>Nature of the Indian Economy Role of different sectors Role of Agriculture, Industry and Services-their problems and growth.</td>
                            </tr>
                            <tr>
                                <td>2.7.2</td>
                                <td>National Income of India-Concepts of national income, Different methods of measuring national income.</td>
                            </tr>
                            <tr>
                                <td>2.7.3 </td>
                                <td>Population-Its size, rate of growth and its implication on economic growth.</td>
                            </tr>
                            <tr>
                                <td>2.7.4</td>
                                <td>Poverty and unemployment- Absolute and relative poverty, types, causes and incidence of unemployment.</td>
                            </tr>
                            <tr>
                                <td>2.7.5 </td>
                                <td>Infrastructure-Energy, Transportation, Communication.</td>
                            </tr>
                            <tr>
                                <td>2.8 </td>
                                <td>Economic Reforms in India: Economic reforms since 1991; Liberalisation, Privatisation, Globalisation and Disinvestment.</td>
                            </tr>
                            <tr>
                                <td>2.9 </td>
                                <td>Money and Banking:</td>
                            </tr>
                            <tr>
                                <td>2.9.1 </td>
                                <td>Monetary/ Fiscal policy- Role and functions of Reserve Bank of India; functions of commercial Banks/RRB/Payment Banks.</td>
                            </tr>
                            <tr>
                                <td>2.9.2 </td>
                                <td>Budget and Fiscal deficits and Balance of payments.</td>
                            </tr>
                            <tr>
                                <td>2.9.3 </td>
                                <td>Fiscal Responsibility and Budget Management Act, 2003.</td>
                            </tr>
                            <tr>
                                <td>2.10 </td>
                                <td>Role of Information Technology in Governance.</td>
                            </tr>
                        </table>
                        </div>
                        <p>Note: Questions in Paper-I will be of Matriculation Level, Paper-II of 10+2 Level and in Paper-III and Paper-IV of Graduation Level.</p>

                        <h2 class="h2">SSC CGL Tier-III Syllabus</h2>
                        <p>Tier-III of 2019 exam is a descriptive exam to test the written skills of the candidates in English/Hindi. The mode of examination is offline (Pen and Paper mode) and students are required to write essays, précis, application, letter etc. in this exam. The exam carries 100 marks and the time allotted for the same is 60 minutes. The time allotted for candidates belonging to PWD category is increased to 80 minutes. Tier-III paper is given by specific candidates who are interested only for the post of “Statistical Investigator Grade II” & “Compiler”.</p>

                        <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Subject</th>
                                    <th>Marks</th>
                                    <th>Time</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Descriptive Paper in English/Hindi (Writing of Essay, Precis, Letter, Application etc.)</td>
                                    <td>100 marks</td>
                                    <td>1 hour or 60 minutes</td>
                                </tr>
                            </tbody>
                        </table>
                        </div>

                        <h2 class="h2">SSC CGL Tier-IV Syllabus</h2>
                        <p>The SSC CGL Tier - IV Exam comprises of a couple of skill tests which are required for certain government posts and document verification procedure:</p>

                        <h2 class="h2">DEST (Data Entry Speed Test):</h2>
                        <ul class="page-list">
                            <li>Data Entry Speed Test (DEST) at 8,000 (eight thousand) Key Depression per hour on Computer is specifically for the post of Tax Assistants (Central Excise & Income Tax).</li>
                            <li>The “Data Entry Speed Test” Skill Test will be conducted for a passage of about 2000 (two thousand) key depressions for a duration of 15 (fifteen) minutes.</li>
                            <li>This test will be of qualifying nature and is used to check the typing speed of the candidate.</li>
                            <li>DEST will be conducted in the manner decided by the Commission for the purpose.</li>
                            <li>Orthopedically Handicapped (OH) candidates opting for the post of Tax Assistant in CBDT are exempted for appearing in Skill Test. OH candidates opting for post of Tax Assistant in CBEC are not exempted from Skill Test.</li>
                            <li>Hearing Handicapped (HH) and Visually Handicapped (VH) candidates are not eligible for exemption from the Skill Test. VH candidates will be allowed additional compensatory time of 5 minutes in DEST. Only those VH candidates who opt for scribes in the written examination will be provided passage reader at the time of Skill Test. </li>
                        </ul>

                        <h2 class="h2">CPT (Computer Proficiency Test):</h2>
                        <ul class="page-list">
                            <li>•	The Commission will hold CPT, comprising three modules –</li>
                            <li>•	Word Processing,</li>
                            <li>•	Spread Sheet and</li>
                            <li>•	Generation of Slides</li>
                            <li>•	The test will be conducted for the post of</li>
                            <li>•	Assistant Section Officer of Central Secretariat Service (CSS), Assistant Section Officer (MEA), Assistant in Serious Fraud Investigation Office (SFIO) under the Ministry of Corporate Affairs and Assistant (GSI) in the Ministry of Mines. </li>
                            <li>•	CPT will be of qualifying nature.</li>
                            <li>•	The CPT will be conducted in the manner decided by the Commission for the purpose. No exemption from CPT is allowed for any category of Persons with Disabilities (PwD) candidates.</li>
                        </ul>

                        <h2 class="h3">Document Verification:</h2>
                        <p>The last step to the final selection would be Document Verification. Candidates will have to submit copies of various documents like Matriculation Certificate, educational qualification, caste certificate, relevant document, if any relaxation is taken, etc. Candidates will be required to produce all documents in original for verification at the time of document verification.</p>

                        <p>After going through the above mentioned detailed syllabus of SSC CGL 2019-20 Exam, the next step is to create a study plan and start working on it. For instance, start your practice by solving some previous year question papers and analyse your strong & weak points. Regular practice will help in achieving accuracy and high score in the exam.</p>

                        <h2 class="h3">Penalty for wrong answers</h2>
                        <p>Giving a wrong answer to a question will lead to deduction of 0.5 marks in Tier – I, III and IV while that of 0.25 marks in Tier-II.</p> 
                    </div>
                    <!--/ col 12--> 
               </div>
               <!--/ row -->
           </div>
            <!-- /container -->
        </div>
        <!--/ sub page body -->
    </main>
    <!--/ sub page main -->
    <!--footer -->
    <?php include 'footer.php' ?>
    <!--/ footer -->  
    <?php include 'footerscripts.php'?>

</body>

</html>