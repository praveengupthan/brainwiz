<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Brainwiz</title>
    <link rel="icon" type="image/png" sizes="32x32" href="img/favicon-32x32.png">
    <!-- style sheets -->
    <?php include 'headerstyles.php' ?>
</head>

<body>
    <!-- sign in -->
    <div class="sign">
        <!-- container fluid -->
        <div class="container-fluid">
            <!-- row -->
            <div class="row align-items-center">
                <!-- left col -->
                <?php include 'sign-leftcol.php'?>
                <!--/left col -->

                <!-- right col -->
                <div class="col-lg-6">                    
                    <div class="signcol">
                        <h2 class="h5">Login to your Account</h2>    
                        <!-- form -->                    
                        <form>
                            <div class="form-group">
                                <input type="text" placeholder="Email/Username" class="form-control" name="" required>
                            </div>
                            <div class="form-group">
                                <input type="password" placeholder="Password" class="form-control" name="" required>
                            </div>
                            <div class="form-group">
                                <input onclick="window.location.href='user-profile.php';" type="submit" value="Login" class="bluebtn w-100 text-uppercase">
                            </div>
                            <div class="form-group text-center">
                                <a href="forgotpassword.php">Forgot Password?</a>
                            </div>
                            <div class="form-group text-center">
                                <p>Don't Have an account? <a class="fblue fbold" href="signup.php">Sign up</a></p>
                            </div>                            
                        </form> 
                        <!--/ form -->
                        <h2 class="h5">For BRAINWIZ Students</h2>   
                        <form>
                            <div class="form-group">
                                <div class="input-group">
                                    <input type="text" placeholder="Enter Registered Mobile Number" class="form-control" name="" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <input type="submit" value="Get OTP" class="bluebtn w-100 text-uppercase">
                            </div>
                        </form>
                    </div>
                </div>
                <!--/ right col -->
            </div>
            <!--/ row -->
        </div>
        <!--/ container fluid -->

    </div>
    <!--/ sign in --> 
    <?php include 'footerscripts.php'?>

</body>

</html>