<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Brainwiz Video Tutorials</title>
    <link rel="icon" type="image/png" sizes="32x32" href="img/favicon-32x32.png">
    <!-- style sheets -->
    <?php include 'headerstyles.php' ?>
</head>

<body>

    <!-- header -->
   <?php include 'header-postlogin.php' ?>
    <!--/ header -->
    <!--sub page main -->
    <main class="subpage">
        <!-- sub page header -->
        <div class="page-header">
            <!-- container -->
            <div class="container">
               <!-- row -->
               <div class="row">
                   <div class="col-lg-6">
                       <h1>Score <span class="fbold">Card</span> </h1>
                   </div>
               </div>
               <!--/ row --> 
            </div>
            <!--/ container -->
        </div>
        <!--/ sub page header -->        

        <!-- sub page body -->
        <div class="subpage-body">
           <!-- container -->
           <div class="container">
              <!-- row -->
              <div class="row justify-content-center py-3">
                        <!--left col -->
                        <div class="col-lg-6 text-center">
                            <div class="whitebox">
                                <img src="img/trophyimg.png" class="trophy-img">
                                <h3 class="h3 fblue">Congrats Praveen! Your Score Card</h3>                                
                                <!-- table -->
                                <table class="table mt-3">                                    
                                    <tbody>
                                        <tr>
                                            <td scope="row">Test Name</td>
                                            <td>Profit & Loss Test 1</td>                                            
                                        </tr>
                                        <tr>
                                            <td scope="row">Total Questions</td>
                                            <td>10</td>                                                                         
                                        </tr>
                                        <tr>
                                            <td scope="row">Questions Attempted</td>
                                            <td>10</td>                                                                         
                                        </tr>
                                        <tr>
                                            <td scope="row">Correct</td>
                                            <td>5</td>                                                                         
                                        </tr>                                        
                                    </tbody>
                                </table>
                                <!--/ table -->
                                <div class="d-flex justify-content-between">
                                    <a href="practise-test-solutions.php" class="bluebtn">View Solutions</a>
                                    <a href="practise-test.php" class="bluebtn">Back to Practise Tests</a>
                                </div>
                                
                            </div>
                        </div>
                        <!--/left col -->                       
                    </div>
                    <!--/ row -->
           </div>
           <!--/ container -->
        </div>
        <!--/ sub page body -->
    </main>

    <!--/ sub page main -->
    <!--footer -->
    <?php include 'footer.php' ?>
    <!--/ footer -->  
    <?php include 'footerscripts.php'?>

</body>

</html>