<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Brainwiz</title>
    <link rel="icon" type="image/png" sizes="32x32" href="img/favicon-32x32.png">
    <!-- style sheets -->
    <?php include 'headerstyles.php' ?>
</head>

<body>

    <!-- header -->
   <?php include 'header.php' ?>
    <!--/ header -->
    <!--sub page main -->
    <main class="subpage topicsPage">
        <!-- sub page header -->
        <div class="page-header">
            <!-- container -->
            <div class="container">
               <!-- row -->
               <div class="row">
                   <div class="col-lg-6">
                       <h1>Questions &amp; Explanations</h1>
                   </div>
               </div>
               <!--/ row --> 
            </div>
            <!--/ container -->
        </div>
        <!--/ sub page header -->

         <!-- brudcrumb -->
         <div class="container">
            <!-- row -->
            <div class="row">
                <!-- col -->
                <div class="col-lg-12">                   
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                        <li class="breadcrumb-item active"><a>Questions &amp; Explanations</a></li>
                    </ul>                    
                </div>
                <!--/col -->
            </div>
            <!--/ row -->
        </div>
        <!--/ brudcrumb -->

        <!-- sub page body -->
        <div class="subpage-body">
           <!-- container -->
           <div class="container">
                <!-- white box -->
                <div>                                
                    <h2 class="py-2">CAT Questions | CAT Number Systems Questions</h2>
                    <h4>CAT Quantitative Aptitude | CAT Number theory: HCF and LCM problems</h4>
                    <p>A CAT Number systems question that appears in the Quantitative Aptitude section of the CAT Exam broadly tests an aspirant on the concepts - factors, HCF and LCM, base system & remainders of the above mentioned concepts. In CAT Exam, one can generally expect to get 1~2 questions from CAT Number Systems: HCF-LCM. In multiple places, extension of HCF and LCM concepts are tested by CAT Exam and one needs to understand HCF-LCM to be able to answer the same. CAT Number theory is an important topic with lots of weightage in the CAT Exam. Make use of 2IIMs Free CAT Questions, provided with detailed solutions and Video explanations to obtain a wonderful CAT score. If you would like to take these questions as a Quiz, head on here to take these questions in a test format, absolutely free.</p> 

                    <!-- social share -->
                    <div class="social-share">
                            <span class="pt-1 span-share">Share this Solution</span>
                            <a href="javascript:void(0)" class="sharesocial"><span class="icon-facebook-square icomoon"></span></a>
                            <a href="javascript:void(0)" class="sharesocial"><span class="icon-twitter-square icomoon"></span></a>
                            <a href="javascript:void(0)" class="sharesocial"><span class="icon-linkedin-square icomoon"></span></a>
                            <a href="javascript:void(0)" class="sharesocial"><span class="icon-instagram icomoon"></span></a>
                            <a href="javascript:void(0)" class="sharesocial"><span class="icon-whatsapp icomoon"></span></a>
                            <a href="javascript:void(0)" class="sharesocial"><span class="icon-pinterest icomoon"></span></a>
                        </div>
                        <!--/ social share -->
                    </div>
                    <!--/ white box -->

                <!-- row -->
                <div class="row pt-4">
                    <!-- left questions -->
                    <div class="col-md-9">
                        <!-- accordion -->
                        <div class="accordion">
                            <!-- question -->
                            <div class="qst">                                                           
                                <h5>Question 01</h5>
                                <h6>How many pairs of integers (x, y) exist such that the product of x, y and HCF (x, y) = 1080?</h6>
                                <!-- options -->
                                <div class="options">                                        

                                    <div class="custom-control custom-radio">
                                        <input type="radio" class="custom-control-input" id="customRadio" name="example" value="customEx">
                                        <label class="custom-control-label" for="customRadio">
                                            <span class="alpha">(A) </span>
                                            <span class="optValue">8 </span>
                                            <small class="textans text-danger fbold">Wrong Answer</small>
                                        </label>
                                    </div>

                                    <div class="custom-control custom-radio">
                                        <input type="radio" class="custom-control-input" id="customRadio2" name="example" value="customEx">
                                        <label class="custom-control-label" for="customRadio2">
                                            <span class="alpha">(B) </span>
                                            <span class="optValue">25 </span>
                                            <small class="textans text-danger fbold">Wrong Answer</small>
                                        </label>
                                    </div>

                                    <div class="custom-control custom-radio">
                                        <input type="radio" class="custom-control-input" id="customRadio3" name="example" value="customEx">
                                        <label class="custom-control-label" for="customRadio3">
                                            <span class="alpha">(C) </span>
                                            <span class="optValue">13 </span>
                                            <small class="textans text-danger fbold">Wrong Answer</small>
                                        </label>
                                    </div>

                                    <div class="custom-control custom-radio">
                                        <input type="radio" class="custom-control-input" id="customRadio4" name="example" value="customEx">
                                        <label class="custom-control-label" for="customRadio4">
                                            <span class="alpha">(D) </span>
                                            <span class="optValue">20 </span>
                                            <small class="textans text-success fbold">Right Answer</small>
                                        </label>
                                    </div>
                                </div>
                                <!-- /options -->

                                <!-- buttons -->                                    
                                <div class="px-3 ">
                                        <a style="color:#0077b5;" class="fblue fbold text-uppercase panel-title d-block" href="javascript:void(0)">View Explanation</a>

                                        <!-- explanation -->
                                    <div class="answer panel-content pt-4">
                                        <h3 class="fbold fblue h6">Explanatory Answer</h3>
                                        <p>Method of solving this CAT Question from Number Theory - HCF and LCM: Given two numbers, finding HCF, LCM is very easy. Given some property about HCF and the numbers, finding the numbers is much tougher.</p>

                                        <article>
                                            <p class="wiz200"> We need to find ordered pairs (x, y) such that xy * HCF(x, y) = 1080.<br> Let x = ha and y = hb where h = HCF(x, y) =&gt; HCF(a, b) = 1.<br> So h<sup>3</sup>(ab) = 1080 = (2<sup>3</sup>)(3<sup>3</sup>)(5).<br> We need to write 1080 as a product of a perfect cube and another number.<br><br> Four cases:<br> 1. h = 1, ab = 1080 and b are co-prime. We gave 4 pairs of 8 ordered pairs (1, 1080), (8, 135), (27, 40) and (5, 216). (Essentially we are finding co-prime a,b such that a*b = 1080).<br><br> 2. h = 2, We need to find number of ways of writing (3<sup>3</sup>) * (5) as a product of two co-prime numbers. This can be done in two ways - 1 and (3<sup>3</sup>) * (5) , (3<sup>3</sup>) and (5)<br> number of pairs = 2, number of ordered pairs = 4<br><br> 3. h = 3, number of pairs = 2, number of ordered pairs = 4<br><br> 4. h = 6, number of pairs = 1, number of ordered pairs = 2<br><br> Hence total pairs of (x, y) = 9, total number of ordered pairs = 18.<br> The pairs are (1, 1080), (8, 135), (27, 40), (5, 216), (2, 270), (10, 54), (3, 120), (24, 15) and (6, 30). </p>
                                        </article>
                                    </div>
                                    <!--/ explanation -->
                                </div>
                                <!--/ buttons -->
                            </div>
                            <!--/ question --> 

                             <!-- question -->
                             <div class="qst">     
                                    <h5>Question 02</h5>                  
                                    <h6 class="fbold">If the number of residents who own only Ford cars is 20, then what is the total number of residents who own only Palio and only Audi cars?</h6>
                                    <!-- options -->
                                    <div class="options">  
                                        <div class="custom-control custom-radio">
                                            <input type="radio" class="custom-control-input" id="customRadio" name="example" value="customEx">
                                            <label class="custom-control-label" for="customRadio">
                                                <span class="alpha">(A) </span>
                                                <span class="optValue">8 </span>
                                                
                                            </label>
                                        </div>

                                        <div class="custom-control custom-radio">
                                            <input type="radio" class="custom-control-input" id="customRadio2" name="example" value="customEx">
                                            <label class="custom-control-label" for="customRadio2">
                                                <span class="alpha">(B) </span>
                                                <span class="optValue">25 </span>
                                                
                                            </label>
                                        </div>

                                        <div class="custom-control custom-radio">
                                            <input type="radio" class="custom-control-input" id="customRadio3" name="example" value="customEx">
                                            <label class="custom-control-label" for="customRadio3">
                                                <span class="alpha">(C) </span>
                                                <span class="optValue">13 </span>
                                                
                                            </label>
                                        </div>

                                        <div class="custom-control custom-radio">
                                            <input type="radio" class="custom-control-input" id="customRadio4" name="example" value="customEx">
                                            <label class="custom-control-label" for="customRadio4">
                                                <span class="alpha">(D) </span>
                                                <span class="optValue">20 </span>
                                                
                                            </label>
                                        </div>
                                    </div>
                                    <!-- /options -->

                                    <!-- buttons -->                                    
                                    <div class="px-3 ">
                                        <a style="color:#0077b5;" class="fblue fbold text-uppercase panel-title d-block" href="javascript:void(0)">View Explanation</a>

                                    <!-- explanation -->
                                    <div class="answer panel-content pt-4">
                                        <h3 class="fbold fblue h6">Explanatory Answer</h3>
                                        <p>Method of solving this CAT Question from Number Theory - HCF and LCM: Given two numbers, finding HCF, LCM is very easy. Given some property about HCF and the numbers, finding the numbers is much tougher.</p>

                                        <article>
                                            <p class="wiz200"> We need to find ordered pairs (x, y) such that xy * HCF(x, y) = 1080.<br> Let x = ha and y = hb where h = HCF(x, y) =&gt; HCF(a, b) = 1.<br> So h<sup>3</sup>(ab) = 1080 = (2<sup>3</sup>)(3<sup>3</sup>)(5).<br> We need to write 1080 as a product of a perfect cube and another number.<br><br> Four cases:<br> 1. h = 1, ab = 1080 and b are co-prime. We gave 4 pairs of 8 ordered pairs (1, 1080), (8, 135), (27, 40) and (5, 216). (Essentially we are finding co-prime a,b such that a*b = 1080).<br><br> 2. h = 2, We need to find number of ways of writing (3<sup>3</sup>) * (5) as a product of two co-prime numbers. This can be done in two ways - 1 and (3<sup>3</sup>) * (5) , (3<sup>3</sup>) and (5)<br> number of pairs = 2, number of ordered pairs = 4<br><br> 3. h = 3, number of pairs = 2, number of ordered pairs = 4<br><br> 4. h = 6, number of pairs = 1, number of ordered pairs = 2<br><br> Hence total pairs of (x, y) = 9, total number of ordered pairs = 18.<br> The pairs are (1, 1080), (8, 135), (27, 40), (5, 216), (2, 270), (10, 54), (3, 120), (24, 15) and (6, 30). </p>
                                        </article>
                                    </div>
                                    <!--/ explanation -->
                                </div>
                                <!--/ buttons -->                                    
                            </div>
                            <!--/ question --> 

                              <!-- question -->
                              <div class="qst">
                               <h5>Question 03</h5>                             
                                <h6 class="fbold">If the number of residents who own only Audi cars is 20, then what is the total number of persons who own at least two cars?</h6>
                                <!-- options -->
                                <div class="options">                                        

                                    <div class="custom-control custom-radio">
                                        <input type="radio" class="custom-control-input" id="customRadio" name="example" value="customEx">
                                        <label class="custom-control-label" for="customRadio">
                                            <span class="alpha">(A) </span>
                                            <span class="optValue">8 </span>                                            
                                        </label>
                                    </div>

                                    <div class="custom-control custom-radio">
                                        <input type="radio" class="custom-control-input" id="customRadio2" name="example" value="customEx">
                                        <label class="custom-control-label" for="customRadio2">
                                            <span class="alpha">(B) </span>
                                            <span class="optValue">25 </span>
                                            
                                        </label>
                                    </div>

                                    <div class="custom-control custom-radio">
                                        <input type="radio" class="custom-control-input" id="customRadio3" name="example" value="customEx">
                                        <label class="custom-control-label" for="customRadio3">
                                            <span class="alpha">(C) </span>
                                            <span class="optValue">13 </span>
                                            
                                        </label>
                                    </div>

                                    <div class="custom-control custom-radio">
                                        <input type="radio" class="custom-control-input" id="customRadio4" name="example" value="customEx">
                                        <label class="custom-control-label" for="customRadio4">
                                            <span class="alpha">(D) </span>
                                            <span class="optValue">20 </span>
                                            
                                        </label>
                                    </div>
                                </div>
                                <!-- /options -->

                                <!-- buttons -->                                    
                                <div class="px-3 ">
                                        <a style="color:#0077b5;" class="fblue fbold text-uppercase panel-title d-block" href="javascript:void(0)">View Explanation</a>

                                        <!-- explanation -->
                                    <div class="answer panel-content pt-4">
                                        <h3 class="fbold fblue h6">Explanatory Answer</h3>
                                        <p>Method of solving this CAT Question from Number Theory - HCF and LCM: Given two numbers, finding HCF, LCM is very easy. Given some property about HCF and the numbers, finding the numbers is much tougher.</p>

                                        <article>
                                            <p class="wiz200"> We need to find ordered pairs (x, y) such that xy * HCF(x, y) = 1080.<br> Let x = ha and y = hb where h = HCF(x, y) =&gt; HCF(a, b) = 1.<br> So h<sup>3</sup>(ab) = 1080 = (2<sup>3</sup>)(3<sup>3</sup>)(5).<br> We need to write 1080 as a product of a perfect cube and another number.<br><br> Four cases:<br> 1. h = 1, ab = 1080 and b are co-prime. We gave 4 pairs of 8 ordered pairs (1, 1080), (8, 135), (27, 40) and (5, 216). (Essentially we are finding co-prime a,b such that a*b = 1080).<br><br> 2. h = 2, We need to find number of ways of writing (3<sup>3</sup>) * (5) as a product of two co-prime numbers. This can be done in two ways - 1 and (3<sup>3</sup>) * (5) , (3<sup>3</sup>) and (5)<br> number of pairs = 2, number of ordered pairs = 4<br><br> 3. h = 3, number of pairs = 2, number of ordered pairs = 4<br><br> 4. h = 6, number of pairs = 1, number of ordered pairs = 2<br><br> Hence total pairs of (x, y) = 9, total number of ordered pairs = 18.<br> The pairs are (1, 1080), (8, 135), (27, 40), (5, 216), (2, 270), (10, 54), (3, 120), (24, 15) and (6, 30). </p>
                                        </article>
                                    </div>
                                    <!--/ explanation -->
                                </div>
                                        <!--/ buttons -->
                            </div>
                            <!--/ question --> 

                        </div>
                        <!--/ accordion -->

                         <!-- navigation -->
                         <ul class="pagination float-right py-4">
                            <li class="page-item"><a class="page-link" href="#">Previous</a></li>
                            <li class="page-item"><a class="page-link" href="#">1</a></li>
                            <li class="page-item"><a class="page-link" href="#">2</a></li>
                            <li class="page-item"><a class="page-link" href="#">3</a></li>
                            <li class="page-item"><a class="page-link" href="#">Next</a></li>
                        </ul>
                        <!--/ navigation -->   
                    </div>
                    <!--/ left questions -->

                    <!-- right questions -->
                    <div class="col-md-3">
                        <div class="career-block text-center">                          
                            <h2 class="h6 py-2">Get More Infrormation </h2>                           
                            <form>
                                <div class="form-group">
                                    <input class="form-control" type="text" placeholder="Your Name"> 
                                </div>
                                <div class="form-group">
                                    <input class="form-control" type="text" placeholder="Enter your Email"> 
                                </div>
                                <div class="form-group">
                                    <input class="form-control" type="text" placeholder="Contact Number"> 
                                </div>
                                <div class="form-group">
                                    <input class="form-control" type="text" placeholder="Location Ex: Hyderabad"> 
                                </div>
                                <div class="form-group">
                                    <input class="bluebtn w-100" type="submit" value="Submit"> 
                                </div>
                            </form>                           
                        </div>

                        <div class="career-block text-center mt-4">                          
                            <h2 class="h5 py-2">Related Topics </h2> 

                            <ul class="relatedTopics">
                                <li>
                                    <a href="javascript:void(0)">Time and Work Concepts</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">Time and Work Formula and Solved Problems</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">Time and Work Problems (Easy)</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">Time and Work Problems (Difficult)</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">Problems on Ages Practice Problems : Level 02</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">Chain Rule : Theory & Concepts</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">Chain Rule Practice Problems: Level 01</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">Chain Rule Practice Problems : Level 02 </a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">Problems on Numbers System : Level 02</a>
                                </li>
                            </ul>                          
                                           
                        </div>


                    </div>
                    <!--/ right questions -->
                </div>
                <!--/ row -->
           </div>
           <!--/ container -->
        </div>
        <!--/ sub page body -->
    </main>

    <!--/ sub page main -->
    <!--footer -->
    <?php include 'footer.php' ?>
    <!--/ footer -->  
    <?php include 'footerscripts.php'?>
</body>

</html>