<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>CoCubes at Brainwiz</title>
    <link rel="icon" type="image/png" sizes="32x32" href="img/favicon-32x32.png">
    <!-- style sheets -->
    <?php include 'headerstyles.php' ?>
</head>

<body>

    <!-- header -->
   <?php include 'header.php' ?>
    <!--/ header -->
    <!--sub page main -->
    <main class="subpage">
        <!-- sub page header -->
        <div class="page-header">
            <!-- container -->
            <div class="container">
               <!-- row -->
               <div class="row">
                   <div class="col-lg-6">
                       <h1>CoCubes </h1>
                   </div>
               </div>
               <!--/ row --> 
            </div>
            <!--/ container -->
        </div>
        <!--/ sub page header -->

        <!-- brudcrumb -->
        <div class="container">
            <!-- row -->
            <div class="row">
                <!-- col -->
                <div class="col-lg-12">                   
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                        <li class="breadcrumb-item"><a href="allcourses.php">Courses</a></li>
                        <li class="breadcrumb-item active"><a>CoCubes</a></li>
                    </ul>                    
                </div>
                <!--/col -->
            </div>
            <!--/ row -->
        </div>
        <!--/ brudcrumb -->

        <!-- sub page body -->
        <div class="subpage-body">
           <!-- container -->
           <div class="container">
               <!-- row -->
               <div class="row">
                   <!-- left col -->
                   <div class="col-lg-8">
                    <!-- row-->
                    <div class="row justify-content-center">
                        <!-- col 12 -->
                       <div class="col-lg-12 ">
                           <div class="bggray p-4 text-center">
                            <img style="height:300px;" src="img/cocubes-course-detail-img.svg" alt="" class="img-fluid">
                                <h2 class="h2 py-2 fbold">About CoCubes? </h2>
                                <p class="text-center">CoCubes is India’s largest assessment and Campus hiring platform. It works with 450+ corporate across fresher’s and lateral’s.  CoCubes conducts the pre-assessment test and the recruiter’s throughout the country uses its score. The Pre-assessment conducted by CoCubes comprises of various modules to test various skills of a candidate. The score of the candidate is being provided to the recruiting companies. More than 200+ companies are using the CoCubes Pre assessment test to hire the fresher graduates.</p>                               
                           </div>

                           <h3 class="h2 py-2 fbold">CoCubes Training in Hyderabad </h3>
                           <p>BRAINWIZ is the Best CoCubes Training Institute in Hyderabad, where thousands of students secured good score and placed in Top MNC’s. Here students will be trained on updated syllabus of CoCubes covering all modules like Quantitative Aptitude, Logical Reasoning, and Verbal Ability. Training on Updated Questions and syllabus helps students to get into the companies easily.  BRAINWIZ material is completely consists of recently asked questions in previous CoCubes exams. We will also provide Online Test series on CoCubes pattern papers. Will give focused Training on Companies like, Microsoft, Bosch, Cap Gemini, ABB, Ericsson, Toshiba, Musigma, Mahindra, Maruti Suzuki etc., which are hiring through this CoCubes assessment exam.  </p>

                           <h3 class="h2 py-2 fbold">CoCubes Pre-Assess Eligibility Criteria</h3>
                           <p>The Eligibility criteria for CoCubes pre-assess test is given below.  </p>

                           <ol class="orderlist">
                               <li>The students who are pursuing Final year and Pass out students. </li>
                               <li>Students from Engineering Background only</li>                              
                           </ol>
                         
                           <h3 class="h2 py-2 fbold">CoCubes Pre-Assessment Fee </h3>
                           <p>The fee for appearing for 1st attempt at CoCubes is INR 1100 + GST for Pass out students and INR 1200 + GST for final year pursuing students.  </p>

                           <h3 class="h2 py-2 fbold">CoCubes Exam Pattern </h3>
                           <p>CoCubes Tests are available across various engineering domains, aptitude domains as well as other IT related domains (for both fresher’s and lateral hires). CoCubes exam paper and pattern over the years is relatively moderate to high in difficulty level. </p>

                           <table class="table">
                                <thead>
                                    <tr>
                                        <th scope="col">Module Type</th>
                                        <th scope="col">Number of Questions  </th>
                                        <th scope="col">Duration  </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td scope="row">Aptitude Module (Numerical, Analytical, English)</td>
                                        <td>15 + 15 + 15 = 45</td> 
                                        <td>45 mins</td> 
                                    </tr> 
                                    <tr>
                                        <td scope="row">Computer Fundamentals</td>
                                        <td>15</td> 
                                        <td>15 mins</td> 
                                    </tr> 
                                    <tr>
                                        <td scope="row">Psychometric Module</td>
                                        <td>50</td> 
                                        <td>12 mins</td> 
                                    </tr> 
                                    <tr>
                                        <td scope="row">Domain Module (You can pick any relevant domain module)</td>
                                        <td>20</td> 
                                        <td>20 mins</td> 
                                    </tr> 
                                    <tr>
                                        <td scope="row">Coding Module</td>
                                        <td>3</td> 
                                        <td>45 mins</td> 
                                    </tr> 
                                    <tr>
                                        <td scope="row">Written English Test (WET)</td>
                                        <td>1</td> 
                                        <td>25 mins</td> 
                                    </tr>                                      
                                </tbody>
                            </table>

                            <p>While hiring for technical roles, it is imperative for organizations to test a candidate’s skills, knowledge, and understanding of the core technical concepts. Hence, domain-specific module is designed to assess a candidate’s grasp on fundamentals (theoretical knowledge) and understanding of applications of those core concepts in the branch of engineering specialization.</p>

                            <p>One can choose among Civil, Mechanical, Electronics, Electrical and Computer Science for the domain module</p>

                           <!-- course curriculum -->
                           <div class="bggray p-4 custom-accord">
                                <h3 class="h2 py-1 fbold text-center">CoCubes Syllabus </h3> 

                                <!-- accordian -->
                                <div class="accordion course-accordion">
                                    <!-- acc-->
                                    <h3 class="panel-title">Quantitative Ability</h3>
                                    <div class="panel-content">                                        

                                        <ul class="page-list">
                                            <li>Ratio & Proportion </li>
                                            <li>Percentages</li>
                                            <li>Profit & Loss</li>
                                            <li>Simple Interest & Compound Interest</li>
                                            <li>Time & Work </li>
                                            <li>Pipes & Cistern</li>
                                            <li>Averages & Mixtures</li>
                                            <li>Time, Speed & Distance</li>
                                            <li>Probability</li>
                                            <li>Geometry </li>
                                            <li>Permutations & Combinations</li>
                                            <li>Data Interpretation</li>
                                            <li>Data Sufficiency</li>
                                        </ul>
                                    </div>
                                    <!--/ acc-->
                                    <!-- acc-->
                                    <h3 class="panel-title">Logical Reasoning</h3>
                                    <div class="panel-content"> 
                                        <ul class="page-list">
                                        <li>Coding & Decoding</li>
                                        <li>Blood Relations</li>
                                        <li>Seating Arrangement</li>
                                        <li>Direction Sense</li>
                                        <li>Number Series</li>
                                        <li>Letter Series</li>
                                        <li>Analogy & Classification</li>
                                        <li>Puzzles</li>         
                                        </ul>
                                    </div>
                                    <!--/ acc-->                                   
                                     <!-- acc-->
                                     <h3 class="panel-title">Verbal Ability</h3>
                                    <div class="panel-content">                                        
                                        <ul class="page-list">
                                            <li>Grammar</li>
                                            <li>Synonyms & Antonyms</li>
                                            <li>Prepositions </li>
                                            <li>Idioms & Phrases </li>
                                            <li>Articles </li>
                                            <li>Tenses </li>
                                            <li>Spotting Err zors</li>
                                            <li>Reading Comprehension </li>
                                            <li>Sentence Arrangement </li>
                                            <li>Sentence Improvement</li>               
                                        </ul>
                                    </div>
                                    <!--/ acc-->
                                     <!-- acc-->
                                     <h3 class="panel-title">Computer Programming</h3>
                                    <div class="panel-content">     
                                        <p>It’s a particular Domain Module</p>                                  
                                        <ul class="page-list">
                                            <li>C </li>
                                            <li>C++ </li>
                                            <li>OOPS Concepts </li>
                                            <li>DBMS </li>
                                            <li>OS Concepts </li>
                                            <li>Data Structures & Algorithm </li>
                                            <li>Computer Networks </li>
                                            <li>Computer Architecture and Organization </li>            
                                        </ul>
                                    </div>
                                    <!--/ acc-->
                                     <!-- acc-->
                                     <h3 class="panel-title">Essay Writing</h3>
                                    <div class="panel-content">  
                                        <p>Some of the frequently asked topics in Essay Writing</p>
                                        <ul class="page-list">
                                            <li>Education – Importance in the development of the country</li>
                                            <li>Your Favorite Sports person </li>
                                            <li>My Dream job </li>
                                            <li>Is Climate change real? </li>
                                            <li>Write an essay on ‘corporate hospitals vs. medical ethics’</li>
                                            <li>Write an essay on ‘Learning vs. Understanding’.</li>
                                            <li>Spending habits drifting away financial security</li>
                                            <li>Child counselling place a vital role in student or children.</li>
                                            <li>Should laptop replace textbooks in schools</li>
                                            <li>Impact of social networking & social networking sites</li>
                                            <li>How technology has connect people</li>
                                            <li>Views on manual or automated work through software</li>
                                            <li>Your view on – we are depending too much on loans</li>
                                            <li>Education needs to be practical</li>
                                            <li>Spirituality in corporate world</li>
                                            <li>A person Education achievement depends upon family, friends and society, give your views for or against this top</li>
                                            <li>First Campus Interview Experience</li>
                                            <li>My Best Friend</li>
                                            <li>Are we too dependent on Computers</li>
                                            <li>Digitization and its benefits</li>
                                            <li>My last vacation with Parents</li>
                                            <li>Are corrupt but efficient politicians better than honest but inefficient politicians</li>    
                                        </ul>
                                    </div>
                                    <!--/ acc-->
                                </div>
                                <!--/ accordian -->
                           </div>
                           <!--/ course curriculum -->
                           
                           <h3 class="h2 py-1 fbold text-center"> CoCubes Assessment Tests  </h3> 
                                <p>There are different assessment tests administered to measure each competency. Based on the company requirement, the assessment tests are chosen. </p>
                                <p>For example, Musigma specifically evaluates a candidate’s skills on Verbal/Written communication skills and problem-solving approach in limited time. Hence the selection process consists of an assessment test by CoCubes – Quantitative Reasoning and English usage.</p>
                                  <!-- table -->
                                  <table class="table">
                                    <thead>
                                        <tr>
                                            <th scope="col">Competency Measured</th>
                                            <th scope="col">Assessment Mechanism </th> 
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td scope="row">Systematic approach, Ability to visualize a problem </td>
                                            <td>Flowcharting and Visual Algorithms</td> 
                                        </tr> 
                                        <tr>
                                            <td scope="row">Creativity, Ability to analyze any problem </td>
                                            <td>Analytical Reasoning</td>                                            
                                        </tr> 
                                        <tr>
                                            <td scope="row">Problem solving approach in limited time, Ability to deal with numbers </td>
                                            <td>Quantitative reasoning</td>
                                        </tr> 
                                        <tr>
                                            <td scope="row">Verbal and written communication skills </td>
                                            <td>English Usage</td>
                                        </tr> 
                                        <tr>
                                            <td scope="row">Transform conceptual models into real time systems </td>
                                            <td>Computer Mathematical Modeling</td>
                                        </tr> 
                                        <tr>
                                            <td scope="row">Domain specific skills and understanding of fundamentals</td>
                                            <td>Engineering/Domain specific tests</td>
                                        </tr> 
                                        <tr>
                                            <td scope="row">Personality analysis and job fitment </td>
                                            <td>Psychometric profiling</td>
                                        </tr> 
                                    </tbody>
                                </table>
                                <!--/ table -->  

                                <h3 class="h2 py-1 fbold text-center">CoCubes Test level of Difficulty </h3> 

                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th scope="col">TOPICS</th>
                                            <th scope="col">No Of Ques </th>
                                            <th scope="col">Time</th>
                                            <th scope="col">Difficulty  </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td scope="row">CoCubes Quants Questions</td>
                                            <td>25</td> 
                                            <td>35</td>
                                            <td>Medium</td> 
                                        </tr> 
                                        <tr>
                                            <td scope="row">Verbal English</td>
                                            <td>25</td> 
                                            <td>35</td>
                                            <td>Medium</td> 
                                        </tr> 
                                        <tr>
                                            <td scope="row">Cocubes Logical and Analytical</td>
                                            <td>25</td> 
                                            <td>35</td>
                                            <td>Medium</td> 
                                        </tr> 
                                        <tr>
                                            <td scope="row">Cocubes Computer Fundamentals</td>
                                            <td>30</td> 
                                            <td>40</td>
                                            <td>Medium</td> 
                                        </tr> 
                                        <tr>
                                            <td scope="row">Written English Test</td>
                                            <td>1</td> 
                                            <td>25</td>
                                            <td>Medium</td> 
                                        </tr>                                         
                                    </tbody>
                                </table>
                       </div>
                       <!-- col 12 -->
                    </div>
                    <!--/ row-->
                   </div>
                   <!--/left col-->

                   <!-- course right faq -->
                   <div class="col-lg-4">
                        
                        <div class="custom-accord sticky-top course-faq">
                            <h3 class="h5 pb-3">Faq's about CoCubes</h3>
                            <!-- accordion -->
                            <div class="accordion">
                                <!-- acc-->
                                <h3 class="panel-title">Is my score good or bad? How can I apply to companies?</h3>
                                <div class="panel-content">
                                    <p>Overall, 60% in each section is considered an acceptable score. An overall score of 550 or above is considered a good score. In general, such a score will ensure you are eligible for several companies/profiles. This increases your chances of getting a job. Aside from an overall score, we also use sectional scores to match candidates with jobs. For example, an IT Product profile will mandatorily require a medium to high score in Coding but may not require a high score in the Written English section. Therefore, pay attention to your sectional scores also (aside from your overall score).
                                    </p>
                                </div>
                                <!--/ acc-->
                                <!-- acc-->
                                <h3 class="panel-title">How many times can I attempt CoCubes?</h3>
                                <div class="panel-content">
                                    <p>Please note that you can write CoCubes exam any number of times, provided we have the availability of the slot (as per your choice) and you are making the payment for each slot.</p>
                                </div>
                                <!--/ acc-->
                                <!-- acc-->
                                <h3 class="panel-title">I wish to reschedule CoCubes.</h3>
                                <div class="panel-content">
                                    <p>Please note that CoCubes can be rescheduled 3 days before the exam. If you want, you can change the timings of the CoCubes from your account itself by logging onto CoCubes</p>                                    
                                </div>
                                <!--/ acc-->
                                <!-- acc-->
                                <h3 class="panel-title">How do I generate hall ticket for CoCubes test?</h3>
                                <div class="panel-content">
                                    <p>Please follow the steps below to generate your Hall ticket:</p>
                                    <ol class="orderlist">
                                        <li>Login to CoCubes</li>
                                        <li>Click on 'Job Posts', Second link from your photograph on the left side of your screen</li>
                                        <li>Go to the Job Post for which the Hall ticket has to be generated.</li>
                                        <li>Read the details and scroll down until you see the Green Button to "Generate Hall Ticket"</li>
                                        <li>Click on "Generate Hall Ticket"</li>
                                    </ol>
                                </div>
                                <!--/ acc-->
                                <!-- acc-->
                                <h3 class="panel-title">I have taken CoCubes test twice and do not have clarity on which score will be considered?</h3>
                                <div class="panel-content">
                                    <p>Please note that the sectional best out of two CoCubes scores is considered and same gets considered in your account. Please go through the below link in order to get more clarity about the CoCubes scores. </p>
                                </div>
                                <!--/ acc-->
                                <!-- acc-->
                                <h3 class="panel-title">How can I register for CoCubes test as an individual?</h3>
                                <div class="panel-content">
                                    <p>We are accepting registrations from 2019 or 2020 Batch (passing out year) in Engineering only. If you are from these batches and Stream.  </p>
                                </div>
                                <!--/ acc-->                               

                            </div>
                            <!--/ accordion -->
                        </div>
                   </div>
                   <!--/ course right faq -->
               </div>
               <!--/ row -->
           </div>
            <!-- /container -->
        </div>
        <!--/ sub page body -->
    </main>
    <!--/ sub page main -->
    <!--footer -->
    <?php include 'footer.php' ?>
    <!--/ footer -->  
    <?php include 'footerscripts.php'?>

</body>

</html>