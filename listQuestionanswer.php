<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Brainwiz</title>
    <link rel="icon" type="image/png" sizes="32x32" href="img/favicon-32x32.png">
    <!-- style sheets -->
    <?php include 'headerstyles.php' ?>
</head>

<body>

    <!-- header -->
   <?php include 'header.php' ?>
    <!--/ header -->
    <!--sub page main -->
    <main class="subpage topicsPage">
        <!-- sub page header -->
        <div class="page-header">
            <!-- container -->
            <div class="container">
               <!-- row -->
               <div class="row">
                   <div class="col-lg-6">
                       <h1>Questions &amp; Explanations</h1>
                   </div>
               </div>
               <!--/ row --> 
            </div>
            <!--/ container -->
        </div>
        <!--/ sub page header -->

         <!-- brudcrumb -->
         <div class="container">
            <!-- row -->
            <div class="row">
                <!-- col -->
                <div class="col-lg-12">                   
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                        <li class="breadcrumb-item active"><a>Questions &amp; Explanations</a></li>
                    </ul>                    
                </div>
                <!--/col -->
            </div>
            <!--/ row -->
        </div>
        <!--/ brudcrumb -->

        <!-- sub page body -->
        <div class="subpage-body">
           <!-- container -->
           <div class="container">               
                <!-- row -->
                <div class="row">
                    <!-- left questions -->
                    <div class="col-md-9">
                    <h2 class="fbold">CAT Quantitative Aptitude Questions | CAT Number Systems - HCF and LCM questions</h2>
                    <h5 class="py-2">CAT Questions | Number Theory | HCF and LCM</h5>
                    <p>The question is about HCF and LCM. We need to find all the possible pairs of (x,y), given the product of them and their HCF. Dealing with HCF and LCM of numbers is a vital component in CAT Number Theory: HCF and LCM. A range of CAT questions can be asked based on this simple concept. In CAT Exam, one can generally expect to get 1~2 questions from CAT Number Systems: HCF and LCM. In multiple places, extension of HCF and LCM concepts are tested by CAT Exam and one needs to understand HCF and LCM to be able to answer the same. CAT Number theory is an important topic with lots of weightage in the CAT Exam.</p>
                        <!-- question -->
                        <div class="qst" style="border:none;">
                            <!-- row -->
                            <div class="row">
                                <!-- col -->
                                <div class="col-md-1">
                                    <h5>01</h5>
                                </div>
                                <!--/ col -->
                                <!-- col -->
                                <div class="col-md-11">
                                    <h3 class="flight">CAT Number Theory: - HCF LCM</h3>
                                    <h6 class="fbold">How many pairs of integers (x, y) exist such that the product of x, y and HCF (x, y) = 1080?</h6>
                                    <!-- options -->
                                    <div class="options">
                                        <div class="form-check">
                                            <label class="form-check-label" for="radio1">
                                                <input type="radio" class="form-check-input" id="radio1" name="optradio" value="option1"> 
                                                <span class="alpha">A. </span>
                                                <span class="optValue">8 </span>
                                                <small class="textans text-danger fbold">Wrong Answer</small>
                                            </label>
                                        </div>

                                        <div class="form-check">
                                            <label class="form-check-label" for="radio2">
                                                <input type="radio" class="form-check-input" id="radio2" name="optradio" value="option2"> 
                                                <span class="alpha">B. </span>
                                                <span class="optValue">7 </span>
                                                <small class="textans text-success fbold">Correct Answer</small>
                                            </label>
                                        </div>

                                        <div class="form-check">
                                            <label class="form-check-label" for="radio3">
                                                <input type="radio" class="form-check-input" id="radio3" name="optradio" value="option3"> 
                                                <span class="alpha">C. </span>
                                                <span class="optValue">9 </span>
                                            </label>
                                        </div>

                                        <div class="form-check">
                                            <label class="form-check-label" for="radio4">
                                                <input type="radio" class="form-check-input" id="radio4" name="optradio" value="option3"> 
                                                <span class="alpha">D. </span>
                                                <span class="optValue">15 </span>
                                            </label>
                                        </div>
                                    </div>
                                    <!-- /options -->
                                </div>
                                <!--/ col -->
                                
                            </div>
                            <!--/ row -->
                        </div>
                        <!--/ question -->

                          <!-- explanation -->
                          <div class="explanation answer panel-content">
                            <h3 class="fbold fblue h4">Explanatory Answer</h3>
                            <p>Method of solving this CAT Question from Number Theory - HCF and LCM: Given two numbers, finding HCF, LCM is very easy. Given some property about HCF and the numbers, finding the numbers is much tougher.</p>

                            <article>
                                <p class="wiz200"> We need to find ordered pairs (x, y) such that xy * HCF(x, y) = 1080.<br> Let x = ha and y = hb where h = HCF(x, y) =&gt; HCF(a, b) = 1.<br> So h<sup>3</sup>(ab) = 1080 = (2<sup>3</sup>)(3<sup>3</sup>)(5).<br> We need to write 1080 as a product of a perfect cube and another number.<br><br> Four cases:<br> 1. h = 1, ab = 1080 and b are co-prime. We gave 4 pairs of 8 ordered pairs (1, 1080), (8, 135), (27, 40) and (5, 216). (Essentially we are finding co-prime a,b such that a*b = 1080).<br><br> 2. h = 2, We need to find number of ways of writing (3<sup>3</sup>) * (5) as a product of two co-prime numbers. This can be done in two ways - 1 and (3<sup>3</sup>) * (5) , (3<sup>3</sup>) and (5)<br> number of pairs = 2, number of ordered pairs = 4<br><br> 3. h = 3, number of pairs = 2, number of ordered pairs = 4<br><br> 4. h = 6, number of pairs = 1, number of ordered pairs = 2<br><br> Hence total pairs of (x, y) = 9, total number of ordered pairs = 18.<br> The pairs are (1, 1080), (8, 135), (27, 40), (5, 216), (2, 270), (10, 54), (3, 120), (24, 15) and (6, 30). </p>
                            </article>
                        </div>
                        <!--/ explanation -->

                        <!-- navigation -->
                        <ul class="pagination float-right py-4">
                            <li class="page-item"><a class="page-link" href="#">Previous</a></li>
                            <li class="page-item"><a class="page-link" href="#">1</a></li>
                            <li class="page-item"><a class="page-link" href="#">2</a></li>
                            <li class="page-item"><a class="page-link" href="#">3</a></li>
                            <li class="page-item"><a class="page-link" href="#">Next</a></li>
                        </ul>
                        <!--/ navigation -->                      
                        
                    </div>
                    <!--/ left questions -->

                    <!-- right questions -->
                    <div class="col-md-3">
                        <div class="career-block text-center">
                                <img src="img/premade-image-09.png">
                                <h2 class="h5 py-3">Advance Your Career</h2>
                                <p class="text-center">Enter your email below to download one of our free career guides</p>
                                <form>
                                    <div class="form-group">
                                        <input class="form-control" type="text" placeholder="Enter your Email"> 
                                    </div>
                                    <div class="form-group">
                                        <input class="bluebtn w-100" type="submit" value="Submit"> 
                                    </div>
                                </form>
                                <p class="small text-left">I consent and agree to receive email marketing communications from Udacity.  " that by clicking “Submit,” my data will be used in accordance with the  Udacity Terms of Use and Privacy Policy.</p>
                        </div>
                    </div>
                    <!--/ right questions -->
                </div>
                <!--/ row -->
           </div>
           <!--/ container -->
        </div>
        <!--/ sub page body -->
    </main>

    <!--/ sub page main -->
    <!--footer -->
    <?php include 'footer.php' ?>
    <!--/ footer -->  
    <?php include 'footerscripts.php'?>
</body>

</html>