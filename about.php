<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Brainwiz</title>
    <link rel="icon" type="image/png" sizes="32x32" href="img/favicon-32x32.png">
    <!-- style sheets -->
    <?php include 'headerstyles.php' ?>
</head>

<body>
    <!-- header -->
   <?php include 'header.php' ?>
    <!--/ header -->
    <!--sub page main -->
    <main class="subpage">
        <!-- sub page header -->
        <div class="page-header">
            <!-- container -->
            <div class="container">
               <!-- row -->
               <div class="row">
                   <div class="col-lg-6">
                       <h1>About <span class="fbold text-uppercase">Brainwiz</span></h1>
                   </div>
               </div>
               <!--/ row --> 
            </div>
            <!--/ container -->
        </div>
        <!--/ sub page header -->

        <!-- brudcrumb -->
        <div class="container">
            <!-- row -->
            <div class="row">
                <!-- col -->
                <div class="col-lg-12">                   
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                        <li class="breadcrumb-item active"><a>About</a></li>
                    </ul>                    
                </div>
                <!--/col -->
            </div>
            <!--/ row -->
        </div>
        <!--/ brudcrumb -->

        <!-- sub page body -->
        <div class="subpage-body">
           <!-- container -->
           <div class="container">
               <!-- row -->
               <div class="row padrow justify-content-around">
                   <div class="col-lg-5 align-self-center">
                       <h2 class="h2 fbold fblue pb-2">BEST CRT Training Institute in Hyderabad </h2>                      
                        <p>BRAINWIZ is one of the leading CRT Institute in South India. It was established in 2015 in Hyderabad to impart quality training for CRT (Campus Recruitment Training) & various competitive Examinations. We have a team of best faculty with respect to different competitive exams in the present world. Teaching methodology is completely unique with the updated content with more logics and helping the students to solve the problems in a very easy way. We have trained huge number of engineering students and helping candidates to move ahead in their career path. This program is exclusively designed for the graduate students preparing them for recruitment of various IT and NON IT companies, and ensuring that the students are well trained and equipped to get through the companies easily. </p>                      
                   </div>
                   <div class="col-lg-7">
                        <img src="img/about-welcome.png" alt="" class="img-fluid">
                   </div>
               </div>
               <!--/  row -->

               <!-- row -->
               <div class="row">
                    <!-- col -->
                    <div class="col-lg-12">
                        <p>This training mainly for all the pre final year and final year students who are looking for placements in their colleges or in any reputation organizations like TCS, Infosys, Accenture, Cognizant, Cap Gemini, Wipro, IBM etc..(i.e. On Campus or off Campus). In CRT students will be trained for all the rounds of Interviews including logical thinking and communication skills. CRT Training at Hyderabad, BRAINWIZ deals with the CRT training having four modules including Aptitude Training, Logical Reasoning, Verbal Ability and Soft Skills. Here students will get training from the very first round written test to final HR round. BRAINWIZ in Hyderabad gives extra focus on AMCAT Training & Elitmus Training which includes Sunday special sessions on different puzzles with Mock online Test series of various MNC Companies. </p>
                    </div>
                    <!--/ col -->
               </div>
               <!--/ row -->

                <!-- row -->
                <div class="row padrow justify-content-around">
                    <!-- col -->
                    <div class="col-lg-4 align-self-center">
                        <h2 class="h2 fbold ">Our Strengths</h2>
                    </div>
                    <!--/ col -->

                    <!-- col -->
                    <div class="col-lg-8">
                        <!-- row -->
                        <div class="row justify-content-around secrow">
                            <!-- col -->
                            <div class="col-lg-2 align-self-center m-text-center">
                                <span class="icon-reading icomoon"></span>
                            </div>
                            <!--/ col -->
                            <!-- col -->
                            <div class="col-lg-10 border-bottom m-text-center">
                                <p><span class="fbold fblue">Mission</span> BRAINWIZ has a mission to remain an unbeatable and reputed organization for providing quality coaching for various sectors like CRT, SSC, IBPS, RRB ... Also to generate successful candidates from our Organization like we do every year. </p>
                            </div>
                            <!--/ col -->
                        </div>
                        <!--/ row -->

                        <!-- row -->
                        <div class="row justify-content-around secrow">
                            <!-- col -->
                            <div class="col-lg-2 align-self-center m-text-center">
                                <span class="icon-select icomoon"></span>
                            </div>
                            <!--/ col -->
                            <!-- col -->
                            <div class="col-lg-10 border-bottom m-text-center">
                                <p><span class="fbold fblue">Vision</span>To train and motivate engineering students and transform them into capable and efficient employees in the TOP MNC and to serve the major It Industries. </p>
                            </div>
                            <!--/ col -->
                        </div>
                        <!--/ row -->

                        <!-- row -->
                        <div class="row justify-content-around secrow">
                            <!-- col -->
                            <div class="col-lg-2 align-self-center m-text-center">
                                <span class="icon-professor icomoon"></span>
                            </div>
                            <!--/ col -->
                            <!-- col -->
                            <div class="col-lg-10 m-text-center">
                                <p><span class="fbold fblue">Experience</span> Offers total CRT and comprehensive training in Logical Reasoning, Quantitative Aptitude, Verbal sections, which are needed for almost every competitive exams. </p>
                            </div>
                            <!--/ col -->
                        </div>
                        <!--/ row -->
                    </div>
                    <!--/ col -->
                </div>
                <!--/row-->               
           </div>
           <!--/ container -->
           <!-- director row-->
           <div class="bgsec">
                <div class="container">
                     <!-- row -->
                    <div class="row secrow justify-content-center">                   
                        <!-- col -->
                        <div class="col-lg-10 text-center">
                        <img src="img/pavanimg.jpg" alt="" class="thumbimg">
                        <h3 class="h5 pt-2">Pavan Jaiswal</h3>
                        <h4 class="h6">Director</h4>
                        <p class="text-center">A CAT aspirant, <span class="fbold">Mr Pavan Jaiswal is the Managing Director & Founder of BRAINWIZ Education.</span> After completing his Masters in Computer Science, he started working in MADEEASY (GATE exam) and GANESH IAS (for CSAT exam) as an aptitude trainer successfully. Following this in the year 2015, he started BRAINWIZ in order to deliver his training to common students to enter into TOP MNCs by his aptitude training. His unique way of teaching has helped thousands of students to solve the problems in effortless way. Pavan Jaiswal has played a pivotal role in the success of BRAINWIZ and taking it further in the current Education Market with the quality and results.</p>
                        </div>
                        <!--/ col -->
                    </div>
                    <!--/ row -->
                </div>
           </div>
           <!--/ director section -->

           <!-- highlets -->
           <div class="highlets py-3">
                <div class="container">
                    <!-- row -->
                    <div class="row">
                        <!-- col -->
                        <div class="col-lg-6">
                            <h2 class="h2 fbold ">Why CRT at BRAINWIZ?</h2>
                            <ul class="page-list">
                               <li>Comprehensive Training on CRT </li> 
                               <li>More than 120 hours of Training</li> 
                               <li>30 to 45 days Program</li> 
                               <li>Covering 55+ Topics with latest MNC Questions</li> 
                               <li>Sunday Special Sessions covering AMCAT & E-Litmus</li> 
                               <li>Basic to Advance Level</li> 
                               <li>Timely Completion of Syllabus</li> 
                               <li>Updated Material</li>
                               <li>Online Test Series</li>
                               <li>Individual doubt Solving</li>
                            </ul>
                        </div>
                        <!-- /col -->
                        <!-- col -->
                        <div class="col-lg-6 align-self-center">
                            <img src="img/corporateimg.jpg" alt="" class="img-fluid"/>
                        </div>
                        <!--/ col -->
                    </div>
                    <!--/ row -->
                </div>
           </div>
           <!--/ highlets -->
        </div>
        <!--/ sub page body -->
    </main>
    <!--/ sub page main -->
    <!--footer -->
    <?php include 'footer.php' ?>
    <!--/ footer -->  
    <?php include 'footerscripts.php'?>

</body>

</html>