<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Brainwiz</title>
    <link rel="icon" type="image/png" sizes="32x32" href="img/favicon-32x32.png">
    <!-- style sheets -->
    <?php include 'headerstyles.php' ?>
</head>

<body>

    <!-- header -->
   <?php include 'header.php' ?>
    <!--/ header -->
    <!--sub page main -->
    <main class="subpage">
        <!-- sub page header -->
        <div class="page-header">
            <!-- container -->
            <div class="container">
               <!-- row -->
               <div class="row">
                   <div class="col-lg-6">
                       <h1>Registration for Seminar </h1>
                   </div>
               </div>
               <!--/ row --> 
            </div>
            <!--/ container -->
        </div>
        <!--/ sub page header -->

         <!-- brudcrumb -->
         <div class="container">
            <!-- row -->
            <div class="row">
                <!-- col -->
                <div class="col-lg-12">                   
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                        <li class="breadcrumb-item active"><a>Enquiry</a></li>
                    </ul>                    
                </div>
                <!--/col -->
            </div>
            <!--/ row -->
        </div>
        <!--/ brudcrumb -->

        <!-- sub page body -->
        <div class="subpage-body">

            <!-- container -->
            <div class="container">
                    <div class="multisteps-form">
                    <!--progress bar-->
                    <div class="row">
                    <div class="col-12 col-lg-8 ml-auto mr-auto mb-4">
                        <div class="multisteps-form__progress">
                            <button class="multisteps-form__progress-btn js-active" type="button" title="User Info">Education</button>
                            <button class="multisteps-form__progress-btn" type="button" title="Address">Seminar</button>
                            <button class="multisteps-form__progress-btn" type="button" title="Order Info">About</button>
                            <button class="multisteps-form__progress-btn" type="button" title="Comments">College  </button>                       
                        </div>
                    </div>
                    </div>
                <!--form panels-->
                <div class="row">
                <div class="col-12 col-lg-8 m-auto">
                    <form class="multisteps-form__form">
                    <!--single form panel1-->
                    <div class="multisteps-form__panel shadow p-4 rounded bg-white js-active" data-animation="scaleIn">
                        <h3 class="multisteps-form__title">Education</h3>
                        <div class="multisteps-form__content">
                        <p>What are your plans after Graduation</p>
                            <div class="form-row">
                                <div class="col-12 col-sm-6">
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1">
                                        <label class="form-check-label" for="inlineCheckbox1">IT companies Like TCS, Wipro</label>
                                    </div>
                                </div>

                                <div class="col-12 col-sm-6">
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1">
                                        <label class="form-check-label" for="inlineCheckbox1">GATE Exams for M.TECH, PSU, etc</label>
                                    </div>
                                </div>

                                
                                <div class="col-12 col-sm-6">
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1">
                                        <label class="form-check-label" for="inlineCheckbox1">GRE</label>
                                    </div>
                                </div>

                                <div class="col-12 col-sm-6">
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1">
                                        <label class="form-check-label" for="inlineCheckbox1">Govt Exams Like SSC, RRB</label>
                                    </div>
                                </div>

                                <div class="col-12 col-sm-6">
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1">
                                        <label class="form-check-label" for="inlineCheckbox1">Others</label>
                                    </div>
                                   
                                </div>   
                                <div class="col-12 col-sm-12 pt-3">
                                    <input class="multisteps-form__input form-control" type="text" placeholder="Write any others"/>
                                </div> 
                                <div class="button-row d-flex mt-4">
                                    <button class="btn bluebtn ml-auto js-btn-next" type="button" title="Next">Next</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--single form panel2-->
                    <div class="multisteps-form__panel shadow p-4 rounded bg-white" data-animation="scaleIn">
                        <h3 class="multisteps-form__title">Seminar</h3>
                        <div class="multisteps-form__content">
                        <p>Would you like to know the patterns of questions & Learn few topics in our seminar? </p>
                        <div class="form-row">
                            <div class="col-6">
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1">
                                    <label class="form-check-label" for="inlineRadio1">Yes</label>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1">
                                    <label class="form-check-label" for="inlineRadio1">No</label>
                                </div>
                            </div>
                        </div>
                      
                     
                        <div class="button-row d-flex mt-4">
                            <button class="btn bluebtn js-btn-prev" type="button" title="Prev">Prev</button>
                            <button class="btn bluebtn ml-auto js-btn-next" type="button" title="Next">Next</button>
                        </div>
                        </div>
                    </div>
                    <!--single form panel3-->
                    <div class="multisteps-form__panel shadow p-4 rounded bg-white" data-animation="scaleIn">
                        <h3 class="multisteps-form__title">About</h3>
                        <div class="multisteps-form__content">

                        <div class="form-row mt-4">
                            <div class="col-12 col-sm-6">
                                <input class="multisteps-form__input form-control" type="text" placeholder="Student Name"/>
                            </div>
                            <div class="col-12 col-sm-6 mt-4 mt-sm-0">
                                <input class="multisteps-form__input form-control" type="text" placeholder="Phone Number"/>
                            </div>
                        </div>                       
                       
                      
                        <div class="row">
                            <div class="button-row d-flex mt-4 col-12">
                            <button class="btn bluebtn js-btn-prev" type="button" title="Prev">Prev</button>
                            <button class="btn bluebtn ml-auto js-btn-next" type="button" title="Next">Next</button>
                            </div>
                        </div>
                        </div>
                    </div>
                    <!--single form panel4-->
                    <div class="multisteps-form__panel shadow p-4 rounded bg-white" data-animation="scaleIn">
                        <h3 class="multisteps-form__title">College</h3>
                        <div class="multisteps-form__content">
                        <p>College and Branch you are studying?</p>

                        <div class="form-row mt-4">
                            <div class="col-12 col-sm-4">
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1">
                                    <label class="form-check-label" for="inlineRadio1">Electronics</label>
                                </div>
                            </div>
                            <div class="col-12 col-sm-4">
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1">
                                    <label class="form-check-label" for="inlineRadio1">Information Technology</label>
                                </div>
                            </div>

                            <div class="col-12 col-sm-4">
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1">
                                    <label class="form-check-label" for="inlineRadio1">Civil</label>
                                </div>
                            </div>

                            <div class="col-12 col-sm-4">
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1">
                                    <label class="form-check-label" for="inlineRadio1">Mechanical</label>
                                </div>
                            </div>

                            <div class="col-12 col-sm-4">
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1">
                                    <label class="form-check-label" for="inlineRadio1">Electrical</label>
                                </div>
                            </div>

                            <div class="col-12 col-sm-4">
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1">
                                    <label class="form-check-label" for="inlineRadio1">Others</label>
                                </div>
                            </div>

                            <div class="col-12 col-sm-12">
                                <div class="form-check form-check-inline w-100 pt-3">
                                     <input class="multisteps-form__input form-control" type="text" placeholder="Specifiy others Details"/>
                                </div>
                            </div>
                        </div>
                      
                            <div class="button-row d-flex mt-4 col-12">
                                <button class="btn bluebtn js-btn-prev" type="button" title="Prev">Prev</button>
                                <button data-toggle="modal" data-target="#modal-confirm" class="btn bluebtn ml-auto" type="button" title="Send">Submit</button>
                            </div>
                        </div>
                    </div>
                   
                    </form>
                </div>
                </div>
            </div>
                    </div>
            <!--/ container -->
       
        </div>
        <!--/ sub page body -->
    </main>

    <!--/ sub page main -->
   
    <?php include 'footerscripts.php'?>

    <!-- on submit form success message  -->
<div id="modal-confirm" class="modal fade" data-keyboard="false" data-backdrop="static">
	<div class="modal-dialog modal-confirm">
		<div class="modal-content">
			<div class="modal-header">
				<div class="icon-box">
                    <img src="img/checkmark.gif" alt="">
				</div>				
				<h4 class="modal-title text-center">Awesome!</h4>	
			</div>
			<div class="modal-body">
				<p class="text-center">Your booking has been confirmed. Check your email for detials.</p>
			</div>
			<div class="modal-footer">
				<button class="btn btn-success btn-block" data-dismiss="modal">OK</button>
			</div>
		</div>
	</div>
</div>
<!--/ on submit form success message -->

  
</body>

</html>