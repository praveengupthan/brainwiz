<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Test Series Detail</title>
    <link rel="icon" type="image/png" sizes="32x32" href="img/favicon-32x32.png">
    <!-- style sheets -->
    <?php include 'headerstyles.php' ?>
</head>

<body>
    <!-- header -->
   <?php include 'header.php' ?>
    <!--/ header -->
    <!--sub page main -->
    <main class="subpage">
        <!-- sub page header -->
        <div class="page-header">
            <!-- container -->
            <div class="container">
               <!-- row -->
               <div class="row">
                   <div class="col-lg-6">
                       <h1>All Exams <span class="fbold">, One Plan!</span> </h1>
                       <p>for India's No. 1 Test Series</p>
                   </div>
               </div>
               <!--/ row --> 
            </div>
            <!--/ container -->
        </div>
        <!--/ sub page header -->

         <!-- brudcrumb -->
         <div class="container">
            <!-- row -->
            <div class="row">
                <!-- col -->
                <div class="col-lg-12">                   
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.php">Home</a></li> 
                        <li class="breadcrumb-item"><a href="testseries.php">Test Series</a></li>                              
                        <li class="breadcrumb-item active"><a>Exams Plan</a></li>
                    </ul>                    
                </div>
                <!--/col -->
            </div>
            <!--/ row -->
        </div>
        <!--/ brudcrumb -->

        <!-- sub page body -->
        <div class="subpage-body">
        <!-- services -->
    <div class="services-home">
        <!-- container -->
        <div class="container">
            <!-- row -->
            <div class="row justify-content-center">
                <!-- col -->
                <div class="col-lg-4 text-center">
                    <div class="service-col">
                        <a href="javascript:void(0)"><span class="icon-laptop icomoon"></span></a>
                        <h4>Smart Test Player</h4>
                        <p class="text-center">which helps you improve your test score with each test.</p>
                    </div>
                </div>
                <!--/ col -->
                <!-- col -->
                <div class="col-lg-4 text-center">
                    <div class="service-col">
                        <a href="video-tutorials.php"><span class="icon-diagram icomoon"></span></a>
                        <h4>Smart Analytics</h4>
                        <p class="text-center">Get to know the information that matters. </p>
                    </div>
                </div>
                <!--/ col -->
                <!-- col -->
                <div class="col-lg-4 text-center">
                    <div class="service-col">
                        <a href="testseries.php"><span class="icon-question-circle icomoon"></span></a>
                        <h4>Smart Solutions</h4>
                        <p class="text-center">Intelligently developed explanation to every single question. </p>
                    </div>
                </div>
                <!--/ col -->               
            </div>
            <!--/ row -->
        </div>
        <!--/ container -->
    </div>
    <!--/ services -->
           <!-- container -->
           <div class="container">
              <!-- row -->
              <div class="row justify-content-center">               
                <!-- col 12 -->
                <div class="col-lg-5"> 
                    <!-- col -->
                    <div class="package-col">
                        <!-- heading div-->
                        <div class="heading-div">
                            <article class="text-center">
                                <h5>Package Plan Starts</h5>
                                <h2>Rs:500</h2>
                                <p class="text-center">Buy now and avail of all the available and to be available Test Series till 31st Dec. 2018 for just Rs. 500/- </p>
                                <a class="bluebtn" href="signin.php">Signup &amp; Buy Now</a>
                            </article>
                        </div>
                        <!--/ heading div -->
                        <!-- package features -->
                        <div class="package-features">
                            <h4 class="h4">This Plan Includes</h4>
                            <ul>
                                <li>Infosys Test 1</li>
                                <li>Infosys Test 2</li>
                                <li>Tech Mahindra Test 1</li>
                                <li>IBM Test 1</li>
                                <li>IBM Test 2</li>
                                <li>Tech Mahindra Test 2</li>
                                <li>Capgemini Test 2</li>
                                <li>Capgemini Test 1</li>
                                <li>Accenture Test 1</li>
                                <li>Accenture test 2</li>
                                <li>Infosys 2018 Test 1</li>
                                <li>Infosys 2018 Test 2</li>
                            </ul>
                        </div>
                        <!--/ package features -->
                    </div>
                    <!--/ col -->                   
                </div>
                <!--/ col 12 -->
              </div>
              <!--/ row -->
           </div>
           <!--/ container -->
        </div>
        <!--/ sub page body -->
    </main>
    <!--/ sub page main -->
    <!--footer -->
    <?php include 'footer.php' ?>
    <!--/ footer -->  
    <?php include 'footerscripts.php'?>
</body>

</html>