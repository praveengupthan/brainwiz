<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Brainwiz</title>
    <link rel="icon" type="image/png" sizes="32x32" href="img/favicon-32x32.png">
    <!-- style sheets -->
    <?php include 'headerstyles.php' ?>
</head>

<body>

    <!-- header -->
   <?php include 'header.php' ?>
    <!--/ header -->
    <!--sub page main -->
    <main class="subpage">
        <!-- sub page header -->
        <div class="page-header course-detail-header">
            <!-- container -->
            <div class="container">
               <!-- row -->
               <div class="row">
                   <div class="col-lg-8">
                        <p class="pb-0 mb-0">Bundle</p>
                       <h1>Campus Recruitment Training</h1>
                       <p>In this Complete Course, we have Logical Reasoning, Arithmetic and Verbal Ability with detailed Videos and Practice Tests to improve accuracy.</p>

                       <ul class="nav list-seperator">
                            <li class="nav-item"><a><span class="fbold">500 Students</span> Enrolled</a></li>
                            <li class="nav-item"><a>Author <span class="fbold">Pavan Jaiswal </span></a></li>
                            <li class="nav-item"><a>Last Updated - <span class="fbold">Oct  2019 </span></a></li>
                       </ul>
                   </div>
               </div>
               <!--/ row --> 
            </div>
            <!--/ container -->
        </div>
        <!--/ sub page header -->

        <!-- brudcrumb -->
        <div class="container">
            <!-- row -->
            <div class="row">
                <!-- col -->
                <div class="col-lg-12">                   
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                        <li class="breadcrumb-item"><a href="premium-courses.php">Premium Coursess</a></li>
                        <li class="breadcrumb-item active"><a>CRT</a></li>
                    </ul>                    
                </div>
                <!--/col -->
            </div>
            <!--/ row -->           
        </div>
        <!--/ brudcrumb -->

        <!-- sub page body -->
        <div class="subpage-body">
           <!-- container -->
           <div class="container">

            <!--course row -->
            <div class="row">
                <!-- course detail left -->
                <div class="col-lg-8">   

                    <h4 class="h5 fblue fbold pb-2">About the Bundle</h4>
                    <p>The Aptitude evaluates the thinking ability, problem solving, decision making and also potential to perform a certain task. This Course will be helpful for Students who are going to appear for any Job Placement/Interview also for those who are appearing for Campus Placements like Infosys, TCS, Accenture, IBM, Cognizant and many more ..Government Jobs, BANK Exams, Campus Placements, GATE, PSU. As we are serving the students since 11 years in the classroom training, Now our same online platform will benefit the students sitting at their place. As far as the GATE exam is concerned, this COURSE covers all topics having very high weightage in Exams like GATE as well as it helps in cracking aptitude papers of Campus Placement Companies.e performance of any business or team.   Start hiring confidently, skilfully and professionally. Make right decisions first time; avoid costly hiring mistakes   </p>                   

                    <!-- list box -->
                    <div class="list-box py-2">
                        <h3 class="h5 fblue fbold pb-2">What are the skills required</h3>
                        <ul class="row">
                            <li class="col-lg-12">Passedout Freshers who are yet to get Job</li>
                            <li class="col-lg-12">Students who are planning for GATE, BANKING, GOVT EXAM</li> 
                            <li class="col-lg-12">Students who are in Final Year of their Graduation</li>                                                     
                        </ul>
                    </div>
                    <!--/ list box -->

                     <!-- list box -->
                     <div class="list-box border p-3">
                        <h2 class="h5 fblue fbold pb-2">What are the Topics Covered in the Course ??</h2>
                        <p>As we all know that Quantitative Aptitude has No Limit, But we cover all those topics needed for different exams. Questions can be asked by merging topics,then also here are All the Important Topics Needed in the Quantitative Aptitude and you will Learn in the Course.</p>
                        <ul class="row">
                            <li class="col-lg-4">NUMBER SERIES</li>
                            <li class="col-lg-4">LETTER SERIES</li>
                            <li class="col-lg-4">CODING & DECODING</li>
                            <li class="col-lg-4">BLOOD RELATIONS</li>
                            <li class="col-lg-4">DIRECTION SENSE</li>
                            <li class="col-lg-4">SEATING ARRANGEMENT</li>
                            <li class="col-lg-4">TIME SEQUENCING RANKING</li>
                            <li class="col-lg-4">DEDUCTIONS SYLLOGISMS</li>
                            <li class="col-lg-4">LOGICAL CONNECTIVES</li>                            
                        </ul>
                    </div>
                    <!--/ list box -->

                    <!--about the author-->
                    <div class="pt-3">
                        <h4 class="h5 fblue fbold pb-2">About the Author</h4>
                        <p>Mr Pavan Jaiswal is the Managing Director & Founder of BRAINWIZ Education. After completing his Masters in Computer Science, he started working in MADEEASY (GATE preparation) and GANESH IAS (for CSAT exam) as an aptitude trainer successfully. Following this, he started BRAINWIZ in order to deliver his training to common students to enter into TOP MNCs by his aptitude training. His unique way of teaching has helped thousands of students to solve the problems in effortless way. Pavan Jaiswal has played a pivotal role in the success of BRAINWIZ and taking it further in the current Education Market with the quality and results. Our team has more than 10 years of Experience  </p>          
                    </div>
                    <!--/ about the author -->

                    <!-- videos list -->
                    <div class="row">
                        <!-- title -->
                        <div class="col-lg-12 d-flex justify-content-between border-bottom">
                            <h5 class="h5 fblue fbold pb-2">Course Content</h5>
                            <ul class="nav list-seperator">
                                <li class="nav-item"><a>44 Lectures  </a></li>
                                <li class="nav-item"><a>02:53:19</a></li>
                            </ul>
                        </div>
                        <!--/ title -->

                        <!-- col -->
                        <div class="col-lg-6">
                           <div class="p-2">
                                <iframe width="100%" height="250" src="https://www.youtube.com/embed/MmsoIcYrXJU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>                                
                                <article>
                                    <h6 class="h6"><a href="javascript:void(0)">Welcome , meet  your trainer and  get started.</a></h6>

                                    <p class="text-right fblue">Duration: 09:39</p>
                                </article>
                           </div>
                        </div>
                        <!--/ col -->
                        <!-- col -->
                        <div class="col-lg-6">
                           <div class="p-2">
                                <iframe width="100%" height="250" src="https://www.youtube.com/embed/sRFEKvsw-vs" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>                        
                                <article>
                                    <h6 class="h6"><a href="javascript:void(0)">Understanding the recruitment and selection process</a></h6>

                                    <p class="text-right fblue">Duration: 09:39</p>
                                </article>
                           </div>
                        </div>
                        <!--/ col -->

                          <!-- col -->
                          <div class="col-lg-6">
                           <div class="p-2">
                                <iframe width="100%" height="250" src="https://www.youtube.com/embed/6oeeedtz9Gg" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>                                
                                <article>
                                    <h6 class="h6"><a href="javascript:void(0)">How to develop knowledge of the job you are hiring for</a></h6>

                                    <p class="text-right fblue">Duration: 09:39</p>
                                </article>
                           </div>
                        </div>
                        <!--/ col -->

                        <!-- col -->
                        <div class="col-lg-6">
                           <div class="p-2">
                                <iframe width="100%" height="250" src="https://www.youtube.com/embed/oEeTWlsdB1o?list=PLd3UqWTnYXOk86tqsjN5UH9fS_GIPTDfG" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> 
                                <article>
                                    <h6 class="h6"><a href="javascript:void(0)">Knowledge of the person - how to specify your exact requirements</a></h6>

                                    <p class="text-right fblue">Duration: 09:39</p>
                                </article>
                           </div>
                        </div>
                        <!--/ col -->

                         <!-- col -->
                         <div class="col-lg-6">
                           <div class="p-2">
                                <iframe width="100%" height="250" src="https://www.youtube.com/embed/MmsoIcYrXJU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>                                
                                <article>
                                    <h6 class="h6"><a href="javascript:void(0)">Attracting suitable applicants -an overview of the key channels available.</a></h6>

                                    <p class="text-right fblue">Duration: 09:39</p>
                                </article>
                           </div>
                        </div>
                        <!--/ col -->

                           <!-- col -->
                           <div class="col-lg-6">
                           <div class="p-2">
                                <iframe width="100%" height="250" src="https://www.youtube.com/embed/MmsoIcYrXJU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>                                
                                <article>
                                    <h6 class="h6"><a href="javascript:void(0)">The interview: Understand the sequence and the part your play.</a></h6>

                                    <p class="text-right fblue">Duration: 09:39</p>
                                </article>
                           </div>
                        </div>
                        <!--/ col -->

                    </div>
                    <!--/ videos list -->

                    <!-- faq's-->
                      <!-- general enquireis -->
                      <div class="custom-accord border-top pt-4">                              
                                <h3 class="h5 pb-3">General Enquries</h3>
                                <div class="accordion">
                                    <h3 class="panel-title">How can we get the services we need?</h3>
                                    <div class="panel-content">
                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                                    </div>
                                    <h3 class="panel-title">Is there any way to receive it?</h3>
                                    <div class="panel-content">
                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                                    </div>
                                    <h3 class="panel-title">What do I need to do to graduate from a Nanodegree program?</h3>
                                    <div class="panel-content">
                                        <p>You will need to review all the content and successfully complete all projects with a grade of “Meets Specifications” for your specific Nanodegree program as set forth in the curriculum and syllabus. Since you pay month-by-month, it's up to you when you want to plan to graduate.</p>
                                    </div>
                                    <h3 class="panel-title">What is a Nanodegree Program?</h3>
                                    <div class="panel-content">
                                        <p>A Udacity Nanodegree® Program is a unique online educational offering designed to bridge the gap between learning and career goals. We partner with industry leaders and experts who understand what skills are in demand in the applicable job market. Students enroll in a specific course offering (e.g. Intro to Programming) and will be prompted to view the online course as well as complete a series of projects and support courses designed to help them develop job-relevant skills and build a portfolio to show prospective employers. Services related to the Nanodegree program may include classroom mentorship, moderated forums, and project reviews to ensure a personalized experience. Additional or different services may be offered in certain Nanodegree Programs which will be noted in their respective Program Details page.</p>
                                    </div>
                                    <h3 class="panel-title">How can we get the services we need?</h3>
                                    <div class="panel-content">
                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                                    </div>
                                    <h3 class="panel-title">Is there any way to receive it?</h3>
                                    <div class="panel-content">
                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                                    </div>
                                    <h3 class="panel-title">What do I need to do to graduate from a Nanodegree program?</h3>
                                    <div class="panel-content">
                                        <p>You will need to review all the content and successfully complete all projects with a grade of “Meets Specifications” for your specific Nanodegree program as set forth in the curriculum and syllabus. Since you pay month-by-month, it's up to you when you want to plan to graduate.</p>
                                    </div>
                                </div>                             
                            </div>
                            <!--/ general enquiries -->
                    <!--/ faq's-->
                </div>
                <!--/ course detail left -->

                <!-- course detail right -->
                <div class="col-lg-4 ">
                    <!-- sticky div-->
                    <div class="sticky-top">
                        <!-- whgite box -->
                        <div class="p2 whitebox">
                            <!-- video -->
                            <iframe width="100%" height="260" src="https://www.youtube.com/embed/MmsoIcYrXJU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>    
                            <!--/ video -->
                            <h3 class="h3 fbold detailprice">Rs: 1200 <span class="oldprice">₹7,040 </span> <span class="fbold">94% off</span></h3>

                            <div class="py-4 d-flex justify-content-between">
                                <a class="whitebtn border d-inline-block w-50 text-center" href="cart.php">BUY BUNDLE</a>
                                <a class="whitebtn border d-inline-block w-50 text-center" href="checkout.php">ENROLL FOR TRIAL</a>
                            </div>
                            <!-- list box -->
                            <div class="list-box p-4">
                                <h3 class="h6 fblue fbold pb-2">This course Include</h3>
                                <ul class="row">
                                    <li class="col-lg-12">4 Courses</li>
                                    <li class="col-lg-12">180 Days Validity</li>                                         
                                </ul>
                            </div>
                            <!--/ list box -->
                        </div>
                        <!--/ white box -->

                        <!-- blue box -->
                        <div class="bluebox mt-3">
                            <h5 class="h6 fwhite">Recommend to Your Friend</h5>
                            <form class="py-2 d-flex subscribebox justify-content-between">
                                <input type="text" placeholder="Enter Your Email">
                                <input type="submit" value="Submit">
                            </form>
                            <p class="text-left">Get access this course and get knowledge and recommend to your friend and get him grow</p>
                        </div>
                        <!--/ blue box -->
                    </div>
                    <!--/ sticky div -->
                </div>
                <!--/ course detail right -->
            </div>
            <!--/ course row -->
           </div>
            <!--/ container -->
        </div>
        <!--/ sub page body -->
    </main>
    <!--/ sub page main -->
    <!--footer -->
    <?php include 'footer.php' ?>
    <!--/ footer -->  
    <?php include 'footerscripts.php'?>

</body>

</html>