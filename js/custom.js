//mobiel dropdown toggle
// $(document).ready(function () {
//     $(".navbar-nav .dropdown-toggle").click(function () {
//         $(".navbar-nav > .dropdown-menu").toggle();
//     });
// });

//toggle icon in navigation
$(".navbar-toggler").click(function () {
  $(this).find("i").toggleClass("icon-bars").toggleClass("icon-close");
});

$(document).ready(function () {
  //on scroll add class to header

  $(window).scroll(function () {
    if ($(this).scrollTop() > 50) {
      $(".fixed-top").addClass("fixed-theme");
    } else {
      $(".fixed-top").removeClass("fixed-theme");
    }
  });

  //on click stop video
  $("#videopop").on("hidden.bs.modal", function (e) {
    $("#videopop iframe").attr("src", "");
  });

  $("#videopop").on("show.bs.modal", function (e) {
    $("#videopop iframe").attr(
      "src",
      "https://www.youtube.com/embed/st0IJgPwtD8?rel=0&amp;autoplay=1"
    );
  });

  //on click stop video for students video testimonials
  $("#student-testimonial").on("hidden.bs.modal", function (e) {
    $("#student-testimonial iframe").attr("src", "");
  });

  $("#student-testimonial").on("show.bs.modal", function (e) {
    $("#student-testimonial iframe").attr(
      "src",
      "https://www.youtube.com/embed/HSgjpQBkR0c?rel=0&amp;autoplay=1"
    );
  });

  //on click stop introduction video popup for home page
  $("#video-autopop").on("hidden.bs.modal", function (e) {
    $("#video-autopop video").attr("src", "");
  });

  $("#video-autopop").on("show.bs.modal", function (e) {
    $("#video-autopop video").attr(
      "src",
      "http://www.gobrainwiz.in/upload/Videos/RAMA7308.mp4?rel=0&amp;autoplay=1"
    );
  });

  //on click stop video popup for video tutorials if video is video element
  $("#videopop").on("hidden.bs.modal", function (e) {
    $("#videopop video").attr("src", "");
  });

  $("#videopop").on("show.bs.modal", function (e) {
    $("#videopop video").attr(
      "src",
      "http://www.gobrainwiz.in/upload/Videos/RAMA7308.mp4"
    );
  });

  //click event to move top

  $(window).scroll(function () {
    if ($(this).scrollTop() > 100) {
      $("#movetop").fadeIn();
    } else {
      $("#movetop").fadeOut();
    }
  });

  //Click event to scroll to top
  $("#movetop").click(function () {
    $("html, body").animate(
      {
        scrollTop: 0,
      },
      800
    );
    return false;
  });
});

//success stories
$(".successStories").slick({
  dots: false,
  infinite: true,
  speed: 300,
  slidesToShow: 4,
  slidesToScroll: 1,
  autoplay: true,
  centerMode: false,
  responsive: [
    {
      breakpoint: 1200,
      settings: {
        slidesToShow: 4,
        slidesToScroll: 1,
        infinite: true,
        dots: true,
      },
    },
    {
      breakpoint: 991,
      settings: {
        slidesToShow: 4,
        slidesToScroll: 1,
      },
    },
    {
      breakpoint: 766,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 1,
      },
    },
    {
      breakpoint: 574,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1,
      },
    },
    // You can unslick at a given breakpoint now by adding:
    // settings: "unslick"
    // instead of a settings object
  ],
});

//home testimonials
$(".students-testimonials").slick({
  dots: false,
  infinite: true,
  speed: 300,
  slidesToShow: 2,
  slidesToScroll: 1,
  autoplay: true,
  centerMode: false,
  responsive: [
    {
      breakpoint: 1200,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1,
        infinite: true,
        dots: true,
      },
    },
    {
      breakpoint: 991,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1,
      },
    },
    {
      breakpoint: 766,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1,
      },
    },
    {
      breakpoint: 574,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
      },
    },
    // You can unslick at a given breakpoint now by adding:
    // settings: "unslick"
    // instead of a settings object
  ],
});

//home companies
$(".companies").slick({
  dots: false,
  infinite: true,
  speed: 300,
  slidesToShow: 8,
  slidesToScroll: 1,
  autoplay: true,
  centerMode: false,
  responsive: [
    {
      breakpoint: 1200,
      settings: {
        slidesToShow: 8,
        slidesToScroll: 1,
        infinite: true,
        dots: true,
      },
    },
    {
      breakpoint: 991,
      settings: {
        slidesToShow: 6,
        slidesToScroll: 1,
      },
    },
    {
      breakpoint: 766,
      settings: {
        slidesToShow: 4,
        slidesToScroll: 1,
      },
    },
    {
      breakpoint: 574,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 1,
      },
    },
    // You can unslick at a given breakpoint now by adding:
    // settings: "unslick"
    // instead of a settings object
  ],
});

//success stories
$(".partnersvideos").slick({
  dots: false,
  infinite: true,
  speed: 300,
  slidesToShow: 2,
  slidesToScroll: 1,
  autoplay: true,
  centerMode: false,
  responsive: [
    {
      breakpoint: 1200,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1,
        infinite: true,
        dots: true,
      },
    },
    {
      breakpoint: 991,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
      },
    },
    {
      breakpoint: 766,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
      },
    },
    {
      breakpoint: 574,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
      },
    },
    // You can unslick at a given breakpoint now by adding:
    // settings: "unslick"
    // instead of a settings object
  ],
});

//tabs
$(document).ready(function () {
  //Horizontal Tab
  $(".parentHorizontalTab").easyResponsiveTabs({
    type: "default", //Types: default, vertical, accordion
    width: "auto", //auto or any width like 600px
    fit: true, // 100% fit in a container
    tabidentify: "hor_1", // The tab groups identifier
    activate: function (event) {
      // Callback function if tab is switched
      var $tab = $(this);
      var $info = $("#nested-tabInfo");
      var $name = $("span", $info);
      $name.text($tab.text());
      $info.show();
    },
  });
  // Child Tab
  $(".ChildVerticalTab_1").easyResponsiveTabs({
    type: "vertical",
    width: "auto",
    fit: true,
    tabidentify: "ver_1", // The tab groups identifier
    activetab_bg: "#fff", // background color for active tabs in this group
    inactive_bg: "#F5F5F5", // background color for inactive tabs in this group
    active_border_color: "#c1c1c1", // border color for active tabs heads in this group
    active_content_border_color: "#5AB1D0", // border color for active tabs contect in this group so that it matches the tab head border
  });
  //Vertical Tab
  $(".parentVerticalTab").easyResponsiveTabs({
    type: "vertical", //Types: default, vertical, accordion
    width: "auto", //auto or any width like 600px
    fit: true, // 100% fit in a container
    closed: "accordion", // Start closed if in accordion view
    tabidentify: "hor_1", // The tab groups identifier
    activate: function (event) {
      // Callback function if tab is switched
      var $tab = $(this);
      var $info = $("#nested-tabInfo2");
      var $name = $("span", $info);
      $name.text($tab.text());
      $info.show();
    },
  });
});

///accordian
// Hiding the panel content. If JS is inactive, content will be displayed
$(".panel-content").hide();

// Preparing the DOM

// -- Update the markup of accordion container
$(".accordion").attr({
  role: "tablist",
  multiselectable: "true",
});

// -- Adding ID, aria-labelled-by, role and aria-labelledby attributes to panel content
$(".panel-content").attr("id", function (IDcount) {
  return "panel-" + IDcount;
});
$(".panel-content").attr("aria-labelledby", function (IDcount) {
  return "control-panel-" + IDcount;
});
$(".panel-content").attr("aria-hidden", "true");
// ---- Only for accordion, add role tabpanel
$(".accordion .panel-content").attr("role", "tabpanel");

// -- Wrapping panel title content with a <a href="">
$(".panel-title").each(function (i) {
  // ---- Need to identify the target, easy it's the immediate brother
  $target = $(this).next(".panel-content")[0].id;

  // ---- Creating the link with aria and link it to the panel content
  $link = $("<a>", {
    href: "#" + $target,
    "aria-expanded": "false",
    "aria-controls": $target,
    id: "control-" + $target,
  });

  // ---- Output the link
  $(this).wrapInner($link);
});

// Optional : include an icon. Better in JS because without JS it have non-sense.
$(".panel-title a").append('<span class="icon">+</span>');

// Now we can play with it
$(".panel-title a").click(function () {
  if ($(this).attr("aria-expanded") == "false") {
    //If aria expanded is false then it's not opened and we want it opened !

    // -- Only for accordion effect (2 options) : comment or uncomment the one you want

    // ---- Option 1 : close only opened panel in the same accordion
    //      search through the current Accordion container for opened panel and close it, remove class and change aria expanded value
    $(this)
      .parents(".accordion")
      .find("[aria-expanded=true]")
      .attr("aria-expanded", false)
      .removeClass("active")
      .parent()
      .next(".panel-content")
      .slideUp(200)
      .attr("aria-hidden", "true");

    // Option 2 : close all opened panels in all accordion container
    //$('.accordion .panel-title > a').attr('aria-expanded', false).removeClass('active').parent().next('.panel-content').slideUp(200);

    // Finally we open the panel, set class active for styling purpos on a and aria-expanded to "true"
    $(this)
      .attr("aria-expanded", true)
      .addClass("active")
      .parent()
      .next(".panel-content")
      .slideDown(200)
      .attr("aria-hidden", "false");
  } else {
    // The current panel is opened and we want to close it

    $(this)
      .attr("aria-expanded", false)
      .removeClass("active")
      .parent()
      .next(".panel-content")
      .slideUp(200)
      .attr("aria-hidden", "true");
  }
  // No Boing Boing
  return false;
});

//accordian ends

$(document).ready(function () {
  $('[data-toggle="popover"]').popover();
});

$(document).ready(function () {
  $('[data-toggle="tooltip"]').tooltip();
});

var h2 = document.getElementsByClassName("time-exam")[0],
  start = document.getElementById("start"),
  stop = document.getElementById("stop"),
  clear = document.getElementById("clear"),
  seconds = 0,
  minutes = 0,
  hours = 0,
  t;

function add() {
  seconds++;
  if (seconds >= 60) {
    seconds = 0;
    minutes++;
    if (minutes >= 60) {
      minutes = 0;
      hours++;
    }
  }

  h2.textContent =
    (hours ? (hours > 9 ? hours : "0" + hours) : "00") +
    ":" +
    (minutes ? (minutes > 9 ? minutes : "0" + minutes) : "00") +
    ":" +
    (seconds > 9 ? seconds : "0" + seconds);

  timer();
}

function timer() {
  t = setTimeout(add, 1000);
}
timer();

/* Start button */
start.onclick = timer;

/* Stop button */
stop.onclick = function () {
  clearTimeout(t);
};

/* Clear button */
clear.onclick = function () {
  h2.textContent = "00:00:00";
  seconds = 0;
  minutes = 0;
  hours = 0;
};

//questino and answer click
$(".primary-details-practise-test .list-group-item").click(function () {
  $(this).addClass("active-answer").siblings().removeClass("active-answer");
});
