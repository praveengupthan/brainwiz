<footer>
        <!-- top footer-->
        <div class="container footer-campus">
            <h2 class="text-center">Get in touch with us to hit the mark on ROI</h2>
            <div class="social-nav">
                <ul class="nav justify-content-center">
                    <li class="nav-item">
                        <a href="javascript:void(0)" target="_blank" class="nav-link">
                            <span class="icon-facebook icomoon"></span></a>
                    </li>
                    <li class="nav-item">
                        <a href="javascript:void(0)" target="_blank" class="nav-link">
                            <span class="icon-twitter icomoon"></span></a>
                    </li>
                    <li class="nav-item">
                        <a href="javascript:void(0)" target="_blank" class="nav-link">
                            <span class="icon-linkedin icomoon"></span></a>
                    </li>
                </ul>
            </div>

            <div class="py-3 text-center">
                <h5>info@brainwiz.com</h5>
                <h5>+91-8181 8787 93 |  +91-7709 48 9434</h5>
            </div>

        </div>
        <!--/ top footer -->
        <!-- bottom footer -->
        <div class="bottom-footer text-center">
            <p>© Copyright 2017 Go Brain Wiz | All Rights Reserved.</p>
        </div>
        <!--/ bottom footer -->
        <a id="movetop" class="movetop" href="javascript:void(0)"><span class="icon-arrow-up icomoon"></span></a>

    </footer>
