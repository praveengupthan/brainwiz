<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Brainwiz</title>
    <link rel="icon" type="image/png" sizes="32x32" href="img/favicon-32x32.png">
    <!-- style sheets -->
    <?php include 'headerstyles.php' ?>
</head>

<body>
    <!-- header -->
   <?php include 'header.php' ?>
    <!--/ header -->
    <!--sub page main -->
    <main class="subpage">
        <!-- sub page header -->
        <div class="page-header">
            <!-- container -->
            <div class="container">
               <!-- row -->
               <div class="row">
                   <div class="col-lg-6">
                       <h1>Our Success  <span class="fbold">Stories</span></h1>
                   </div>
               </div>
               <!--/ row --> 
            </div>
            <!--/ container -->
        </div>
        <!--/ sub page header -->

        <!-- brudcrumb -->
        <div class="container">
            <!-- row -->
            <div class="row">
                <!-- col -->
                <div class="col-lg-12">                   
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                        <li class="breadcrumb-item active"><a>Students</a></li>
                    </ul>                    
                </div>
                <!--/col -->
            </div>
            <!--/ row -->
        </div>
        <!--/ brudcrumb -->

        <!-- sub page body -->
        <div class="subpage-body">
           <!-- container -->
           <div class="container">
             <!-- row -->
             <div class="row">
                <!-- col -->
                <div class="col-lg-6">
                    <div class="student-col d-flex position-relative">
                        <figure class="align-self-center">
                            <a href="javascript:void(0)" data-toggle="modal" data-target=".student-testimonial"><img src="img/data/student01.jpg"></a>
                        </figure>
                        <article class="align-self-center">
                            <h2 class="h5">Praveen Kumar Nandipati</h2>
                            <p class="d-flex justify-content-between small">
                                <span>Warangal</span>
                                <span class="fblue fbold">Placed in Infosys</span>
                            </p>
                            <p class="quote-student">
                            It was superb classes and i think  will never be able to get another superb teacher like pavan sir,i am so grateful to him for making me learn such crazy tricks for apti,reasoning,etc..tq Brainwiz team..
                            </p>
                        </article>

                        <div class="linktext d-flex">
                            <h6 class="align-self-end pr-4 fblue">Watch Video</h6>
                            <div class="video-testimonil-link" data-toggle="modal" data-target=".student-testimonial">
                                <a href="javascript:void(0)"> 
                                    <span class="icon-play icomoon"></span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <!--/ col -->

                  <!-- col -->
                  <div class="col-lg-6">
                    <div class="student-col d-flex position-relative">
                        <figure class="align-self-center">
                        <a href="javascript:void(0)"><img src="img/data/student02.jpg"></a>
                        </figure>
                        <article class="align-self-center">
                            <h2 class="h5">Praveen Kumar Nandipati</h2>
                            <p class="d-flex justify-content-between small">
                                <span>Warangal</span>
                                <span class="fblue fbold">Placed in Infosys</span>
                            </p>
                            <p class="quote-student">
                            It was superb classes and i think  will never be able to get another superb teacher like pavan sir,i am so grateful to him for making me learn such crazy tricks for apti,reasoning,etc..tq Brainwiz team..
                            </p>
                        </article>                     
                    </div>
                </div>
                <!--/ col -->

                  <!-- col -->
                  <div class="col-lg-6">
                    <div class="student-col d-flex position-relative">
                        <figure class="align-self-center">
                        <a href="javascript:void(0)"><img src="img/data/student03.jpg"></a>
                        </figure>
                        <article class="align-self-center">
                            <h2 class="h5">Praveen Kumar Nandipati</h2>
                            <p class="d-flex justify-content-between small">
                                <span>Warangal</span>
                                <span class="fblue fbold">Placed in Infosys</span>
                            </p>
                            <p class="quote-student">
                            It was superb classes and i think  will never be able to get another superb teacher like pavan sir,i am so grateful to him for making me learn such crazy tricks for apti,reasoning,etc..tq Brainwiz team..
                            </p>
                        </article>                      
                    </div>
                </div>
                <!--/ col -->

                  <!-- col -->
                  <div class="col-lg-6">
                    <div class="student-col d-flex position-relative">
                        <figure class="align-self-center">
                        <a href="javascript:void(0)"><img src="img/data/student04.jpg"></a>
                        </figure>
                        <article class="align-self-center">
                            <h2 class="h5">Praveen Kumar Nandipati</h2>
                            <p class="d-flex justify-content-between small">
                                <span>Warangal</span>
                                <span class="fblue fbold">Placed in Infosys</span>
                            </p>
                            <p class="quote-student">
                            It was superb classes and i think  will never be able to get another superb teacher like pavan sir,i am so grateful to him for making me learn such crazy tricks for apti,reasoning,etc..tq Brainwiz team..
                            </p>
                        </article>                      
                    </div>
                </div>
                <!--/ col -->

                  <!-- col -->
                  <div class="col-lg-6">
                    <div class="student-col d-flex position-relative">
                        <figure class="align-self-center">
                        <a href="javascript:void(0)"><img src="img/data/student05.jpg"></a>
                        </figure>
                        <article class="align-self-center">
                            <h2 class="h5">Praveen Kumar Nandipati</h2>
                            <p class="d-flex justify-content-between small">
                                <span>Warangal</span>
                                <span class="fblue fbold">Placed in Infosys</span>
                            </p>
                            <p class="quote-student">
                            It was superb classes and i think  will never be able to get another superb teacher like pavan sir,i am so grateful to him for making me learn such crazy tricks for apti,reasoning,etc..tq Brainwiz team..
                            </p>
                        </article>                      
                    </div>
                </div>
                <!--/ col -->
             </div>
             <!--/ row -->
           </div>
           <!--/ container -->
        </div>
        <!--/ sub page body -->
    </main>
    <!--/ sub page main -->
    <!--footer -->
    <?php include 'footer.php' ?>
    <!--/ footer -->  
    <?php include 'footerscripts.php'?>

    <!-- Modal video -->
    <div class="modal fade video-modal student-testimonial"  tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog  modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">               
                    <button id="close" type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <iframe id="ytplayer" class="modalvideo" width="100%" src="https://www.youtube.com/embed/HSgjpQBkR0c"  allow="autoplay" frameborder="0" allow="accelerometer; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </div>
    <!--/ modal video -->


    <script>
   
    $('.video-modal').on('hidden.bs.modal', function (e) {
    $('.video-modal iframe').attr('src', '');
    });

    $('.video-modal').on('show.bs.modal', function (e) {
        $('.video-modal iframe').attr('src', 'https://www.youtube.com/embed/HSgjpQBkR0c?rel=0&amp;autoplay=1');
    });
    </script>

</body>

</html>