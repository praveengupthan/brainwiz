<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Brainwiz Video Tutorials</title>
    <link rel="icon" type="image/png" sizes="32x32" href="img/favicon-32x32.png">
    <!-- style sheets -->
    <?php include 'headerstyles.php' ?>
</head>

<body>

    <!-- header -->
   <?php include 'header-postlogin.php' ?>
    <!--/ header -->
    <!--sub page main -->
    <main class="subpage">
        <!-- sub page header -->
        <div class="page-header">
            <!-- container -->
            <div class="container">
               <!-- row -->
               <div class="row">
                   <div class="col-lg-6">
                       <h1>Make <span class="fbold">Payment</span> </h1>
                   </div>
               </div>
               <!--/ row --> 
            </div>
            <!--/ container -->
        </div>
        <!--/ sub page header -->

         <!-- brudcrumb -->
         <div class="container">
            <!-- row -->
            <div class="row">
                <!-- col -->
                <div class="col-lg-12">                   
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                        <li class="breadcrumb-item active"><a>Make Payment</a></li>
                    </ul>                    
                </div>
                <!--/col -->
            </div>
            <!--/ row -->
        </div>
        <!--/ brudcrumb -->

        <!-- sub page body -->
        <div class="subpage-body">
           <!-- container -->
           <div class="container">
              <!-- row -->
              <div class="row justify-content-around">
                    <!-- left col -->
                    <div class="col-lg-8">
                       <!-- white box -->
                       <div class="whitebox">
                           <!-- tab -->
                           <div class="parentVerticalTab">
                                <ul class="resp-tabs-list hor_1">
                                    <li><span class="icon-credit-card icomoon"></span> Debit Card</li>
                                    <li><span class="icon-credit-card icomoon"></span> Credit Card </li>
                                    <li><span class="icon-bank icomoon"></span> Net Banking</li> 
                                </ul>

                                <div class="resp-tabs-container hor_1">
                                    <!-- debit card payment -->
                                    <div> 
                                        <h5 class="h5">Pay Using Debit Card</h5>
                                        <form>
                                            <div class="form-group">
                                                <input type="text" placeholder="Enter Card Number" class="form-control">
                                            </div>
                                            <div class="form-group w-25 float-left">
                                                <select class="form-control ">
                                                    <option>MM</option>
                                                    <option>January</option>
                                                    <option>February</option>
                                                    <option>March</option>
                                                    <option>April</option>
                                                    <option>May</option>
                                                    <option>June</option>
                                                    <option>July</option>
                                                    <option>August</option>
                                                    <option>September</option>
                                                    <option>October</option>
                                                    <option>Novembeer</option>
                                                    <option>December</option>
                                                </select>
                                            </div>
                                            <div class="form-group w-25 float-left mx-2">
                                                <select class="form-control ">
                                                    <option>YY</option>
                                                    <option>2011</option>
                                                    <option>2012</option>
                                                    <option>2013</option>
                                                    <option>2014</option>
                                                    <option>2015</option>
                                                    <option>2016</option>
                                                    <option>2017</option>
                                                    <option>2018</option>
                                                    <option>2019</option>
                                                    <option>2020</option>
                                                    <option>2021</option>
                                                    <option>2022</option>
                                                </select>
                                            </div>
                                            <div class="form-group w-25 float-left">
                                                <input type="text" placeholder="CVV" class="form-control">
                                            </div>

                                            <div class="form-group w-100">
                                                <input type="text" placeholder="Name on Card Holder" class="form-control">
                                            </div>
                                            <h2 class="h4 fbold">Your Pay <span class="fblue float-right">Rs: 8,800</span></h2>
                                            <a href="payment-success.php"class="bluebtn my-2 d-inline-block">Pay Rs:2500</a>
                                            <p class="small py-2">This card will be saved for a faster payment experience</p>

                                        </form>
                                    </div>
                                    <!--/ debit card payment -->
                                    <!-- credit card payment -->
                                    <div> 
                                    <h5 class="h5">Pay Using Credit Card</h5>
                                        <form>
                                            <div class="form-group">
                                                <input type="text" placeholder="Enter Card Number" class="form-control">
                                            </div>
                                            <div class="form-group w-25 float-left">
                                                <select class="form-control ">
                                                    <option>MM</option>
                                                    <option>January</option>
                                                    <option>February</option>
                                                    <option>March</option>
                                                    <option>April</option>
                                                    <option>May</option>
                                                    <option>June</option>
                                                    <option>July</option>
                                                    <option>August</option>
                                                    <option>September</option>
                                                    <option>October</option>
                                                    <option>Novembeer</option>
                                                    <option>December</option>
                                                </select>
                                            </div>
                                            <div class="form-group w-25 float-left mx-2">
                                                <select class="form-control ">
                                                    <option>YY</option>
                                                    <option>2011</option>
                                                    <option>2012</option>
                                                    <option>2013</option>
                                                    <option>2014</option>
                                                    <option>2015</option>
                                                    <option>2016</option>
                                                    <option>2017</option>
                                                    <option>2018</option>
                                                    <option>2019</option>
                                                    <option>2020</option>
                                                    <option>2021</option>
                                                    <option>2022</option>
                                                </select>
                                            </div>
                                            <div class="form-group w-25 float-left">
                                                <input type="text" placeholder="CVV" class="form-control">
                                            </div>

                                            <div class="form-group w-100">
                                                <input type="text" placeholder="Name on Card Holder" class="form-control">
                                            </div>
                                            <h2 class="h4 fbold">Your Pay <span class="fblue float-right">Rs: 8,800</span></h2>
                                            <a href="payment-success.php"class="bluebtn my-2 d-inline-block">Pay Rs:2500</a>
                                            <p class="small py-2">This card will be saved for a faster payment experience</p>
                                        </form>
                                    </div>
                                    <!--/ credit card payment -->
                                    <!-- net banking -->
                                    <div> 
                                    <h5 class="h5">Pay Net Banking</h5>
                                        <form>
                                            <div class="form-group">
                                                <input type="text" placeholder="Enter Card Number" class="form-control">
                                            </div>
                                            <div class="form-group w-25 float-left">
                                                <select class="form-control ">
                                                    <option>MM</option>
                                                    <option>January</option>
                                                    <option>February</option>
                                                    <option>March</option>
                                                    <option>April</option>
                                                    <option>May</option>
                                                    <option>June</option>
                                                    <option>July</option>
                                                    <option>August</option>
                                                    <option>September</option>
                                                    <option>October</option>
                                                    <option>Novembeer</option>
                                                    <option>December</option>
                                                </select>
                                            </div>
                                            <div class="form-group w-25 float-left mx-2">
                                                <select class="form-control ">
                                                    <option>YY</option>
                                                    <option>2011</option>
                                                    <option>2012</option>
                                                    <option>2013</option>
                                                    <option>2014</option>
                                                    <option>2015</option>
                                                    <option>2016</option>
                                                    <option>2017</option>
                                                    <option>2018</option>
                                                    <option>2019</option>
                                                    <option>2020</option>
                                                    <option>2021</option>
                                                    <option>2022</option>
                                                </select>
                                            </div>
                                            <div class="form-group w-25 float-left">
                                                <input type="text" placeholder="CVV" class="form-control">
                                            </div>

                                            <div class="form-group w-100">
                                                <input type="text" placeholder="Name on Card Holder" class="form-control">
                                            </div>
                                            <h2 class="h4 fbold">Your Pay <span class="fblue float-right">Rs: 8,800</span></h2>
                                            <a href="payment-success.php"class="bluebtn my-2 d-inline-block">Pay Rs:2500</a>
                                            <p class="small py-2">This Bank details will be saved for a faster payment experience</p>
                                        </form>
                                    </div>
                                    <!--/ net banking -->                                   
                                </div>
                            </div>
                           <!--/ tab -->
                       </div>
                       <!--/ white box -->
                    </div>
                    <!--/ left col -->

                    <!-- right col for total -->
                    <div class="col-lg-3">
                      <div class="whitebox">
                      <h6 class="pb-2 fblue">Cart Items (3 Items)</h6>
                        <ul>
                            <li>
                                <p class="pb-0">Course Name will be here</p>
                                <p><span>Rs: 8,800</span></p>
                            </li>
                            <li>
                                <p class="pb-0">Course Name will be here</p>
                                <p><span>Rs: 8,800</span></p>
                            </li>                                    
                            <li>
                                <p class="pb-0">Course Name will be here</p>
                                <p><span>Rs: 8,800</span></p>
                            </li>
                            <li>                                        
                                <p class="pb-0"><span class="fblue fbold">Total: 	</span> <span class="float-right  fblue fbold h5">Rs. 8,880</span></p>
                               
                            </li>
                        </ul>
                      </div>
                       
                    </div>
                    <!--/ right col for total  -->
              </div>
              <!--/ row -->
           </div>
           <!--/ container -->
        </div>
        <!--/ sub page body -->
    </main>

    <!--/ sub page main -->
    <!--footer -->
    <?php include 'footer.php' ?>
    <!--/ footer -->  
    <?php include 'footerscripts.php'?>

   
    


</body>

</html>