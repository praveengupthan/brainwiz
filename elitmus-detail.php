<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>E-litmus at Brainwiz</title>
    <link rel="icon" type="image/png" sizes="32x32" href="img/favicon-32x32.png">
    <!-- style sheets -->
    <?php include 'headerstyles.php' ?>
</head>

<body>

    <!-- header -->
   <?php include 'header.php' ?>
    <!--/ header -->
    <!--sub page main -->
    <main class="subpage">
        <!-- sub page header -->
        <div class="page-header">
            <!-- container -->
            <div class="container">
               <!-- row -->
               <div class="row">
                   <div class="col-lg-6">
                       <h1>E-litmus </h1>
                   </div>
               </div>
               <!--/ row --> 
            </div>
            <!--/ container -->
        </div>
        <!--/ sub page header -->

        <!-- brudcrumb -->
        <div class="container">
            <!-- row -->
            <div class="row">
                <!-- col -->
                <div class="col-lg-12">                   
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                        <li class="breadcrumb-item"><a href="allcourses.php">Courses</a></li>
                        <li class="breadcrumb-item active"><a>E-litmus</a></li>
                    </ul>                    
                </div>
                <!--/col -->
            </div>
            <!--/ row -->
        </div>
        <!--/ brudcrumb -->

        <!-- sub page body -->
        <div class="subpage-body">
           <!-- container -->
           <div class="container">
               <!-- row -->
               <div class="row">
                   <!-- left col -->
                   <div class="col-lg-8">
                    <!-- row-->
                    <div class="row justify-content-center">
                        <!-- col 12 -->
                       <div class="col-lg-12 ">
                           <div class="bggray p-4 text-center">
                            <img style="height:300px;" src="img/elitmus-courseimg.svg" alt="" class="img-fluid">
                                <h2 class="h2 py-2 fbold">About E-Litmus </h2>
                                <p class="text-center">E-litmus is an Indian recruitment organization founded by ex-Infosys employees. It helps companies in hiring freshers for their entry-level jobs through its effective and unique test called E-litmus PH test.</p>

                                <p class="text-center">Every year Lakhs of students graduated, even though most of the colleges offers on campus placements but not everyone getting placed. So these unplaced students will go and attend off-campus drives/placements. But what happens, now a days most of the companies are conducting their own drives rather than conducting off campus drives. They are hiring the freshers graduates through a process called E-Litmus PH test.</p>

                                <p class="text-center">This test is used by most of the companies including fortune 500 companies like Dell, Accenture, Mindtree etc. Freshers take this Elitmus pH Test and then apply to all companies which have posted their job requirements on the Elitmus web site. This test is typically held on Sundays in up to 37 cities across India. </p>

                                <p class="text-center">Elitmus is a little tricky exam to crack compared to other employability tests. Elitmus is a content challenged test (less number of questions; high quality in depth of question).</p>
                           </div>

                           <h3 class="h2 py-2 fbold">Elitmus Training in Hyderabad </h3>
                           <p>BRAINWIZ is the Best Elitmus Training Institute in Hyderabad, where thousands of students secured a good percentile in Elitmus exam and got placed in Top MNC’s.  Here students will get effective training based on all Modules like Quantitative Ability, Logical Reasoning, Verbal Ability and various Questions on MNC Puzzles, Cryptarithmetic, Binary logic, Geometry etc,. Elitmus Coaching in Ameerpet, BRAINWIZ helps candidates to get into their dream companies easily by giving the training focussed on different companies on different modules covering entire Elitmus Syllabus and providing the Elitmus Test Series as a post Assessment after completed the training which helps the students to get a good Elitmus Score.   </p>

                           <h3 class="h2 py-2 fbold">E-Litmus Exam pattern </h3>
                           <p>E-litmus PH Test is an aptitude test. It is a pen & paper test and no longer administered online. </p>
                           
                           <p>The test is designed to be completed in two hours, and often has three sections Quantitative, verbal and analytical - but there could be changes in this structure from time to time, as our testing experts continually fine-tune and evolve the test, in their constant effort to draw the best measure of your true ability. Each section currently has twenty questions making it a total of sixty questions. The maximum marks are 600.</p>
                          
                           <p>The questions in each section are objective in nature with multiple choices, and are categorized into several levels of difficulty. Your final score will reflect not only the number of questions that you have answered correctly, but also their difficulty level, and on how your peers have fared on the same questions. </p>

                           <p>The test measures a broad range of skills that you have gained throughout your education. It does not test your proficiency in any specific areas or subjects. It presumes only a familiarity with high school level mathematics, and some proficiency in essential English language skills. All required formulae are provided in the question paper.</p>

                           <p>The test uses a unique handicap based negative marking approach. You are encouraged to read sections pertaining to pH Test, why evaluations fail and pH Test Demystified. </p>

                           <p>Absolute scores as well as percentile scores are provided to each candidate.</p>

                           <p>Three sections included in the E-Litmus Exam pattern:</p>

                            <!-- table -->
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th scope="col">Sections</th>
                                        <th scope="col">No. Of Questions </th>
                                        <th scope="col">Score </th>                               
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td scope="row">Quantitative Ability </td>
                                        <td>20</td>
                                        <td>200</td>                               
                                    </tr>
                                    <tr>
                                        <td scope="row">Logical Reasoning</td>
                                        <td>20</td>
                                        <td>200</td>                                
                                    </tr>
                                    <tr>
                                        <td scope="row">Verbal Ability </td>
                                        <td>20</td>
                                        <td>200</td>                                
                                    </tr>
                                    <tr>
                                        <td scope="row">Total </td>
                                        <td>60</td>
                                        <td>600</td>                               
                                    </tr>
                                </tbody>
                            </table>
                            <!--/ table -->

                            <p>Unlike other exams, there is no sectional timings. So that candidate should wisely allocate time among the three sections based on his/her proficiency in order to ace the exam. There is an negative marking in Elitmus exam. And the Elitmus Exam score is valid for 2 years.</p>

                            

                           <!-- course curriculum -->
                           <div class="bggray p-4 custom-accord">
                            <h3 class="h2 py-2 fbold">Elitmus Syllabus  </h3>
                            <p>Below is the Updated Elitmus syllabus, where student need to practice each topic given in syllabus to get a good percentile in the test.</p>                             

                                <!-- accordian -->
                                <div class="accordion course-accordion">
                                    <!-- acc-->
                                    <h3 class="panel-title">Quantitative Ability Syllabus for Elitmus</h3>
                                    <div class="panel-content">
                                        <ul class="page-list">
                                            <li>Geometry </li>
                                            <li>Time, Speed & Distance</li>
                                            <li>Time & Work</li>
                                            <li>Permutation & Combination</li>
                                            <li>Probability </li>
                                            <li>Mensuration</li>
                                            <li>Number System</li>
                                            <li>Progressions</li>
                                            <li>Algebra</li>
                                            <li>Ratio & Proportions</li>
                                            <li>Percentages</li>
                                            <li>Profit & Loss</li>
                                        </ul>
                                    </div>
                                    <!--/ acc-->
                                    <!-- acc-->
                                    <h3 class="panel-title">Logical Reasoning Syllabus for Elitmus </h3>
                                    <div class="panel-content"> 
                                        <p>You need to answer 10-12 questions from this section to score good percentile.</p>                                       
                                        <ul class="page-list">
                                            <li>Cryptarithmetic</li>
                                            <li>Logical Reasoning</li>
                                            <li>Data Interpretation</li>
                                            <li>Data Sufficiency</li>
                                            <li>Deductions & Syllogisms</li>
                                            <li>Puzzles</li>
                                            <li>Blood Relations </li>
                                            <li>Number Series</li>
                                            <li>Direction Sense</li>
                                            <li>Seating Arrangement</li>
                                            <li>Clocks & Calendars </li> 
                                        </ul>
                                    </div>
                                    <!--/ acc-->                                   
                                     <!-- acc-->
                                     <h3 class="panel-title">Verbal Ability Syllabus for Elitmus</h3>
                                    <div class="panel-content">                                       
                                       
                                        <ul class="page-list">
                                            <li>Sentence Completion</li>
                                            <li>Jumbled Sentences</li>
                                            <li>Fill in the blanks</li>
                                            <li>Spotting Errors</li>
                                            <li>Reading Comprehension</li>
                                            <li>One word Substitution</li>
                                            <li>Sentence Completion</li>
                                            <li>Sentence Correction</li>
                                        </ul>
                                    </div>
                                    <!--/ acc-->

                                </div>
                                <!--/ accordian -->

                           </div>
                           <!--/ course curriculum -->                           

                             <!-- section -->
                             <div class="py-2">
                                <h3 class="h2 py-2 fbold">Scoring Topics in Elitmus Syllabus</h3>
                                <p>For Elitmus Exam, check out the syllabus and prepare topics in the order of importance. </p>
                                <h4 class="h6 py-2">Quantitative Ability (20 Questions) </h4>
                                <table class="table">
                                    <tr>
                                        <td>1</td>
                                        <td>Number System (High)</td>
                                    </tr>
                                    <tr>
                                        <td>2</td>
                                        <td>Permutation and Combinations (Medium)</td>
                                    </tr>
                                    <tr>
                                        <td>3</td>
                                        <td>Probability (Medium)</td>
                                    </tr>
                                    <tr>
                                        <td>4</td>
                                        <td>Geometry (High)</td>
                                    </tr>
                                    <tr>
                                        <td>5</td>
                                        <td>Mensuration (Low)</td>
                                    </tr>
                                    <tr>
                                        <td>6</td>
                                        <td>Time and Distance (High)</td>
                                    </tr>
                                    <tr>
                                        <td>7</td>
                                        <td>Time and Work (medium)</td>
                                    </tr>
                                    <tr>
                                        <td>8</td>
                                        <td>Algebra (Low)</td>
                                    </tr>
                                </table>
                                <h4 class="h6 py-2">Verbal Ability (20 Questions) </h4>
                                <table class="table table-responsive">
                                    <tr>
                                        <td>1</td>
                                        <td>Sentence Completion 1</td>
                                        <td>4 Questions</td>
                                        <td>(Based on Subject-Verb Agreement)</td>
                                    </tr>
                                    <tr>
                                        <td>2</td>
                                        <td>Reading Comprehension 1</td>
                                        <td>4 Questions</td>
                                        <td>(Around 200-230 words)</td>
                                    </tr>
                                    <tr>
                                        <td>3</td>
                                        <td>Reading Comprehension 2</td>
                                        <td>4 Questions</td>
                                        <td>(Around 200-230 words)</td>
                                    </tr>
                                    <tr>
                                        <td>4</td>
                                        <td>Reading Comprehension 3</td>
                                        <td>4 Questions</td>
                                        <td>(Around 200-230 words)</td>
                                    </tr>
                                    <tr>
                                        <td>5</td>
                                        <td>Sentence Completion 2</td>
                                        <td>2 Questions</td>
                                        <td>(Based on Vocabulary)</td>
                                    </tr>
                                    <tr>
                                        <td>6</td>
                                        <td>Para Jumbles</td>
                                        <td>2 Questions</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    
                                </table>

                                <h4 class="h6 py-2">Problem Solving (20 Questions) </h4>
                                <table class="table">
                                    <tr>
                                        <td>1</td>
                                        <td>Logical Reasoning-1</td>
                                        <td>5 Questions</td>                                               
                                    </tr>
                                    <tr>
                                        <td>2</td>
                                        <td>Logical Reasoning-1</td>
                                        <td>3 Questions</td>                                               
                                    </tr>
                                    <tr>
                                        <td>3</td>
                                        <td>Data Sufficiency</td>
                                        <td>4 Questions</td>                                               
                                    </tr>
                                    <tr>
                                        <td>4</td>
                                        <td>Data Interpretation</td>
                                        <td>5 Questions</td>                                               
                                    </tr>
                                    <tr>
                                        <td>5</td>
                                        <td>Cryptarithmetic</td>
                                        <td>3 Questions</td>                                               
                                    </tr>
                                </table>
                                <p>(Solve 5-10 Questions in this section of getting good percentile)</p> 
                            </div>
                            <!-- section /-->

                            <!-- section -->
                           <div class="py-2">
                                <h3 class="h2 py-2 fbold">Scoring Pattern (Negative Marking) for Elitmus</h3>
                                <p>In Elitmus exam, Negative marking is done based on </p>                                       
                                <ul class="page-list">
                                    <li>The Number of questions attempted by the candidate</li>
                                    <li>The accuracy of wrong answers</li>
                                </ul>

                                <h4 class="h6 py-2">Negative Marking Rule </h4>

                                <!-- table -->
                                <table class="table table-responsive">
                                    <thead>
                                        <tr>
                                            <th scope="col">SI No</th>
                                            <th scope="col">No. of quest. Attempted </th>
                                            <th scope="col">Correct </th>
                                            <th scope="col">Incorrect</th>
                                            <th scope="col">Marks for correct  answer </th>
                                            <th scope="col">Negative(*approx) </th>
                                            <th scope="col">Marks Obtained (*approx) </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td scope="row">1 </td>
                                            <td>4</td>
                                            <td>3</td>
                                            <td>1</td>
                                            <td>30</td>        
                                            <td>0</td>
                                            <td>30</td>                                       
                                        </tr>
                                        <tr>
                                            <td scope="row">2 </td>
                                            <td>4</td>
                                            <td>2</td>
                                            <td>2</td>
                                            <td>20</td>        
                                            <td>-5</td>
                                            <td>15</td>                                       
                                        </tr>
                                        <tr>
                                            <td scope="row">3 </td>
                                            <td>8</td>
                                            <td>6</td>
                                            <td>2</td>
                                            <td>60</td>        
                                            <td>0</td>
                                            <td>60</td>                                       
                                        </tr>
                                        <tr>
                                            <td scope="row">4 </td>
                                            <td>8</td>
                                            <td>5</td>
                                            <td>3</td>
                                            <td>50</td>        
                                            <td>-5</td>
                                            <td>&nbsp;</td>                                       
                                        </tr>                                               
                                    </tbody>
                                </table>
                                <!--/ table -->
                            </div>
                            <!--/ section -->

                            <!-- section -->
                            <div class="py-2">
                                <h3 class="h2 py-2 fbold">Elitmus Score Vs Percentile</h3>
                                <h4 class="h6 py-2">Quantitative Ability </h4>
                                    <!-- table -->
                                    <table class="table">
                                    <thead>
                                        <tr>
                                            <th scope="col">Marks</th>
                                            <th scope="col">Percentile </th>                                                  
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td scope="row">120.00 </td>
                                            <td>99.80</td>     
                                        </tr>
                                        <tr>
                                            <td scope="row">90.00 </td>
                                            <td>98.75</td>     
                                        </tr>
                                        <tr>
                                            <td scope="row">78.80 </td>
                                            <td>97.23</td>     
                                        </tr>
                                        <tr>
                                            <td scope="row">70.00 </td>
                                            <td>96.19</td>     
                                        </tr>
                                        <tr>
                                            <td scope="row">63.80 </td>
                                            <td>94.80</td>     
                                        </tr>
                                        <tr>
                                            <td scope="row">60.00 </td>
                                            <td>94.11</td>     
                                        </tr>
                                        <tr>
                                            <td scope="row">56.30 </td>
                                            <td>92.42</td>     
                                        </tr>
                                        <tr>
                                            <td scope="row">120.00 </td>
                                            <td>99.80</td>     
                                        </tr>
                                        <tr>
                                            <td scope="row">52.5 </td>
                                            <td>91.37</td>     
                                        </tr>
                                        <tr>
                                            <td scope="row">48.8</td>
                                            <td>89.57</td>     
                                        </tr>
                                        <tr>
                                            <td scope="row">41.3 </td>
                                            <td>85.97</td>     
                                        </tr>
                                        <tr>
                                            <td scope="row">40 </td>
                                            <td>84.44</td>     
                                        </tr>                                                
                                        <tr>
                                            <td scope="row">37.5 </td>
                                            <td>83.32</td>     
                                        </tr>
                                        <tr>
                                            <td scope="row">33.8 </td>
                                            <td>80.51</td>     
                                        </tr>
                                        <tr>
                                            <td scope="row">30 </td>
                                            <td>77.8</td>     
                                        </tr>
                                        <tr>
                                            <td scope="row">26.3 </td>
                                            <td>73.55</td>     
                                        </tr>
                                        <tr>
                                            <td scope="row">22.5 </td>
                                            <td>69.87</td>     
                                        </tr>
                                        <tr>
                                            <td scope="row">18.8 </td>
                                            <td>64.76</td>     
                                        </tr>
                                        <tr>
                                            <td scope="row">15 </td>
                                            <td>60</td>     
                                        </tr>
                                        <tr>
                                            <td scope="row">11.3 </td>
                                            <td>54.74</td>     
                                        </tr>
                                        <tr>
                                            <td scope="row">7.5 </td>
                                            <td>49.14</td>     
                                        </tr>
                                        <tr>
                                            <td scope="row">3.8 </td>
                                            <td>43.31</td>     
                                        </tr>
                                        <tr>
                                            <td scope="row">-3.8 </td>
                                            <td>30</td>     
                                        </tr>
                                        <tr>
                                            <td scope="row">-7.5 </td>
                                            <td>24.2</td>     
                                        </tr>
                                        <tr>
                                            <td scope="row">-15 </td>
                                            <td>14.45</td>     
                                        </tr>
                                        
                                                                                    
                                    </tbody>
                                </table>
                                <!--/ table -->      
                                
                                <h4 class="h6 py-2">Logical Reasoning  </h4>
                                    <!-- table -->
                                    <table class="table">
                                    <thead>
                                        <tr>
                                            <th scope="col">Marks</th>
                                            <th scope="col">Percentile </th>                                                  
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td scope="row">100.00 </td>
                                            <td>99.5</td>     
                                        </tr>
                                        <tr>
                                            <td scope="row">90.00 </td>
                                            <td>99.20</td>     
                                        </tr>
                                        <tr>
                                            <td scope="row">80.00 </td>
                                            <td>98.42</td>     
                                        </tr>
                                        <tr>
                                            <td scope="row">70.00</td>
                                            <td>96.93</td>     
                                        </tr>
                                        <tr>
                                            <td scope="row">67.50 </td>
                                            <td>96.34</td>     
                                        </tr>
                                        <tr>
                                            <td scope="row">60.00 </td>
                                            <td>95.07</td>     
                                        </tr>
                                        <tr>
                                            <td scope="row">56.30 </td>
                                            <td>93.31</td>     
                                        </tr>
                                        <tr>
                                            <td scope="row">100.00 </td>
                                            <td>99.5</td>     
                                        </tr>
                                        <tr>
                                            <td scope="row">52.5</td>
                                            <td>92.14</td>     
                                        </tr>
                                        <tr>
                                            <td scope="row">48.8</td>
                                            <td>90.47</td>     
                                        </tr>
                                        <tr>
                                            <td scope="row">45 </td>
                                            <td>88.61</td>     
                                        </tr>
                                        <tr>
                                            <td scope="row">41.3 </td>
                                            <td>86.21</td>     
                                        </tr>                                                
                                        <tr>
                                            <td scope="row">40 </td>
                                            <td>84.46</td>     
                                        </tr>
                                        <tr>
                                            <td scope="row">37.5 </td>
                                            <td>83.16</td>     
                                        </tr>
                                        <tr>
                                            <td scope="row">26.3 </td>
                                            <td>72.73</td>     
                                        </tr>
                                        <tr>
                                            <td scope="row">22.5 </td>
                                            <td>68.47</td>     
                                        </tr>
                                        <tr>
                                            <td scope="row">11.3 </td>
                                            <td>52.66</td>     
                                        </tr>
                                        <tr>
                                            <td scope="row">7.5 </td>
                                            <td>47.04</td>     
                                        </tr>
                                        <tr>
                                            <td scope="row">3.8 </td>
                                            <td>41.19</td>     
                                        </tr>
                                        <tr>
                                            <td scope="row">0 </td>
                                            <td>35.31</td>     
                                        </tr>
                                        <tr>
                                            <td scope="row">-3.8 </td>
                                            <td>27.96</td>     
                                        </tr>
                                        <tr>
                                            <td scope="row">-11.3 </td>
                                            <td>17.78</td>     
                                        </tr>
                                        <tr>
                                            <td scope="row">-15 </td>
                                            <td>13.71</td>     
                                        </tr>                                    
                                    </tbody>
                                </table>
                                <!--/ table --> 
                                
                                <h4 class="h6 py-2">Verbal Ability </h4>
                                    <!-- table -->
                                    <table class="table">
                                    <thead>
                                        <tr>
                                            <th scope="col">Marks</th>
                                            <th scope="col">Percentile </th> 
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td scope="row">170.00 </td>
                                            <td>99.92</td>     
                                        </tr>
                                        <tr>
                                            <td scope="row">150.00 </td>
                                            <td>99.27</td>     
                                        </tr>
                                        <tr>
                                            <td scope="row">130.00 </td>
                                            <td>96.89</td>     
                                        </tr>
                                        <tr>
                                            <td scope="row">110.00</td>
                                            <td>92.79</td>     
                                        </tr>
                                        <tr>
                                            <td scope="row">105.00 </td>
                                            <td>91.39</td>     
                                        </tr>
                                        <tr>
                                            <td scope="row">97.50 </td>
                                            <td>88.54</td>     
                                        </tr>
                                        <tr>
                                            <td scope="row">93.80 </td>
                                            <td>87.26</td>     
                                        </tr>
                                        <tr>
                                            <td scope="row">170.00 </td>
                                            <td>99.92</td>     
                                        </tr>
                                        <tr>
                                            <td scope="row">82.5</td>
                                            <td>80.92</td>     
                                        </tr>
                                        <tr>
                                            <td scope="row">78.8</td>
                                            <td>78.95</td>     
                                        </tr>
                                        <tr>
                                            <td scope="row">75 </td>
                                            <td>76.57</td>     
                                        </tr>
                                        <tr>
                                            <td scope="row">71.3</td>
                                            <td>72.84</td>     
                                        </tr>                                                
                                        <tr>
                                            <td scope="row">67.5 </td>
                                            <td>70.69</td>     
                                        </tr>
                                        <tr>
                                            <td scope="row">63.8 </td>
                                            <td>67.52</td>     
                                        </tr>
                                        <tr>
                                            <td scope="row">60 </td>
                                            <td>65.34</td>     
                                        </tr>
                                        <tr>
                                            <td scope="row">52.5 </td>
                                            <td>57.97</td>     
                                        </tr>
                                        <tr>
                                            <td scope="row">48.8 </td>
                                            <td>54.9</td>     
                                        </tr>
                                        <tr>
                                            <td scope="row">45 </td>
                                            <td>51.42</td>     
                                        </tr>
                                        <tr>
                                            <td scope="row">41.3 </td>
                                            <td>46.37</td>     
                                        </tr>
                                        <tr>
                                            <td scope="row">33.8 </td>
                                            <td>40.1</td>     
                                        </tr>
                                        <tr>
                                            <td scope="row">30</td>
                                            <td>36.4</td>     
                                        </tr>
                                        <tr>
                                            <td scope="row">26.3</td>
                                            <td>31.99</td>     
                                        </tr>
                                        <tr>
                                            <td scope="row">22.5 </td>
                                            <td>28.36</td>     
                                        </tr> 
                                        <tr>
                                            <td scope="row">18.8</td>
                                            <td>25.27</td>     
                                        </tr>
                                        <tr>
                                            <td scope="row">15 </td>
                                            <td>22.11</td>     
                                        </tr>                                      
                                    </tbody>
                                </table>
                                <!--/ table -->    
                            </div>
                            <!--/ section -->


                           <!-- section -->
                            <div class="py-2">
                                <h3 class="h2 py-2 fbold">Elitmus Eligibility Criteria</h3>
                                <ul class="page-list">
                                    <li>Any Eligibility Criteria: No Specific eligibility required, and there is no Age limit also. Do check the eligibility criteria for the company you are applying to, in order to ascertain the suitability of your profile for the same.</li>
                                    <li>Educational Qualification: BE/B.Tech, MCA, ME/M.Tech or M.Sc (CS/IT/Electronics)  (An undergraduate major in the subject)</li>
                                    <li>ID Requirement: Government-issued ID (like Driving license, passport, Voter Id card etc). Printout of eAadhar is not accepted.</li>
                                    <li>Documents Requirement: Xth, XIIth, Graduation/Post graduation mark sheets (Final year students need to carry the marks sheets available till date)</li>
                                </ul>
                            </div>
                            <!-- section /-->
                       </div>
                       <!-- col 12 -->
                    </div>
                    <!--/ row-->
                   </div>
                   <!--/left col-->

                   <!-- course right faq -->
                   <div class="col-lg-4">   
                   <!-- sticky top -->
                   <div class="sticky-top">                     
                        <div class="custom-accord course-faq">
                            <h3 class="h5 pb-3">Faq's E-litmus</h3>
                            <!-- accordion -->
                            <div class="accordion">
                                <!-- acc-->
                                <h3 class="panel-title">What is the validity of ELitmus Score?</h3>
                                <div class="panel-content">
                                    <p>The ELitmus score is valid for a period of 2 years.
                                    </p>
                                </div>
                                <!--/ acc-->
                                <!-- acc-->
                                <h3 class="panel-title">Can I give Elitmus Exam again? Is there any limit on the number of times to give exam?</h3>
                                <div class="panel-content">
                                    <p>There is no restriction on taking the pH test. You can take the pH test any number of times without restriction and during shortlisting, your best score will be considered.</p>

                                    <p>However in Elitmus Exam, significant number of companies prefer to shortlist candidates who have taken no more than 3 attempts. Hence, no more than 3 attempts are recommended.</p>
                                </div>
                                <!--/ acc-->
                                <!-- acc-->
                                <h3 class="panel-title">If I have a backlog in the last semester. Can I take the pH test?</h3>
                                <div class="panel-content">
                                    <p>Though there is no eligibility criteria to take the pH test, companies hiring through elitmus may have their own criteria in regards to backlogs. Therefore, BRAINWIZ suggest that you clear your backlogs before taking the pH test. </p>
                                   
                                </div>
                                <!--/ acc-->
                                <!-- acc-->
                                <h3 class="panel-title">What is the Cost of Elitmus PH Test?</h3>
                                <div class="panel-content">
                                    <p>The cost of PH Test at present is 920/-. This cost has not been changed from a long period. You can either pay in cash (accepted at E-litmus Bangalore office only), or through DD or ONLINE PAYMENT.</p>
                                </div>
                                <!--/ acc-->
                               
                            </div>
                            <!--/ accordion -->
                        </div>

                        <!-- question and answers -->
                        <div class="question-intro">
                            <h3 class="h5 pb-3">Previous Questions &amp; Answers </h3>
                            <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Voluptate reprehenderit optio, nostrum, ex doloribus ipsam maiores facilis totam, labore dicta excepturi ducimus! Beatae, necessitatibus voluptatem.</p>

                            <a class="bluebtn" href="elitmus-questions.php">View All Questions and Answers</a>
                        </div>
                        <!--/ question and answers -->
                        </div>    
                        <!--/ sticky top -->    
                   </div>
                   <!--/ course right faq -->
               </div>
               <!--/ row -->
           </div>
            <!-- /container -->
        </div>
        <!--/ sub page body -->
    </main>
    <!--/ sub page main -->
    <!--footer -->
    <?php include 'footer.php' ?>
    <!--/ footer -->  
    <?php include 'footerscripts.php'?>

</body>

</html>