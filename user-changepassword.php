<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Brainwiz</title>
    <link rel="icon" type="image/png" sizes="32x32" href="img/favicon-32x32.png">
    <!-- style sheets -->
    <?php include 'headerstyles.php' ?>
</head>

<body>

    <!-- header -->
   <?php include 'header-postlogin.php' ?>
    <!--/ header -->
    <!--sub page main -->
    <main class="subpage">
        <!-- sub page header -->
        <div class="page-header">
            <!-- container -->
            <div class="container">
               <!-- row -->
               <div class="row">
                   <div class="col-lg-6">
                       <h1>Profile</h1>
                   </div>
               </div>
               <!--/ row --> 
            </div>
            <!--/ container -->
        </div>
        <!--/ sub page header -->

        <!-- brudcrumb -->
        <div class="container">
            <!-- row -->
            <div class="row">
                <!-- col -->
                <div class="col-lg-12">                   
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                        <li class="breadcrumb-item"><a href="user-profile.php">Student Name Will be here</a></li>
                        <li class="breadcrumb-item active"><a>Change Password</a></li>
                    </ul>                    
                </div>
                <!--/col -->
            </div>
            <!--/ row -->
        </div>
        <!--/ brudcrumb -->

        <!-- sub page body -->
        <div class="subpage-body">
           <!-- container -->
           <div class="container">
                <!-- row -->
                <div class="row">
                    <!-- left col 4-->
                    <div class="col-lg-4">
                       <?php include 'userleft-nav.php'?>
                    </div>
                    <!--/ left col 4-->

                    <!-- right container -->
                    <div class="col-lg-8">
                        <div class="right-usersection">
                            <!-- title -->
                            <div class="user-right-title">
                                <h3 class="h4 mb-0">Change Password</h3>
                                <p><small>Edit Your Current Password and Update</small></p>
                            </div>
                            <!--/ title -->

                            <!-- right user body -->
                            <div class="user-rightsection">
                                <form>
                                    <!-- row -->
                                    <div class="row">
                                        <!-- col -->
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>Current Password</label>
                                                <input type="password" placeholder="Current Password" class="form-control">
                                            </div>

                                            <div class="form-group">
                                                <label>New Password</label>
                                                <input type="password" placeholder="New Password" class="form-control">
                                            </div>

                                            <div class="form-group">
                                                <label>Confirm Password</label>
                                                <input type="password" placeholder="Confirm Password" class="form-control">
                                            </div>
                                        </div>
                                        <!--/ col -->                                         
                                    </div>
                                    <!--/ row -->
                                        <input type="submit" class="bluebtn" value="Update">
                                        <input type="submit" class="bluebtn" value="Cancel">
                                    </div>
                                    <!--/ col -->                                    
                                    </div>
                                    <!--/ row -->

                                </form>
                            </div>
                            <!--/ right user body -->
                        </div>
                    </div>
                    <!--/ right container -->
                </div>
                <!--/ row -->
           </div>
           <!--/ container -->
        </div>
        <!--/ sub page body -->
    </main>
    <!--/ sub page main -->
    <!--footer -->
    <?php include 'footer.php' ?>
    <!--/ footer -->  
    <?php include 'footerscripts.php'?>

</body>

</html>