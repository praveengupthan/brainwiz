<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Campus Recruitment Training at Brainwiz</title>
    <link rel="icon" type="image/png" sizes="32x32" href="img/favicon-32x32.png">
    <!-- style sheets -->
    <?php include 'headerstyles.php' ?>
</head>

<body>

    <!-- header -->
   <?php include 'header.php' ?>
    <!--/ header -->
    <!--sub page main -->
    <main class="subpage">
        <!-- sub page header -->
        <div class="page-header">
            <!-- container -->
            <div class="container">
               <!-- row -->
               <div class="row">
                   <div class="col-lg-6">
                       <h1>Campus Recruitment Training <span class="fbold">(CRT)</span></h1>
                   </div>
               </div>
               <!--/ row --> 
            </div>
            <!--/ container -->
        </div>
        <!--/ sub page header -->

        <!-- brudcrumb -->
        <div class="container">
            <!-- row -->
            <div class="row">
                <!-- col -->
                <div class="col-lg-12">                   
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                        <li class="breadcrumb-item"><a href="allcourses.php">Courses</a></li>
                        <li class="breadcrumb-item active"><a>CRT</a></li>
                    </ul>                    
                </div>
                <!--/col -->
            </div>
            <!--/ row -->
        </div>
        <!--/ brudcrumb -->

        <!-- sub page body -->
        <div class="subpage-body">
           <!-- container -->
           <div class="container">
               <!-- row -->
               <div class="row">
                   <!-- left col -->
                   <div class="col-lg-8">
                    <!-- row-->
                    <div class="row justify-content-center">
                        <!-- col 12 -->
                       <div class="col-lg-12 ">
                           <div class="bggray p-4 text-center">
                            <img style="height:250px;" src="img/crtdetail01.svg" alt="" class="img-fluid">
                                <h2 class="h2 py-2 fbold">What is CRT (Campus Recruitment Training)? </h2>
                                <p class="text-center"><span class="fbold fblue">Campus Recruitment Training </span>is a comprehensive program which is exclusively designed for the graduate students preparing them for recruitment of various IT and NON IT companies, and ensuring that the students are well trained and equipped to get through the companies easily. This training mainly for all the pre final year and final year students who are looking for placements in their colleges or in any reputation organizations like TCS, Infosys, Accenture, Wipro, IBM etc..(i.e. On Campus or off Campus). In CRT students will be trained for all the rounds of Interviews including logical thinking and communication skills.</p>
                           </div>

                           <h3 class="h2 py-2 fbold">Best CRT Training in Hyderabad </h3>
                           <p>BRAINWIZ is one of the Best Campus Recruitment Training Institute in South India. This is a Hyderabad based leading organization that offers Best CRT Training in Ameerpet. CRT in BRAINWIZ helps all the graduate students to get placed into different MNC Companies easily by clearing all the rounds of Interview. BRAINWIZ trains the students to equip with all the skills which are required to meet the various top IT & Non IT Companies expectations. CRT Coaching helped thousands of students to get placed in top MNC’s after getting trained here by our expert faculties. Teaching methodology is completely unique with the updated content with more logics and helping the students to solve the problems in a very easy way. In this way BRAINWIZ is leading as the Best CRT Training Institute in Ameerpet, Hyderabad.  </p>

                           <p>CRT Training at Hyderabad, BRAINWIZ deals with the CRT training having four modules including Aptitude Training, Logical Reasoning, Verbal Ability and Soft Skills. Here students will get training from the very first round written test to final HR round. BRAINWIZ in Hyderabad gives extra focus on AMCAT Training & Elitmus Training which includes Sunday special sessions on different puzzles with Mock online Test series of various MNC Companies.</p>

                           <h3 class="h2 py-2 fbold">How CRT Helpful for fresher’s? </h3>
                           <p>CRT is specially necessary for all the fresher’s why because in all MNC companies Interview process is completely based on to judge the Logical Thinking ability of the candidates like the way of solving problems. And the level of competition is more and the opportunities are less. So to become Successful, students should become very good enough to grab the opportunities having the both logical and Technical knowledge. This Campus Recruitment Process helps to filter and choose the right candidates for the companies. So this proper CRT Training at BRAINWIZ will project the right candidates to the top IT companies.  </p>

                           <h3 class="h2 py-2 fbold">Where CRT is Useful?</h3>
                           <p>CRT Training is useful in different platforms like </p>
                           <ul class="page-list">
                                <li>MNC Interviews</li>
                                <li>AMCAT</li>
                                <li>E-Litmus</li>
                                <li>CoCubes</li>
                                <li>GATE</li>
                                <li>C-CAT</li>
                                <li>CLAT</li>
                                <li>SSC</li>
                                <li>RRB</li>
                                <li>CSAT</li>
                                <li>AFCAT</li>
                            </ul>
                                                        
                           <h3 class="h2 py-2 fbold">An Overview of the CRT Training Modules</h3>                           
                           <ul class="page-list">
                                <li>Quantitative Aptitude</li>
                                <li>Logical Reasoning</li>
                                <li>Verbal Ability</li>
                                <li>Technical Skill Development</li>
                                <li>Communication Skill Development</li>
                                <li>Resume Preparation</li>
                                <li>Group Discussions</li>
                                <li>Mock Interviews</li>
                                <li>Assessments</li>                                          
                            </ul>

                           <!-- course curriculum -->
                           <div class="bggray p-4 custom-accord">
                                <h3 class="h2 py-1 fbold text-center">Course Curriculum </h3>
                                <p class="text-center">It Stretches Your Mind, Think Better And Create Even Better.</p>

                                <!-- accordian -->
                                <div class="accordion course-accordion">  
                                    <h4 class="h5 py-3">CRT Syllabus in detail:</h4>
                                     <!-- acc-->
                                     <h3 class="panel-title">Quantitative Aptitude:</h3>
                                    <div class="panel-content">
                                    <ul class="page-list">
                                          <li>Number Series</li>
                                          <li>Letter Series</li>
                                          <li>Coding & Decoding</li>
                                          <li>Blood Relations</li>
                                          <li>Cubes & Dices</li>
                                          <li>Deductions & Syllogisms</li>
                                          <li>Logical Connectives</li>
                                          <li>Analytical Puzzles</li>
                                          <li>Seating Arrangement</li>
                                          <li>Direction Sense</li>
                                          <li>Statement & Conclusions</li>
                                          <li>Time Sequencing & Ranking</li>
                                          <li>Non – Verbal Reasoning</li>
                                          <li>Calendars</li>
                                          <li>Clocks</li>
                                          <li>Analogy & Classification</li>                            
                                        </ul>
                                    </div>
                                    <!--/ acc-->    
                                     <!-- acc-->
                                     <h3 class="panel-title">Logical Reasoning:</h3>
                                    <div class="panel-content">
                                    <ul class="page-list">
                                          <li>Percentages</li>
                                          <li>Profit & Loss</li>
                                          <li>Ages</li>
                                          <li>Ratio & Proportions</li>
                                          <li>Partnership</li>
                                          <li>Chain Rule</li>
                                          <li>Time & Work</li>
                                          <li>Pipes & Cistern</li>
                                          <li>Time, Speed and Distance</li>
                                          <li>Problems on Trains</li>
                                          <li>Averages & Mixtures</li>
                                          <li>Data Interpretation</li>
                                          <li>Data Sufficiency</li>
                                          <li>Discounts</li>
                                          <li>Number system</li>
                                          <li>Permutation & Combination</li>
                                          <li>Probability</li>
                                          <li>Simple Interest & Compound Interests</li>                
                                        </ul>
                                    </div>
                                    <!--/ acc-->
                                     <!-- acc-->
                                     <h3 class="panel-title">Verbal Ability:</h3>
                                    <div class="panel-content">
                                    <ul class="page-list">
                                          <li>Spotting Errors</li>
                                          <li>Reading Comprehension</li>
                                          <li>Sentence Correction</li>
                                          <li>Sentence Arrangement</li>
                                          <li>Sentence Improvement</li>
                                          <li>Vocabulary</li>
                                          <li>Grammar</li>
                                          <li>Para Jumbles</li>             
                                        </ul>
                                    </div>
                                    <!--/ acc-->
                                     <!-- acc-->
                                     <h3 class="panel-title">Extra Focus Topics on AMCAT & E-Litmus</h3>
                                    <div class="panel-content">
                                        <ul class="page-list">
                                            <li>CryptArithmetic</li>
                                            <li>Games</li>
                                            <li>Binary Logic</li>
                                            <li>Puzzles</li>
                                            <li>Numbers/li>
                                            <li>Geometry</li>                                            
                                        </ul>
                                        <h4 class="h5">Best Features of Training</h4>
                                        <ul class="page-list">
                                            <li>Comprehensive Training</li>
                                            <li>Interactive Sessions</li>
                                            <li>Experienced Faculty</li>
                                            <li>Company Specific pattern papers</li>
                                            <li>MNC Mock Tests</li>
                                            <li>Online Assessments</li>
                                            <li>Individual feedback</li>  
                                        </ul>
                                    </div>
                                    <!--/ acc-->
                                </div>
                                <!--/ accordian -->
                           </div>
                           <!--/ course curriculum -->
                       </div>
                       <!-- col 12 -->
                    </div>
                    <!--/ row-->
                   </div>
                   <!--/left col-->

                   <!-- course right faq -->
                   <div class="col-lg-4">
                        <div class="bggray p-4 mb-3">
                            <h4 class="h5">Why CRT at BRAINWIZ?</h4>
                            <ul class="page-list">
                                <li>Comprehensive Training on CRT </li>
                                <li>More than 120 hours of Training</li>
                                <li>30 to 45 days Program</li>
                                <li>Covering 55+ Topics with latest MNC Questions</li>
                                <li>Sunday Special Sessions covering AMCAT & E-Litmus</li>
                                <li>Basic to Advance Level</li>
                                <li>Timely Completion of Syllabuss</li>  
                                <li>Updated Material</li>
                                <li>Online Test Series</li>
                            </ul>
                        </div>
                        
                        <div class="custom-accord sticky-top course-faq">
                            <h3 class="h5 pb-3">Faq's about CRT</h3>
                            <!-- accordion -->
                            <div class="accordion">
                                <!-- acc-->
                                <h3 class="panel-title">What is the duration of CRT course?</h3>
                                <div class="panel-content">
                                    <p>In BRAINWIZ CRT Training will be 120 hours covering 55+ topics. So the duration of CRT course is 60 days. And you can attend parallel sessions for rapid completion. </p>
                                </div>
                                <!--/ acc-->
                                <!-- acc-->
                                <h3 class="panel-title">What is the cost of CRT Training?</h3>
                                <div class="panel-content">
                                    <p>CRT course fee is 2700/- which excludes Registration and Material charges.</p>
                                </div>
                                <!--/ acc-->
                                <!-- acc-->
                                <h3 class="panel-title">What are the modules covers in CRT course?</h3>
                                <div class="panel-content">
                                    <p>This Training covers Logical Reasoning, Quantitative Aptitude, Verbal Ability and Soft Skills.</p>
                                </div>
                                <!--/ acc-->
                                <!-- acc-->
                                <h3 class="panel-title">Where this CRT is useful?</h3>
                                <div class="panel-content">
                                    <p>CRT Training useful in different platforms like </p>

                                    <ul class="page-list">
                                        <li>MNC Interviews  </li>
                                        <li>AMCAT  </li>
                                        <li>E-Litmus  </li>
                                        <li>CoCubes  </li>
                                        <li>GATE  </li>
                                        <li>C-CAT  </li>
                                        <li>CLAT  </li>
                                        <li>SSC  </li>
                                        <li>RRB  </li>
                                        <li>CSAT  </li>
                                        <li>AFCAT </li>
                                    </ul>
                                </div>
                                <!--/ acc-->
                                <!-- acc-->
                                <h3 class="panel-title">How this CRT Training is helpful for fresher’s?</h3>
                                <div class="panel-content">
                                    <p>This CRT course helps mainly for fresher’s to grab their dream jobs easily. Because for fresher’s under CRT course they will get trained for all rounds of the Interview.</p>
                                </div>
                                <!--/ acc-->
                                <!-- acc-->
                                <h3 class="panel-title">What is the Right time to take this CRT Course?</h3>
                                <div class="panel-content">
                                    <p>For Engineering students, after completing their 3-2 semester will be the right time to take this training. As they will get Campus Placements in their 4-1 semester, so it will be easy for them to take this training in prior and get aware about the entire recruitment process and get placed easily in their dream jobs without missing those opportunities.</p>
                                </div>
                                <!--/ acc-->                               
                            </div>
                            <!--/ accordion -->
                        </div>
                   </div>
                   <!--/ course right faq -->
               </div>
               <!--/ row -->
           </div>
            <!-- /container -->
        </div>
        <!--/ sub page body -->
    </main>
    <!--/ sub page main -->
    <!--footer -->
    <?php include 'footer.php' ?>
    <!--/ footer -->  
    <?php include 'footerscripts.php'?>

</body>

</html>