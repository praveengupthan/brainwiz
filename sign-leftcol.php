 <div class="col-lg-6 leftcol d-flex align-items-center">  
                    <div class="row justify-content-center w-100">
                        <div class="col-lg-10">
                            <p>
                                <a class="pagelink" href="index.php">Home</a> 
                            </p>
                           <div>
                            <h1>BRAINWIZ  <span class="fbold">Test Series !</span></h1> 
                                <p>Evaluate Yourself by our Online Test Series.</p>
                               
                            </div>
                            <ul class="sign-list pb-4">
                                <li>Real Time Examination Environment</li>
                                <li>Designed according to the Updated questions and Pattern Papers</li>
                                <li>Improves your Speed and Accuracy</li>
                                <li>Find your Strong and weak areas</li>
                                <li>Quick results with solutions</li>
                                <li>Keep track of your performance</li>
                            </ul>
                            <!-- statics row -->
                            <div class="row statsrow">
                            <div class="col-lg-12">
                                <h5 class="h5">We Help You Succeed In Your Campus Interviews.</h5>
                            </div>
                                <!-- col -->
                                <div class="col-lg-4 user_stat">
                                    <div class="statin text-center">
                                        <figure>
                                            <img src="img/statistics-1.png" alt="" title="">
                                        </figure>
                                        <p>Learn & Practice</p>
                                    </div>
                                </div>
                                <!--/ col -->
                                 <!-- col -->
                                 <div class="col-lg-4 user_stat">
                                    <div class="statin text-center">
                                        <figure>
                                            <img src="img/statistics-2.png" alt="" title="">
                                        </figure>
                                        <p>Improve Your Self</p>
                                    </div>
                                </div>
                                <!--/ col -->
                                 <!-- col -->
                                 <div class="col-lg-4 user_stat">
                                    <div class=" statin text-center">
                                        <figure>
                                            <img src="img/statistics.png" alt="" title="">
                                        </figure>
                                        <p>Get Hired</p>
                                    </div>
                                </div>
                                <!--/ col -->
                            </div>
                            <!--/ statics row -->
                            
                        </div>
                    </div>
                </div>