<footer>
        <!-- top footer-->
        <div class="container">
            <!-- row -->
            <div class="row pb-3">
                <!-- col -->
                <div class="col-sm-4">                   
                    <p class="py-1 pr-3">BRAINWIZ is one of the leading CRT Institute in South India. It was established
                        in 2015 in Hyderabad to impart quality training for CRT (Campus Recruitment Training) & various
                        competitive Examinations.</p>

                    <a href="about.php">Read More..</a>
                    <div class="social-nav">
                        <ul class="nav">
                            <li class="nav-item">
                                <a href="javascript:void(0)" target="_blank" class="nav-link">
                                    <span class="icon-facebook icomoon"></span></a>
                            </li>
                            <li class="nav-item">
                                <a href="javascript:void(0)" target="_blank" class="nav-link">
                                    <span class="icon-twitter icomoon"></span></a>
                            </li>
                            <li class="nav-item">
                                <a href="javascript:void(0)" target="_blank" class="nav-link">
                                    <span class="icon-linkedin icomoon"></span></a>
                            </li>
                        </ul>
                    </div>
                </div>
                <!--/ col -->
                <!-- col -->
                <div class="col-sm-2 col-6">
                    <h4 class="footer-title">Brainwiz</h4>
                    <ul class="footer-nav">
                        <li><a href="index.php">Home</a></li>
                        <li><a href="about.php">About</a></li>
                        <li><a href="blog.php">Blog</a></li>
                        <li><a href="contact.php">Contact</a></li>
                        <li><a href="students.php">Success Stories</a></li>
                        <li><a href="terms.php">Terms & Conditions</a></li>
                        <li><a href="privacy.php">Privacy Policy</a></li>
                        <li><a href="faq.php">Faq</a></li>
                    </ul>
                </div>
                <!--/ col -->
                <!-- col -->
                <div class="col-sm-3 col-6">
                    <h4 class="footer-title">Videos</h4>
                    <ul class="footer-nav">
                        <li><a href="javascript:void(0)">Time Speed and Distance</a></li>
                        <li><a href="javascript:void(0)">Time & work</a></li>
                        <li><a href="javascript:void(0)">Number System</a></li>
                        <li><a href="javascript:void(0)">Crypthrithmetic</a></li>
                        <li><a href="javascript:void(0)">Mensuration</a></li>
                        <li><a href="javascript:void(0)">Cubes</a></li>
                        <li><a href="javascript:void(0)">Percentage</a></li>
                        <li><a href="javascript:void(0)">Data Suffiency</a></li>
                    </ul>
                </div>
                <!--/ col -->
                <!-- col -->
                <div class="col-sm-3">
                    <h4 class="footer-title">Reach us</h4>
                    <table>
                        <tr>
                            <td><span class="icon-pin icomoon"></span></td>
                            <td>
                                <p>BRAINWIZ ,Inc.b
                                    # 3rd Floor, Beside Reliance Fresh , Near Satyam Theatre , Srinivasa Nagar,
                                    Ameerpet, Hyderabad.</p>
                            </td>
                        </tr>                                      
                    </table>

                    <div class="interested-course text-center pt-4">
                        <h5 class="h6 pb-3">Interested in Joining our Course?</h5>
                        <a href="contact.php" class="orangebtn">Enroll Now</a>
                    </div>
                </div>
                <!--/ col -->
            </div>
            <!--/ row -->
        </div>
        <!--/ top footer -->
        <!-- bottom footer -->
        <div class="bottom-footer text-center">
            <p>© Copyright 2017 Go Brain Wiz | All Rights Reserved.</p>
        </div>
        <!--/ bottom footer -->
        <a id="movetop" class="movetop" href="javascript:void(0)"><span class="icon-arrow-up icomoon"></span></a>
        
    </footer>

   

   

