<div class="whitebox left-usernav">
    <figure class="profile-pic position-relative">
        <a href="javascript:void(0)">
            <img src="img/user-profilepic.jpg" alt="Student Name will be here">
        </a>
        <div class="upload-btn-wrapper">
            <button class="whitebtn mx-auto"><span class="icon-edit icomoon"></span></button>
            <input type="file" name="myfile" />
        </div>
    </figure>
    <h2 class="h6 text-center pt-3 mb-0">Student Name will be here</h2>
    <p class="text-center"><small>praveennandipati@gmail.com</small></p>

    <ul>
        <li>
            <a class="usernav-active" href="user-profile.php">My Profile</a>
        </li>
        <li>
            <a href="user-mycourses.php">My Courses</a>
        </li>
        <li>
            <a href="user-purchase-history.php">Purchase History</a>
        </li>
        <li>
            <a href="user-wishlist.php">Wishlist Courses</a>
        </li>
        <li>
            <a href="user-scrorecard.php">Score Card</a>
        </li>
        <li>
            <a href="user-changepassword.php">Change Password</a>
        </li>
        <li>
            <a href="user-notifications.php">Notifications</a>
        </li>
        <li>
            <a href="index.php">Logout</a>
        </li>
    </ul>
</div>