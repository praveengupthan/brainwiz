<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Brainwiz</title>
    <link rel="icon" type="image/png" sizes="32x32" href="img/favicon-32x32.png">
    <!-- style sheets -->
    <?php include 'headerstyles.php' ?>
</head>

<body>

    <!-- header -->
   <?php include 'header-postlogin.php' ?>
    <!--/ header -->
    <!--sub page main -->
    <main class="subpage">
        <!-- sub page header -->
        <div class="page-header">
            <!-- container -->
            <div class="container">
               <!-- row -->
               <div class="row">
                   <div class="col-lg-6">
                       <h1>Profile</h1>
                   </div>
               </div>
               <!--/ row --> 
            </div>
            <!--/ container -->
        </div>
        <!--/ sub page header -->

        <!-- brudcrumb -->
        <div class="container">
            <!-- row -->
            <div class="row">
                <!-- col -->
                <div class="col-lg-12">                   
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                        <li class="breadcrumb-item"><a href="user-profile.php">Student Name Will be here</a></li>
                        <li class="breadcrumb-item active"><a>Purchase History</a></li>
                    </ul>                    
                </div>
                <!--/col -->
            </div>
            <!--/ row -->
        </div>
        <!--/ brudcrumb -->

        <!-- sub page body -->
        <div class="subpage-body">
           <!-- container -->
           <div class="container">
                <!-- row -->
                <div class="row">
                    <!-- left col 4-->
                    <div class="col-lg-4">
                       <?php include 'userleft-nav.php'?>
                    </div>
                    <!--/ left col 4-->

                    <!-- right container -->
                    <div class="col-lg-8">
                        <div class="right-usersection">
                            <!-- title -->
                            <div class="user-right-title">
                                <h3 class="h4 mb-0">Purchase History</h3>
                                <p><small>Your Courses Purchased</small></p>
                            </div>
                            <!--/ title -->

                            <!-- right user body -->
                            <div class="user-rightsection">
                                <!--  row -->
                                <div class="row">
                                 <!-- table -->
                                    <table class="table purchase-history table-responsive">
                                        <thead>
                                            <tr>
                                                <th scope="col">&nbsp;</th>
                                                <th scope="col">Course</th>
                                                <th scope="col">Date </th>
                                                <th scope="col">Total </th>                               
                                                <th scope="col">Payment Type </th>
                                                <th scope="col">Invoice </th>    
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td scope="row">
                                                    <img src="img/crtcourseimg.svg" class="thumb-course">
                                                </td>
                                                <td>CRT</td>
                                                <td>22-11-2019</td>                               
                                                <td><span class="fbold">Rs: 2500</span></td>
                                                <td>Visa Card Payment</td>
                                                <td><a href="javascript:void(0)"><span class="icon-download"></span></a></td>
                                            </tr>

                                            <tr>
                                                <td scope="row">
                                                    <img src="img/amcat-courseimg.svg" class="thumb-course">
                                                </td>
                                                <td>Amcat</td>
                                                <td>22-11-2019</td>                               
                                                <td><span class="fbold">Rs: 2500</span></td>
                                                <td>Visa Card Payment</td>
                                                <td><a href="javascript:void(0)"><span class="icon-download"></span></a></td>
                                            </tr>

                                            <tr>
                                                <td scope="row">
                                                    <img src="img/elitmus-courseimg.svg" class="thumb-course">
                                                </td>
                                                <td>E-Litmus</td>
                                                <td>20-10-2019</td>                               
                                                <td><span class="fbold">Rs: 2500</span></td>
                                                <td>Visa Card Payment</td>
                                                <td><a href="javascript:void(0)"><span class="icon-download"></span></a></td>
                                            </tr>
                                           
                                        </tbody>
                                    </table>
                                    <!--/ table --> 
                                </div>
                                <!--/ row -->
                            </div>
                            <!--/ right user body -->
                        </div>
                    </div>
                    <!--/ right container -->
                </div>
                <!--/ row -->
           </div>
           <!--/ container -->
        </div>
        <!--/ sub page body -->
    </main>
    <!--/ sub page main -->
    <!--footer -->
    <?php include 'footer.php' ?>
    <!--/ footer -->  
    <?php include 'footerscripts.php'?>

</body>

</html>