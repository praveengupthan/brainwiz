<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Brainwiz</title>
    <link rel="icon" type="image/png" sizes="32x32" href="img/favicon-32x32.png">
    <!-- style sheets -->
    <?php include 'headerstyles.php' ?>
</head>

<body class="signupBody">
    <!-- sign in -->
    <div class="signUpNew">
        <!-- container fluid -->
        <div class="containerSign">
            <!-- row -->
           <div class="row">
               <!-- col -->
               <div class="col-md-6 px-0">
                   <img src="img/signupimg.jpg" alt="" class="img-fluid signleftimg">
               </div>
               <!--/ row -->
               <!-- col -->
               <div class="col-md-6 px-0 align-self-center">
                   <div class="NewSignupRight">
                   <!-- registration form -->
                   <div class="registrationForm">
                       <h2 class="text-uppercase">STUDENT REGISTRATION FORM</h2>
                      <!-- form -->
                      <form>
                        <!-- row -->
                       <div class="row">
                           <!-- col -->
                           <div class="col-md-12">
                               <div class="form-group">
                                   <label>Name</label>
                                   <div class="input-group">
                                       <input type="text" class="form-control">
                                   </div>
                               </div>
                           </div>
                           <!--/ col -->

                          <!-- col -->
                          <div class="col-md-6">
                               <div class="form-group">
                                   <label>Mobile Number</label>
                                   <div class="input-group">
                                       <input type="text" class="form-control">
                                   </div>
                               </div>
                           </div>
                           <!--/ col -->

                             <!-- col -->
                          <div class="col-md-3">
                             <label>&nbsp;</label>
                            <button type="submit" class="buttonNewFull">Get OTP <span class="icon-long-arrow-right icomoon"></span> </button>
                           </div>
                           <!--/ col -->

                           <!-- col -->
                          <div class="col-md-3">
                               <div class="form-group">
                                   <label>4 Digits OTP</label>
                                   <div class="input-group">
                                       <input type="number" class="form-control">
                                   </div>
                               </div>
                           </div>
                           <!--/ col -->

                           <!-- col -->
                           <div class="col-md-6">
                               <div class="form-group">
                                   <label>College Name</label>
                                   <div class="input-group">
                                       <input type="text" class="form-control">
                                   </div>
                               </div>
                           </div>
                           <!--/ col -->

                           
                           <!-- col -->
                           <div class="col-md-3">
                               <div class="form-group">
                                   <label>Select Branch</label>
                                   <div class="input-group">
                                       <input list="branches" type="text" class="form-control">
                                       <datalist id="branches">
                                            <option value="Technology">
                                            <option value="Science">
                                            <option value="Computers">
                                            <option value="Engineering">
                                            <option value="Tech">
                                        </datalist>
                                   </div>
                               </div>
                           </div>
                           <!--/ col -->

                            <!-- col -->
                            <div class="col-md-3">
                               <div class="form-group">
                                   <label>Select Year</label>
                                   <div class="input-group">
                                       <input list="year" type="text" class="form-control">
                                       <datalist id="year">
                                            <option value="2020">
                                            <option value="2019">
                                            <option value="2018">
                                            <option value="2017">
                                            <option value="2016">
                                        </datalist>
                                   </div>
                               </div>
                           </div>
                           <!--/ col -->

                             <!-- col -->
                             <div class="col-md-6">
                               <div class="form-group">
                                   <label>Email ID</label>
                                   <div class="input-group">
                                       <input type="text" class="form-control">
                                   </div>
                               </div>
                           </div>
                           <!--/ col -->

                            <!-- col -->
                            <div class="col-md-6">
                               <div class="form-group">
                                   <label>Create Password</label>
                                   <div class="input-group">
                                       <input type="password" class="form-control">
                                   </div>
                               </div>
                           </div>
                           <!--/ col -->
                       </div>
                       <!--/ row -->
                       <!-- row -->
                       <div class="row pt-3">                          
                           <div class="col-12 pt-4">
                            <button type="submit" class="buttonNewFull">Submit Now <span class="icon-long-arrow-right icomoon"></span> </button>
                           </div>

                            <!-- col -->
                            <div class="col-md-12 pt-4">
                                    <p>Already Registered User? <a class="fblue" href="signupNewLogin.php">Click here</a></p>
                              </div>
                              <!--/ col -->   
                       </div>
                       <!--/ row -->
                      </form>
                      <!--/ form -->
                   </div>
                   <!--/ registrationform -->
                   </div>
               </div>
               <!--/ row -->
           </div>
           <!--/ row -->
        </div>
        <!--/ container fluid -->

    </div>
    <!--/ sign in --> 
    <?php include 'footerscripts.php'?>

</body>

</html>