<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>BRAINWIZ</title>
    <link rel="icon" type="image/png" sizes="32x32" href="img/favicon-32x32.png">
    <!-- style sheets -->
    <?php include 'headerstyles.php' ?>
</head>

<body class="home-body">
    <!-- header -->
   <?php include 'header.php' ?>
    <!--/ header -->
    
    <!--main -->
    <!-- slider home -->
    <div id="carouselExampleIndicators" class="carousel slide homecarousel">
        <ol class="carousel-indicators">
            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="4"></li>
        </ol>
        <div class="carousel-inner" role="listbox">
            <!-- Slide One - Set the background image for this slide in the line below -->
            <div class="carousel-item active" style="background-image: url('img/slider-01.jpg')">
                <div class="carousel-caption">
                    <h1 class="fbold py-1">Join CRT Course to Crack Campus Interviews </h1>
                    <p>If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything
                        embarrassing hidden in the middle of text.</p>
                </div>
            </div>
            <!-- Slide Two - Set the background image for this slide in the line below -->
            <div class="carousel-item" style="background-image: url('img/slider-02.jpg')">
                <div class="carousel-caption">
                    <h2 class="fbold py-1">Join CRT Course to Crack Campus Interviews</span></h2>
                    <p>If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything
                        embarrassing hidden in the middle of text.</p>
                </div>
            </div>
            <!-- Slide Three - Set the background image for this slide in the line below -->
            <div class="carousel-item" style="background-image: url('img/slider-03.jpg')">
                <div class="carousel-caption">
                    <h2 class="fbold py-1">Join CRT Course to Crack Campus Interviews</span></h2>
                    <p>If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything
                        embarrassing hidden in the middle of text.</p>
                </div>
            </div>

        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <span class="icon-chevron-left icomoon"></span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span class="icon-chevron-right icomoon"></span> </a>

            <!-- crt course form -->
            <div class="course-form" id="course-form">
                <div class="course-formin">
                    <h3 class="text-center">Like to know about CRT Course?</h3>               
                    <form>
                        <input class="form-control" type="text" placeholder="Write your name">                   
                        <input class="form-control" type="text" placeholder="Phone Number">
                        <textarea class="form-control" placeholder="Write Quyery" style="height:100px;"></textarea>                                 
                    </form>
                    <input data-toggle="modal" data-target="#modal-confirm" type="submit" value="Submit"> 
                </div>
            </div>
            <!--/ crt course form -->
    </div>
    <!--/ slider home -->  

    <!-- services -->
    <div class="services-home">
        <!-- container -->
        <div class="container">
            <!-- row -->
            <div class="row">
                <!-- col -->
                <div class="col-lg-3 text-center">
                    <div class="service-col">
                        <a data-toggle="modal" data-target="#weeklyschedule" href="javascript:void(0)"><span class="icon-timetable icomoon"></span></a>
                        <h4>Weekly Schedule</h4>
                        <p>Keeping daily track on syllabus for Quick completion </p>
                    </div>
                </div>
                <!--/ col -->
                <!-- col -->
                <div class="col-lg-3 text-center">
                    <div class="service-col">
                        <a href="video-tutorials.php"><span class="icon-youtube icomoon"></span></a>
                        <h4>Video Tutorials</h4>
                        <p>Learn More from our Online Tutorials</p>
                    </div>
                </div>
                <!--/ col -->
                <!-- col -->
                <div class="col-lg-3 text-center">
                    <div class="service-col">
                        <a href="testseries.php"><span class="icon-computer icomoon"></span></a>
                        <h4>Test Series</h4>
                        <p>Get live Exam Experience by Various MNC Mock Tests</p>
                    </div>
                </div>
                <!--/ col -->
                <!-- col -->
                <div class="col-lg-3 text-center">
                    <div class="service-col">
                        <a href="practise-test.php"><span class="icon-wedding-planning icomoon"></span></a>
                        <h4>Practice Tests</h4>
                        <p>Improve your speed & accuracy by Topic wise Tests. </p>
                    </div>
                </div>
                <!--/ col -->
            </div>
            <!--/ row -->
        </div>
        <!--/ container -->
    </div>
    <!--/ services -->

     <!-- top Tutorials -->
     <div class="top-tutorial">
        <!-- container -->
        <div class="container">
             <!-- title row -->
             <div class="row justify-content-center titlerow">
                <div class="col-lg-6 text-center">
                    <h3>Courses <span> We Offer</span></h3>
                    <p>Explore more with our featured programs.</p>
                </div>
            </div>
            <!--/ title row -->
        </div>
        <!--/ container -->

        <!-- container -->
        <div class="container">
            <!-- row -->
            <div class="row pt-4">
               <!-- col -->
               <div class="col-lg-4">
                    <div class="course-col">                       
                        <a href="crt-detail.php"><img src="img/crtcourseimg.svg" alt="" class="img-fluid "></a>
                        <article class="p-3 text-center">
                            <h3><a href="crt-detail.php">Campus Recruitment Training </a></h3>
                            <p  class="pb-3 text-center">Campus Recruitment Training is a comprehensive program which is exclusively designed for the graduate students preparing them for recruitment of various IT and NON IT companies...... </p>
                            <div class="d-flex justify-content-between">
                                <h5 class="h6 fblue"></h5>
                                <h6 class="h6 fblue"> <a href="#"><span class="icon-download icomoon"></span> Curriculum</a></h6>
                            </div>
                        </article>
                    </div>
                </div>
                <!--/ col -->

                  <!-- col -->
                  <div class="col-lg-4">
                    <div class="course-col">                       
                        <a href="amcat-detail.php"><img src="img/amcat-courseimg.svg" alt="" class="img-fluid "></a>
                        <article class="p-3 text-center">
                            <h3><a href="amcat-detail.php">Amcat </a></h3>
                            <p  class="pb-3 text-center">The AMCAT is an computer adaptive test which measures the job applicants on critical areas like logical reasoning, communication skills, quantitative skills and job specific domain skills.... </p>
                            <div class="d-flex justify-content-between">
                            <h5 class="h6 fblue"></h5>
                            <h6 class="h6 fblue"> <a href="#"><span class="icon-download icomoon"></span> Curriculum</a></h6>                      
                            </div>
                        </article>
                    </div>
                </div>
                <!--/ col -->

                 <!-- col -->
                 <div class="col-lg-4">
                    <div class="course-col">                       
                        <a href="elitmus-detail.php"><img src="img/elitmus-courseimg.svg" alt="" class="img-fluid "></a>
                        <article class="p-3 text-center">
                            <h3><a href="elitmus-detail.php">E-Litmus </a></h3>
                            <p  class="pb-3 text-center">E-litmus is an Indian recruitment organization founded by ex-Infosys employees. It helps companies in hiring fresher’s for their entry-level jobs through its effective and unique test.... </p>
                            <div class="d-flex justify-content-between">
                                <h5 class="h6 fblue"></h5>
                                <h6 class="h6 fblue"> <a href="#"><span class="icon-download icomoon"></span> Curriculum</a></h6>
                            </div>
                        </article>
                    </div>
                </div>
                <!--/ col -->
            </div>
            <!--/ row -->
        </div>
        <!--/ container -->
    </div>
    <!--/ top Tutorials -->

    <!-- table section -->
    <div class="table-section sectionmedium">
        <div class="container-fluid">
            <!-- row -->
            <div class="row">
                <!-- col -->
                <div class="col-lg-7 align-self-center">
                    <!-- table -->
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th scope="col">Course name</th>
                                <th scope="col">Date </th>
                                <th scope="col">Timings </th>                               
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td scope="row">Campus Recruitment Training <span class="fgreen blink-text batchtxt">(New Batch)</span>
                                </td>
                                <td>26th September 2019</td>
                                <td>2.00 PM - 4.00PM</td>                               
                            </tr>
                            <tr>
                                <td scope="row">Campus Recruitment Training </span>
                                </td>
                                <td>5th August 2019</td>
                                <td>6.00 PM - 8.00PM</td>                                
                            </tr>
                            <tr>
                                <td scope="row">Campus Recruitment Training </span>
                                </td>
                                <td>26th August 2019</td>
                                <td>6.00 PM - 8.00PM</td>                                
                            </tr>
                            <tr>
                                <td scope="row">Campus Recruitment Training </span>
                                </td>
                                <td>5th August 2019</td>
                                <td>6.00 PM - 8.00PM</td>                               
                            </tr>
                        </tbody>
                    </table>
                    <!--/ table -->
                </div>
                <!--/ col -->

                <!-- slider col -->
                <div class="col-lg-5">
                    <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <img class="d-block w-100" src="img/tabslider01.jpg" alt="First slide">
                            </div>
                            <div class="carousel-item">
                                <img class="d-block w-100" src="img/tabslider02.jpg" alt="Second slide">
                            </div>
                            <div class="carousel-item">
                                <img class="d-block w-100" src="img/tabslider03.jpg" alt="Third slide">
                            </div>
                            <div class="carousel-item">
                                <img class="d-block w-100" src="img/tabslider04.jpg" alt="Third slide">
                            </div>
                            <div class="carousel-item">
                                <img class="d-block w-100" src="img/tabslider05.jpg" alt="Third slide">
                            </div>
                        </div>
                        <a class="carousel-control-prev" href="#carouselExampleControls" role="button"
                            data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselExampleControls" role="button"
                            data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>
                <!--/ slider col -->
            </div>
            <!--/ row -->
        </div>
    </div>
    <!--/ table section -->

    <!-- why choose section -->
    <div class="why-choosesection">
        <!-- container -->
        <div class="container">
            <!-- row -->
            <div class="row">
                <!-- col -->
                <div class="col-lg-6 align-self-center">                  
                    
                    <!-- video -->
                    <div class="video-utube pt-2">
                        <div class="whitebox">
                            <video width="100%"  playsinline controls="" muted="" autoplay="autoplay">
                                <source src="http://www.gobrainwiz.in/upload/Videos/RAMA7308.mp4" type="video/mp4">
                            Your browser does not support the video tag.
                            </video>                           
                            
                            <a class="position-absolute play-icon" href="javascript:void(0)" data-toggle="modal" data-target="#video-autopop">
                                <span class="icon-youtube-play icomoon"></span>
                            </a>
                        </div>
                    </div>
                    <!--/ video -->
                </div>
                <!--/ col -->
                <!-- col 6-->
                <div class="col-lg-6">
                    <h5 class="m-text-center m-py-10">Why Most of the Students <span>Prefer BRAINWIZ!</span></h5>
                    <p class="fmediumsize m-text-center"> Stay ahead in your career by exploring our featured programs <a class="fgreen d-block" href="about.php">Read More</a> </p>
                    <!-- row -->
                    <div class="row">
                        <!-- col -->
                        <div class="col-lg-6 whycol">
                            <span class="icon-professor icomoon"></span>
                            <h6>Expert Faculty</h6>
                            <p>Mentoring by expert faculty to learn in depth concepts efficiently</p>
                        </div>
                        <!--/ col -->
                        <!-- col -->
                        <div class="col-lg-6 whycol">
                            <span class="icon-first icomoon"></span>
                            <h6>Assessments</h6>
                            <p>Get the Quality Training for all Assessment Exams like AMCAT, Elitmus & Co-Cubes to grab a good score. </p>
                        </div>
                        <!--/ col -->
                        <!-- col -->
                        <div class="col-lg-6 whycol">
                            <span class="icon-customer icomoon"></span>
                            <h6>Comprehensive Training</h6>
                            <p>Giving In-depth Training on 5 modules covering 55+ topics with complete Tricks & Shortcuts </p>
                        </div>
                        <!--/ col -->
                        <!-- col -->
                        <div class="col-lg-6 whycol">
                            <span class="icon-person-giving-a-lecture-for-big-auditory icomoon"></span>
                            <h6>Best Results</h6>
                            <p>Trained more than 45,000+ Students with more than 90% Success rate. </p>
                        </div>
                        <!--/ col -->
                    </div>
                    <!--/ row -->
                </div>
                <!--/ col 6-->
            </div>
            <!--/ row -->
        </div>
        <!--/ container -->
    </div>
    <!--/ why choose section -->

    <!-- our track record -->
    <div class="our-trackrecord d-none d-sm-block">
        <!-- container fluid -->
        <div class="container-fluid">
            <!-- row -->
            <div class="row">
                <!-- col -->
                <div class="col-lg-4 leftTrackRecordColumn align-self-center">
                    <article class="position-relative">
                        <h2>Our Track Record</h2>                        
                    </article>
                </div>
                <!--/ col -->

                <!-- right column -->
                <div class="col-lg-8 align-self-center">
                    <!-- row -->
                    <div class="row">
                        <!-- col -->
                        <div class="col-lg-4 text-center recordcolumn">
                            <span class="icon-reading icomoon"></span>
                            <h2 class="mb-0">1800+</h2>
                            <p>Students we trained</p>
                        </div>
                        <!--/ col -->

                        <!-- col -->
                        <div class="col-lg-4 text-center recordcolumn">
                            <span class="icon-select icomoon"></span>
                            <h2 class="mb-0">1300+</h2>
                            <p>Students we Placed</p>
                        </div>
                        <!--/ col -->

                        <!-- col -->
                        <div class="col-lg-4 text-center recordcolumn">
                            <span class="icon-presentation icomoon"></span>
                            <h2 class="mb-0">760+</h2>
                            <p>Taken Batches</p>
                        </div>
                        <!--/ col -->
                    </div>
                    <!--/ row -->
                </div>
                <!-- right column -->
            </div>
            <!--/ row -->
        </div>
        <!-- container fluid -->
    </div>
    <!--/ our track record -->

    <!-- our success stories -->
    <div class="success-stories">
        <!-- container -->
        <div class="container">
            <!-- title row -->
            <div class="row justify-content-center titlerow">
                <div class="col-lg-6 text-center">
                    <h3>Our Success <span>Stories</span></h3>
                    <p>Students Trained by Pavan Jaiswal 2019 Batch <a href="students.php" class="fgreen d-inline-block pl-2"> View All</a></p>
                </div>
            </div>
            <!--/ title row -->
        </div>
        <!--/ container -->
        <!-- container fluid -->
        <div class="container">
            <!-- slick -->
            <!-- slick slider row -->
            <div class="successStories custom-slick students-slick">
                <!-- slide -->
                <div class="slide">
                    <a href="javascript:void(0)" data-toggle="modal" data-target="#student-testimonial">
                        <img src="img/data/student01.jpg" alt="" class="img-fluid">
                        <h4>Praveen guptha</h4>
                        <h5>Infosys</h5>
                    </a>
                </div>
                <!--/ slide -->
                <!-- slide -->
                <div class="slide">
                    <a href="javascript:void(0)">
                        <img src="img/data/student02.jpg" alt="" class="img-fluid">
                        <h4>Pavan Jaiswal</h4>
                        <h5>Infosys</h5>
                    </a>
                </div>
                <!--/ slide -->
                <!-- slide -->
                <div class="slide">
                    <a href="javascript:void(0)">
                        <img src="img/data/student03.jpg" alt="" class="img-fluid">
                        <h4>Sarwani</h4>
                        <h5>Infosys</h5>
                    </a>
                </div>
                <!--/ slide -->
                <!-- slide -->
                <div class="slide">
                    <a href="javascript:void(0)">
                        <img src="img/data/student04.jpg" alt="" class="img-fluid">
                        <h4>Kiran Kumar</h4>
                        <h5>Infosys</h5>
                    </a>
                </div>
                <!--/ slide -->

                <!-- slide -->
                <div class="slide">
                    <a href="javascript:void(0)">
                        <img src="img/data/student05.jpg" alt="" class="img-fluid">
                        <h4>Suresh Varma</h4>
                        <h5>Infosys</h5>
                    </a>
                </div>
                <!--/ slide -->

                <!-- slide -->
                <div class="slide">
                    <a href="javascript:void(0)">
                        <img src="img/data/student06.jpg" alt="" class="img-fluid">
                        <h4>Ragini</h4>
                        <h5>Infosys</h5>
                    </a>
                </div>
                <!--/ slide -->

                <!-- slide -->
                <div class="slide">
                    <a href="javascript:void(0)">
                        <img src="img/data/student01.jpg" alt="" class="img-fluid">
                        <h4>Ramesh Kumar</h4>
                        <h5>TCS</h5>
                    </a>
                </div>
                <!--/ slide -->

                <!-- slide -->
                <div class="slide">
                    <a href="javascript:void(0)">
                        <img src="img/data/student02.jpg" alt="" class="img-fluid">
                        <h4>Acaharya Upadhoya</h4>
                        <h5>Infosys</h5>
                    </a>
                </div>
                <!--/ slide -->
            </div>
            <!--/ slick slider row -->
            <!--/ slick -->
        </div>
        <!--/ container fluid -->
    </div>
    <!--/ our success stories -->

    <!-- Academic Resources -->
    <div class="resourcesAcademic">
        <!-- container -->
        <div class="container">
             <!-- title row -->
             <div class="row justify-content-center titlerow">
                <div class="col-lg-6 text-center py-5">
                    <h3>Academic  <span>Resources</span></h3>
                    <p>Comprehensive hubs for all your preparation needs</p>
                </div>
            </div>
            <!--/ title row -->

            <!-- row -->
            <div class="row">
                <!-- left col -->
                <div class="col-md-8">
                    <!-- row -->
                    <div class="row">
                        <!-- col -->
                        <div class="col-md-4 acaMicCol text-center border-bottom">
                            <div class="iconDiv">
                                <span class="icon-diagram icomoon"></span>
                            </div>
                            <h5>Quant Zone</h5>
                            <p>Knowledge-laden videos & Articles</p>
                        </div>
                        <!--/ col-->

                        <!-- col -->
                        <div class="col-md-4 acaMicCol text-center border-bottom border-right border-left">
                            <div class="iconDiv">
                                <span class="icon-select icomoon"></span>
                            </div>
                            <h5>Verbal Zone</h5>
                            <p>In-depth RC, Grammer & Vocabulary Coverage</p>
                        </div>
                        <!--/ col-->

                        <!-- col -->
                        <div class="col-md-4 acaMicCol text-center border-bottom">
                            <div class="iconDiv">
                                <span class="icon-reading icomoon"></span>
                            </div>
                            <h5>Data Zone</h5>
                            <p>Expert tips and strategies</p>
                        </div>
                        <!--/ col-->

                        <!-- col -->
                        <div class="col-md-4 acaMicCol text-center">
                            <div class="iconDiv">
                                <span class="icon-professor icomoon"></span>
                            </div>
                            <h5>Reasoning Zone</h5>
                            <p>Boost reasoning and logical thinking skills</p>
                        </div>
                        <!--/ col-->

                        <!-- col -->
                        <div class="col-md-4 acaMicCol text-center border-right border-left">
                            <div class="iconDiv">
                                <span class="icon-computer icomoon"></span>
                            </div>
                            <h5>Interview Zone</h5>
                            <p>Master essential personality skills</p>
                        </div>
                        <!--/ col-->

                        <!-- col -->
                        <div class="col-md-4 acaMicCol text-center">
                            <div class="iconDiv">
                                <span class="icon-puzzle icomoon"></span>
                            </div>
                            <h5>GK Zone</h5>
                            <p>One-stop solution for your GK needs</p>
                        </div>
                        <!--/ col-->
                    </div>
                    <!--/ row -->
                </div>
                <!--/ left col -->

                 <!-- right col -->
                 <div class="col-md-4 align-self-center border-left">
                    <h5 class="m-text-center m-py-10 h4 pb-1 fblue">What's New</h5>
                    <ul class="newAcademic">
                        <li>
                            <a href="javascript:void(0)">CAT 2020 Test Series</a>
                        </li>
                        <li>
                            <a href="javascript:void(0)">CAT 2020 Online Classroom : Crack CAT with 3 Live Lectures per week</a>
                        </li>
                        <li>
                            <a href="javascript:void(0)">Scholarship up to 100% for CAT 2020</a>
                        </li>
                        <li>
                            <a href="javascript:void(0)">IPM Crash Course : Propel your IPM preparation to the next level</a>
                        </li>
                        <li>
                            <a href="javascript:void(0)">CLAT/Law'20 Online Classroom</a>
                        </li>
                        <li>
                            <a href="javascript:void(0)">CAT 20 Online Classroom Demo : Get 10 days FREE access to 3 live lectures</a>
                        </li>
                    </ul>
                 </div>
                <!--/ right col -->

            </div>
            <!--/ row -->
        </div>
        <!--/ container -->
    </div>
    <!--/ academic Resources -->

    <!-- home page testimonials -->
    <div class="home-testimonials">
        <!-- container -->
        <div class="container">
            <!-- title row -->
            <div class="row justify-content-center titlerow">
                <div class="col-lg-6 text-center">
                    <h3>What our<span> Students says</span></h3>
                    <p>                        
                        <a target="_blank" href="https://www.google.com/search?sxsrf=ACYBGNRTLTAgLveMpFHo3LHZKhUJW6X-9A%3A1577011136159&source=hp&ei=wEf_XYfNB5yc4-EPoOaP2As&q=brainwiz&oq=brainwiz&gs_l=psy-ab.3..35i39j0l7j0i10j0.3358.4032..4179...1.0..0.171.943.3j5......0....1..gws-wiz.......0i131j0i67j0i10i67j0i131i67.Z7_diWgoIew&ved=0ahUKEwiHrc2kiMnmAhUczjgGHSDzA7sQ4dUDCAY&uact=5#lrd=0x3bcb90c62074a435:0xc940fb93864b35d,1,,," class="fgreen d-inline-block pl-4"> Google Reviews</a>
                    </p>
                </div>
            </div>
            <!--/ title row -->

            <!-- slick slide row -->
            <div class="students-testimonials custom-slick">
                <!-- slide -->
                <div class="slide text-center">
                    <img src="img/data/student01.jpg" alt="" title="" class="img-fluid">
                    <h5>Student Name will be here</h5>
                    <h6>Placed in Wipro</h6>
                    <p>Thanks sir for giving me such a CRT training,due to which i have cleared all Apti round for wipro
                        and Infosys.Unfortunately i was rejected in Wipro after final round but finally I got placed in
                        Infosys and recently I have new job from TCS.</p>
                </div>
                <!--/ slide -->
                <!-- slide -->
                <div class="slide text-center">
                    <img src="img/data/student02.jpg" alt="" title="" class="img-fluid">
                    <h5>Student Name will be here</h5>
                    <h6>Placed in Wipro</h6>
                    <p>Thanks sir for giving me such a CRT training,due to which i have cleared all Apti round for wipro
                        and Infosys.Unfortunately i was rejected in Wipro after final round but finally I got placed in
                        Infosys and recently I have new job from TCS.</p>
                </div>
                <!--/ slide -->
                <!-- slide -->
                <div class="slide text-center">
                    <img src="img/data/student03.jpg" alt="" title="" class="img-fluid">
                    <h5>Student Name will be here</h5>
                    <h6>Placed in Wipro</h6>
                    <p>Thanks sir for giving me such a CRT training,due to which i have cleared all Apti round for wipro
                        and Infosys.Unfortunately i was rejected in Wipro after final round but finally I got placed in
                        Infosys and recently I have new job from TCS.</p>
                </div>
                <!--/ slide -->
                <!-- slide -->
                <div class="slide text-center">
                    <img src="img/data/student04.jpg" alt="" title="" class="img-fluid">
                    <h5>Student Name will be here</h5>
                    <h6>Placed in Wipro</h6>
                    <p>Thanks sir for giving me such a CRT training,due to which i have cleared all Apti round for wipro
                        and Infosys.Unfortunately i was rejected in Wipro after final round but finally I got placed in
                        Infosys and recently I have new job from TCS.</p>
                </div>
                <!--/ slide -->
            </div>
            <!--/ slick slide row -->
        </div>
        <!--/ container -->
    </div>
    <!--/ hoem page testimonials -->

    <!-- android mobile app-->
    <div class="apstore">
        <!-- container -->
        <div class="container">
            <!-- row -->
            <div class="row">
                <!-- col -->
                <div class="col-lg-6 align-self-center">
                    <article>
                        <h3>Download & Enjoy</h3>
                        <p class="py-2">Access your courses anywhere, anytime & prepare with practice tests</p>
                        <a href="https://play.google.com/store/apps/details?id=com.gobrainwiz" target="_blank" class="d-flex p-2">
                            <span class="icon-android icomoon"></span>
                            <div>
                                <p class="small">Available on</p>
                                <h4>Google Play</h4>
                            </div>
                        </a>
                    </article>
                </div>
                <!--/ col -->
                <!-- col -->
                <div class="col-lg-6 text-right align-self-end d-none d-sm-block">
                    <img src="img/appmobileimg.png" class="img-fluid" alt="">
                </div>
                <!--/ col -->
            </div>
            <!--/ row -->
        </div>
        <!--/ coontainer -->
    </div>
    <!--/ android mobile app-->

   

    <!-- major companies -->
    <div class="major-companies">
        <!-- container -->
        <div class="container">
            <!-- title row -->
            <div class="row justify-content-center titlerow">
                <div class="col-lg-6 text-center">
                    <h3>Training Focus on <span>Major Companies</span></h3>
                    <p>Join 1,700 companies using BRAINWIZ to upskill their employees</p>
                </div>
            </div>
            <!--/ title row -->
        </div>
        <!--./ container -->

        <!-- container fluid -->
        <div class="container-fluid">
            <div class="companies custom-slick">
                <!-- slide -->
                <div class="slider">
                    <img src="img/comp01.jpg" alt="" title="">
                </div>
                <!--/ slide -->
                <!-- slide -->
                <div class="slider">
                    <img src="img/comp02.jpg" alt="" title="">
                </div>
                <!--/ slide -->
                <!-- slide -->
                <div class="slider">
                    <img src="img/comp03.jpg" alt="" title="">
                </div>
                <!--/ slide -->
                <!-- slide -->
                <div class="slider">
                    <img src="img/comp04.jpg" alt="" title="">
                </div>
                <!--/ slide -->
                <!-- slide -->
                <div class="slider">
                    <img src="img/comp05.jpg" alt="" title="">
                </div>
                <!--/ slide -->
                <!-- slide -->
                <div class="slider">
                    <img src="img/comp06.jpg" alt="" title="">
                </div>
                <!--/ slide -->
                <!-- slide -->
                <div class="slider">
                    <img src="img/comp07.jpg" alt="" title="">
                </div>
                <!--/ slide -->
                <!-- slide -->
                <div class="slider">
                    <img src="img/comp08.jpg" alt="" title="">
                </div>
                <!--/ slide -->
                <!-- slide -->
                <div class="slider">
                    <img src="img/comp01.jpg" alt="" title="">
                </div>
                <!--/ slide -->
            </div>
        </div>
        <!--/ container fluid -->
    </div>
    <!--/ major companies -->

    <!-- blogs -->
    <div class="home-blogs d-none d-sm-block">
        <!-- container -->
        <div class="container">
            <!-- title row -->
            <div class="row justify-content-center titlerow">
                <div class="col-lg-6 text-center">
                    <h3>Our <span>Blogs</span></h3>
                    <p>News & Updates <a href="javascript:void(0)"class="fgreen d-inline-block pl-2"> View All</a></p>
                </div>
            </div>
            <!--/ title row -->

            <!-- row -->
            <div class="row py-3">
                <!-- blog col -->
               <div class="col-lg-4">
                    <div class="blogcol">
                        <a href="javascript:void(0)">
                            <img src="img/data/blogimg01.jpg" alt="" class="img-fluid">
                        </a>
                        <article>
                            <h4><a href="javascript:void(0)">Blog Title will be here</a></h4>
                            <p>There are many variations of passages of Lorem Ipsum available There are many variations of </p>
                        </article>
                        <p class="d-flex justify-content-between">
                            <span class="fblue">Admin</span>
                            <span class="fblue">Posted on 25-10-2019</span>
                        </p>
                    </div>
                </div>
                <!--/ blog col -->

                 <!-- blog col -->
               <div class="col-lg-4">
                    <div class="blogcol">
                        <a href="javascript:void(0)">
                            <img src="img/data/blogimg02.jpg" alt="" class="img-fluid">
                        </a>
                        <article>
                            <h4><a href="javascript:void(0)">Blog Title will be here</a></h4>
                            <p>There are many variations of passages of Lorem Ipsum available There are many variations of </p>
                        </article>
                        <p class="d-flex justify-content-between">
                            <span class="fblue">Admin</span>
                            <span class="fblue">Posted on 25-10-2019</span>
                        </p>
                    </div>
                </div>
                <!--/ blog col -->

                 <!-- blog col -->
               <div class="col-lg-4">
                    <div class="blogcol">
                        <a href="javascript:void(0)">
                            <img src="img/data/blogimg03.jpg" alt="" class="img-fluid">
                        </a>
                        <article>
                            <h4><a href="javascript:void(0)">Blog Title will be here</a></h4>
                            <p>There are many variations of passages of Lorem Ipsum available There are many variations of </p>
                        </article>
                        <p class="d-flex justify-content-between">
                            <span class="fblue">Admin</span>
                            <span class="fblue">Posted on 25-10-2019</span>
                        </p>
                    </div>
                </div>
                <!--/ blog col -->
            </div>
            <!--/ row -->
        </div>
        <!--/ container -->
    </div>
    <!--/ blogs -->  
    <!--/ main -->
    <!--footer -->
    <?php include 'footer.php' ?>
    <!--/ footer -->      
    <?php include 'footerscripts.php'?>

<!-- mobile call and message -->
<div class="call-mobile d-flex justify-content-around d-sm-none d-block">
    <a href="tel:+91 81421 23938"><span class="icon-call-answer icomoon"></span> Call us </a>
    <a href="javascript:void(0)"><span class="icon-comments icomoon"></span> Live Chat </a>
</div>
<!--/ mobile call and message -->


<!-- Video auto pop for introduction video-->    
<div class="modal fade" id="video-autopop" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">                   
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <video width="100%"  playsinline controls="">
                    <source src="http://www.gobrainwiz.in/upload/Videos/RAMA7308.mp4" type="video/mp4">
                    Your browser does not support the video tag.
                </video>               
            </div>        
        </div>
    </div>
</div>
<!--/ Video auto pop up for introduction video-->

<!-- weekly schedule pop-->    
<div class="modal fade" id="weeklyschedule" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Weekly Schedule</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <img src="img/weekschedule.jpg" alt="" class="img-fluid">
            </div>        
        </div>
    </div>
</div>
<!--/ weekly schedule pop-->

<!-- on submit form success message  -->
<div id="modal-confirm" class="modal fade" data-keyboard="false" data-backdrop="static">
	<div class="modal-dialog modal-confirm">
		<div class="modal-content">
			<div class="modal-header">
				<div class="icon-box">
                    <img src="img/checkmark.gif" alt="">
				</div>				
				<h4 class="modal-title text-center">Awesome!</h4>	
			</div>
			<div class="modal-body">
				<p class="text-center">Your booking has been confirmed. Check your email for detials.</p>
			</div>
			<div class="modal-footer">
				<button class="btn btn-success btn-block" data-dismiss="modal">OK</button>
			</div>
		</div>
	</div>
</div>
<!--/ on submit form success message -->

<!-- Modal video -->
<div class="modal fade video-modal" id="student-testimonial" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog  modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">               
                <button id="close" type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <iframe id="ytplayer" mute="1" class="modalvideo" width="100%" src="https://www.youtube.com/embed/HSgjpQBkR0c" allow="autoplay" frameborder="0" allow="accelerometer; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>               
            </div>
        </div>
    </div>
</div>
<!--/ modal video -->  

</body>
</html>