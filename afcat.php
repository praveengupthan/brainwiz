<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>AFCAT Exam Pattern</title>
    <link rel="icon" type="image/png" sizes="32x32" href="img/favicon-32x32.png">
    <!-- style sheets -->
    <?php include 'headerstyles.php' ?>
</head>

<body>

    <!-- header -->
   <?php include 'header.php' ?>
    <!--/ header -->
    <!--sub page main -->
    <main class="subpage">
        <!-- sub page header -->
        <div class="page-header">
            <!-- container -->
            <div class="container">
               <!-- row -->
               <div class="row">
                   <div class="col-lg-6">
                       <h1>AFCAT Exam </h1>
                   </div>
               </div>
               <!--/ row --> 
            </div>
            <!--/ container -->
        </div>
        <!--/ sub page header -->

        <!-- brudcrumb -->
        <div class="container">
            <!-- row -->
            <div class="row">
                <!-- col -->
                <div class="col-lg-9">                   
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Examinations</a></li>
                        <li class="breadcrumb-item active"><a>AFCat</a></li>
                    </ul>                    
                </div>
                <!--/col -->
            </div>
            <!--/ row -->
        </div>
        <!--/ brudcrumb -->

        <!-- sub page body -->
        <div class="subpage-body">
           <!-- container -->
           <div class="container">
               <!-- row -->
               <div class="row">
                    <!-- col 8 -->
                    <div class="col-lg-8">
                        <h2 class="h2 pb-2">What is AFCAT?</h2>                        
                        <p>The Air Force Common Admission Test (AFCAT) is conducted by the Indian Air Force. The exam is held twice a year in February and August, respectively, to select Class-I Gazetted Officers in Flying and Ground Duties (Technical and Non-Technical).</p>
                       
                        <p>AFCAT is a gateway for both male and female candidates who wish to join the Indian Air Force for both Short Service Commission in Flying Branch and Permanent Commission/Short Service Commission in Ground Duties for both Technical and Non-Technical roles.</p>

                        <p>Candidates are selected based on their performance in online exam and Air Force Selection Board (AFSB) Interview. Candidates opting for Ground Duty (Technical) branch are required to appear in both AFCAT and Engineering Knowledge Test (EKT)</p>

                        <h2 class="h2 pb-2">AFCAT 2020 EXAM</h2>  
                        <p>Indian Air Force (IAF) has released the detailed notification for AFCAT (1) 2020 exam on December 1. According to the notification, the online test of AFCAT is scheduled to be conducted on February 22 and 23, 2020. Interested candidates can fill AFCAT application form 2020 online from December 1 to 30, 2019. Total 249 vacancies will be filled through AFCAT entry. Earlier, IAF had released short notification for AFCAT 2020 exam. </p>
                       
                        <h3 class="h2">AFCAT Training in Hyderabad</h3>

                       <p>For AFCAT Training, BRAINWIZ is the Best AFCAT Training institute in Hyderabad. BRAINWIZ trained thousands of students across PAN India to secure a good score in AFCAT Exam. The updated syllabus of BRAINWIZ helps students to clear this Online Test of AFCAT, which is considered as the first round of Interview in the AFCAT Selection process. BRAINWIZ trains the students according to AFCAT Syllabus covering, Numerical Ability, Logical Reasoning and Verbal Ability concepts. BRAINWIZ has a team of expert faculties who has complete knowledge on various Competitive streams who will train the students to excel in the exams and get good score to get qualified. And the Training also covers the previous years asked questions, model papers etc. so it is considered as the Best AFCAT Training Institute in Ameerpet, Hyderabad. </p>                      

                       <h3 class="h2">AFCAT Eligibility Criteria 2020</h3>
                       <p>To be eligible for AFCAT exam 2020, candidates need to fulfil the following criteria: </p>    
                       <p>Nationality: Candidates must be a citizen of India</p>   
                       <p>Age limit: The age limits prescribed for the exam are different for different branches.</p>

                       <div class="table-responsive">
                        <table class="table">   
                            <thead>
                                <tr>
                                    <th>Branches</th>
                                    <th>Age Limit</th>
                                </tr>
                            </thead>                        
                            <tbody>
                                <tr>
                                    <td>Flying branch</td>
                                    <td>Candidates should be between 20 to 24 years. The upper age limit for candidates having Commercial Pilot License is relaxed upto 26 years.</td>
                                </tr>
                                <tr>
                                    <td>
                                    Ground Duty (Technical & Non-Technical) Branch
                                    </td>
                                    <td>
                                    The age of candidates should be between 20 to 26 years.
                                    </td>
                                </tr>
                                                           
                            </tbody>                           
                        </table>
                        </div>

                        <h3 class="h3">Martial Status:</h3>

                        <ul class="page-list">
                            <li>Candidates below 25 years of age must be unmarried at the time of commencement of course.</li>
                            <li>Widows/Widowers and divorcees below 25 years of age are also not eligible.</li>
                            <li>Candidates who marry to the date of his application though successful at SSB or medical will not be eligible for training.</li>
                            <li>Those who marry during the period of training will be discharged and will be liable to refund all expenditure incurred on him by the government.</li>
                            <li>Married candidates above 25 years are eligible to apply but during their training period, they will neither be provided married accommodation nor can they live with family.</li>
                        </ul>
                      
                        <h3 class="h2">Detailed AFCAT 2020 Syllabus </h3>
                        <h3 class="h4">AFCAT Syllabus 2020 for English</h3>                                           

                        <ul class="page-list pb-3">                          
                            <li>Reading Comprehension</li>
                            <li>Error Detection</li>
                            <li>Sentence Completion</li>
                            <li>Filling in of correct word</li>
                            <li>Synonyms</li>
                            <li>Antonyms </li>
                            <li>Vocabulary</li>
                            <li>Idioms and Phrases.</li>
                        </ul>

                        <h3 class="h4">AFCAT Syllabus 2020 for General Awareness</h3>
                        <ul class="page-list pb-3">                          
                            <li>History </li>
                            <li>Geography</li>
                            <li>Civics</li>
                            <li>Politics</li>
                            <li>Current Affairs</li>
                            <li>Environment</li>
                            <li>Basic Science</li>
                            <li>Defence</li>
                            <li>Art</li>
                            <li>Culture</li>
                            <li>Sports, etc.</li>
                        </ul>

                        <h3 class="h4">AFCAT Syllabus 2020 for Numerical Ability</h3>
                        <ul class="page-list pb-3">                          
                            <li>Numerical Ability </li>
                            <li>Decimal Fraction</li>
                            <li>Time and Work</li>
                            <li>Average</li>
                            <li>Profit & Loss</li>
                            <li>Percentage</li>
                            <li>Ratio & Proportion </li>
                            <li>Simple Interest</li>
                            <li>Time & Distance (Trains/Boats & Streams).</li>
                        </ul>



                        <h3 class="h4">AFCAT Syllabus 2020 for Reasoning and Military Aptitude Test</h3>
                        <p>Questions will be asked from Verbal Skills and Spatial Ability.</p>

                        <h2 class="h2">EKT syllabus 2020</h2>
                        <p>EKT is conducted for candidates opting for Ground Duty (Technical) branch. The syllabus comprises topics from Mechanical, Computer Science and Electrical & Electronics. This section comprises total 50 questions. The test carries total 150 marks.</p>

                        <h2 class="h2">EKT Syllabus for Computer Science Engineering</h2>

                        <div class="table-responsive">
                        <table class="table">                            
                            <tbody>
                                <tr>
                                    <td>Fundamental Engineering</td>
                                    <td>
                                        <ul class="page-list">
                                            <li>Engineering Physics</li>
                                            <li>Engineering Mathematics</li>
                                            <li>Engineering Drawing</li>                                            
                                        </ul>
                                    </td>                                    
                                </tr>
                                <tr>
                                    <td>Specialised branch topics</td>
                                    <td>
                                        <ul class="page-list">
                                            <li>Information Technology</li>
                                            <li>Network Theory Design</li>
                                            <li>Analog and Digital Electronics</li>
                                            <li>Computer Networks</li>
                                            <li>Switching Theory</li>
                                            <li>Electronic Devices</li>                                       
                                        </ul>
                                    </td>                                    
                                </tr>
                                <tr>
                                    <td>Allied Engineering Topics</td>
                                    <td>
                                        <ul class="page-list">
                                            <li>Control Engineering</li>
                                            <li>Telecommunication Systems</li>
                                            <li>Electrical Engineering</li>
                                            <li>Radar Theory</li>
                                            <li>Instrumentation</li>
                                            <li>Antenna and Wave Propagation</li>
                                            <li>Microwave Engineering</li>                       
                                        </ul>
                                    </td>                                    
                                </tr>                      
                            </tbody>                           
                        </table>
                        </div>

                        <h2 class="h2">EKT Syllabus for Mechanical Engineering</h2>

                        <div class="table-responsive">
                        <table class="table">                            
                            <tbody>
                                <tr>
                                    <td>Fundamental Engineering syllabus</td>
                                    <td>
                                        <ul class="page-list">
                                            <li>Engineering Physics</li>
                                            <li>Engineering Mathematics</li>
                                            <li>Engineering Drawing/ Engineering Graphics</li>                                  
                                        </ul>
                                    </td>                                    
                                </tr>
                                <tr>
                                    <td>Specialised Branch topics</td>
                                    <td>
                                        <ul class="page-list">
                                            <li>Thermodynamics</li>
                                            <li>Fluid Mechanics/Hydraulic Machines</li>
                                            <li>Engineering Mechanics</li>
                                            <li>Materials Science</li>
                                            <li>Thermodynamics</li>
                                            <li>Manufacturing Science</li>
                                            <li>Machine Drawing</li>                           
                                        </ul>
                                    </td>                                    
                                </tr>
                                <tr>
                                    <td>Allied Engineering Topics</td>
                                    <td>
                                        <ul class="page-list">
                                            <li>Control Engineering</li>
                                            <li>Telecommunication Systems</li>
                                            <li>Electrical Engineering</li>
                                            <li>Radar Theory</li>
                                            <li>Instrumentation</li>
                                            <li>Antenna and Wave Propagation</li>
                                            <li>Microwave Engineering</li>                       
                                        </ul>
                                    </td>                                    
                                </tr> 
                                <tr>
                                    <td>Allied Engineering Topics</td>
                                    <td>
                                        <ul class="page-list">
                                            <li>Aerodynamics</li>
                                            <li>Flight Mechanics</li>
                                            <li>Aircraft Structures</li>
                                            <li>Power Plant Engineering.</li>
                                            <li>Industrial Engineering</li>
                                            <li>Automotive Engineering </li>          
                                        </ul>
                                    </td>                                    
                                </tr>                               
                            </tbody>                           
                        </table>
                        </div>

                        <h2 class="h2">EKT Syllabus for Electrical & Electronics Engineering:</h2>

                        <div class="table-responsive">
                        <table class="table">                            
                            <tbody>
                                <tr>
                                    <td>Fundamental Engineering syllabus</td>
                                    <td>
                                        <ul class="page-list">
                                            <li>Engineering Physics</li>
                                            <li>Engineering Mathematics</li>
                                            <li>Engineering Drawing</li>                                  
                                        </ul>
                                    </td>                                    
                                </tr>
                                <tr>
                                    <td>Specialised Branch topics</td>
                                    <td>
                                        <ul class="page-list">
                                            <li>Microwave Engineering</li>
                                            <li>Analog and Digital Electronics</li>
                                            <li>Electronic Devices</li>
                                            <li>Telecommunication Systems</li>
                                            <li>Microwave Engineering</li>
                                            <li>Control Engineering</li>
                                            <li>Electrical Engineering</li>            
                                        </ul>
                                    </td>                                    
                                </tr>
                                <tr>
                                    <td>Allied Engineering Topics</td>
                                    <td>
                                        <ul class="page-list">
                                            <li>Switching Theory</li>
                                            <li>Computer Networks</li>
                                            <li>Radar Theory</li>
                                            <li>Network Theory Design</li>
                                            <li>Instrumentation</li>
                                            <li>Information Technology</li>             
                                        </ul>
                                    </td>                                    
                                </tr>                                                   
                            </tbody>                           
                        </table>
                        </div>

                        
                        <h2 class="h2">Educational Qualifications:</h2>

                        <div class="table-responsive">
                        <table class="table">                            
                            <tbody>
                                <tr>
                                    <td>Flying branch</td>
                                    <td>
                                       <p>Candidates must have passed with minimum 60% in Maths and Physics at Class 12 and Graduation in any discipline with 60% or equivalent. </p>
                                       <p>BE/BTech with 60% or equivalent. </p>
                                       <p>Candidates have cleared section A & B examination of Associate Membership of Institution of Engineers (India) or Aeronautical Society of India from a recognised University with 60%.</p>
                                    </td>                                    
                                </tr>
                                <tr>
                                    <td>Ground Duty Technical Branch</td>
                                    <td>
                                        <p>Aeronautical Engineer (Electronics):Passed Class 10+2 with 60% marks each in Physics and Mathematics and four years degree Engineering/ Technology</p>
                                        <p>Or</p>
                                        <p>Cleared Sections A and B of Associate Membership of Institution of Engineers (India) or Aeronautical Society of India or Graduate membership examination of the Institution of Electronics and Telecommunication Engineers by actual studies with 60% in disciplines specified by the examination authority.</p>
                                        <p>Aeronautical Engineer (Mechanical):  </p>
                                        <p>Candidates must have passed 10+2 with 60% in Physics and Mathematics and degree/integrated post-graduation in Engineering/Technology</p>

                                        <p>Or</p>
                                        <p>Cleared sections A & B examination of Associate Membership of Institution of Engineers (India) or Aeronautical Society of India by actual studies with minimum 60% marks or equivalent in certain disciplines specified by IAF.</p>
                                    </td>                                    
                                </tr>
                                <tr>
                                    <td>Ground Duty (Non-Technical) branches</td>
                                    <td>
                                        <p>Administration: Graduation in any discipline with minimum 60%</p>
                                        <p>Or</p>
                                        <p>Cleared section A&B examination of Associate Membership of Institution of Engineers (India) or Aeronautical Society of India from a recognised university with 60%.</p>
                                    </td>                                    
                                </tr> 
                                <tr>
                                    <td>Education</td>
                                    <td>
                                        <p>Completed MBA/MCA or MA/MSc in English/Physics/Mathematics/Chemistry/ Statistics/ International relations/ International Studies/ Defence Studies/ Psychology/ Computer Science/ IT/ Management/ Mass Communication/ Journalism/ Public Relation with 50% and 60% in graduation.</p>
                                    </td>                                    
                                </tr>                                                          
                            </tbody>                           
                        </table>
                        </div>

                        <h2 class="h2">AFCAT EXAM Dates 2020</h2>

                        <div class="table-responsive">
                        <table class="table">       
                            <thead>
                                <tr>
                                    <th>AFCAT 2020 events</th>
                                    <th>AFCAT 2020 dates</th>
                                    <th>Salient Features</th>
                                </tr>
                            </thead>                     
                            <tbody>
                                <tr>
                                    <td>Release of notification</td>
                                    <td>November 23, 2019</td>   
                                    <td>As of now, IAF has released the short notice regarding AFCAT (1) exam. The detailed notification will be released on December 1, 2019</td>                                 
                                </tr>
                                <tr>
                                    <td>AFCAT application form 2020</td>
                                    <td>December 1, 2019</td>   
                                    <td>The application forms is available online on the official website.</td>                                 
                                </tr>
                                <tr>
                                    <td>Last date to apply</td>
                                    <td>December 30, 2019</td>   
                                    <td>Candidates can fill the application form and pay the fees online. The editing of details in the application form are allowed within the prescribed dates of application.</td>                                 
                                </tr>       
                                <tr>
                                    <td>AFCAT exam</td>
                                    <td>February 22 and 23, 2020</td>   
                                    <td>The exam will be conducted online in consecutive days and in multiple days. </td>                                 
                                </tr>                                      
                            </tbody>                           
                        </table>
                        </div>

                        <h2 class="h2">AFCAT 2020 Vacancies</h2>
                        <p>A total of 249 vacancies will be filled through AFCAT entry. The branch-wise AFCAT vacancy break-up is given below:</p>

                        <div class="table-responsive">
                        <table class="table">       
                            <thead>
                                <tr>
                                    <th>Branch </th>
                                    <th>Vacancies </th>                                    
                                </tr>
                            </thead>                     
                            <tbody>
                                <tr>
                                    <td>Flying Branch</td>
                                    <td>SSC - 60</td>                                       
                                </tr>
                                <tr>
                                    <td>Ground Duty (Technical)</td>
                                    <td>AE(L):PC - 40, SSC - 26, <br> AE(M):PC-23, SSC - 16</td>                                                                      
                                </tr>
                                <tr>
                                    <td>Ground Duty (Non -Technical)</td>
                                  <td>
                                  Admin: PC -24, SSC -16 <br> Accts : PC -14, SSC -10 <br> Lgs ; PC- 12, SSC - 08
                                  </td>                         
                                </tr>                                                                    
                            </tbody>                           
                        </table>
                        </div>

                        <h2 class="h2">AFCAT Selection Process </h2>
                        <p>The selection process of AFCAT comprises the following stages:</p>

                        <h5 class="h5">Online Test:</h5>
                        <p> All registered candidates are required to first appear for the online test. The question paper of AFCAT will carry questions from General Awareness, Verbal Ability in English, Numerical Ability and Reasoning and Military Aptitude Test. EKT question paper carries questions from subjects -Mechanical, Computer Science and Electrical & Electronics. Maximum marks allotted to AFCAT are 300. The test paper of EKT carries total 150 marks.</p>

                        <h5 class="h5">AFSB Interview:</h5>
                        <p> Candidates declared successful in AFCAT exam are called for AFSB Interview. The interview comprises three stages.</p>

                        <h5 class="h5">Stage I: </h5>
                        <p>Officer Intelligence Rating Test along with Picture Perception and discussion test will be conducted on the first day.</p>

                        <h5 class="h5">Stage II </h5>
                        <p>Psychological test will be conducted on Day 1(Afternoon) and the Group Tests and Interview would commence after document check for the next five days.</p>

                        <h5 class="h5">For Flying Branch: </h5>
                        <p>Candidates applying for flying branch are required to appear in Computerised Pilot Selection System (CPSS).</p>
                    </div>
                    <!--/ col 8--> 

                     <!-- course right faq -->
                   <div class="col-lg-4">   
                   <!-- sticky top -->
                   <div class="sticky-top">                     
                        <div class="custom-accord course-faq">
                            <h3 class="h5 pb-3">AFCAT FAQ’S</h3>
                            <!-- accordion -->
                            <div class="accordion">
                                <!-- acc-->
                                <h3 class="panel-title">What is the syllabus of AFCAT?</h3>
                                <div class="panel-content">
                                    <p>The syllabus of AFCAT comprises topics from General Awareness, Verbal Ability in English, Numerical Ability and Reasoning and Military Aptitude Test. The syllabus of EKT comprises topics from Mechanical, Computer Science and Electrical and Electronics
                                    </p>
                                </div>
                                <!--/ acc-->
                                <!-- acc-->
                                <h3 class="panel-title">How many papers are there in AFCAT? </h3>
                                <div class="panel-content">
                                    <p>There will be only four sections in AFCAT exam</p>
                                </div>
                                <!--/ acc-->
                                <!-- acc-->
                                <h3 class="panel-title">How difficult is AFCAT?</h3>
                                <div class="panel-content">
                                    <p>It is not that difficult to crack AFCAT if candidates have followed right strategy.</p>
                                </div>
                                <!--/ acc-->

                                 <!-- acc-->
                                 <h3 class="panel-title">How can I prepare for AFCAT?</h3>
                                <div class="panel-content">
                                    <p>To prepare for AFCAT, candidates must have a clear understanding of the syllabus and AFCAT exam pattern. Candidates should brush up their basics and be clear with the concepts.</p>                                   
                                </div>
                                <!--/ acc-->

                                <!-- acc-->
                                <h3 class="panel-title">Do AFCAT repeat questions?</h3>
                                <div class="panel-content">
                                    <p>There are very less chances of questions getting repeated from the previous years’ question papers.</p> 
                                </div>
                                <!--/ acc-->

                                <!-- acc-->
                                <h3 class="panel-title">Is AFCAT easy to crack?</h3>
                                <div class="panel-content">
                                    <p>Yes, it is easy to crack AFCAT</p>                                   
                                </div>
                                <!--/ acc-->     

                                <!-- acc-->
                                <h3 class="panel-title">Which books are best for AFCAT?</h3>
                                <div class="panel-content">
                                    <p>There are many books available in the market. Candidates should choose the books that have covered the entire syllabus.</p>                                   
                                </div>
                                <!--/ acc-->                                
                            </div>
                            <!--/ accordion -->
                        </div>
                        </div>    
                        <!--/ sticky top -->    
                   </div>
                   <!--/ course right faq -->
               </div>
               <!--/ row -->
           </div>
            <!-- /container -->
        </div>
        <!--/ sub page body -->
    </main>
    <!--/ sub page main -->
    <!--footer -->
    <?php include 'footer.php' ?>
    <!--/ footer -->  
    <?php include 'footerscripts.php'?>

</body>

</html>