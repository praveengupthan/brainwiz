<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Elitmus Questions and Answers</title>
    <link rel="icon" type="image/png" sizes="32x32" href="img/favicon-32x32.png">
    <!-- style sheets -->
    <?php include 'headerstyles.php' ?>
</head>

<body>
    <!-- header -->
   <?php include 'header.php' ?>
    <!--/ header -->
    <!--sub page main -->
    <main class="subpage">
        <!-- sub page header -->
        <div class="page-header">
            <!-- container -->
            <div class="container">
               <!-- row -->
               <div class="row">
                   <div class="col-lg-6">
                       <h1>Test <span class="fbold">Series</span> </h1>
                   </div>
               </div>
               <!--/ row --> 
            </div>
            <!--/ container -->
        </div>
        <!--/ sub page header -->

         <!-- brudcrumb -->
         <div class="container">
            <!-- row -->
            <div class="row">
                <!-- col -->
                <div class="col-lg-12">                   
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.php">Home</a></li>                       
                        <li class="breadcrumb-item active"><a>Test Series</a></li>
                    </ul>                    
                </div>
                <!--/col -->
            </div>
            <!--/ row -->
        </div>
        <!--/ brudcrumb -->

        <!-- sub page body -->
        <div class="subpage-body">
           <!-- container -->
           <div class="container">
              <!-- row -->
              <div class="row qanda-page">               
                <!-- col 12 -->
                <div class="col-lg-12"> 
                    <!-- navigation -->
                    <nav class="navbar navbar-expand-lg navbar-light px-0">                           
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#answersnav" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                        Test Series   
                       
                        </button>
                        <div class="collapse navbar-collapse" id="answersnav">
                            <ul class="navbar-nav mr-auto">
                                <li class="nav-item ">
                                    <a class="nav-link active" href="#">Up Coming Tests (10) </span></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#">Completed Test (0)</span></a>
                                </li>                                 
                            </ul>                                
                        </div>
                    </nav>
                    <!--/ navigation -->  

                    <!-- row -->
                    <div class="row text-series-row">
                        <!-- col -->
                        <div class="col-lg-4">
                            <div class="testseries-col">
                                <!-- figure -->
                                <figure>
                                    <a href="javascript:void(0)"><img src="img/comp05.jpg" alt=""></a>                                   
                                </figure>
                                <!--/ figure -->

                                <!-- article -->
                                <article>
                                    <h4 class="h5">Tech Mahindra Test 1</h4>
                                    <ul class="list-seperator nav">
                                        <li>
                                            <a>Questions : <span>75</span></a>
                                        </li>
                                        <li>
                                            <a>Duration : <span>80 mins</span></a>
                                        </li>
                                    </ul>
                                </article>
                                <!--/ article -->
                                <p class="text-center"><a href="testseries-detail.php" class="bluebtn">Take Test</a></p>
                            </div>
                        </div>
                        <!--/ col -->

                         <!-- col -->
                         <div class="col-lg-4">
                            <div class="testseries-col">
                                <!-- figure -->
                                <figure>
                                    <a href="javascript:void(0)"><img src="img/comp02.jpg" alt=""></a>                                   
                                </figure>
                                <!--/ figure -->

                                <!-- article -->
                                <article>
                                    <h4 class="h5">IBM Test 1</h4>
                                    <ul class="list-seperator nav">
                                        <li>
                                            <a>Questions : <span>55</span></a>
                                        </li>
                                        <li>
                                            <a>Duration : <span>90 mins</span></a>
                                        </li>
                                    </ul>
                                </article>
                                <!--/ article -->
                                <p class="text-center"><a href="testseries-detail.php" class="bluebtn">Take Test</a></p>
                            </div>
                        </div>
                        <!--/ col -->

                         <!-- col -->
                         <div class="col-lg-4">
                            <div class="testseries-col">
                                <!-- figure -->
                                <figure>
                                    <a href="javascript:void(0)"><img src="img/comp02.jpg" alt=""></a>                                    
                                </figure>
                                <!--/ figure -->

                                <!-- article -->
                                <article>
                                    <h4 class="h5">IBM Test 2</h4>
                                    <ul class="list-seperator nav">
                                        <li>
                                            <a>Questions : <span>55</span></a>
                                        </li>
                                        <li>
                                            <a>Duration : <span>90 mins</span></a>
                                        </li>
                                    </ul>
                                </article>
                                <!--/ article -->
                                <p class="text-center"><a href="packages.php" class="bluebtn">Buy Test</a></p>
                            </div>
                        </div>
                        <!--/ col -->

                         <!-- col -->
                         <div class="col-lg-4">
                            <div class="testseries-col">
                                <!-- figure -->
                                <figure>
                                    <a href="javascript:void(0)"><img src="img/comp05.jpg" alt=""></a>                                    
                                </figure>
                                <!--/ figure -->

                                <!-- article -->
                                <article>
                                    <h4 class="h5">Tech Mahindra Test 2</h4>
                                    <ul class="list-seperator nav">
                                        <li>
                                            <a>Questions : <span>75</span></a>
                                        </li>
                                        <li>
                                            <a>Duration : <span>60 mins</span></a>
                                        </li>
                                    </ul>
                                </article>
                                <!--/ article -->
                                <p class="text-center"><a href="testseries-detail.php" class="bluebtn">Buy Test</a></p>
                            </div>
                        </div>
                        <!--/ col -->

                         <!-- col -->
                         <div class="col-lg-4">
                            <div class="testseries-col">
                                <!-- figure -->
                                <figure>
                                    <a href="javascript:void(0)"><img src="img/comp04.jpg" alt=""></a>                                   
                                </figure>
                                <!--/ figure -->

                                <!-- article -->
                                <article>
                                    <h4 class="h5">Capgemini Test 2</h4>
                                    <ul class="list-seperator nav">
                                        <li>
                                            <a>Questions : <span>32</span></a>
                                        </li>
                                        <li>
                                            <a>Duration : <span>50 mins</span></a>
                                        </li>
                                    </ul>
                                </article>
                                <!--/ article -->
                                <p class="text-center"><a href="testseries-detail.php" class="bluebtn">Buy Test</a></p>
                            </div>
                        </div>
                        <!--/ col -->

                         <!-- col -->
                         <div class="col-lg-4">
                            <div class="testseries-col">
                                <!-- figure -->
                                <figure>
                                    <a href="javascript:void(0)"><img src="img/comp04.jpg" alt=""></a>                                    
                                </figure>
                                <!--/ figure -->

                                <!-- article -->
                                <article>
                                    <h4 class="h5">Capgemini Test 1</h4>
                                    <ul class="list-seperator nav">
                                        <li>
                                            <a>Questions : <span>75</span></a>
                                        </li>
                                        <li>
                                            <a>Duration : <span>60 mins</span></a>
                                        </li>
                                    </ul>
                                </article>
                                <!--/ article -->
                                <p class="text-center"><a href="testseries-detail.php" class="bluebtn">Buy Test</a></p>
                            </div>
                        </div>
                        <!--/ col -->

                         <!-- col -->
                         <div class="col-lg-4">
                            <div class="testseries-col">
                                <!-- figure -->
                                <figure>
                                    <a href="javascript:void(0)"><img src="img/comp01.jpg" alt=""></a>                                   
                                </figure>
                                <!--/ figure -->

                                <!-- article -->
                                <article>
                                    <h4 class="h5">Accenture Test 1</h4>
                                    <ul class="list-seperator nav">
                                        <li>
                                            <a>Questions : <span>55</span></a>
                                        </li>
                                        <li>
                                            <a>Duration : <span>60 mins</span></a>
                                        </li>
                                    </ul>
                                </article>
                                <!--/ article -->
                                <p class="text-center"><a href="testseries-detail.php" class="bluebtn">Buy Test</a></p>
                            </div>
                        </div>
                        <!--/ col -->

                         <!-- col -->
                         <div class="col-lg-4">
                            <div class="testseries-col">
                                <!-- figure -->
                                <figure>
                                    <a href="javascript:void(0)"><img src="img/comp01.jpg" alt=""></a>                                    
                                </figure>
                                <!--/ figure -->

                                <!-- article -->
                                <article>
                                    <h4 class="h5">Accenture Test 2</h4>
                                    <ul class="list-seperator nav">
                                        <li>
                                            <a>Questions : <span>55</span></a>
                                        </li>
                                        <li>
                                            <a>Duration : <span>60 mins</span></a>
                                        </li>
                                    </ul>
                                </article>
                                <!--/ article -->
                                <p class="text-center"><a href="testseries-detail.php" class="bluebtn">Buy Test</a></p>
                            </div>
                        </div>
                        <!--/ col -->

                        <!-- col -->
                        <div class="col-lg-4">
                            <div class="testseries-col">
                                <!-- figure -->
                                <figure>
                                    <a href="javascript:void(0)"><img src="img/comp03.jpg" alt=""></a>                                    
                                </figure>
                                <!--/ figure -->

                                <!-- article -->
                                <article>
                                    <h4 class="h5">Infosys 2018 Test 1</h4>
                                    <ul class="list-seperator nav">
                                        <li>
                                            <a>Questions : <span>65</span></a>
                                        </li>
                                        <li>
                                            <a>Duration : <span>95 mins</span></a>
                                        </li>
                                    </ul>
                                </article>
                                <!--/ article -->
                                <p class="text-center"><a href="testseries-detail.php" class="bluebtn">Buy Test</a></p>
                            </div>
                        </div>
                        <!--/ col -->

                         <!-- col -->
                         <div class="col-lg-4">
                            <div class="testseries-col">
                                <!-- figure -->
                                <figure>
                                    <a href="javascript:void(0)"><img src="img/comp03.jpg" alt=""></a>                                   
                                </figure>
                                <!--/ figure -->

                                <!-- article -->
                                <article>
                                    <h4 class="h5">Infosys 2018 Test 2</h4>
                                    <ul class="list-seperator nav">
                                        <li>
                                            <a>Questions : <span>65</span></a>
                                        </li>
                                        <li>
                                            <a>Duration : <span>95 mins</span></a>
                                        </li>
                                    </ul>
                                </article>
                                <!--/ article -->
                                <p class="text-center"><a href="testseries-detail.php" class="bluebtn">Take Test</a></p>
                            </div>
                        </div>
                        <!--/ col -->
                    </div>
                    <!--/ row --> 
                </div>
                <!--/ col 12 -->
              </div>
              <!--/ row -->
           </div>
           <!--/ container -->
        </div>
        <!--/ sub page body -->
    </main>
    <!--/ sub page main -->
    <!--footer -->
    <?php include 'footer.php' ?>
    <!--/ footer -->  
    <?php include 'footerscripts.php'?>
</body>

</html>