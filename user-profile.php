<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Brainwiz</title>
    <link rel="icon" type="image/png" sizes="32x32" href="img/favicon-32x32.png">
    <!-- style sheets -->
    <?php include 'headerstyles.php' ?>
</head>

<body>

    <!-- header -->
   <?php include 'header-postlogin.php' ?>
    <!--/ header -->
    <!--sub page main -->
    <main class="subpage">
        <!-- sub page header -->
        <div class="page-header">
            <!-- container -->
            <div class="container">
               <!-- row -->
               <div class="row">
                   <div class="col-lg-6">
                       <h1>Profile</h1>
                   </div>
               </div>
               <!--/ row --> 
            </div>
            <!--/ container -->
        </div>
        <!--/ sub page header -->

        <!-- brudcrumb -->
        <div class="container">
            <!-- row -->
            <div class="row">
                <!-- col -->
                <div class="col-lg-12">                   
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                        <li class="breadcrumb-item"><a href="user-profile.php">Student Name Will be here</a></li>
                        <li class="breadcrumb-item active"><a>My Profile</a></li>
                    </ul>                    
                </div>
                <!--/col -->
            </div>
            <!--/ row -->
        </div>
        <!--/ brudcrumb -->

        <!-- sub page body -->
        <div class="subpage-body">
           <!-- container -->
           <div class="container">
                <!-- row -->
                <div class="row">
                    <!-- left col 4-->
                    <div class="col-lg-4">
                       <?php include 'userleft-nav.php'?>
                    </div>
                    <!--/ left col 4-->

                    <!-- right container -->
                    <div class="col-lg-8">
                        <div class="right-usersection">
                            <!-- title -->
                            <div class="user-right-title">
                                <h3 class="h4 mb-0">My Profile</h3>
                                <p><small>Add information about yourself</small></p>
                            </div>
                            <!--/ title -->

                            <!-- right user body -->
                            <div class="user-rightsection">
                                <form>
                                    <!-- row -->
                                    <div class="row">
                                        <!-- col -->
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>First Name</label>
                                                <input type="text" placeholder="First Name" class="form-control">
                                            </div>
                                        </div>
                                        <!--/ col -->
                                         <!-- col -->
                                         <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>Last Name</label>
                                                <input type="text" placeholder="Last Name" class="form-control">
                                            </div>
                                        </div>
                                        <!--/ col -->
                                    </div>
                                    <!--/ row -->

                                    <!-- row -->
                                    <div class="row">
                                        <!-- col -->
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>Email</label>
                                                <input type="text" placeholder="Email Address" class="form-control">
                                            </div>
                                        </div>
                                        <!--/ col -->
                                         <!-- col -->
                                         <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>Contact Number</label>
                                                <input type="text" placeholder="+91 9642123254" class="form-control">
                                            </div>
                                        </div>
                                        <!--/ col -->
                                    </div>
                                    <!--/ row -->                                

                                     <!-- row -->
                                     <div class="row">
                                        <!-- col -->
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>Qualification</label>
                                                <select class="form-control">
                                                    <option>Post Graduation</option>
                                                    <option> Graduation</option>
                                                    <option> Intermediate</option>  
                                                </select>
                                            </div>
                                        </div>
                                        <!--/ col -->
                                         <!-- col -->
                                         <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>Location</label>
                                                <input type="text" placeholder="Location / City / State" class="form-control">
                                            </div>
                                        </div>
                                        <!--/ col -->
                                    </div>
                                    <!--/ row -->

                                     <!-- row -->
                                     <div class="row">
                                        <!-- col -->
                                        <div class="col-lg-12">                                          
                                            <input type="submit" class="bluebtn" value="Save Profile">
                                        </div>
                                        <!--/ col -->                                        
                                    </div>
                                    <!--/ row -->
                                </form>
                            </div>
                            <!--/ right user body -->
                        </div>
                    </div>
                    <!--/ right container -->
                </div>
                <!--/ row -->
           </div>
           <!--/ container -->
        </div>
        <!--/ sub page body -->
    </main>
    <!--/ sub page main -->
    <!--footer -->
    <?php include 'footer.php' ?>
    <!--/ footer -->  
    <?php include 'footerscripts.php'?>

</body>

</html>