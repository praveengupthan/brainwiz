<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Brainwiz</title>
    <link rel="icon" type="image/png" sizes="32x32" href="img/favicon-32x32.png">
    <!-- style sheets -->
    <?php include 'headerstyles.php' ?>
</head>

<body>

    <!-- header -->
   <?php include 'header.php' ?>
    <!--/ header -->
    <!--sub page main -->
    <main class="subpage">
        <!-- sub page header -->
        <div class="page-header">
            <!-- container -->
            <div class="container">
               <!-- row -->
               <div class="row">
                   <div class="col-lg-6">
                       <h1>Premium <span class="fbold">Courses</span></h1>
                   </div>
               </div>
               <!--/ row --> 
            </div>
            <!--/ container -->
        </div>
        <!--/ sub page header -->

        <!-- brudcrumb -->
        <div class="container">
            <!-- row -->
            <div class="row">
                <!-- col -->
                <div class="col-lg-12">                   
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                        <li class="breadcrumb-item active"><a>Premium Courses</a></li>
                    </ul>                    
                </div>
                <!--/col -->
            </div>
            <!--/ row -->
        </div>
        <!--/ brudcrumb -->

        <!-- sub page body -->
        <div class="subpage-body">
           <!-- container -->
           <div class="container">
                <!-- row -->
                <div class="row justify-content-center">
                   <div class="col-lg-6 text-center">
                       <h2 class="h2 fblue fbold">All Courses </h2>
                       <p class="text-center">Widely Researched, Cherry Picked and Industry Approved Courses</p>                       
                   </div>
               </div>
               <!--/ row --> 
               <!-- row -->
               <div class="row py-3">
                <!-- col -->
                <div class="col-lg-4">
                    <div class="course-col position-relative">          
                        <a class="position-absolute addtowish" href="javascript:void(0)" data-toggle="tooltip" title="Add to Wishlist"><span class="icon-heart-o icomoon"></span></a>             
                        <a href="premium-course-detail.php">
                            <img src="img/crtcourseimg.svg" alt="" class="img-fluid" data-toggle="popover" data-trigger="hover" title="Campus Recruitment Training" data-content="Campus Recruitment Training is a comprehensive program which is exclusively designed for the graduate students preparing them for recruitment of various IT and NON IT companies, and ensuring that the students are well trained and equipped to get through the companies easily.">
                        </a>
                        <article class="p-3 text-center">
                            <h3><a href="premium-course-detail.php">Campus Recruitment Training </a></h3>
                            <p  class="pb-3 text-center">Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptate   </p>
                            <div class="d-flex justify-content-between">
                                <h5 class="h6 fblue">Faculty Name</h5>
                                <h6 class="h5 fblue"><span class="oldprice">Rs: 2500</span> Rs: 1000</h6>
                            </div>
                        </article>
                    </div>
                </div>
                <!--/ col -->

                  <!-- col -->
                  <div class="col-lg-4">
                    <div class="course-col position-relative">     
                        <a class="position-absolute addtowish" href="javascript:void(0)" data-toggle="tooltip" title="Add to Wishlist"><span class="icon-heart-o icomoon"></span></a>      
                                      
                        <a href=premium-course-detail.php">
                            <img src="img/amcat-courseimg.svg" alt="" class="img-fluid" data-toggle="popover" data-trigger="hover" title="Amcat" data-content="Campus Recruitment Training is a comprehensive program which is exclusively designed for the graduate students preparing them for recruitment of various IT and NON IT companies, and ensuring that the students are well trained and equipped to get through the companies easily.">
                        </a>
                        <article class="p-3 text-center">
                            <h3><a href="premium-course-detail.php">Amcat </a></h3>
                            <p  class="pb-3 text-center">Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptate   </p>
                            <div class="d-flex justify-content-between">
                                <h5 class="h6 fblue">Faculty Name</h5>
                                <h6 class="h5 fblue"><span class="oldprice">Rs: 2500</span> Rs: 1000</h6>
                            </div>
                        </article>
                    </div>
                </div>
                <!--/ col -->

                 <!-- col -->
                 <div class="col-lg-4">
                    <div class="course-col position-relative">     
                        
                        <a class="position-absolute addtowish" href="javascript:void(0)" data-toggle="tooltip" title="Add to Wishlist"><span class="icon-heart-o icomoon"></span></a>      

                        <a href="premium-course-detail.php">
                            <img src="img/elitmus-courseimg.svg" alt="" class="img-fluid" data-toggle="popover" data-trigger="hover" title="E-Litmus" data-content="Campus Recruitment Training is a comprehensive program which is exclusively designed for the graduate students preparing them for recruitment of various IT and NON IT companies, and ensuring that the students are well trained and equipped to get through the companies easily.">
                        </a>
                        <article class="p-3 text-center">
                            <h3><a href="premium-course-detail.php">E-Litmus </a></h3>
                            <p  class="pb-3 text-center">Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptate   </p>
                            <div class="d-flex justify-content-between">
                                <h5 class="h6 fblue">Faculty Name</h5>
                                <h6 class="h5 fblue"><span class="oldprice">Rs: 2500</span> Rs: 1000</h6>
                            </div>
                        </article>
                    </div>
                </div>
                <!--/ col -->

                 <!-- col -->
                 <div class="col-lg-4">
                    <div class="course-col position-relative">      
                        
                        <a class="position-absolute addtowish" href="javascript:void(0)" data-toggle="tooltip" title="Add to Wishlist"><span class="icon-heart-o icomoon"></span></a>

                        <a href="premium-course-detail.php">
                            <img src="img/cocubescourseimg.svg" alt="" class="img-fluid" data-toggle="popover" data-trigger="hover" title="CoCubes" data-content="Campus Recruitment Training is a comprehensive program which is exclusively designed for the graduate students preparing them for recruitment of various IT and NON IT companies, and ensuring that the students are well trained and equipped to get through the companies easily.">
                        </a>
                        <article class="p-3 text-center">
                            <h3><a href="premium-course-detail.php">CoCubes </a></h3>
                            <p  class="pb-3 text-center">Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptate   </p>
                            <div class="d-flex justify-content-between">
                                <h5 class="h6 fblue">Faculty Name</h5>
                                <h6 class="h5 fblue"><span class="oldprice">Rs: 2500</span> Rs: 1000</h6>
                            </div>
                        </article>
                    </div>
                </div>
                <!--/ col -->

                <!-- col -->
                <div class="col-lg-4">
                    <div class="course-col position-relative"> 
                        
                        <a class="position-absolute addtowish" href="javascript:void(0)" data-toggle="tooltip" title="Add to Wishlist"><span class="icon-heart-o icomoon"></span></a>      
                                      
                        <a href="premium-course-detail.php">
                            <img src="img/cprogramming.svg" alt="" class="img-fluid" data-toggle="popover" data-trigger="hover" title="C Language Programming" data-content="Campus Recruitment Training is a comprehensive program which is exclusively designed for the graduate students preparing them for recruitment of various IT and NON IT companies, and ensuring that the students are well trained and equipped to get through the companies easily.">
                        </a>
                        <article class="p-3 text-center">
                            <h3><a href="premium-course-detail.php">C Language Programming</a></h3>
                            <p  class="pb-3 text-center">Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptate</p>
                            <div class="d-flex justify-content-between">
                                <h5 class="h6 fblue">Faculty Name</h5>
                                <h6 class="h5 fblue"><span class="oldprice">Rs: 2500</span> Rs: 1000</h6>
                            </div>
                        </article>
                    </div>
                </div>
                <!--/ col -->

               </div>
               <!--/ row -->
           </div>
            <!--/ container -->
        </div>
        <!--/ sub page body -->
    </main>
    <!--/ sub page main -->
    <!--footer -->
    <?php include 'footer.php' ?>
    <!--/ footer -->  
    <?php include 'footerscripts.php'?>

</body>

</html>