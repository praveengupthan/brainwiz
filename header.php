<header class="fixed-top">
    <!-- top header -->
    <div class="top-header">
        <!-- container fluid -->
        <div class="container-fluid">
            <div class="row justify-content-between">
                <!-- col 6-->
                <div class="col-sm-6 col-8">
                    <ul class="nav ltnav">
                        <li class="nav-item">
                            <a href="tel:+91 81421 23938" class="nav-link"><span class="icon-telephone icomoon"></span> +91 81421 23938</a>
                        </li>
                        <li class="nav-item">
                            <a href="index.php" class="nav-link">Home</a>
                        </li>
                        <li class="nav-item d-none d-sm-block">
                            <a href="contact.php" class="nav-link">Contact</a>
                        </li>
                        <li class="nav-item d-none d-sm-block">
                            <a href="blog.php" class="nav-link">Blog</a>
                        </li>
                    </ul>
                </div>
                <!--/ col 6-->

                <!-- col 6-->
                <div class="col-sm-6 text-right col-4">
                    <ul class="float-right nav">
                        <li class="nav-item">
                            <a href="signin.php" class="nav-link"> <span class="icon-user-circle"></span> Login</a>
                        </li>
                        <li class="nav-item d-none d-sm-block">
                            <a href="signup.php" class="nav-link"> Register</a>
                        </li>
                    </ul>
                </div>
                <!--/ col 6-->
            </div>
        </div>
        <!--/ container fludi -->
    </div>
    <!--/ top header -->

    <!-- nav -->
    <nav class="navbar navbar-expand-lg navbar-light" id="main_navbar">
        <a class="navbar-brand" href="index.php"><img src="img/logo.svg" alt=""></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <i class="icon-bars icomoon"></i>
        </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto leftnav">
                    <li class="nav-item">
                        <a class="nav-link active" href="about.php">About us </a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#"  id="navbarDropdownone" role="button"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Courses
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdownone">
                            <a class="dropdown-item" href="crt-detail.php">CRT</a>
                            <a class="dropdown-item" href="amcat-detail.php">AMCAT</a>
                            <a class="dropdown-item" href="elitmus-detail.php">E-Litmus</a>
                            <a class="dropdown-item" href="cocubes-detail.php">CoCubes</a>
                            <a class="dropdown-item" href="javascript:void(0)">C Language</a>
                            <a class="dropdown-item" href="allcourses.php">All Courses</a>
                            <a class="dropdown-item" href="premium-courses.php">Premium Courses</a>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="batches.php">Batches</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="video-tutorials.php">Tutorials</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="testseries.php">Test Series</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="students.php">Students</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown"  role="button"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Examinations
                        </a>
                        <div class="dropdown-menu" >
                            <a class="dropdown-item" href="gate.php">GATE</a>
                            <a class="dropdown-item" href="clat.php">CLAT</a>
                            <a class="dropdown-item" href="ccat.php">C-CAT</a>
                            <a class="dropdown-item" href="afcat.php">AFCAT</a>
                            <a class="dropdown-item" href="ssc-cgl.php">SSC-CGL</a>
                        </div>
                    </li>
                </ul>
                <ul class="navbar-nav  my-lg-0 rtnav">
                    <li class="nav-item">
                        <a class="nav-link cartlink" href="cart.php">
                            <span class="icon-shopping-cart-of-checkered-design icomoon"></span>
                            <span class="value text-center">1</span>
                        </a>
                    </li>
                </ul>
            </div>
        </nav>
        <!--/ nav -->
    </header>
