<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Brainwiz Batches</title>
    <link rel="icon" type="image/png" sizes="32x32" href="img/favicon-32x32.png">
    <!-- style sheets -->
    <?php include 'headerstyles.php' ?>
</head>

<body>
    <!-- header -->
   <?php include 'header.php' ?>
    <!--/ header -->
    <!--sub page main -->
    <main class="subpage">
        <!-- sub page header -->
        <div class="page-header">
            <!-- container -->
            <div class="container">
               <!-- row -->
               <div class="row">
                   <div class="col-lg-6">
                       <h1>Batches </h1>
                   </div>
               </div>
               <!--/ row --> 
            </div>
            <!--/ container -->
        </div>
        <!--/ sub page header -->

         <!-- brudcrumb -->
         <div class="container">
            <!-- row -->
            <div class="row">
                <!-- col -->
                <div class="col-lg-12">                   
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                        <li class="breadcrumb-item active"><a>Batches</a></li>
                    </ul>                    
                </div>
                <!--/col -->
            </div>
            <!--/ row -->
        </div>
        <!--/ brudcrumb -->
        <!-- sub page body -->
        <div class="subpage-body">
           <!-- container -->
           <div class="container">
              <!-- row -->
              <div class="row">
                <!-- col left table -->
                <div class="col-lg-8">
                   <!-- table -->                  
                    <div class="table-section">
                        <table class="table table-responsive">
                            <thead>
                                <tr>   
                                    <th>Batch Name</th>                                                
                                    <th>Start Date</th>
                                    <th>Timings</th>
                                    <th>Duration</th>
                                    <th>Admission Fee</th>                                               
                                    <th>Class Type</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>  
                                    <td>CRT</td>                                             
                                    <td>05 Nov 2019</td>
                                    <td>7 PM - 9 PM</td>
                                    <td>6 Months </td>
                                    <td>Rs: 200</td>                                               
                                    <td>Class Room</td>
                                </tr>
                                <tr>     
                                    <td>CRT</td>                                                  
                                    <td>05 Nov 2019</td>
                                    <td>7 PM - 9 PM</td>
                                    <td>6 Months </td>
                                    <td>Rs: 200</td>                                               
                                    <td>Class Room</td>
                                </tr>
                                <tr>   
                                    <td>CRT</td>                                                    
                                    <td>05 Nov 2019</td>
                                    <td>7 PM - 9 PM</td>
                                    <td>6 Months </td>
                                    <td>Rs: 200</td>                                               
                                    <td>Class Room</td>
                                </tr>
                                <tr>     
                                    <td>C Lang</td>                                                  
                                    <td>05 Nov 2019</td>
                                    <td>7 PM - 9 PM</td>
                                    <td>6 Months </td>
                                    <td>Rs: 200</td>                                               
                                    <td>Class Room</td>
                                </tr>
                                <tr>  
                                    <td>C Lang</td>                                                      
                                    <td>05 Nov 2019</td>
                                    <td>7 PM - 9 PM</td>
                                    <td>6 Months </td>
                                    <td>Rs: 200</td>                                               
                                    <td>Class Room</td>
                                </tr>
                                <tr>  
                                    <td>C Lang</td>                                                    
                                    <td>05 Nov 2019</td>
                                    <td>7 PM - 9 PM</td>
                                    <td>6 Months </td>
                                    <td>Rs: 200</td>                                                
                                    <td>Class Room</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                   <!--/ table -->
                </div>
                <!--/ col left table-->

                <!-- col right contact form -->
                <div class="col-lg-4">
                    <!-- gray bg -->
                    <div class="bggray p-4 mb-3">
                        <h2 class="h5 py-2 fbold">Request for Demo </h2>
                        <form>
                            <!-- form group -->
                            <div class="form-group">
                                <label>First Name</label>
                                <div class="input-group">
                                    <input type="text" placeholder="First Name" class="form-control">
                                </div>
                            </div>
                            <!--/ form group-->

                             <!-- form group -->
                             <div class="form-group">
                                <label>Last Name</label>
                                <div class="input-group">
                                    <input type="text" placeholder="Last Name" class="form-control">
                                </div>
                            </div>
                            <!--/ form group-->

                             <!-- form group -->
                             <div class="form-group">
                                <label>Mobile Number</label>
                                <div class="input-group">
                                    <input type="text" placeholder="Mobile Number" class="form-control">
                                </div>
                            </div>
                            <!--/ form group-->

                            <!-- form group -->
                            <div class="form-group">
                                <label>Email ID</label>
                                <div class="input-group">
                                    <input type="text" placeholder="Email ID" class="form-control">
                                </div>
                            </div>
                            <!--/ form group-->

                             <!-- form group -->
                             <div class="form-group">
                                <label>Select Course</label>
                                <div class="input-group">
                                    <select class="form-control">
                                        <option>CRT</option>
                                        <option>C Programming</option>
                                        <option>Amcat</option>
                                    </select>
                                </div>
                            </div>
                            <!--/ form group-->

                            <input type="submit" value="Submit Enquiry" class="bluebtn">

                        </form>
                    </div>
                    <!--/ gray bg -->
                    <!-- bg gray -->
                    <div class="bggray p-4 mb-3">
                        <h2 class="h5 py-2 fbold">Key Features</h4>
                        <ul class="page-list">
                            <li>Comprehensive Training on CRT </li>
                            <li>More than 120 hours of Training</li>
                            <li>30 to 45 days Program</li>
                            <li>Covering 55+ Topics with latest MNC Questions</li>
                            <li>Sunday Special Sessions covering AMCAT & E-Litmus</li>                           
                        </ul>
                    </div>   
                    <!--/ bg gray -->                 
                </div>
                <!--/ col right contact form -->
              </div>
              <!--/ row -->
           </div>
           <!--/ container -->
        </div>
        <!--/ sub page body -->
    </main>

    <!--/ sub page main -->
    <!--footer -->
    <?php include 'footer.php' ?>
    <!--/ footer -->  
    <?php include 'footerscripts.php'?>
</body>

</html>